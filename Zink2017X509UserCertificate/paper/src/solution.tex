%!TEX root = ../x509auth.tex
\section{Solution}

Our solution is based on the observation, that the vast majority of web applications use the user's email address -- or the local part of the email address -- as the user's login name.
This holds for most public web services, like Amazon or eBay, but also for many public or private organizations and companies that operate their own email infrastructure.
Usually, the user's email user name\slash{}address is also his unique identifier (UID) used for organization-wide authentication.
If the email address is not used for authentication in the organization, another UID scheme usually is in place.
X.509 certificates must also be unique for a user and often demand the usage of a `distinguished name' as subject.
This subject can also include the UID, which can then be used for authentication instead of the user's email address.

Many organizations also either have their own certificate authority, or use the services of a public or commercial certificate authority.
Especially the public authorities are likely to operate their own public certificate authority for use in government agencies or educational institutions.

While client certificates slowly take off, particularly in areas where trust or confidentiality is crucial, x.509 certificates are still usually found on servers and used for host authentication and SSL\slash{}TLS encryption.
Because of this, support for x.509 client certificates has long been neglected.
However, interest in stronger authentication schemes and x.509 client certificates as well as cybersecurity in general has risen in recent years.
This is due to a combination of events, including Snowden's leak of thousands of confidential NSA documents and well-known attacks on cloud providers (e.g. Apple's iCloud ``celebrity breaches'').
As a result application developers and service providers have started to adopt a variety of multi-factor authentication schemes, from relying on trusted devices (Apple) to hardware tokens (Amazon, Github).

Still, aside from specialized applications that use smartcard authentication, x.509 client certificates are usually only used on web servers to protect locations or directories.
For example, anyone providing a valid certificate signed by a specific authority is allowed to access the login screen of an application (\cref{img:cert-wall}).
While this adds another authentication factor this cannot be considered 2FA, since there is no process that actually validates that the owner of the certificate is the same entity that tries to login to the application.
The reason for this is because the certificate request is issued by the web server while the login is requested by the application.

\begin{figure}[th]
	\centering
	\includegraphics[width=.8\columnwidth]{img/cert-wall}
	\caption{ \label{img:cert-wall} A typical certificate ``wall''. The server requests a valid certificate from the client to allow access to a location.}
\end{figure}

\subsection{2FA with client certificates}

In order to enable true multi-factor authentication the application needs to check the identity of the client certificate and somehow match that to the identity of the user trying to login.
This requires additional effort in identity management to provide this missing link.

We observed, that for most web-based applications, users login with their email address or the local-part of their email address.
Since x.509 user certificates are issued for a specific email address, it is surprisingly easy to extract the user name directly from the certificate on the server and use this as a validated remote user for the application.
If the web server supports client certificate parsing, this can even be done directly on the web server.
If not, the web server needs to expose the client certificate to the application which in turn parses the certificate and extracts the user name.

Apache2 supports the necessary options since version 2.4.12.
Prior to that, the client certificate must be exposed to the application.
\Cref{lst:apache2} shows the configuration for apache2.
The important part is the export of the certificate and the environment variables.
\begin{lstlisting}[language=bash, basicstyle=\footnotesize, frame=single, breaklines=True, label=lst:apache2, caption=Apache config prior to version 2.4]
<VirtualHost *:443>
...
SSLOptions +ExportCertData +StdEnvVars
...
SSLVerifyClient require
SSLVerifyDepth 3
SSLRequire ( %{SSL_CIPHER} !~ m/^(EXP|NULL)-/ )
...
</VirtualHost>
\end{lstlisting}

On the application side, parsing the client certificate and extracting the email address can be achieved in few lines of code (\cref{lst:login1}).
\begin{lstlisting}[language=php, basicstyle=\footnotesize, frame=single, breaklines=True, label=lst:login1, caption=Email extraction with apache $<$ 2.4.12]
$clientcert = openssl_x.509_parse($_SERVER['SSL_CLIENT_CERT']);
$subjectaltname = $clientcert['extensions']['subjectAltName'];
$email = substr($subjectaltname,strpos($subjectaltname,":")+1);
\end{lstlisting}

With apache2 version 2.4.12 or later, extraction of the user's email address can be done with a single line of code (\cref{lst:login2}).
\begin{lstlisting}[language=php, basicstyle=\footnotesize, frame=single, breaklines=True, label=lst:login2, caption=Email extraction with apache $>$ 2.12.24]
$email = $_SERVER['SSL_CLIENT_SAN_Email_0'];
\end{lstlisting}

In any case, \cref{lst:login1} shows the appropriate login form for the application.
The form uses the formerly extracted email address as readonly value for the user name.
The user is thus not able to actively change the login name.
Since the web server verifies the authenticity of the client certificate, the user's email address is also verified.
\begin{lstlisting}[language=html, basicstyle=\footnotesize, frame=single, breaklines=True, label=lst:loginform, caption=Login form with readonly user name]
<form>
<label><b>Username: </b></label>
<input type='text' name='user' value='<?php echo $email; ?>' readonly /> <br/>

<label><b>Password: </b></label>
<input type="password" placeholder="Enter Password" name="psw" required />

<button type="submit">Login</button>
<button type="button">Cancel</button>
</form>
\end{lstlisting}


\subsection{Securing web applications}

The proposed scheme can be used to efficiently secure any kind of web application with multiple authentication factors.
A remaining issue is the storage location of the client certificate itself.
Saving the certificate in the browsers store exposes the private key to a number of attacks that might lead to it's loss.

Fortunately, our solution can be easily combined with hardware tokens like smartcards or USB cryptokens to ensure that the private key stays private, even if the user's machine is compromised.
Not all browsers support hardware-based certificate stores, though.
Depending on the required trust level, the application can be secured accordingly.

%\Cref{img:evolution} shows the different stages, or combinations, to add certificate-based authentication.

%\begin{figure}[t]
%\centering
%\includegraphics[width=.8\columnwidth]{img/Evolution-crop}
%\caption{\label{img:evolution}The different stages of securing a web application with certificate-based second factors. Adding a software certificate provides an additional factor without linking it to the users identity. Application support is required to link users with their certificate. Instead of storing the certificate in software, it can be stored on a hardware token, to guarantee it stays private.}
%\end{figure}

\subsubsection{Example use cases}

The ease with which our authentication scheme can be implemented allows many applications.

\begin{description}
	\item[Web Server or Proxy] Using http auth and authentication modules, the whole web server can be secured.
	%Given web server support, the user name can be extracted directly in the site configuration simply by accessing the certificate field \texttt{SSL\_CLIENT\_SAN\_Email\_0}.
	To quickly add 2FA support to multiple applications or servers, an authentication proxy could even be used.
	
	\item[Locations\slash{}Specific Applications] Secure only specific locations or applications served by the web server, using true two-factor authentication.
	
	\item[Specific Actions] Using the option \texttt{QUERY\_STRING} to match post parameters or keywords in the URL, additional authentication factors can be requested for these specific actions.
\end{description}
