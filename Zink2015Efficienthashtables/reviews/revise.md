# Revision Suggestions by Reviewers

## Typos and Grammar

* [X] p 2: "both on-chip memory and to lookup performance"-> delete "to".
* [X] p 2: “most improbably worst-case” -> “most improbable worst-case”.
* [X] p 3: “that allowing” -> “that by allowing”.
* [X] p 3: "Their natural application" -> "..applications"
* [X] p 4: " that hash to it cells" -> "are hashed..."; "They name.." -> "..named.."
* [X] p 4: “Fan et al. [6] address” -> “Fan et al. [6] addressed”.
* [X] p 4: “Song et al. propose” -> “Song et al. proposed”.
* [X] p 5: "In the most aggressive optimization scheme" -> "..schemes"
* [X] p 5: the “c1.c2” in equation 5 should be “c_1.c_2”;
* [X] p 5: “Later [23] the authors” -> “Later in [23] the authors”.
* [X] p 6: "To allow deletions significantly more effort"-> ".. more significant effort.."? _changed to considerably more effort_
* [X] p 7: "To also support deletions an additional.."-> it is better to add a comma after "to do sth,". This is also applied to other places I did not list here;
* [X] p 7: "If lookup is unsuccessful,.." <- add a comma
* [X] p 8: "as long as.. chosen optimal" -> "is chosen optimally"
* [X] p 9: "naïve"-> "i"
* [X] p 10: "it is not be possible" -> "it is not possible"
* [X] p 12: "counters retrieved and ... identified"-> "..are retrieved and .. are identified" _is identified_
* [X] p 17: "H the class of hash functions,..." -> add "be" between symbol and its description. This is also applied to other places; "Let [W]" -> add "be"

## Organization

* [X] organization needs to be improved. suggestions: Section 3 can be part of Related work section. _moved section 3 to be a subsection of section 2. Other organizational issues are discussed below._

* [X] It is better to outline the contributions and techniques in short in Section 1 “Introduction” to let readers have a better overview of the paper. _added new paragraph that outlines the paper._

* [X] authors need to introduce a section to indicate where and how they start to introduce the proposed hashing scheme. It could be an overview or a brief outline for the
following Section 4 ,5 ,6, 7. _new section EHT with overview_

* [X] it is better to provide an overview of the design, especially in charts. users are easier to get the idea of this paper if authors can explain the process of the whole design. _new section EHT with overview_

* [X] It is better to present the experiment results in the
way to clearly indicate the advantages of the proposed solution in terms of the three goals (1) reducing on-chip memory cost (2) achieving constant lookup (3) being updatable. _added subsections and short introductions into evaluation section_

## Add / Change Content

* [X] It is better to have a table of notations to let readers have an easy reference of the meaning of parameters in equations. _added a table referenced in section 3 and the evaluation that explains equations and parameters_

* [X] In Figure 5, most components are proposed in [3]. It is better to emphasize new components in this paper. _new architecture overview figure with highlighted changes_

* [X] authors need to describe and explain the results (figures and tables) mentioned in Section 8, especially the comparison results with previous related schemes. _added more text explaining the figures and tables_

* [O] In performance evaluation section, it is better to show the results of false positive with different parameter configurations. _unfortunately, we are unsure what is meant by this statement. there is no evaluation of false positives, and the parameters used in the evaluation section basically present all practical configurations._

* [O] In experiments, the EHT is filled with “n random keys and values”. However, in reality, the value and key may not follow an even distribution. It is better to show experimental results with bias generation of the keys and values. _This really depends on the kind of application. While a more biased evaluation might be preferable, it is hard to actully do, since simulation and evaluation takes a lot of time. We also do not think that this is really required, since good hash functions provide enough skew to distribute the keys. We also follow the evaluation of related work, which usually also uses random keys._

* [O] frequency of updating the online entry is quite crucial, as it affects the efficiency of the design and the false positive rate of queries. However, this issue is not discussed in the paper. _added a remark regarding this in introduction and in section "Separate Engines"_

* [X] There are few recent works discussed in the related work section, it is better to include and discuss some works from recent years if there is any. _a quick search on general fast hash tables did not reveal recent work with similar scope as the ones already discussed. An additional search on hash table summaries revealed new work from 2013 which is now discussed, although it appeared **after** first submission of this paper_

## Figures and Tables

* [X] Figure 4 is hard to distinguish different curves with different values of c, since there are overlaps. Maybe you can mark them using label beside each line on
the figure. _changed the colors of figure 9 (cdist)_, _added labels to lines in figure 4_
