%!TEX root = ../paper.tex
%=============================================================================
% EVALUATION
%=============================================================================
\section{Evaluation} %
\label{sec:eval}

In this section we present and discuss results of a conceptual implementation of the EHT. The implementation is conceptual in the sense that it does {\em not} fully resemble the structure of the EHT but simulates it's behavior appropriately.

For simulations we use the following parameters:\\
$n = \{ 10^5; 10^6\} ; c = \{ 3.2;1.6 \}; \chi = \{ 4;5 \}; |\omega| = \{ 64;128 \}$\\
This leads to a total of $16$ different parameter configurations. The number of hash functions $k$ is always chosen optimal. In the following, when referencing the parameter configurations, we will use a single hexadecimal digit $p = [0,F]$ representing the encoding depicted in table \ref{tab:parameter}.

%
\begin{table}[ht!]
\begin{center}
	\tbl{Parameter configurations $p$ of the software simulations}{
\begin{tabular}{ c l | r r r r }
	\hline \hline
	 & bit & 3 & 2 & 1 & 0 \\
	\hline
	 & parameter & $n$ & $c$ & $\chi$ & $|\omega|$ \\
	\hline
	\multirow{2}*{value} 
	 & 0 & $10^5$ & $1.6$ & $4$ & $64$ \\
	 & 1 & $10^6$ & $3.2$ & $5$ & $128$ \\
	\hline \hline
\end{tabular}}
	\label{tab:parameter}
	%\caption{Parameter configurations $p$ of the software simulations}
\end{center}
\end{table}
%
 
On each simulation we perform ten trials, that is we instantiate the EHT and fill it with $n$ random keys and values. No updates are performed but the EHT is queried for all $n$ and additional $2n$ random keys to verify that every key can be retrieved and to analyze the false-positive probability. As summary we use HC-CBF. The compression rate $\gamma_\textrm{h}$ is calculated using algorithm \ref{algo:compress}. No hard compression is used, since we want to evaluate the quality of the compression algorithm. The cost of using hard compression can be derived by examining the resulting HC-CBF and is included in the analysis.

For each try, we calculate the size of the offline CBF, the size of a CCBF and the size of the online HC-CBF. We count the frequency of all counter values in the offline summary and derive the number of overflown counters in the online summary. Every compressed word in the HC-CBF is analyzed for the number of bits that are actually used to encode counters, resulting in a histogram of code-lengths per word. In addition, the load of all online buckets is calculated and the number of CAM entries counted. Finally, we compare the on-chip requirements of the EHT with the theoretical requirements of the MHT and FHT.

\paragraph{Counter Distribution}

Since the parameters $\chi$ and $|\omega |$ have no effect on the counter distribution, we count the counter frequencies for $n = \{ 1E+6, 1E+5 \}$ with sizes of $c = \{ 1.6,3.2 \}$ and also calculate the expected frequency for each counter value. The results are shown in figure \ref{fig:cdist}. The real frequencies resemble the expected frequencies almost exactly.

%
\begin{figure}[ht!]
	\centering
	\label{fig:cdist}
	\includegraphics[scale=0.45]{\gfx cdist}
	\caption{Real and expected counter frequencies}
\end{figure}
%

\paragraph{Bucket Load}

The maximum load depends on the number of choices $k$ and the number of items $n$. We aggregate the results of the combinations for $n$ and $c$ and count the number of entries in every online bucket. We then take the maximum of the frequencies to evaluate the worst-case behavior. The results are shown in table \ref{tab:load}.

%
\begin{table}[ht!]
\begin{center}
	\tbl{Entry distribution and expected maximum load}{
\begin{tabular}{ c | c r r r r r }
	\hline \hline
		& & \multicolumn{5}{c}{load} \\
	$p$ & $E$ & 0 & 1 & 2 & 3 & 4 \\
	\hline
	$0-3$ & $3$ & $167662$ & $89728$ & $5327$ & $24$ & $0$ \\
	$4-7$ & $2$ & $424659$ & $99411$ & $369$ & $0$ & $0$ \\	
	$8-B$ & $3$ & $1184464$ & $837562$ & $80950$ & $684$ & $1$ \\
	$C-F$ & $2$ & $3204894$ & $980039$ & $10438$ & $1$ & $0$ \\
	\hline \hline
\end{tabular}}
	\label{tab:load}
	%\caption{Entry distribution and expected maximum load}
\end{center}
\end{table}
%

In the worst-case there was only a single unexpected bucket overflow, for tables with $n=10^6$. %With these results the best solution to deal with bucket overflows is to artificially increase the counter of the affected bucket to $\chi$ and move the entries to CAM. This can be performed during pruning and by the update engine which adjusts the counters accordingly.

\paragraph{Overflow Entries}

We aggregate the results for $\chi$ according to $n$ and $c$, calculate the average, and take the minimum\slash maximum values encountered. Following equation \ref{eq:ecam} we also calculate the expected number of CAM entries. Table \ref{tab:cam} shows the results. Once again, the results closely resemble the expectations.

%
\begin{table}[ht!]
\begin{center}
	\tbl{Real and expected number of CAM entries}{
\begin{tabular}{ l r r r r }
	\hline \hline 
	$p$ & min & max & avg & E\\
	\hline
	$0-1$ & $144$ & $209$ & $177.95$ & $178$\\
	$2-3$ & $2$ & $11$ & $6.05$ & $6$\\
	$4-5$ & $0$ & $1$ & $0.15$ & $0$ \\
	$6-7$ & $0$ & $0$ & $0.00$ & $0$ \\
	\hline
	$8-9$ & $5017$ & $5446$ & $5194.05$ & $5181$\\
	$A-B$ & $236$ & $287$ & $258.20$ & $265$\\
	$C-D$ & $40$ & $61$ & $47.00$ & $47$\\
	$E-F$ & $0$ & $0$ & $0.00$ & $0$\\
	\hline \hline 
\end{tabular}}
	\label{tab:cam}
	%\caption{Real and expected number of CAM entries}
\end{center}
\end{table}
%

\paragraph{Compression}

To analyze the achieved compression we take the minimum, maximum and average $\gamma_\textrm{h}$ and compare that to $\gamma_\textrm{p}$ and the number of counters if no compression is used (denoted $\gamma_{0}$). We also include the maximum number of bits actually used to compress the counters.

%
\begin{table}[ht!]
\begin{center}
	\tbl{Compression rate}{
%\begin{tabular10}{ r r r r | r r r | r r | r }
\begin{tabular}{ r r r r | r r r | r r | r }
	  &   &        &            & \multicolumn{3}{c|}{$\gamma_h$} & &       & bits\\
	n & c & $\chi$ & $|\omega|$ & min & max & avg & $\gamma_p$ & $\gamma_0$ & max\\
	\hline
	\multirow{8}*{$10^6$}
	 & \multirow{4}*{$1.6$} 
	   & $4$ & $64$ & $22$ & $24$ & $22.8$ & $27$ & $21.3$ & $63.3$\\
	 & & $5$ & $64$ & $21$ & $22$ & $21.5$ & $24$ & $21.3$ & $63.3$\\
	 & & $4$ & $128$ & $50$ & $53$ & $51.0$ & $55$ & $42.6$ & $126.4$\\
	 & & $5$ & $128$ & $47$ & $51$ & $49.5$ & $49$ & $42.6$ & $125.1$\\
	\cline{2-10}
	 &  \multirow{4}*{$3.2$}
	   & $4$ & $64$ & $23$ & $26$ & $24.6$ & $27$ & $21.3$ & $62.7$\\
	 & & $5$ & $64$ & $24$ & $25$ & $24.9$ & $24$ & $21.3$ & $63.2$\\
	 & & $4$ & $128$ & $56$ & $59$ & $57.7$ & $55$ & $42.6$ & $126.3$\\
	 & & $5$ & $128$ & $55$ & $58$ & $56.9$ & $49$ & $42.6$ & $126.3$\\
	\hline
	\multirow{8}*{$10^5$}
	 & \multirow{4}*{$1.6$} 
	   & $4$ & $64$ & $25$ & $27$ & $26.0$ & $27$ & $21.3$ & $62.6$\\
	 & & $5$ & $64$ & $24$ & $26$ & $25.4$ & $24$ & $21.3$ & $62.5$\\
	 & & $4$ & $128$ & $57$ & $60$ & $58.8$ & $55$ & $42.6$ & $126.6$\\
	 & & $5$ & $128$ & $55$ & $60$ & $57.8$ & $49$ & $42.6$ & $125.7$\\
	\cline{2-10}
	 & \multirow{4}*{$3.2$}
	   & $4$ & $64$ & $23$ & $26$ & $25.5$ & $27$ & $21.3$ & $63.0$\\
	 & & $5$ & $64$ & $23$ & $26$ & $24.6$ & $24$ & $21.3$ & $62.1$\\
	 & & $4$ & $128$ & $57$ & $60$ & $58.3$ & $55$ & $42.6$ & $126.9$\\
	 & & $5$ & $128$ & $56$ & $59$ & $57.0$ & $49$ & $42.6$ & $125.8$\\
%\end{tabular10}}
\end{tabular}}
	\label{tab:gamma}
	%\caption{Compression rate}
\end{center}
\end{table}
%

The numbers in table \ref{tab:gamma} provide a lot of useful information. With sufficiently large $|\omega|$ or larger $\chi$, Huffman compression performs better than word packing, even without using {\em hard compression}. If $|\omega|$ is small and $\chi$ is also small, word packing is the better choice. In all cases, compression yields an improvement over not using compression. The counter limit $\chi$ only slightly influences the compression rate $\gamma_\textrm{h}$. It's impact on $\gamma_\textrm{p}$ is greater by far. The reason probably is that the values for $\chi$ differ only by $1$. It is expected that for higher differences $\gamma_\textrm{h}$ is more affected.

%
\begin{figure}[ht!]
	\centering
	\label{fig:bitfreq}
	\includegraphics[scale=0.45]{\gfx bitfreq}
	\caption{Frequencies of used bits per compressed word for $|\omega|=128$}
\end{figure}
%

Another interesting aspect is the frequency of used bits per word (figure \ref{fig:bitfreq}). The distribution follows a Poisson binomial distribution, which is to be expected. The graphs show a shift of the center depending on $\chi$, which is a result of nearly equal $\gamma_\textrm{p}$ with different code lengths. The graphs reveal potential to further reduce SRAM requirements. The compression can be improved by reducing $|\omega|$ while keeping the same $\gamma_h$, thus, effectively resembling \emph{hard compression}. For example, by reducing $|\omega|$ from $128$ to $118$ bits, $10$ bits per word can be saved. Of course, this leads to a higher number of word overflows. However, making use of the frequency distribution the number of expected overflows can be kept small. By providing CAM for an additional few overflown words, some bits per on-chip memory word can be saved.

\paragraph{On-Chip Requirements}

We now compare the on-chip requirements of different EHT configurations to the summaries presented in Section \ref{sec:review}. None of the authors present evaluation of table sizes larger than $10^4$ entries. We are interested in much larger tables with $n = 10^6$. Thus, we calculate the expected summary sizes using the mathematical models presented in the respective original papers. Equation \ref{eq:bitsfht} is used for the FHT summary. Equation \ref{eq:mmhtprime} provides the number of bits needed for both the MHT occupancy and deletion bitmaps. Care must be taken in choosing the parameter $c$. The original paper \cite{Kirsch2005Simple} suggests $c=6$. The later refinement \cite{Kirsch2010Power} determines that with extensive optimization effort $c$ can be smaller than two for $n=10^4$. Unfortunately, there is no information on the lower bound for $c$ with $n=10^6$. In our calculation we therefore assume $c$ to be optimal and set $c=1$. This is not a fair comparison and clearly favors the MHT but due to the lack of evidence we choose to rather be progressive. For the MHT summaries we consider both lazy deletions as well as counter-based deletions. For summaries with lazy deletions both the occupancy and deletion bitmap sizes are added to the summary size. For summaries with counters, only the size of the occupancy bitmap is added. We use Eq. \ref{eq:bitssf} for SF with lazy deletions, Eq. \ref{eq:bitsmbf} for MBF with lazy deletions, Eq. \ref{eq:bitssfc} for counting SF, and Eq. \ref{eq:bitsMCBF} for counting MBF. For the segmented hash \cite{Kumar2005Segmented} and discriminator table \cite{Ficara:2009:DDA:1882486.1882519} we assume a fixed number of bits per item (bpi) and a linear growth. This is a realistic assumption for the Bloom filters used in segmented and peacock hash but there is no evidence that this also holds for the discriminator table. So we follow \cite{Ficara:2009:DDA:1882486.1882519} that suggests $4$ bpi. For the peacock hash \cite{4509624} we assume a main table equal in size to the segmented hash above. We then calculate the size of all subtables with a scaling factor of $r=0.1$. All parameters not mentioned here are chosen optimal as suggested by the respective authors. The resulting sizes are shown in Table \ref{tab:sizes}.

%
\begin{table}[ht!]
\begin{center}
	\tbl{Comparison of on-chip requirements of different Bloom filter-based summaries for $n=10^6$}{
\begin{tabular}{ l | c | c | l }
	\hline \hline 
	summary & Size (KiB) & bpi & Eq., parameter \\
	\hline
	%FHT\cite{Song2005Fast} & 6292 & 50.33 & \ref{eq:bitsfht}, $c=12.8,\ b=3$ \\
	%SF lazy\cite{Kirsch2008Simple} & 5508 & 44.06 & \ref{eq:mmhtprime},\ref{eq:bitssf}. $c=1$ \\
	%SF counting\cite{Kirsch2008Simple} & 30023 & 240.18 & \ref{eq:mmhtprime},\ref{eq:bitssfc}, $c=1,\ v=2,\ d=6$  \\
	%MBF lazy\cite{Kirsch2008Simple} & 2742 & 21.93 & \ref{eq:mmhtprime},\ref{eq:bitsmbf}, $c=1$ \\
	%MBF counting\cite{Kirsch2008Simple} & 8347 & 66.77 & \ref{eq:mmhtprime},\ref{eq:bitsMCBF}, $c=1$ \\
	%Segmented\cite{Kumar2005Segmented} & 2000 & 16 & \\
	%Peacock\cite{4509624} & 223 & 1.78 & $r=0.1$ \\               
	%Discriminator\cite{Ficara:2009:DDA:1882486.1882519} & 500 & 4 & \\ 
	FHT & 6292 & 50.33 & \ref{eq:bitsfht}, $c=12.8,\ b=3$ \\
	SF lazy & 5508 & 44.06 & \ref{eq:mmhtprime},\ref{eq:bitssf}. $c=1$ \\
	SF counting & 30023 & 240.18 & \ref{eq:mmhtprime},\ref{eq:bitssfc}, $c=1,\ v=2,\ d=6$  \\
	MBF lazy & 2742 & 21.93 & \ref{eq:mmhtprime},\ref{eq:bitsmbf}, $c=1$ \\
	MBF counting & 8347 & 66.77 & \ref{eq:mmhtprime},\ref{eq:bitsMCBF}, $c=1$ \\
	Segmented & 2000 & 16 & \\
	Peacock & 223 & 1.78 & $r=0.1$ \\               
	Discriminator & 500 & 4 & \\
	\hline \hline 
\end{tabular}}
	\label{tab:sizes}
	%\caption{Comparison of on-chip requirements of different Bloom filter-based summaries for $n=10^6$}
\end{center}
\end{table}
%

\begin{table}[ht!]
\begin{center}
	\tbl{On-chip requirements of EHT configurations with $n=10^6$}{
\begin{tabular}{ c | c c | c c | c c }
	\hline \hline 
	configuration p & \multicolumn{2}{c|}{uncompressed} & \multicolumn{2}{c|}{packed} & \multicolumn{2}{c}{huffman} \\
	  & KiB & bpi & KiB & bpi & KiB & bpi \\
	\hline
	8 & \multirow{4}*{787} & \multirow{4}*{6.29} & 622 & 4.97 & 737 & 5.89 \\ 
	9 &  &  & 611 & 4.88 & 659 & 5.26 \\
	A &  &  & 700 & 5.59 & 781 & 6.24 \\
	B &  &  & 685 & 5.48 & 679 & 5.42 \\
	\hline
	C & \multirow{4}*{1573} & \multirow{4}*{12.58} & 1243 & 9.94 & 1367 & 10.93 \\
	D &  &  & 1221 & 9.76 & 1164 & 9.31 \\
	E &  &  & 1399 & 11.18 & 1348 & 10.78 \\
	F &  &  & 1370 & 10.96 & 1180 & 9.44 \\
	\hline \hline 
\end{tabular}}
	\label{tab:eht}
	%\caption{On-chip requirements of EHT configurations with $n=10^6$}
\end{center}
\end{table}
%

Table \ref{tab:eht} shows the summary sizes of all EHT configurations with $n=10^6$ without compression, word packed and huffman compressed. Our EHT summaries outperform all other summaries except the peacock hash and discriminator table. However, the discriminator table only works with static sets and is not updatable. Peacock hash requires significantly more hashing than the EHT and is non-deterministic. It needs multiple sequential or parallel lookups in the worst-case, which might not be accaptable depending on the application. The EHT is the only solution that guarantees a deterministic and constant lookup of $O(1)$, allows near real-time updates, and still requires only a few bits per item.

\paragraph{Summary}

The results fully meet the expectations and backup our theoretical analysis. We have shown that our initial assumptions allow fundamental improvements over previous suggestions. In conclusion, when constructing an EHT, the following aspects must be considered.

%
\begin{itemize}
	\item Reducing the size $m$ is achieved by ignoring the false positive probability. As a result, bucket loads will increase which can be compensated by parallel banks, increasing the off-chip memory width or by better hashing. Analysis has shown, that the expected maximum load will not exceed $3$ as long as $\frac{m}{n}>2$. Bucket overflows are extremely rare, even for a large set of items. So only a very small extra overflow memory is needed. 
	\item By separating updates from lookups the lookup summary can be optimized for smaller size and performance. The lookup summary is not exact and limited in counter range $[\chi]$.
	\item Choosing $\chi$ depends on the fraction $\frac{m}{n}$. Starting with $\chi = 5$ for $2 < \frac{m}{n} < 2.5$, $\chi$ can be decremented by one each time $\frac{m}{n}$ is doubled for a small overhead in terms of CAM. Performance will degrade when $\frac{m}{n} \rightarrow 2$.
	\item Word packing is favorable over Huffman compression both in complexity as well as resulting size, unless the word-size $|\omega|$ and the counter limit $\chi$ are big. At the cost of few additional CAM cells, the performance of Huffman compression can be improved.
\end{itemize}
%

Of course, the improvement in on-chip requirements is not free and is bought with additional computational complexity, wider memory and on-chip\slash online compression. Depending on the application and cost of hardware components, some of the suggested optimizations might not be applicable. They are, however, independent and can be easily implemented individually to optimize the total cost. A cost function can now be defined as follows. Let $\alpha_S$ be a constant cost factor of on-chip memory, $\alpha_D$ the equivalent for off-chip memory, $w$ the width of off-chip memory in bits, $E_o$ the expected number of bucket overflows and $\alpha_C$ the cost of CAM cells.
%
\begin{equation}\label{eq:cost1}
	f_{EHT} = \alpha_S \times \beta_{eht} + \alpha_D \times (m \cdot w) + \alpha_C \times (E_{CAM}+ E_o).
\end{equation}
%
Depending on the costs of the components the parameters for the EHT can be chosen such that the total cost is minimized.
%=============================================================================
