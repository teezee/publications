%!TEX root = ../paper.tex
%=============================================================================
% bloom filter review
%=============================================================================
\nohyphen{\section{Review of Hash Table Summaries}} %
\label{sec:review}

\remove{
\textcolor{red}{have to move this to related work, because of layout issues.}
%
\begin{figure}[!t]
	\centering
	\label{fig:fht}
	\includegraphics[scale=0.5]{\gfx fht}
	\caption{The two Fast Hash Tables. The Basic FHT (top) replicates every item. The Pruned FHT (bottom) only keeps the `left'most (`Left' in the literature refers to the table entry with the least index, which in this figure is topmost)}
\end{figure}
%
}

Our work is based on the schemes presented by Song et al.\ \cite{Song2005Fast} and Kirsch and Mitzenmacher \cite{Kirsch2005Simple}, which we will now review for completeness.

Song et al.\ \cite{Song2005Fast} presented a new hash table design, named \emph{Fast Hash Table}, based on hashing with choices and counting Bloom filter summaries that targets hardware implementations and provides fast lookups by utilizing on-chip memory to optimize performance. Their scheme eliminates the need for parallel lookups usually required by multiple-choice hashing. Each $b$-bit counter ($b = 3$) in CBF summary corresponds to a bucket in the hash table and represents the number of items hashed into it. Note, that if $b$ is small, the probability of counter overflows can't be neglected. Song et al.\ propose using a small CAM for overflown counters. There are a total of $m$ counters (and buckets) where $m$ is calculated using equation \ref{eq:mfht}.
%
\begin{equation}\label{eq:mfht}
	m_\textrm{FHT} = 2^{\lceil \log{c\ n}\rceil}
\end{equation}
%
The constant $c$ needs to be sufficiently large to provide low false positive and collision probabilities. It is set to $12.8$ which is considered optimal. $k$ independent hash functions, where $k$ is derived by equation \ref{eq:kbloom}, are used to index both CBF and the hash table. 
%
\begin{equation}\label{eq:kbloom}
	k = \lceil \frac{m}{n} \ln{2} \rceil
\end{equation}
%
The \emph{Basic Fast Hash Table (BFHT)} simply replicates all inserted items to all $k$ locations in the table and increments the counters. As an improvement the table can be \emph{pruned} leading to a \emph{Pruned Fast Hash Table (PFHT)}. All replicas are removed except for the leftmost with the lowest counter value (figure \ref{fig:fht}). A lookup only requires examining the least loaded bucket, i.e., the one with the lowest counter value. While pruning improves lookup time by reducing bucket loads, updates require an additional offline BFHT since items need to be relocated when their associated counters change.

% this does not work out. with c = 12.8, n=10**6, b = 3 bpi 2**(ceil(log(c*n,2)))*b = 50.33
%With $c=12.8$ buckets per item and $b = 3$ bit wide counters CBF summary requires $38.4$ bits per item of on-chip memory.
Following equation \ref{eq:mfht} the total amount of bits $\beta$ needed for the on-chip summary is dependent on the number of items and defined as
%
\begin{equation}\label{eq:bitsfht}
	\beta_\textrm{FHT} = 2^{\lceil \log{c\ n}\rceil} \cdot b
\end{equation}
%

The rather high requirement of SRAM has later been addressed by Kirsch and Mitzenmacher \cite{Kirsch2005Simple,Kirsch2008Simple}. Their key idea is to separate the hash table from its summary to allow individual optimizations. They propose using a \emph{Multilevel Hash Table (MHT)} \cite{Broder1990Multilevel} consisting of $d = \log \log n + 1$ individual tables geometrically decreasing in size. An occupancy bitmap is kept in on-chip memory that allows efficient queries for empty buckets (see figure \ref{fig:mht}).

%
\begin{figure}[!t]
	\centering
	\label{fig:mht}
	\includegraphics[scale=0.5]{\gfx mht}
	\caption{Multilevel hash table with on-chip occupancy bitmap}
\end{figure}
%

The bitmap requires a number of bits equal to the number of buckets $m$ which is defined as
%
\begin{equation}\label{eq:mmht}
	\beta_\textrm{MHT} = m_\textrm{MHT} = \sum_{i=1}^{d} (c1 \cdot c2^{i-1} \cdot n)
\end{equation}
%
with the constants $c_1$, $c_2$ chosen such that $c_1 > 1$, $c_2 < 1$, $c_1 c_2 > 1$. Considering only the number of buckets per item the equation boils down to
%
\begin{equation}\label{eq:mmhtprime}
	\beta_\textrm{MHT'} = m_\textrm{MHT'} = c \cdot n
\end{equation}
%
The authors argue that $c=6$ buckets per item suffice. Later \cite{Kirsch2010Power} the authors refine the MHT by limiting the amount that items are allowed to be moved during insertions. In the most aggressive optimization scheme this can reduce the number of buckets per item to $c<2$ for $n=10^4$ at the cost of additional complexity. Note, that this does not affect the on-chip requirements of the MHT summaries, since they are deliberately separated from the actual hash table and their size only depends on the number of items. It has, however, an impact on the size of the occupancy (and deletion) bitmap.

Following Song et al.\ to eliminate parallel lookup overhead, Kirsch and Mitzenmacher present three summary structures, the \emph{interpolation search (IS)}, \emph{single filter (SF)} and \emph{multiple Bloom filter (MBF)} summaries. Since IS is not applicable in our targeted environment we will cover only the latter two summaries which are based on Bloom filters. They are depicted in figure \ref{fig:sfmbf}.

%
\begin{figure}[!t]
	\centering
	\label{fig:sfmbf}
	\includegraphics[scale=0.6]{\gfx sfmbf}
	\caption{Single filter (SF) and multiple Bloom filter (MBF) summaries. The SF is a single Bloomier filter representing the type of an item. The MBF is an array of Bloom filters decreasing in size}
\end{figure}
%

The SF summary is a single Bloomier filter\cite{Chazelle2004Bloomier} representing the type $t$ of an item where $t$ corresponds to the sub-table of the MHT where the item is located. In addition to false positives, it can also return type failure. To keep the probability small the filter must be sufficiently large. The number of cells $m$ is defined as
%
\begin{equation}\label{eq:msf}
	m = n\ \log n
\end{equation}
%
%With $d = \log \log n + 1$ sub-tables (types) the total number of bits needed is 
With $\log \log \log n$ bits per cell the number of bits needed is
%
\begin{equation}\label{eq:bitssf}
	\beta_\textrm{SF} = n\ \log n\ (\log \log \log n)
\end{equation}
%

The MBF summary is constructed of an array of  Bloom filters $B = \{ B_0,...,B_{t-1} \}$. Each filter $B_i$ represents the set of items with type of at least $i+1$. Thus, a false positive on $B_i$ is equal to a type $i$ failure. This leads to the need of extremely small false positive probabilities to guarantee successful lookup. For a well designed MHT the number of bits the MBF requires is
%
\begin{equation}\label{eq:bitsmbf}
	\beta_\textrm{MBF} = n\ \log n
\end{equation}
%

Both, the SF and MBF summaries, support only inserts. To allow deletions significantly more effort is required. Kirsch and Mitzenmacher suggest two approaches. For \emph{lazy deletions} a deletion bitmap is kept alongside the occupancy bitmap in on-chip memory with one bit for every bucket in the MHT. On deletion, the corresponding bit is set to 1. During lookup, items in buckets that have a set deletion bit are ignored. The \emph{counter based deletions} add counters to the SF and MBF summaries to keep track of the actual number of items. The authors do not suggest specific values for the counter width nor provide evaluation. They state however, that a counting MBF requires about $3.3$ times more space than a simple MBF, that is 
%
\begin{equation}\label{eq:bitsMCBF}
	\beta_\textrm{MCBF} = 3.3 \cdot n \log n
\end{equation}
%
With $d$ choices and $\upsilon$ wide counters the modified SF requires
%
\begin{equation}\label{eq:bitssfc}
	\beta_\textrm{SFc} = \upsilon \cdot d \cdot n\ \log n
\end{equation}
bits.

A predecessor to the MHT is the \emph{Segmented Hash Table} \cite{Kumar2005Segmented} that also divides the hash table into multiple segments. Unlike the MHT, however, segments are equal sized. Each segment uses a Bloom filter to support membership queries for an item. The false positive probability needs to be extremely low to prevent sequential or parallel probing of multiple segments. A novel \emph{selective filter insertion} algorithm minimizes the number of non-zero counters by selecting that segment for insertion that leads the most empty counters. Thus false positive probability can be reduced. The authors argue that $16$ bits per item of on-chip memory and 16 or more segments suffice to provide good performance. To also support deletions an additional counting Bloom filter must be kept offline.

The authors later refine segmented hashing in \cite{4509624} which they name \emph{peacock hash}. As with the MHT the idea is to have multiple segments that geometrically decrease in size according to a so called {\em scaling factor}. Each table, except the biggest main table, has an on-chip Bloom filter for membership queries. When an item is searched the filters of the subtables are queried. If lookup is unsuccessful the main table is probed. Again, the false positive probability needs to be extremely low to prevent multiple table accesses. With a scaling factor of 10 (each successive table has a size of $10\%$ of the former) and following the observations in \cite{Kumar2005Segmented}, about $2$ bits per item are needed for the on-chip Bloom filters. 

The problem of non-deterministic lookup performance is addressed in \cite{Ficara:2009:DDA:1882486.1882519}. Here each item is associated with a {\em fingerprint} that is cut into chucks and stored in a small {\em discriminator} table. This table is used to index the main table and is stored on-chip. Fingerprints must be unique to prevent collisions. A genetic algorithm is suggested to find the perfect mapping. The authors show that a discriminator table with $4$ bits per item can be found in a reasonable amount of time. While it is possible to "build a perfect match [\dots] with fewer [2] bits per item [\dots] the effort [\dots] greatly exceeds the advantages." (\cite{Ficara:2009:DDA:1882486.1882519}, p.141.) Also, being a perfect hashing scheme, it works only on static sets and the discriminator table can only be built if the set of items is known a priori.
%\vfill
%=============================================================================
