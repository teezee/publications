%!TEX root = ../paper.tex
%=============================================================================
% introduction
%=============================================================================
\section{Introduction} %
\label{sec:intro}

Efficient hashing in network applications is still a challenging task, because tremendously increasing line speed, demand for low power consumption and the need for performance predictability pose high constraints on data structures and algorithms. At the same time, memory access speed has almost stayed constant, especially because of the latency and waiting time between sequential repeated accesses. Hashing has yet to be widely accepted as an ingredient in hard real-time systems and hardware implementations, as prejudices concerning the unpredictability of size and time requirements due to collisions still persist.

Modern approaches make use of multiple choices in hashing \cite{Broder2001Using,Vocking2003How} to improve load and the number of memory accesses. Unfortunately, \emph{d-ary} hashing requires $d$ independent parallel lookups. To mitigate the need for high parallelism, table summaries \cite{Song2005Fast,Kirsch2008Simple}, based on (counting) Bloom filters \cite{Bloom1970Space/Time,Fan1998Summary} and derivates, further reduce the number of table accesses to one with high probability (w.h.p.) at the cost of fast but expensive on-chip memory (SRAM). The summaries allow set membership queries with a low false positive rate and some approaches also reveal the correct location of an item if present.

Although these improvements address space and time requirements, they come at a high price. SRAM is extremely expensive and, while external DRAM can be shared, it must be replicated for every network processor. In addition, numerous networking applications compete for their slice of this precious memory. For many - like socket lookups, Layer-2 switching, packet classification and packet forwarding - tables and their summaries tend to grow extremely large, up to the point where providing enough SRAM is not applicable. Perfect hashing, on the other hand, can lead to a near perfect match \cite{Hagerup2001} but only works on static sets, does not allow updates and requires complex computations.

The options for a network application designer are grim. With millions of lookups per second, even the most improbably worst-case is likely to happen, slowing down the entire application and leading to packet loss and network congestion. Na\"ive hash tables are too unpredictable and yield too many collisions. $d-ary$ hashing requires high parallelism to minimize sequential lookups. Expensive SRAM-based table summaries optimize the average case performance but still require multiple lookups in the worst case. Perfect hashing can potentially guarantee a perfect match and a constant lookup performance but requires a static set. To be fully accepted in practical network applications hashing needs to guarantee constant lookup performance, require minimal on-chip memory, and allow regular updates.

We propose mechanisms to construct an improved data structure which we name {\em Efficient Hash Table (EHT)}, where efficient relates to both on-chip memory (SRAM) usage and to lookup performance. The design aggressively reduces the amount of bits per item needed for the on-chip summary, guarantees a constant lookup time and is still updatable in near real-time. To the best of our knowledge, the EHT is the only data structure offering these characteristics. 

Previous approaches suffer from the need for full generality. Careful observation of network applications reveals certain degrees of freedom which can be exploited to achieve significant improvements. These observations lead to the following four key ideas:

\begin{itemize}
	\item The summary's false positive rate can be ignored, it is irrelevant in respect to lookup performance. %\ref{sec:falsep}
	\item The update and lookup engines can be separated. The on-chip summary need not to be exact. %\ref{sec:engines}
	\item The summary can be de\slash compressed in real time. %\ref{sec:sram}
	\item The load of a bucket can potentially be larger than one without increasing memory accesses. %\ref{sec:dram}
	%\item Memory width is flexible. A DRAM bucket can hold more than one item. 
\end{itemize}

In concert, these concepts reduce SRAM memory size up to an order of magnitude, but they can also be applied and configured individually depending on the target application.
%\vfill
%=============================================================================
