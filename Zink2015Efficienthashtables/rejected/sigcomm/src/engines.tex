%!TEX root = ../paper.tex
%=============================================================================
% engines
%=============================================================================
\nohyphen{\section{Separate Update and Lookup Engines}} %
\label{sec:engines}

Previous suggestions have shown that support for updates is accompanied by enormous overhead to the tables and their summaries. The PFHT needs an additional offline BFHT to identify entries that have to be relocated. The MHT requires an occupancy bitmap and the summaries require either a deletion bitmap for lazy deletions or counting filters. %Figure \ref{fig:analysis1} shows the total SRAM requirement of FHT and MHT with increasing number of items $n$.
\remove{
%
\begin{figure}[!t]
	\centering
	\label{fig:analysis1}
	\includegraphics[scale=0.45]{\gfx analysis1}
	\caption{Total SRAM consumption in KiB, $n \leq 1.5m$ for FHT with 3 and 4 bits for the CBF summary and MHT with lazy as well as counter based deletions, including the occupancy bitmap}
\end{figure}
%
}

In most real-world applications, especially those that require fast lookups, updates are much rarer than lookups. By completely separating update and lookup engines, on-chip requirements can be reduced. The idea is to keep two separate summaries. One is kept online in on-chip memory and is optimized for lookups. It does not need to be exact and can be different from the update summary which is kept offline. Keeping only an approximate online summary allows individual optimization and more efficient encoding. The update engine precomputes all changes and sends modifications to the online structures.

Although, some of the techniques we describe are applicable to different table and summary structures such as the FHT and MHT, we concentrate on optimizing the scheme of Song et al. \cite{Song2005Fast}, which we argue has most room for improvement. Figure \ref{fig:engines} shows a simplified overview of our design. It is composed of the offline CBF and BFHT, an online on-chip compressed CBF (CCBF), the online PFHT in off-chip memory and a small extra memory (CAM, registers) for overflown entries (we will refer to the overflow memory as CAM in the following). Strictly, the offline CBF is not needed, the counter values could also be computed by examining the length of the linked list. However, this would lead to significant overhead when querying counters, so we keep the offline CBF for performance reasons.

%
\begin{figure}[!t]
	\centering
	\label{fig:engines}
	\includegraphics[width=0.9\linewidth]{\gfx engines}
	\caption{Efficient Hash Table design overview. The offline update engine precomputes all updates. The online lookup engine is optimized for time\slash space efficient lookups. The online summary is not exact and compressed. The table is pruned. A small extra memory is used for counter and entry overflows}
\end{figure}
%

%\paragraph{Lookups}
\subsection{Maximum Counter Value}
%-----------------------------------------------------------------------------
A lookup requires retrieving the leftmost smallest counter in the CBF summary. Successful lookup is guaranteed as long as not all counters corresponding to a key are overflown. If all the counters are overflown, it is not be possible to identify the correct bucket. The goal is to identify a maximum allowed counter value $\chi$ where the probability that all $k' < k$ chosen counters for an item equal $\chi$ is appropriately small. In essence, choosing an appropriate value for $\chi$ is a trade-off between storage saved, the number of counter overflows, and the number of expected lookup failures.

\remove{
\textcolor{red}{
TODO: find an appropriate equation for calculating the expected number of lookup failures for any given $\chi$ and $c$, OR the probability of a lookup failure. Is this equal to Pr(smallest counter value) or to Pr(linked list length)? I'm not sure (here linked list len). Whats with expected number of CAM entries below? In \cite{Song2005Fast} we have an ``Expected number of items for which the searched bucket contains > j items'' (before pruning) which should actually fit here also, maybe. Have to think about that. Or let it be as is.
RESOLVED: was actually correct, p smallest counter value is the one to go with, also meets results.
}
}

\cite{Song2005Fast} gives an analysis of the probability that in any $k' < k$ chosen buckets the counter value has a specific height $s$. The derivation of the equation is quite complex and for simplicity left out at this point. Interested readers are referred to the actual paper. Figure \ref{fig:psmallestcounter} shows the expected smallest counter value in $k'$ chosen counters depending on the size $m$, or to be specific, the buckets per item constant $c$. The constant $c$ is chosen to divide $m$ by multiples of 2. As expected the table size has significant impact on the smallest counter value. That is, for smaller $c$ the probability of choosing a higher counter is higher. When reducing $c$ the maximum counter value $\chi$ must be higher.

%
\begin{figure}
	\centering
	\label{fig:psmallestcounter}
	\includegraphics[scale=0.5]{\gfx psmallestcounter}
	\caption{Probability of smallest counter value in k' counters for different c}
\end{figure}
%

To be able to retrieve all entries the event that all chosen $k' < k$ counters equal $\chi$ must be dealt with. The easiest solution is to move entries which cannot be retrieved by calculating the counters to CAM. A small CAM must already be maintained for overflown buckets. If $\chi$ is chosen appropriately large the overhead is minimal.

The expected number of entries that are diverted to CAM can easily be calculated. Let $Pr\{ C = s \}$ be the probability, that of $k'$ chosen counters the smallest counter has value $s$ and let $l$ be the highest counter value to be expected in the offline summary.
%
\begin{equation}\label{eq:ecam}
	E_\textrm{CAM} = \sum_{i = \chi}^{l} Pr\{ C = i\} \times n
\end{equation}
%
The expected number of CAM entries for $n = 10^6, c = \{ 12.8,6.4,3.2,1.6,1\}$ and $\chi = \{ 3,4,5 \}$ can be seen in table \ref{tab:ecam}. The numbers can be used as a guideline for choosing $\chi$. For example, with $c = 12.8$ and $\chi = 3$, the expected number of CAM entries is still $0$. Without any additional cost, the counter-width of the summary can be reduced to $2$ bits, achieving a reduction in size of $30 \%$. By further providing a small CAM for few entries, $c$ can be halved, leading to a summary only $\frac{1}{3}$ of the optimum in size. The trade-off improves for increasing $\chi$. Consulting the numbers, each time $\chi$ is incremented once, $c$ can be halved, at the cost of few additional CAM entries.
\vfill

%
\begin{table}[!t]
\begin{center}
\begin{tabular}{r|rrr}
	\hline \hline
		& \multicolumn{3}{c}{$\chi$}\\
	$c$ & $5$ & $4$ & $3$\\
	\hline
	$12.8$ & $0$ & $0$ & $0$\\
	$6.4$ & $0$ & $0$ & $17$\\
	$3.2$ & $0$ & $47$ & $4183$\\
	$1.6$ & $285$ & $5181$ & $61110$\\
\end{tabular}
	\label{tab:ecam}
	\caption{Expected number of CAM entries for different $c$ and $\chi$ with $n = 10^6$ inserted items.}
\end{center}
\end{table}
%

\remove{
%
\begin{figure}
	\centering
	\label{fig:ecam}
	\includegraphics[scale=0.5]{\gfx ecam}
	\caption{Expected number of CAM entries for different c and $\chi$}
\end{figure}
%
}

\subsection{Encoding}
%-----------------------------------------------------------------------------
Limiting the counter range allows for better optimized encoding of the summary. We follow a simple and well known approach that is also used in \cite{Kirsch2008Simple} to pack few counters into one Byte. The difference is that we extend the scheme to an arbitrary word size to achieve higher compression rates. We argue, that SRAM, being implemented on-chip, can potentially have an arbitrary word size. Basically, the wider the memory, the more counters can be packed into one word and the more bits can be saved. In reality, one will not find memory widths $> 128$.

Counters that are limited in range can easily be encoded in a specified number of bits. Let $\omega$ be a memory word, $|\omega|$ its width in bits, and counters limited to the range $[0,\chi]$, then the number of counters that can be packed into $\omega$ is defined as
%
\begin{equation}\label{eq:packrate}
	\gamma_p = \lfloor \frac{log\ 2^b}{log\ |[\chi+1]| }\rfloor
\end{equation}
%
We will also refer to $\gamma$ as the compression rate. Compression (equation \ref{eq:pack}) and decompression is trivial. Implemented in hardware, all counters can be unpacked in parallel.
%
\begin{equation}\label{eq:pack}
	\omega = \sum_{i=0}^{\gamma_\textrm{p}-1} \varsigma_i \cdot |[\chi+1]|^i\
\end{equation}
%

\remove{
%
\begin{algorithm}
	\caption[Word decoding]{Word decoding}
	\label{algo:unpack}
	\KwIn{$\omega$: the memory word to decode, $\chi$: maximum counter value, $\gamma_\textrm{p}$: counters per word}
	\KwOut{decoded counter values}
\textbf{function:} decode ($\omega$, $\chi$, $\gamma_\textrm{p}$) \Begin{
	$C = \emptyset$ \;
	\For{$\forall i \in [\gamma_\textrm{p}]$} {
		$C\ \leftarrow C \cup \omega\ \%\ \chi$ \;
		$\omega\ \leftarrow \omega / \chi$ \;
	}
	\Return{$C$} \;
}
\end{algorithm}
%
%One drawback of this scheme is the expensive modulo computation to decode the counters. Implemented in hardware, however, all counters can be unpacked in parallel.
}
 
We will introduce a more sophisticated Huffman compressed summary in section \ref{sec:sram}.

%\paragraph{Updates}
\subsection{Updates}
%-----------------------------------------------------------------------------
In our design we want to completely separate updates from lookups to keep interference with the lookup process as small as possible. When performing updates, the offline table pre-computes all changes and applies them to the online CCBF, PFHT and CAM.

There are three types of entries that must be distinguished. \emph{Offline entries} are kept in the offline BFHT. Due to overflows, each offline entry has a corresponding online entry either in the online PFHT (\emph{table entry}) or in extra memory (\emph{cam entry}). The update engine must be able to identify which of the \emph{offline entries} in affected buckets are \emph{table entries}, and which are \emph{cam entries}. Else, it would not be possible to compute relocations without examining all possible locations in the online structure. Since we want to minimize online table access all \emph{offline entries} are paired with a \emph{locator}. In case the corresponding entry is a \emph{table entry}, the \emph{locator} is simply the index of the hash function used to store the \emph{table entry}. If it is a \emph{cam entry}, the locator is set to $\infty$. An offline entry of item $x$ thus is defined as $E_\textrm{offline}(x) \leftarrow (k,v,i)$, where $k$ denotes the key, $v$ the associated value, and $i$ the locator.

%
\begin{algorithm}
	\caption[Insert]{Insert}
	\label{algo:insert}
	\KwData{
		$k$: number of choices, $B$: offline BFHT, $C$: offline CBF, $\chi$: maximum counter value
		% MW->TZ: Was sind \zeta und \chi?
	}
	\KwIn{
		$x$: the item to insert
	}
	\KwOut{
		updated tables and summaries such that they include $x$
	}
	\SetKwFunction{Space}{SpaceLeft}
	\SetKwFunction{Smi}{SmallestIndexOf}
	\SetKwFunction{OnTabE}{TableEntries}
	\SetKwFunction{OnCamE}{CamEntries}
	\SetKwFunction{UpdateOn}{UpdateOnline}
	\SetKwFunction{Upd}{Update}
	\SetKw{From}{for}
	\SetKw{To}{to}
\textbf{procedure:} insert ($x$) \Begin{
	$R, L \leftarrow \emptyset$\;
	$M \leftarrow \textrm{map:}$ \{bucket, content\}\;
	$H \leftarrow \{ h_i(x)\ \From\ i \leftarrow 0\ \To\ k \}$\;
	$\zeta \leftarrow \{ C_{h} \forall h \in H \} $\;
	$l, a \leftarrow \infty$\;
	\If{$ !(c \ge \chi)\ \forall c \in \zeta$} {
		$l \leftarrow$ \Smi{$\min{(\zeta)},\zeta$}\;
		$a \leftarrow H_l$\;
	}
	\tcp{collect and insert}
	$e \leftarrow$ new offline entry$(x,\infty)$\;
	\For{$\forall h \in H$}{
		$R \leftarrow $ insert \OnTabE{$B_h$}\;
		$R \leftarrow $ append \OnCamE{$B_h$}\;
		$B_h \leftarrow B_h \cup e$\;
		\lIf{$C_h < \chi$} {$L \leftarrow L \cup C_h$}\;
		$C_h ++$\;
	}
	\tcp{compute relocations}
	\For{$\forall r \in R$}{
		%$H_\textrm{n}, \zeta_\textrm{n} \leftarrow \textrm{hash values, corresponding counters}$\;
		%$l_\textrm{n} \leftarrow \textrm{new locator, smallest index or }\infty$\;
		%$a_\textrm{n} \leftarrow \textrm{new address, } H_{l_\textrm{n}} \textrm{ or }\infty$\;
		%$a_\textrm{o} \leftarrow \textrm{old address, } H_{r.l} \textrm{ or }\infty$\;
		%$E \leftarrow $ \OnTabE{$B_{a_n}$}\;
		compute new Hash values $H_\textrm{n}$, counters $zeta_\textrm{n}$, locator $l_\textrm{n}$, old and new bucket address $a_\textrm{o}, a_\textrm{n}$\;
		\If{$a_\textrm{n} \neq a_\textrm{o} \&\& a_\textrm{o} != \infty$}{
			\tcp{entry moved within table}
			\If{!\Space{$B_{a_n}$}} {
				$a_\textrm{n} = \infty$\;
			}
			\Else{
				$M.$\Upd{$\{ a_\textrm{o},0\} , \{ a_\textrm{n},r\}$}\;
				%update $M$ with \{$a_\textrm{o},0$\} , \{$a_\textrm{n},r$\}\;
			}
		}
		\If{$a_\textrm{n} \neq a_\textrm{o} \&\& a_\textrm{o} == \infty$} {
			\tcp{entry moved from cam to table}
			\If{\Space{$B_{a_n}$}} {
				$M.$\Upd{$\{ \infty,r\} , \{ a_\textrm{n},r\}$}\;
				%update $M$ with \{$\infty,r$\} , \{$a_\textrm{n},r$\}\;
			}		
		}
		\If{$a_\textrm{n} \neq a_\textrm{o} \&\& a_\textrm{n} == \infty$} {
			\tcp{entry moved from table to cam}
			$M.$\Upd{$\{ a_\textrm{o},0\}, \{ \infty,r\}$}\;
			%update $M$ with \{$a_\textrm{o},0$\} , \{$\infty,r$\}\;
		}
	} % end relocation
	\tcp{calculate position of new item}
	\If {$l \ne \infty$} {
		\If{\Space{$B_{H_l}$}} {
			$e.l = l$ \;
			\If{!$H_l \in M$} {
				$M.$\Upd{\OnTabE{$B_{H_l}$}}\;
			}
				%update $M$ with \OnTabE{$B_{H_l}$}\;
			%}
			$M.$ \Upd{$B_{H_l},e$}\;
			%update $M$ with \{$H_l,e$\}\:
		}
	}
	\UpdateOn{$M,L$}\;
}
\end{algorithm}
%

Algorithm \ref{algo:insert} shows the pseudocode for insertions. First we initialize a relocation list $R$, a counter increment list $L$ and an update map $M$. The list is used to collect all entries that are considered for relocation while the update map maps online buckets to their new value. The hash values for $x$ are computed, counters retrieved and the target location identified. If all counters are equal to or exceed the maximum allowed value $\chi$, the new entry must be placed into CAM and the locator is set to $\infty$. Otherwise the entry's locator is set to the index of the hash function used to store $x$. Note, that in any case we create a new offline entry with a locator set to $\infty$ since we cannot yet know where the item is placed. Only after relocation we can be sure, whether the item is put to the table or to CAM. We then collect all entries in affected buckets that are also either table entries or cam entries, add the new offline entry and increment the counters. Note, that the table entries are inserted at the head of the list, while the cam entries are appended to the end. This is for balancing reasons. Online entries must be relocated prior to CAM entries since it is possible that space becomes available to hold the entries from CAM. Next the collected entries must be considered for relocation. For each collected entry we compute the hash values, the new locator and the new bucket address. We also collect all online entries for the target bucket. If the new address is different from the old address the entry $r$ might be relocated. There are 3 possible events:
%
\begin{enumerate}
	\item The entry is moved inside the table. $M$ is updated with an empty entry at the old bucket. If the new bucket has enough space left, $M$ is updated with the new bucket and $r$, else $r$ must be moved to cam and $M$ is updated with an $\infty$ bucket (indicating overflow memory) and $r$.
	\item The entry is moved from cam to table. If the new bucket has enough space left, $M$ is updated with \{new bucket,$r$\} and \{$\infty$, $r$\}. Else $r$ can't be moved to table and $M$ is not updated.
	\item The entry is moved from table to cam. $M$ is updated with \{new bucket,$0$\} and \{$\infty$, $r$\}.
\end{enumerate}
%
In any case, the locator of a relocated offline entry must be updated.

The actual update of the online structure is performed by the procedure ``UpdateOnline''. The update map $M$ contains bucket addresses and their associated content. The buckets in $M$ are simply replaced with their new value. A special case is if bucket address is $\infty$, which indicates overflow memory. In this case the overflow memory is probed for the associated entries. If the entry is present, it is removed, else it is inserted. The list $L$ contains a list of counter addresses that must be incremented.

The PFHT needs only be accessed to write changed buckets. Hence, the complexity is optimal and upper bound by the number of changed buckets. With $n$ items stored in $m$ buckets and $k = \frac{m}{n} \log 2$ choices, the upper bound is $O(1 + \frac{m}{n}k) = O(1 + \log 2)$. Similarly, the online CCBF needs only be accessed for counters that actually change, i.e. those that have not yet reached $\chi$.

Deletions work similar to insertions with minor differences. The deleted entry $x$ is removed from the offline BFHT prior to collecting entries. Then all entries in affected buckets buckets are collected and relocation computed. Afterwards, the bucket from which the item is removed is added to $M$ if not already present. Then the online updates are performed. Deletions have the same complexity as insertions.
%=============================================================================
