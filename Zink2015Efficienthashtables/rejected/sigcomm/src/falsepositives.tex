%!TEX root = ../paper.tex
%=============================================================================
% false positives
%=============================================================================
\nohyphen{\section{Ignore the false positive probability}} %
\label{sec:falsep}

Bloom filters are usually constructed to optimize the false positive probability. In case of the MHT summaries having a negligible small false positive rate is essential to prevent type failure. In general, applications that require exact knowledge about set membership are dependent on minimizing false positives. This inevitably leads to relatively large filters.

We observe that applications using Bloom filter-based summaries as an index into another data structure, like the FHT, do not suffer from false positives, as long as a successful lookup independent of the false positive probability is guaranteed. The structure must provide a predictable worst-case lookup performance. A false positive returned by the summary leads to a table lookup that returns NULL. The worst-case performance is not affected. In conclusion, Bloom filter-based summaries can be potentially much smaller.

By reducing the address space of the summary while keeping the number of entries $n$ constant, counter values and the load of buckets are expected to increase. There exists a trade-off between reducing on-chip memory requirements and the resulting counter values and bucket loads.

%
\begin{figure}[!t]
	\label{fig:pcountervalue}
	\includegraphics[scale=0.45]{\gfx pcountervalue}
	\caption{Counter value probabilities for different $c$. For $c > 1.6$ there is no effect on the counter distribution. For $c \le 1.6$ the probability for higher counters increases}
\end{figure}
%

%\paragraph{Counter values}
\subsection{Counter Values}
%-----------------------------------------------------------------------------
Counters values follow a binomial distribution. With $m$ possible locations and $nk$ insertions (each insertion increments $k$ counters) the probability $p_i$ that a counter received is incremented exactly $i$ times can be calculated using the following equation \cite{Song2005Fast}.
%
\begin{equation}\label{eq:countervalue}
	p_i = \binom{nk}{i} (\frac{1}{m} )^i (1 - \frac{1}{m})^{nk-i}
\end{equation}
%
This is not entirely accurate. The probability that, due to collisions, less than $k$ counters for an item can be increased, is neglected. But the estimate is close enough to allow counter value predictions. Figure \ref{fig:pcountervalue} shows the counter distribution for different $c$. The constant $c$ is chosen to divide $m$ into multiples of $2$. As long as $c > 1.6$ the counter distribution is not affected. For $c \le 1.6$ the probability for higher counters increases. This is the result of an overestimate of the number of choices $k$. Following equation \ref{eq:kbloom}, $k$ depends on the number of buckets per item $\frac{m}{n}$. As $\frac{m}{n} \rightarrow 2$, $k$ will lead an overestimate resulting in higher counter values. In conclusion, as long as $\frac{m}{n} > 2$ and $k$ chosen optimal, the counter values are not affected by smaller sized filters. Hence the counter width in terms of bits is unaffected.

\subsection{Bucket load}
%-----------------------------------------------------------------------------
\begin{table}
\begin{center}
\begin{tabular}{r|r|r}
	\hline \hline
	c & k & $E$ \\
	\hline
	12.8 & 12 & 1 \\
	6.4 & 6 & 2 \\
	3.2 & 3 & 2 \\
	1.6 & 2 & 3 \\
	1 & 1 & 5 \\
	\hline \hline
\end{tabular}
	\label{tab:maxload}
	\caption{Expected maximum load for different $c$}
\end{center}
\end{table}
%
We follow \cite{Azar1994Balanced} to predict the expected maximum load that occurs with high probability. With $n$ items, $m$ buckets and $k$ choices the expected maximum load is defined as
%
\begin{equation}\label{eq:maxload1}
	E_\textrm{maxload} = \frac{\ln \ln m}{\ln k}
\end{equation}
%
The equation holds for any $m \rightarrow \infty$ with $n = m$ and $k \ge 2$. In our design, however, $m \gg n$. The result leads an overestimate of the maximum load, which in practice should be smaller. To compensate we apply the \emph{floor} function to round to the next lower integer. A special case arises for $k = 1$. This happens when $\frac{n}{m} \rightarrow 1$. Then the  expected maximum load is defined as
%
\begin{equation}\label{eq:maxload2}
	E1_\textrm{maxload} = \frac{\ln n}{\ln \ln n}
\end{equation}
%

Table \ref{tab:maxload} shows the expected maximum load in respect to different $c$. The results are surprisingly positive. Setting $c = 3.2$ results in a summary size $\frac{1}{4}$ of the optimum proposed in \cite{Song2005Fast}. The maximum load increases from $1$ to $2$ w.h.p.. In other words, allowing two entries per bucket leads to a reduction in on-chip memory size by a factor of four. The trade-off even improves for $c = 1.6$. With three entries per bucket, the on-chip memory size can be reduced to $\frac{1}{8}$ of the optimum.

The problem arising is how to deal with more than one entry per bucket. A na\"ive solution is to use $E$ memory backs, one for each possible entry, and query them in parallel. The additional cost is acceptable compared to the saved SRAM. In section \ref{sec:dram} we will discuss this issue in more detail and present techniques that allow multiple entries per bucket and do not require parallel or sequential memory accesses.
%=============================================================================