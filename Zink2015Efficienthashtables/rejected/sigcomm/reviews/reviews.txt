===========================================================================
                         SIGCOMM 2011 Review #195A
                Updated Wednesday 9 Mar 2011 8:27:08pm PST
---------------------------------------------------------------------------
  Paper #195: Exploiting Degrees of Freedom for Efficient Hashing in
              Network Applications
---------------------------------------------------------------------------

                      Overall merit: 3. Borderline reject
          Confidence in your review: 2. Fair
                            Novelty: 3. Incremental improvement
                  Technical Quality: 3. Only minor technical problems
               Presentation Quality: 1. Poor
                           Maturity: 2. Needs significant work

                         ===== Paper summary =====

This paper presents a number of ad hoc and engineering optimizations to minimize the on-chip memory consumption of various modern hash table data structures. None of these optimizations is breakthrough, but they do seem to be useful. The end results in the paper are impressive, showing an order of magnitude reduction on memory consumption. 

The presentation of this paper is very poor, which prevented me from properly judging the significance of the optimizations, as well as properly deciding whether I should buy the evaluation results. If the paper is accepted, serious shepherding will be necessary.

                        ===== Paper strengths =====

- The end results seem to be impressive.

                       ===== Paper weaknesses =====

- It is not clear how significant (i.e., non-trivial) these optimizations are. In particular, I am not sure whether people are not aware these optimizations, or whether these are just engineering optimizations that people know (or can easily derive if needed) and do not even bother to publish.

- It is not clear whether the 10 times improvement is real. In particular, the paper gives me little confidence that the comparison is a fair comparison.

                      ===== Comments for author =====

This paper suffers seriously from its poor presentation, from the introduction to the conclusion. For example, despite that I know the previous works [21] and [15], Section 3 in this paper is still very hard to understand. I don't see how a reader who did not previously know [21] and [15] can understand Section 3. 

Perhaps due to the poor presentation, the paper fails to convince me on two key points. First, it is not clear to me whether the engineering optimizations proposed by this paper are really smart optimizations, or whether they are just natural optimizations that people will apply when these hash table data structures are actually implemented in a real system. For example, it seems using multiple entries per word and multiple words per bucket is a natural thing that people do. Using compression (as in [12]) also seems to be natural. Of course, the current paper uses Huffman coding, which probably is better than [12]. 

Second, it is not clear to me whether the 10 times reduction on memory consumption is real. It seems that by applying all these aggressive optimizations, we do pay some costs. For example, we will need to compress online, and the architecture in Figure 5 is more complex than naive architectures. The paper's evaluation does not quantify these costs. It makes me wonder how large/small these costs are. The paper should also give more details in the evaluation section on how the previous hash table data structures are implemented. This will help to convince the reader than the 10 times improvement is not due to poor implementation of previous designs. It will help further if the paper can clearly explain where the 10 times improvement comes from, compare to individual existing designs. 

I don't think it is acceptable to dismiss [12] by simply saying that "we assume it is unable to provide the required performance".

===========================================================================
                         SIGCOMM 2011 Review #195B
                 Updated Monday 14 Mar 2011 8:29:19am PDT
---------------------------------------------------------------------------
  Paper #195: Exploiting Degrees of Freedom for Efficient Hashing in
              Network Applications
---------------------------------------------------------------------------

                      Overall merit: 4. Borderline accept
          Confidence in your review: 3. Average
                            Novelty: 3. Incremental improvement
                  Technical Quality: 3. Only minor technical problems
               Presentation Quality: 4. Good
                           Maturity: 3. Almost finished; some details need
                                        work

                         ===== Paper summary =====

This paper presents a hashing scheme that can be used to minimize on-chip memory requirements while maintaining single off-chip memory access lookups.  The scheme allows higher false positive rates and more entries per hash bucket to achieve this.

                        ===== Paper strengths =====

This paper presents a successful hashing scheme that requires less on-chip SRAM without sacrificing performance.

                       ===== Paper weaknesses =====

By increasing the number of entries per hash bucket, this scheme requires wider accesses to external memory.  There is clearly a trade-off between saving on-chip memory and requiring more bandwidth to off-chip memory that is largely ignored

                      ===== Comments for author =====

The four insights that are used to guide the design of a more efficient hashing algorithm are interesting.  The paper compellingly shows that by exploiting these insights, you can achieve efficient hash table lookups using much smaller on-chip memory sizes.

However, you are reducing on-chip memory at the cost of increasing the amount of off-chip memory bandwidth.  By requiring that each hash bucket be required to hold more than one entry, you either need to perform multiple off-chip accesses or increase the width of off-chip memory.  Both choices are expensive, as they require increased off-chip memory bandwidth.  The trade-off between on-chip memory capacity and off-chip memory bandwidth is not an obvious one and likely depends on the details of a specific implementation.  While the paper presents some ideas in section 7 to mitigate the off-chip bandwidth demands, these ideas could also be used with other hashing techniques to achieve bandwidth reductions, as well.  While the technique is still interesting, you should be more honest about this and other trade-offs that are being made for the sake of decreasing the required on-chip memory capacity.

For example, while packing counters into a word is an obvious choice, further compressing them using Huffman encoding is not as obviously a good choice.  While you can get a few more counters per SRAM word (Table 6 shows the increase is pretty negligible), you still have to decode every counter within the SRAM word to find the one you are looking for.  This clearly isn't free, and is likely to cost far more than an SRAM access given that they are bit operations on data packed into words.  While it may often be the case that saving a few words of SRAM is valuable, the increase in time to access the counter may outweigh that savings.

In the evaluation it is incredibly cryptic to encode the 16 configurations using a hexadecimal digit.  It makes it nearly impossible to decipher the tables and figures in section 8 without significant effort from the reader.  Don't do this.

===========================================================================
                         SIGCOMM 2011 Review #195C
                Updated Tuesday 15 Mar 2011 10:27:55pm PDT
---------------------------------------------------------------------------
  Paper #195: Exploiting Degrees of Freedom for Efficient Hashing in
              Network Applications
---------------------------------------------------------------------------

                      Overall merit: 2. Weak reject
          Confidence in your review: 3. Average
                            Novelty: 3. Incremental improvement
                  Technical Quality: 2. Flawed but probably fixable in the
                                        final version
               Presentation Quality: 3. Average
                           Maturity: 2. Needs significant work

                         ===== Paper summary =====

1. Authors started with FHT which is a fairly low baseline. Authors point our a number of weaknesses of FHT and later introduce a number of orthogonal strategies to address them. First authors prove an upper bound on the counter width of the counting bloom filter array - they argue that 2-3 bits are enough with a small overflow CAM; this bound is very well known and authors could have very well skipped this. Next, authors encode the counter array using wide 128bit words at a time, and later extending to more sophisticated Huffman compression scheme. Since counter array is sparse it is fairly intuitive that the result will lead to significant compression. After doing all these optimizations and tuning the compression parameters authors achieve 10x reduction over the baseline FHT. Compared to baseline MHT reduction is less than 4x. Compared to more recent work of Krisch et al. the reduction will be marginal. Furthermore, authors fail to compare against most recent work of Ficara et al. which shows that slightly more than 1-bit per entry is sufficient which is better than EHT. Authors also fail to compare against segmented hash table and peacock hash table both of which have roughly comparable level of compression as EHT and are much simpler algorithms.

                       ===== Paper weaknesses =====

2. Aside from the above issues, the primary drawback of the algorithm is that it is excessively complex and requires fine tuning of a number of parameters, which substantially reduces its scope. Most practical applications requires fast updates and simpler designs; complex algorithms most often stay on paper and never get properly implemented. EHT will require complex hardware to perform the encoding, compression, and insertions and deletions. The reduction in SRAM size, whatsoever, might not be worth the increased area, power and design and verification time. It is worth noting that memory cells are packed much more efficiently than the logic cells. With the advent of more dense and low power eDRAM memory technology, adding logic gates and design complexity to save memory bits may not be the right design choice. Academic world has gone on too long in getting the most compact memory footprint to implement various lookup algorithms and in the process ignored the other more important factors such as design time and complexity, algorithm simplicity and update rates, and the fact that logic cells are not free; in fact they are much more power and area inefficient than memory cells.
