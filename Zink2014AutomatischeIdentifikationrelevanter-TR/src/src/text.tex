%!TEX root = ../report.tex
\section{Einleitung}

Die Archivierung digitaler Unterlagen ist Pflicht von Einrichtungen des \"offent\-lich\-en Rechts \cite{BArchG}.
Dennoch ist Web-Archivierung bei \"offentlichen Einrichtungen nur l\"uckenhaft vorhanden.
Die Gr\"unde hierf\"ur sind vielf\"altig.
Die inhaltliche und zeitliche dynamik digitaler Inhalte machen deren Archivierung zu einer technisch hoch anspruchsvollen Aufgabe die mehrere Disziplinen umfasst.
So erfordert eine zeitlich l\"uckenlose Speicherung riesige versionierte Datenbanken.
Moderne Inhalte werden dynamisch in Abh\"angigkeit von Nutzer\-eingaben aufbereitet was intelligente Algorithmen zur Erfassung vorraussetzt.
Dies f\"uhrt dazu, dass ausnahmsweise die Technik der Gesetzgebung hinterher hinkt.

Der Standard in der Web-Archivierung ist daher noch immer das sogenannte Web-Crawling, also das Folgen von Links.
Ausgehend von einer Startseite werden hyperlinks eingesammelt und anhand eines vorher definierten Regelwerks gepr\"uft, ob den Links gefolgt werden soll oder nicht.
Das Schreiben solcher Regelwerke erfordert in der Regel Expertenwissen und erfolgt \"uber komplexe Konfigurationsdateien des Web-Crawlers. 
Der prominenteste Web-Crawler ist {\em Heritrix}, entwickelt vom amerikanischen Internet Archive \cite{heritrix}.
Er ist in Java implementiert und quelloffen und verwendet einen XML-Dialekt f\"ur die Konfiguration.

Ein wichtiger Schritt, um das n\"otige Vorwissen zu reduzieren, die Konfiguration von Web-Crawlern zu vereinfachen, und das Crawlen selbst intelligenter zu machen, ist Verfahren zu finden, welche vor oder w\"ahrend der Laufzeit automatisch ermitteln k\"onnen, welchen Hyperlinks gefolgt werden muss.
Dies ist insbesondere problematisch bei gr\"o{\ss}eren Einrichtungen wie Hochschulen.
Denn oftmals werden solche Organisationseinheiten von vielen dezentralen Domains und Webservern repr\"asentiert.
Dem Anwender des Web-Crawlers sind diese nicht unbedingt bekannt und m\"ussten aufendig manuell erfasst werden, was zeitintensiv und fehleranf\"allig ist.

Wir zeigen Verfahren, mit denen es m\"oglich ist, Domains, die eine Organisation vertreten, automatisch zu bestimmen und damit zur Laufzeit des Web-Crawlers festzustellen, welchen Links gefolgt werden muss.
Dies reduziert den Konfigurationsaufwand und manuelles Auditing erheblich.

\section{Problembeschreibung}

Das Problem l\"asst sich in mehrere Teilprobleme gliedern.
\Cref{fig:paths} stellt dies graphisch dar.

\begin{figure}[htb]
	\begin{center}
		\includegraphics[width=.9\linewidth]{./fig/paths}
		\caption{\label{fig:paths}Webserver einer Organisation.}
	\end{center}
\end{figure}

Unter der Annahme, man kennt mindestens eine Domain der Organisation, die auf dem Haupt Server gehostet wird (im folgenden {\bf Root}) ergeben sich folgende M\"oglichkeiten.

\begin{enumerate}
	\item Interne Webserver mit direktem Pfad von Root.
	Die Webserver befinden sich im Netz der Organisation.
	Domains auf diesen Servern sind entweder direkt von Root verlinkt, oder es gibt einen Pfad \"uber interne Server die zu Root f\"uhren. 
	\item Externe Webserver mit Pfad von Root.  
	Diese Webserver befinden sich nicht im Addressbereich der Organisation.
	Es gibt mindestens einen Pfad von Root, entweder direkt, oder \"uber interne oder externe Server.
	\item Interne Webserver mit indirektem Pfad von Root.  
	Die Webserver befinden sich im Netz der Organisation.
	Domains sind aber nicht direkt \"uber interne Pfade verlinkt, sondern nur durch externe erreichbar.
	\item Interne Webserver ohne Pfad von Root.  
	Die Webserver befinden sich im Netz der Organisation. Es gibt keinen Pfad zu Root.
	\item Externe Webserver ohne Pfad von Root.  
	Diese Webserver befinden sich nicht im Addressbereich der Organisation und es gibt keinen Pfad, der zu Root verfolgt werden kann.
\end{enumerate}

Es handelt sich also prinzipiell um einen gerichteten Graphen, deren Knoten die Organisation repr\"asentieren.
Es k\"onnen isolierte Knoten und Teilgraphen auftreten (4. und 5.).
Die Teilprobleme h\"angen teilweise logisch voneinander ab.
Die zentrale Frage, die gel\"ost werden muss, lautet daher wie folgt.

\emph{Wie kann die Zugeh\"origkeit von Domains zur Organisationseinheit bestimmt werden?}

\subsection{Webserver und Domains}

Die Kenntnis von Webservern (im Sinne von Hosts, identifiziert mittels IP-Adres\-sen) sagt nichts \"uber gehostete Domains aus, da Webserver und Domains keine bijektive Relation aufweisen.
Es kann daher nicht beliebig von einem auf das andere geschlossen werden.
Ein Host kann zum einen mehrere IP-Adressen haben, und zum anderen kann der Webserver mehrere Domains hosten.

Es ist m\"oglich mittels DNS lookup von einer Domain auf die IP-Adresse zu schliessen.
Aber es ist nicht m\"oglich mittels reverse DNS lookup die gehosteten Domains einer IP-Adresse zu erhalten.
Man erh\"alt lediglich den ``CNAME'' (canonical name) des hosts.
Ihm k\"onnen aber auch andere Domains zugeordnet sein, bsp durch `virtual hosts` oder `redirects`.
\Cref{lst:nslookup} und \Cref{fig:ServerVsDomain} verdeutlichen dies.

\begin{lstlisting}[label=lst:nslookup,caption=Lookup von Webservern und Domains.]
$ dig www.htwg-konstanz.de
www.htwg-konstanz.de.	29	IN	CNAME	cms.htwg-konstanz.de.
cms.htwg-konstanz.de.	60	IN	A	141.37.11.233

$ dig -x 141.37.11.233
233.11.37.141.in-addr.arpa. 30332 IN	PTR	cms.htwg-konstanz.de.
\end{lstlisting}

\begin{figure}[htb]
	\begin{center}
		\includegraphics[width=.5\linewidth]{./fig/ServerVsDomain}
		\caption{\label{fig:ServerVsDomain}Verh\"altnis zwischen Hostname, IP-Adressen und Domains.}
	\end{center}
\end{figure}

Es ist auch nicht garantiert, da{\ss} mittels IP-Adresse auf eine gehostete Domain zugegriffen werden kann.
Je nach Konfiguration liefert der Webserver bei einem \verb+GET+ request ohne gesetztem Host Feld keine Seite aus.
Dies ist dargestellt in \Cref{fig:IPvsDomain}.
Beim Crawlen erh\"alt man in der Regel Hyperlinks auf Domains. 
Daher ist es nicht m\"oglich nur mit Kenntnis der IP-Adressen Domains zu Crawlen.
Der Zusammenhang zwischen IP-Addresse und Links muss zur Zeit des Crawlens feststellbar sein. 
Kenntnis \"uber Webserver reicht demnach alleine noch nicht aus, um alle Domains zu Crawlen.

\begin{figure}[htb]
	\begin{center}
		\includegraphics[width=.5\linewidth]{./fig/IPvsDomain}
		\caption{\label{fig:IPvsDomain}GET request auf Domains und auf IP-Adressen.}
	\end{center}
\end{figure}

\section{L\"osungsans\"atze}

Um die Zugeh\"origkeit von Domains zu einer Organisation festzustellen gibt es 3 Indikatoren.

\begin{itemize}
	\item Die IP-Adresse der Domain liegt im Addressbereich der Organisation.
	\item Die Nameserver der Organisation sind authoritativ f\"ur die Domain.
	\item Die Organisation ist Registrant der Domain.
\end{itemize}

Nicht alle drei Indikatoren m\"ussen erf\"ullt sein.
Es ist z.B. m\"oglich, da{\ss} eine Domain auf einem externen Server gehostet wird, aber der Nameserver der Organisation authoritativ f\"ur diese Domain ist.
Auch kann eine Domain extern gehostet sein und einen externen Nameserver verwenden, obwohl sie auf die Organisation registriert ist.

\subsection{Interne Webserver mit Pfad von Root}\label{sec:fall1}

Das identifizieren interner Webserver die einen Link-Pfad zu der Root Domain haben, ist theoretisch vergleichsweise einfach.
Zuerst muss der Addressbereich ({\em NetRange}) der Organisation festgestellt werden.
Dieser wird dem Web-Crawler \"ubergeben.
St\"osst dieser auf einen Link, wird die IP-Adresse des Links aufgel\"ost.
Nun muss gepr\"uft werden, ob diese IP-Adresse im vorher festgestellten NetRange der Organisation liegt.
Ist dies der Fall, wird dem Link gefolgt.
Da dieses Verfahren relativ einfach bei Pr\"ufung aller eingesammelten Links angewendet werden kann, deckt es somit Problem 1. und 3. ab.
Sofern die Adresse eines Link innerhalb des NetRange der Organisation liegt, wird dem Link gefolgt, unabh\"angig davon, ob er von einem internen oder externen Server kommt.

Dieses Verfahren setzt vorraus, dass der Web-Crawler einen Vergleich von aufgel\"osten IP-Adressen mit Adressbereichen bzw Wildcards auf Adressen unterst\"utzt.
Zumindest bei Heritrix ist dies nicht der Fall.
Allerdings erlaubt Heritrix die Konfiguration sog. \verb+deciderules+, also Entscheidungsregeln zum Folgen von Links.
Eine Erweiterung dieser \verb+deciderules+ ist \verb+IpAddressSetDecideRule+ mit welcher IP-Adressen definiert werden k\"onnen.
Findet Heritrix einen Link, der auf eine definierte IP-Adresse zeigt, so wird diesem Link gefolgt.

Dadurch wird es m\"oglich einfach den NetRange der Organisation zu explodieren, und eine Liste aller m\"oglichen IP-Adressen zu \"ubergeben.
Obwohl einfach umsetzbar, ist dies zumindest speicherineffizient.
Auch m\"ochte man unter Umst\"anden nur den IP-Adressen folgen, auf denen tats\"achlich Webserver laufen, oder nur denen, die \"offentlich erreichbar sind.
In diesen F\"allen sollte man vorher die IP-Addressen der Webserver ermitteln.
Diese Liste von IPs dient dann als Grundlage f\"ur den Web-Crawler.

Das Verfahren l\"a{\ss}t sich folgenderma{\ss}en zusammenfassen.
\begin{enumerate}
	\item NetRange festellen
	\item NetRange nach Webservern scannen oder zu IP-Liste umwandeln
	\item IP-Adresse von Link aufl\"osen
	\item IP-Adresse mit Liste von IP-Adressen vergleichen
	\begin{enumerate}
		\item Ist IP-Adresse in der Liste vorhanden, dem Link folgen
	\end{enumerate}
	\item Sind weitere Links vorhanden, gehe zu 3
\end{enumerate}

\subsection{Externe Webserver mit Pfad von Root}

Die Zugeh\"origkeit zur Organisation einer Domain auf einem externen Webserver kann nicht \"uber NetRange oder IP-Adressen erfolgen. Daher muss bei einem Link auf eine externe gehostete Domain diese Zugeh\"origkeit anders gepr\"uft werden.
Dies kann mit Hilfe des authoritativen Nameservers und des Registranten erfolgen.

Man stellt zuerst den Registrant und die authoritativen Nameserver der Root Domain fest.
Diese Informationen dienen als Vergleichsbasis f\"ur den Web-Crawler.
Beim Crawlen wird dann f\"ur jeden Link, der nicht auf den NetRange der Organisation zeigt, der authoritative Nameserver und der Registrant festgestellt.
Diese werden mit den vorher festgestellten Referenzwerten verglichen.
Ergibt der Vergleich eine \"Ahnlichkeit, die \"uber einem definierten Ma{\ss} liegt, so wird dem Link gefolgt.

Der Algorithmus sieht wie folgt aus.
\begin{enumerate}
	\item Nameserver und Registrant von Root feststellen
	\item Nameserver und Registrant des erhaltenen Link feststellen.
	\item Nameserver des Links mit den Nameservern von Root vergleichen
	\begin{enumerate}
		\item Gibt es identische Nameserver, folge Link und gehe zu 5
	\end{enumerate}
	\item \"Ahnlichkeit der Registranten berechnen.
	\begin{enumerate}
		\item Ist \"Ahnlichkeit \"uber einem definierten Ma{\ss}, folge Link
	\end{enumerate}
	\item Sind weitere Links vorhanden gehe zu 2
\end{enumerate}

Heritrix unterst\"utzt leider keine Konfiguration und Entscheidungsregeln f\"ur Nameserver oder Registrant. Daher erfordert dieses Verfahren direkte \"Anderungen im Web-Crawler.

\subsection{Webserver ohne Pfad von Root}

Bei Fall 4 und 5 handelt es sich um Webserver bzw Domains, die keinen Link-Pfad von Root aufweisen. Solche Domains k\"onnen beim Crawlen nicht gefunden werden.
Es ist also notwendig, diese Domains vor dem eigentlichen Crawlen zu identifizieren, und dem Crawler als zus\"atzliche Domains mitzugeben.

Bei extern gehosteten Domains gibt es keine praktisch anwendbare M\"oglich\-keit, diese automatisiert zu identifizeren.
Wenn man weder Domains, noch Webserver kennt und ebenfalls keine Links darauf findet, so ist es nicht m\"oglich, eine extern gehostete Domain automatisch zu finden.
Das bedeutet, dass Fall 5 tats\"achlich ohne Expertenwissen nicht l\"osbar ist.

F\"ur Fall 4, also intern gehostete Domains, muss man die Konfigurationen aller internen Websever auswerten.
Wie in \Cref{sec:fall1} beschrieben, sagt weder die IP-Adresse noch der Hostname bzw. CNAME eines Webservers etwas \"uber die dort gehosteten Domains aus.
Dadurch ist leider auch ein DNS Zone Transfer keine L\"osung, zum einen, weil solche Zone Transfers nicht beliebig durchf\"uhrbar sind, und zum anderen, weil sie nur eine Liste von Hostnamen liefern.
Man muss zumindest die internen Webserver kennen, und administrativen Zugriff darauf haben.
Die Konfiguration der Webserver enth\"alt die dort gehosteten Domains, oft \verb+virtual host+ bezeichnet.
Ein automatisches Auswerten der Konfigurationen ist abh\"angig von der jeweiligen Webserver-Implementierung. 

\subsection{\"Ubersicht}

\Cref{fig:overview} zeigt eine \"Ubersicht des gesamten Verfahrens.
Es gibt zwei Phasen.
Die erste ist ``Information Retrieval'' in der die notwendigen Informationen mittels der bekannten Root Domain gesammelt werden.
Diese Informationen gehen dann in die Konfiguration des Web-Crawlers ein.
Die zweite Phase ist das eigentliche Crawlen. Hierbei werden auf jeder navigierten Seite alle Links eingesammlt und f\"ur jeden Link berechnet, ob diese der Organisation zugewiesen werden k\"onnen.
Dazu wird f\"ur den zu pr\"ufenden Link die IP-Adresse sowie die Nameserver und der Registrant ermittelt und mit den vorher knofigurierten Werten verglichen.
Ergibt sich eine befriedigende \"Ahnlichkeit, so wird dem Link gefolgt.

\begin{figure}[htb]
	\begin{center}
		\includegraphics[width=.9\linewidth]{./fig/overview}
		\caption{\label{fig:overview}Flussdiagramm zur automatischen Identifikation relevanter Domains.}
	\end{center}
\end{figure}

Ob einem Link gefolgt wird oder nicht, wird \"uber Entscheidungsregeln ent\-schie\-den.
Der Vergleich von IP-Adressen wird bei Heritrix bereits unterst\"utzt und erlaubt eine bin\"are Entscheidung.
Ebenfalls bin\"ar ist der Vergleich der Nameserver, allerdings gibt es hief\"ur keine Unterst\"utzung in Heritrix, was eine Anpassung des Codes erfordert.
Dies ist auch zutreffend beim Vergleichen der Registranten. Allerdings ist hier die \"Ahnlichkeit nicht unbedingt so eindeutig.
So k\"onnten die registrierten Adressen voneinander abweichen, bspw. wenn die Domain auf eine Au{\em ss}enstelle oder andere eigenst\"andige Einheit der Organisation registriert ist.
Auch kann es Tippfehler bei der Registrierung oder Zeichensubstitutionen (bspw. \"a zu ae) geben.
Der Registrant kann prizipiell mittels der Felder \verb+Name+, \verb+Adresse+, \verb+eMail+ identifiziert werden.
Am aussagekr\"aftigsten ist hierbei die Domain der eMail-Adresse.
Stimmt diese mit der Domain des Registranten bzw mit der eMail-Domain des Registranten \"uberein, so ist die Wahrscheinlichkeit gro{\ss}, dass es sich um den gleichen Registrant handelt.
Dadurch wird auch hier ein einfacher bin\"arer Vergleich m\"oglich.
Sollten die Felder \verb+Name+ und \verb+Adresse+ ebenfalls evaluiert werden, so sollte man auf einen exakten Vergleich verzichten und stattdessen Text\"ahnlichkeiten berechnen. Verfahren wie die {\em Levenshtein-Distanz} \cite{levenshtein1966bcc} bieten sich an.

\section{Methoden}

\subsection{Ermittlung des NetRange einer Organisation}

Man benötigt mindestens eine Domain oder eine IP-Adresse innerhalb der Domain.
Der NetRange kann mittels einer \verb+whois+-Anfrage ermittelt werden.
Stellt man diese Anfrage \"uber die Domain, so erh\"alt man administrative Informationen.
Diese kann man aber verwenden, um eine IP-Adresse zu extrahieren, und mit einer weiteren \verb+whois+-Anfrage den NetRange zu erhalten.
Hierbei ist wichtig, entweder RIPE oder ARIN als Datenbank zu verwenden, da nur diese auch Netzwerknummern beinhalten.
\Cref{lst:netrange} zeigt ein Beispiel mit der Domain der Universit\"at Konstanz.

\begin{lstlisting}[label=lst:netrange,caption=Ermittlung des NetRange einer Organisation.]
$ whois uni-konstanz.de | egrep '\<[0-9]+\.[0-9]+\.' | cut -d " " -f 3
134.34.3.3
134.34.3.2

$ whois -h whois.ripe.net 134.34.3.3 | egrep 'route'
route:          134.34.0.0/16

$ whois -h whois.arin.net 134.34.3.3 | egrep 'NetRange'
NetRange:       134.34.0.0 - 134.34.255.255
\end{lstlisting}

Die Antworten der beiden Anfragen unterscheiden sich leicht.
Dies ist bei whois generell ein Problem. Je nach angefragter Domain und zugeh\"origer Domain-Datenbank bekommt man Antworten in unterschiedlichen Formaten.
Leider gibt es keinen Standard, der whois Antworten definiert.
Daher muss man f\"ur jede Top Level Domain (TLD) die erfragt werden soll einen eigenen whois Parser haben, um die Antworten zu interpretieren.

\subsection{Ermittlung von Nameservern einer Organisation}

Auch die Nameserver einer Organisation lassen sich mittels \verb+whois+ einfach erfragen.
Tats\"achlich ist die Anfrage dieselbe, wie zum ermitteln der IP-Adresse f\"ur die Ermittlung des NetRange (\Cref{lst:named}).

\begin{lstlisting}[label=lst:named,caption=Ermittlung der Nameserver einer Organisation.]
$ whois uni-konstanz.de | egrep '\<[0-9]+\.[0-9]+\.' | cut -d " " -f 2-3
pan.rz.uni-konstanz.de 134.34.3.3
uranos.rz.uni-konstanz.de 134.34.3.2
\end{lstlisting}

Auch hier gilt, dass jede TLD einen eigenen Parser f\"ur die whois Antworten erfordert.

\subsection{Ermittlung des Registranten einer Domain}

Den Registrant einer Domain ermittelt man ebenfalls mit Hilfe von whois. Die in \Cref{lst:registrar} gezeigte Anfrage funktioniert mit vielen h\"aufig verwendeten TLDs, allerdings unterscheiden sich die zur\"uckgegebenen Felder oft deutlich im Format.

\begin{lstlisting}[label=lst:registrar,caption=Ermittlung des Registranten einer Organisation.]
$ whois uni-konstanz.de | egrep 'Address|Email|Registrant Organization|Registrant Street'
Address: Universitaet Konstanz
Address: Rechenzentrum
Address: Postfach 5560
Email: joerg.vreemann@uni-konstanz.de
Address: Universitaet Konstanz
Address: Rechenzentrum
Address: Postfach 5560
Email: joerg.vreemann@uni-konstanz.de
\end{lstlisting}

Auff\"allig in der Antwort ist die wiederholte angabe der Felder. Der Grund ist, dass whois sowohl den technischen als auch den administrativen Ansprechpartner zur\"uck liefert, der in diesem Beispiel identisch ist.

Das gleiche Verfahren ist auch anwendbar bei der Ermittlung des Registranten einer beliebigen Domain eines zu pr\"ufenden Links.
Starke Indikatoren für den Zusammenhang mit der Organisation sind vor allem Felder wie \verb+Address+ und besonders \verb+Email+, welches auch ohnehin von den mei{\ss}ten whois Datenbanken ausgeliefert wird.

Die Domain der eMail-Adresse l\"a{\ss}t sich mittels regul\"arer Ausdr\"ucke bewerkstelligen. \Cref{lst:email} zeigt wie aus der whois Antwort die Domains der eMail Adressen extrahiert werden k\"onnen.

\begin{lstlisting}[label=lst:email,caption=Ermittlung des eMail-Domains des Registranten einer Organisation.]
$ whois uni-konstanz.de | egrep 'Email' | grep -Eio '\b@[A-Z0-9.-]+\.[A-Z]{2,4}\b' | sed 's/@//g' | tr '\n' ' '
uni-konstanz.de uni-konstanz.de
\end{lstlisting}

\subsection{Ermittlung von Webservern einer Organisation}

Webserver k\"onnen mittels eines Netzwerk Scans ermittelt werden. Hierbei wird einfach nach Hosts gescannt, die Port 80 oder 443 ge\"offnet haben. 
\Cref{lst:nmap} zeigt, wie das gesamte Netz der Uni Konstanz dementsprechend gescannt, und automatisch eine Liste in einem Heritrix-kompatiblen Format ausgegeben werden kann.

\begin{lstlisting}[label=lst:nmap,caption=Ermittlung von Webservern einer Organisation.]
$ nmap -n -p80,443 -Pn -oG -  134.34.0.0/16 | awk '/open/{print $2}' | sed -e 's/^/<value>/' | sed -e 's/$/<value\/>/'
\end{lstlisting}

Eine Schw\"ache dieses Vorgehens ist, dass nur Webserver gefunden werden, die auf den Ports 80 bzw. 443 laufen.
Auch ist es ein erheblicher Unterschied, von wo aus der Scan erfolgt.
Befindet sich der Scanner au{\ss}erhalb der Organisationsnetzes, so werden nur \"offentlich erreichbare Webserver gefunden, die nicht von einer Firewall blockiert werden.
Innerhalb des Organisationsnetzes findet man alle von diesem Subnetz aus erreichbaren Webserver, inklusive solcher, die auf Druckern, Routern, Arbeitsplatzrechnern oder anderen Ger\"aten laufen.
Man erh\"alt also ein superset der Webserver, die eigentlich von interesse sind.
All diese Eigenschaften stellen allerdings keine Beschr\"ankung der Anwendbarkeit dar.
Geht man davon aus, dass die Organisation {\em Ver\"offentlichungen} archivieren m\"ochte, so kann man durchaus annehmen, dass es sich auch \"offentliches Material handelt.
In diesem Falle muss man davon ausgehen, dass die Webserver erstens \"offentlich erreichbar sind und zweitens auch auf den bekannten Standard-Ports 80 und 443 laufen.

\section{Ergebnis}

Wir haben Verfahren gezeigt, die es erlauben, beim Crawlen von Webseiten zum Zwecke der Web Archivierung relevante Links auf Domains einer Organisation automatisch zu identifizieren.
Dies reduziert die Notwendigkeit von Expertenwissen, und manuellen Konfigurationsaufwand erheblich.
Die gezeigten Verfahren sind einfach zu implementieren und erlauben indivduelle und granulare Einstellm\"oglichkeiten f\"ur Anwender.
