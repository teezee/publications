%!TEX root = ../report.tex
\section{Introduction}

At the time of this writing, the most commonly used user authentication scheme for login to digital services of all kinds is still a combination of username and password.
If an attacker guesses or steals the user's password he can effectively steal the user's digital identity, access the user's personal information, and perform actions in the user's name.
The password is often the weakest link\cite{Florencio:2007:LSW:1242572.1242661}.

For many applications this level of security might be enough.
But sensitive information or the ability to perform critical actions require additional layers of security.
Multi-factor authentication (MFA) is a methodology that introduces additional, independent authentication factors that all need to be validated when authenticating a user.
In some application domains, like e-banking, multi-factor authentication is already an established mechanism.
Many well-known service providers have also adopted 2FA (two-factor authentication, often called `2-step verification'), including Apple, Google, Microsoft, and even Steam.

Authentication factors differ in nature and include (see \autoref{img:auth-fact}):
%\remove{(1) something you know (knowledge of a secret, like a password or PIN); (2) something you have (possession of an object, like a smart card or cryptographic token, aka cryptoken); and (3) something you are (physical features, like fingerprint or retinal patterns).}

\begin{itemize}
	\item Something you know (knowledge of a secret, like a password or PIN)
	\item Something you have (possession of an object, like a smart card or cryptographic token, aka cryptoken)
	\item Something you are (physical features, like fingerprint or retinal patterns)
\end{itemize}

\begin{figure}[th]
	\centering
	\includegraphics[width=.7\columnwidth]{img/auth-factors}
	\caption{ \label{img:auth-fact} Types of authentication factors - knowledge, possession, physical features.}
\end{figure}

Independence of the authentication factors is critical to ensure that a compromise of a single factor does not affect the others.
Although the authentication factors are independent, their affiliation to the authenticating identity must be apparent and verified by the service.
All authentication factors must also be verified by the service.
In fact, many authentication schemes, that claim to offer MFA, actually fail to associate all authentication factors with the same single identity, or to check all authentication factors server-side, as discussed in \Cref{sec:state}.
For example, the typical usage of password-protected user certificates or SSH keys does not comprise a multi-factor authentication, since the password is not checked by the server but by the client.

MFA introduces additional complexity in applications as well as identity management\allowbreak\cite{Simon2016-2FA}.
Many standard services like LDAP or AD do not support MFA out-of-the-box.
As a result, additional authentication services, user management, and changes to client applications are usually required.
These requirements can lead to significant costs in time and money.
It introduces additional effort not only in implementation but also in operation.

In this paper, we suggest another unique form of two-factor authentication for web applications that requires little changes in the application and builds on existing identity management infrastructures.
We utilize standard X.509 user certificates (aka S/MIME certificate) as proof for the user's identity and as a first factor for authentication. 
After the user has provided his certificate and thus proven his identity he is prompted for his password.
This scheme can easily be combined with standard hardware solutions like smartcards or USB cryptokens (\Cref{img:evolution}).
It is surprisingly easy to implement, yet affective and user friendly, and can be adapted quickly and flexibly for any type of web application without the need for another online service.

\begin{figure}[t]
	\centering
	\includegraphics[width=.7\columnwidth]{img/Evolution-crop}
	\caption{\label{img:evolution}The different stages of securing a web application with certificate-based second factors. Adding a software certificate provides an additional factor without linking it to the users identity. Application support is required to link users with their certificate. Instead of storing the certificate in software, it can be stored on a hardware token, to guarantee it stays private.}
\end{figure}
