"""Abstract Classes

Implements abtract base classes ABC for python.
An interface can be implemented by using Abstract()
as base class and define abstract methods raising
NotImplementedError on call.
"""

__all__ = ["Abstract","abstract"]

class Abstract ():
    """Abstract Base Class"""
    def __init__ (self):
        """Can not be instantiated"""
        raise NotImplementedError("Abstract Base Class cannot be instantiated")

def abstract ():
    raise NotImplementedError("Abstract method must be overridden")
