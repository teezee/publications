"""Classes related to abstract methods and exceptions

Currently implements:
NotImplementedException         --      already implemented in python 2.5
"""





##########################################
# word wrapper class
# a wrapper class to wrap compressors to word-wise compression
##########################################

class Wordwrap (Compressor):
    """Word Wrapper class

    Takes any compressor an encapsulates it into
    word wise compression.

    Members:
    _wsize      --      word size in bits
    _cmpr       --      the compressor
    _crate      --      compression rate,how many values per word
    """
    def __init__ (self,wsize,cmpr,rate=0,*plain=None):
        """Instantiate a word wrapper

        @wsize       --      word size in bits
        @cmpr        --      any compressor
        @rate        --      compression rate, if 0, it is computed
        """
        if rate == 0:
            if plain == None:
                raise ValueError, "either rate or *plain have to be set"
            rate = self._calcrate(*plain)
        
        self._wsize = wsize
        self._cmpr = cmpr
        self._rate = rate

    def __repr__ (self):
        return 'Wordwrap (%s,%s,%s)' % (self._wsize,self._cmpr,self._crate)

    def _calcrate (self,*plain):
        """_calcrate(*plain) -- calculate self._rate

        Calculate the compression rate.
        @plain      --      uncompressed sequence
        """
        pass


##########################################
# Compressed Counting Bloom Filter class
# Implements a compressed counting Bloom filter
##########################################

class CompressedCountingBloomFilter (Bloom):
    """Implements a compressed counting Bloom filter

    Needs an uncompressed counting Bloom filter and a compressor.
    Calculates a compression rate, that is, number of compressed
    counters that fit into one word. The compressor must support
    range and word size which is evaluated.
    Members:
    Bloom.*         --      inherited from Bloom
    _comp           --      the compressor
    _wsize          --      word size
    _
    """
    #def __init__ (self,
    
