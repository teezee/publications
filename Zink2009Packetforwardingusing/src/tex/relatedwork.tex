\section{State-of-the-art in Packet Forwarding}\label{sec:related}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
This section discusses data structures and algorithms that are widely used in networking applications and establishes the basis for future sections. Specifically we focus on packet forwarding mechanisms, however, many techniques have other uses in networking which are briefly mentioned where appropriate.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Longest Prefix Matching}\label{sec:lpm}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
A lot of work has been done in longest prefix matching algorithms. For an extensive overview see \cite{Ruiz-BD2001Surveya}. This work focuses on optimizing the data structures used for storing prefixes so only a brief introduction to LPM techniques will be given here to show the complexity and challenges.

The most basic data structure is a binary trie. Prefixes are stored in a search trie and nodes that represent a match are marked as terminal nodes. The destination address of an incoming packet is then used to traverse the trie until the longest match has been identified. This solution has linear complexity and thus is rather inefficient. Various improvements have been suggested to reduce complexity which will not be recounted here.

Waldvogel et al. \cite{WaldvVTP1997Scalable} propose a "binary search on prefix lengths". The idea is threefold. First, hashing is used to identify matches of a specific prefix length. Then, binary search on the prefix length levels is performed to divide the potentially large search domain into sub-domains. Finally, to avoid backtracking in case of failure, so called markers are precomputed that can be used to identify matches in higher levels. The prefixes are sorted and grouped in hash tables by length. The binary search begins with the longest prefix length and proceeds backwards. A match in a table indicates, that the longest matching prefix is at least as long as the size of the prefixes in the queried table while a failure indicates that tables for shorter prefixes must be searched. This scheme reduces the complexity to logarithmic which is a significant improvement over previous linear search tries. 

A similar approach is taken by Dharmapurikar et al. \cite{DharmKT2003Longest} who introduce a Bloom filter based approach for LPM. As in the binary search scheme the prefixes are grouped and stored in hash tables by their length. Instead of building a search tree, Bloom filters are used to represent the sets of prefixes of a specific length. A query on all filters results in a matching vector and the associated hash tables are probed to retrieve the longest prefix.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Bloom Filters and Derivates}\label{sec:sum}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Bloom filters have long been popular in database systems and gained a lot of attention in network applications. Only a brief introduction to the most prominent representatives is given here. For a detailed overview of Bloom filters in network applications see \cite{BrodeM2002Networka}.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Bloom Filter.}\label{sec:bf}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
{\em Bloom\ filters}, first introduced by Burton H. Bloom \cite{Bloom1970Space/Timea}, are used to represent set memberships of a set $S$ from a universe $U$. They allow false positives, that is, they can falsely report the membership of an item not in the set, but never return false negatives. Basically, a Bloom filter is a bit array of arbitrary length $m$ where each bit is initially cleared. For each item $x$ inserted into the set $k$ hash values $\{ h_0, \dots, h_{k-1} \}$ are produced while $\forall{h} \in \mathbb{N} : 0 \leq h < m$. The bits at the corresponding positions are then set. When a query for an item $y$ is performed, the $k$ bits $y$ hashes to are checked. If all of them are set, $y$ is reported to be a member of $S$. If at least one of the bits is clear, $y$ is not present in the set. A false positive occurs, if all bits corresponding to an item not in the set are 1. The probability that this happens depends on the number of items $n$ inserted, the array length $m$, and the number of hash functions $k$. It is given as 
%
\begin{equation}\label{eq:fpbloom}
	\epsilon = (1-(1-\frac{1}{m})^{kn})^k.
\end{equation}
%

It can be proven, that for given $n$ and $m$ the optimal number of hash functions is 
%
\begin{equation}\label{eq:kbloom}
	k = \frac{m}{n} \ln{2}.
\end{equation}
%

To minimize the false positive probability $m$ must be chosen appropriately large. To keep it constant $m$ must grow linearly with $n$. Generally
%
\begin{equation}\label{eq:mbloom}
	m = c \cdot n
\end{equation}
%
for a sufficiently large constant $c$.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Counting Bloom Filter.}\label{sec:cbf}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
The problem of standard Bloom filters is that they do not support deletions. Since one cannot know how many items hash to specific locations in the filter the bits cannot be cleared upon removal of an item. Thus, when items are deleted the filter must be completely rebuilt. Fan et al. \cite{FanCAB1998Summary} address this issue by introducing a {\em counting Bloom filter} (CBF). Instead of a bit array, the CBF maintains an array of counters $C = \{\varsigma_0, \dots, \varsigma_{m-1}\}$ to represent the number of items that hash to its cells. Insertions and deletions can now be handled easily by incrementing and decrementing the corresponding counters. The counters are typically three to four bits wide, so the CBF needs about three to four times the space of a standard Bloom filter. Using a small fixed amount of bits to represent the counters introduces the problem of a possible counter overflow. If more items hash to a counter than it can represent an overflow occurs. Thus, the counter-width must be chosen appropriately large for a given application. In general the counter-width is derived from the expected maximum counter value $max(C)$, which is equal to the expected maximum number of collisions per counter and can be easily computed using probabilistic methods. There are multiple approaches for dealing with overflows. One is to simply ignore counters that have reached their maximum value and stop updating them. Though this is a simple solution it leads to inaccuracies in the filter that must somehow be resolved. Another solution is to keep the exact counter value in dedicated memory. With a counter-width of $\upsilon$ bits the amount of bits needed for the CBF is
%
\begin{equation}\label{eq:bitscbf}
	\beta = m \cdot \upsilon .
\end{equation}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Multistage Counting Bloom Filters.}\label{sec:mcbf}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Estan and Varghese \cite{EstanV2003New} use counting Bloom filters for traffic management and large flow identification. When a packet arrives, its flow ID is hashed into multiple independent Bloom filters. If all counters exceed a certain threshold the flow ID is added to the flow memory, dedicated for large flows. The stages can either be organized and accessed in parallel or sequentially, in which case subsequent stages need only be accessed if the flow ID passes the previous stage.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Spectral Bloom Filters.}\label{sec:sbf}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Similar to counting Bloom filters, Cohen and Matias \cite{CohenM2003Spectral} introduce a structure called {\em spectral Bloom filter} (SBF). The SBF also uses counters, but, whereas Bloom filters in general are used to represent a set of unique items, the SBF serves as a histogram of multi-sets keeping track of the frequency of items. Since the counters in this scenario can grow large rapidly, the authors emphasize minimizing space. The counters are compressed using elias encoding and stored consecutively. A complex data structure composed of multiple offset vectors is used to index the compressed counter string.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Bloomier Filters.}\label{sec:bloomier}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
{\em Bloomier filters} \cite{ChazeKRT2004Bloomier} generalize Bloom filters to represent arbitrary functions that map a subset $S = \{s_0,\dots, s_n\}$ of a given domain $D$ to a defined range $R = \{v_0,\dots, v_{|R|-1}\}$. Items can be associated with values which are encoded in the bloomier filter such that $f(s_i) = v_i$ for $s_i \in S$ and $f(x) = 0$ for $x \notin S$. The basic construction is composed of a Bloom filter cascade. For every possible value in $R$ one Bloom filter is used to represent the items mapping to this value. However, due to false positives, multiple filters could return the membership of an item and thus the correct value could not be retrieved. To resolve this problem, filter pairs are introduced that hold the items producing false positives. A query on the pairs can then identify filters that produced false positives.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Compressed Bloom Filters.}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Mitzenmacher \cite{Mitze2001Compressed} proposes arithmetic coding for Bloom filters used as messages. In his scenario the Bloom filters are used in a distributed system to exchange web cache information. Clearly, to prevent network stresses the transmission size must be small. Using arithmetic or delta encoding the Bloom filter can be compressed without sacrificing performance (in terms of false positive rate). Note, that while queries on such a compressed filter are possible, it does not support updates.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Multilayer Compressed Counting Bloom Filters.}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Recently, Ficara et al. \cite{FicarGPV2008MultiLayer} introduced a compression scheme for counting Bloom filters that allows updates. It is known as ML-CCBF (MultiLayer Compressed Counting Bloom Filter) and is based on Huffman coding. They use a simple code where the number of $1$s denote the value of the counter. Each string is terminated by $0$. So the number of bits needed to encode a counter value $\varphi$ is $\varphi+1$. Since, with an optimal Bloom filter configuration the probability of increasing counters falls exponentially this poses an optimal encoding. Increasing or decreasing a counter is also simple by just adding or removing $1$. To avoid indexing and memory alignment issues the counters are not stored consecutively but each one is distributed over multiple layers of bitmaps $L_0,\dots,L_N$, with $N$ dynamically changing on demand. Thus $L_i$ holds the $i$th bit of the code-string. The first layer is a standard bloom filter representing items with $\varphi \geq 1$. To index the bitmaps $k+N$ hash functions are needed. The $k$ hash functions are random hash functions used for the Bloom Filter. The other $N$ hash functions index the bitmaps $L_1,\dots,L_N$ and must be perfect to prevent collisions. To retrieve a counter value its position $u_0$ in $L_0$ is first determined. If the bit at $u_0$ is $0$ then $\varphi=0$. Else $L_1$ must be examined. Let $popcount(u_i)$ be the number of ones in bitmap $i$ before index $u$. $popcount(u_i)$ is then hashed using the perfect hash function $H_{k+i}$ to find the index in $L_{i+1}$. If this bit is set, $1$ is added to the current counter value and the next bitmap must be examined. Otherwise, the end of the code is reached. Note, that $N$ must be as large as the maximum counter value + $1$. With increasing counter values new layers of bitmaps can simply be added. This scheme provides near optimal counter storage. However, while it is easy to check the membership of an item by probing the Bloom filter $L_0$ retrieving all counters for an item is very expensive due to the need of computing $popcount(u_i)$ for all $k$ counters. The authors propose an index structure to lower the cost of a counter lookup. All bitmaps are split into $D$ equal sized blocks. An index table is kept in extra memory that holds the number of '1' at the start of each block. With a bitmap size of $m_i$ split into $D$ blocks, $log\ \frac{m_i}{D}$ bits are needed to index the table. Thus, only the number of '1' from start of the block to $u_i$ need to be counted.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Hash Tables}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Hashing with multiple Choices.}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
The na\"ive hash table can be seen as an array of linked lists. Each item to be inserted is hashed to find a bucket in the hash table and is appended to the list of items in this bucket. In this scheme, the load of the buckets can grow quite high. Azar et al. observed \cite{AzarBKU1994Balanced}, that by allowing more possible destinations and choosing that with lowest load, the upper bound can be reduced exponentially. This effect became popular as 'the power of two choices', a term coined by Mitzenmacher in \cite{Mitze1996Power}. Further improvement achieved V\"ocking \cite{Vo1999How} with the 'always-go-left' algorithm. Here, the items are distributed asymmetrically among the buckets. Broder and Mitzenmacher \cite{BrodeM2001Using} suggest using multiple hash functions to improve the performance of hash tables. The $n$ buckets of the table are split into $d$ equal parts imagined to run from left to right. An item is hashed $d$ times to find the $d$ possible locations. It is then placed in the least loaded bucket, ties are broken by going left. A lookup now requires examining the $d$ locations. However, since the $d$ choices are independent, the lookup can be done in parallel or pipelined.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{d-left Counting Bloom Filters.}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
A major problem with CBFs is the massive amount of space needed compared to a basic Bloom filter. Bonomi et al. \cite{BonomMPSV2006Improved} present an improved version of CBF based on d-left hashing. They make use of the fact that with d-left hashing one can achieve an "almost perfect hashing"\cite{BrodeM2001Using}. The idea is as follows. The $m$ buckets are split into $d$ equal sized sub-tables imagined to run from left to right, leading to $d$ choices to store an item. Each bucket consists of $c$ cells holding a fingerprint of an item and a small counter. The fingerprint is twofold, the first part is the bucket index and the next part is called the remainder $r$. Only the remainder is stored. One hash function $H: U\rightarrow [B] \times [R]$, where [B] ([R]) is the range of buckets (remainder), produces the fingerprint. Then $d$ random permutations are used to identify the buckets in the sub-tables. Note, that the remainder is different depending on the sub-table. Upon insertion, the $d$ buckets are first checked whether they already hold the remainder $r_i$. If so, the cell counter for $r_i$ is incremented. If not, the item is stored in the most left sub-table with the smallest load. Ties are broken by going left. Retrieving items requires $d$ parallel lookups. A false positive can occur iff $H(x) = H(y)$ since this leads to the same permutations.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Fast Hash Table.}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
A drawback of hashing with $d$ choices is that it requires at least $d$ lookups in the hash table. Though these are independent and can be done in parallel, it is inefficient to do so. In \cite{SongDTL2005Fast} Song et al. present a data structure named {\em fast hash table} (FHT) that eliminates the need for parallel lookups through the use of a counting Bloom filter summary. In their scheme only one bucket access is needed. Each counter corresponds to a bucket in the hash table and represents the number of items hashed into it. Thus, the $k$ hash functions which are derived by equation \ref{eq:kbloom} are used to index both the CBF and the hash table. Song et al. use a class of universal hash functions \cite{CarteW1977Universal} to construct the Bloom filter and the hash table. These only work with multiples of 2. They use the following equation to compute the number of buckets.
%
\begin{equation}\label{eq:mfht}
	m = 2^{\lceil \log{c\ n}\rceil}
\end{equation}
%
Where $c = 12.8$. When searching for an item $x$ it is hashed to find its $k$ counters. The minimum $z$ of these counters is computed. If $z == 0$ the item is not present in the hash table, else it is retrieved from the far left bucket corresponding to $z$. Note, that while there is only one access to a bucket, it may be necessary to follow next pointers to traverse the list of items in one bucket. Insertion and deletion of items depend on the type of FHT.
%
\paragraph{{\bf Basic Fast Hash Table.}}\label{BFHT}
%
In the basic FHT (BFHT) items are simply inserted $k$ times, once in every location it hashes to. The corresponding counters are incremented. Due to collisions it is possible that an item is inserted less than $k$ times. In this case the counter experiencing the collision is incremented only once. Deletions are equally simple. The item is removed from the buckets and the counters are decremented. Lookup is done by hashing the item $k$ times and computing the minimum counter value $z$. If $z \neq 0$, the item is retrieved from the far left bucket corresponding to $z$, limiting the lookup time to $z$. This scheme leads to high bucket loads, thus, retrieval of an item is most certainly accompanied by following multiple pointers. Figure \ref{fig:bfht} shows an example BFHT.
%
\begin{figure}[h!]
	\centering
	\label{fig:bfht}
	\includegraphics[scale=0.5]{\gfx bfht}
	\caption{\footnotesize{Basic fast hash table}}
\end{figure}
%
%%%%%%%%%%%%%%%
\paragraph{{\bf Pruned Fast Hash Table.}}\label{PFHT}
%%%%%%%%%%%%%%%
The pruned FHT (PFHT) is an improvement on the BFHT. Items are only stored at the far left bucket with minimum counter value. Counters and lookups are handled as in the BFHT. This improves bucket load and lookup time. The authors show that given a well designed table the buckets will hold only one item with high probability. However, not storing every item in all corresponding buckets complicates updates since they influence the counters of already present items. Minimum counters of items inserted earlier might get changed during update leading to a lookup in the wrong bucket. For insertions the items in affected buckets must be considered for relocation. Deletions require even more effort. Decrementing a counter may result in this counter being the smallest one for items hashing to it. But since a bucket does not store all its items, it is not possible to identify items that have to be relocated. This can either be achieved by examining the whole PFHT and check every item (obviously this is very expensive), or by keeping an offline BFHT and examining affected buckets offline. Thus, the PFHT is only suitable for applications where updates are much rarer than queries. Figure \ref{fig:pfht} illustrates the pruned version of the BFHT depicted in figure \ref{fig:bfht}.
%
\begin{figure}[h!]
	\centering
	\label{fig:pfht}
	\includegraphics[scale=0.5]{\gfx pfht}
	\caption{\footnotesize{Pruned fast hash table}}
\end{figure}
%
%%%%%%%%%%%%%%%
\paragraph{{\bf Shared-node Fast Hash Table.}}\label{SFHT}
%%%%%%%%%%%%%%%
The shared-node FHT (SFHT) provides support for update critical applications at the cost of slightly higher memory consumption. Here the buckets only store a pointer to the first item that has been inserted. The items are stored in extra memory and carry a pointer to the next item in the list. Special care must be taken when an item is inserted that hashes to empty and non-empty buckets. Appending this item to the linked lists would lead to inconsistencies. It must be replicated and pointers set in the empty buckets and the linked lists accordingly. Again counters and lookup are treated as in the BFHT. Though updates are much easier compared to the PFHT, lookup now requires following at least one pointer.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Multilevel Hash Table.}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
The Fast Hash Table uses one counter per bucket to keep track of the number of items stored. While this is a straightforward approach that is easy to implement, it has rather high memory requirements for the counting Bloom filter summary. Kirsch and Mitzenmacher \cite{KirscM2008Simple} observe, that the summary structure need not correspond to a bucket in the underlying data structure. This allows separation of the hash table and its summary and independent optimization. They use a multilevel hash table (MHT), first introduced by Broder and Karlin \cite{BrodeK1990Multilevel}, to store the items. The MHT consists of 
%
\begin{equation}\label{eq:dmht}
	d = \log \log n + 1
\end{equation}
%
sub-tables where each sub-table $T_i$ has $c_1c_2^{i-1}n$ single item buckets with $c_1 > 1$ and $c_2 < 1$. Thus $|T_i|$ is decreasing geometrically for increasing $i$. An occupancy bitmap is kept in on-chip memory with one bit per available bucket that allows efficient queries for empty buckets. The total number of bits needed for the occupancy bitmap is equal to the number of buckets which can be derived by
%
\begin{equation}\label{eq:mmht}
	\beta = m = \sum_{i=1}^{d} (c1 \cdot c2^{i-1} \cdot n).
\end{equation}
%
When an item is inserted it is hashed $d$ times to find one possible bucket in each sub-table. The item is put in $T_i$ with the lowest $i$ for which the bucket is empty. Figure \ref{fig:mht} shows an example with four sub-tables. 
%
\begin{figure}[h!]
	\centering
	\label{fig:mht}
	\includegraphics[scale=0.5]{\gfx mht}
	\caption{\footnotesize{Multilevel hash table}}
\end{figure}
%
A so called crisis can occur when all $d$ buckets are occupied. However, it can be proven that for any $c_1c_2 > 1$ the crisis probability is insignificantly small. Kirsch and Mitzenmacher present three summary data structures which will now be reviewed.
%%%%%%%%%%%%%
\paragraph{{\bf Interpolation Search Summary.}}
%%%%%%%%%%%%%
All items inserted are hashed to a $b$-bit string, where $b$ must be uniformly distributed and sufficiently large. The index $i$ of $T_i$, where the item is placed, is stored along with its string $b$. Figure \ref{fig:is} illustrates the construction. 
%
\begin{figure}[h!]
	\centering
	\label{fig:is}
	\includegraphics[scale=0.5]{\gfx is}
	\caption{\footnotesize{Interpolation search summary}}
\end{figure}
%
Interpolation search is used to search for an item which requires the array of strings to be ordered. Insertions and deletions requires shifting subsequent strings to keep the ordering. A failure can occur if two inserted items hash to the same $b$-bit string. The failure probability is 
%
\begin{equation}\label{eq:pfailis}
	p_{fail}(n,b) = 1 - \prod_{k=0}^{n+1} \frac{2^b - k + 1}{2^b}.
\end{equation}
%
A false positive occurs when a not inserted item hashes to a string present in the summary. Supposed no failure occurred the false positive probability is 
%
\begin{equation}\label{eq:fpis}
	p_{fp}(n,b) = \frac{n}{2^b}.
\end{equation}
%
Thus, by choosing $b$ appropriately large for given $n$, both the failure and false positive probability can be optimized. The authors suggest $b = 61$ for $n = 100.000$. Note, that $b$ must grow with larger $n$ to keep the probabilities constant. $log\ d$ bits are additionally needed to represent $i$. With $d = 8$ the total number of bits per item needed for the summary is $64$ and is derived by 
%
\begin{equation}\label{eq:bitsis}
	\beta = n\ (b + \log d).
\end{equation}
%
%%%%%%%%%%%%%%%
\paragraph{{\bf Single Bloomier Filter.}}
%%%%%%%%%%%%%%%
The Single Bloomier Filter summary (SF) has 
%
\begin{equation}\label{eq:msf}
	m = n\ \log n
\end{equation}
%
cells initialized to $0$ and represents the type $t$ of an item where $t$ is the sub-table the item is stored in.
%
\begin{equation}\label{eq:ksf}
	k = \log n
\end{equation}
%
hash functions are used to access the Bloomier Filter. To insert an item, first its type is identified by inserting it into the MHT. Then it is hashed $k$ times and the corresponding cell values are replaced with the maximum of their value and the type of the item. Insertion is depicted in Figure \ref{fig:sf}.
%
\begin{figure}[h!]
	\centering
	\label{fig:sf}
	\includegraphics[scale=0.5]{\gfx sf}
	\caption{\footnotesize{Single filter summary}}
\end{figure}
%
To search for an item the $k$ cells are examined and the minimum $z$ is computed. If $z == 0$ the item is not present in the MHT. Otherwise, it has a type of at most $z$. In addition to false positives this structure can also return type failures, iff $z$ yields an incorrect type for an item. With $d = \log \log n + 1$ types the number of bits needed for the single filter summary is
%
\begin{equation}\label{eq:bitssf}
	\beta = n\ \log n\ \log \log \log n
\end{equation}
%%%%%%%%%%%%%%%
\paragraph{{\bf Multiple Bloom Filters.}}
%%%%%%%%%%%%%%%
The single filter approach introduces type failures and care must be taken during construction since false positives and type failures are competitive in respect to the number of hash functions used. The multiple Bloom filter summary (MBF) eliminates this additional effort by making use of the skew of the items in the MHT. Since the number of items in subsequent sub-tables decreases geometrically an array of Bloom filters $B = \{ B_0,...,B_{t-1} \}$ decreasing in size can easily be used to represent the set of items of a specific type. Each filter $B_i$ represents the set of items with type of at least $i+1$. Thus a false positive on $B_i$ is equal to a type $i$ failure. Obviously, the false positive probability must be extremely small for successful lookup. This leads to the need of significantly more hashing. Unfortunately, the authors do not clarify how the number of hash functions needed can be derived, but give examples of seven hash functions for $B_0$ and $49$ for each of the other filters with $n = \{ 10k, 100k \}$. However, the hash functions between Bloom filters do not need to be independent, so the same set of hash functions can be used for each filter. Figure \ref{fig:mbf} illustrates the design.
%
\begin{figure}[h!]
	\centering
	\label{fig:mbf}
	\includegraphics[scale=0.5]{\gfx mbf}
	\caption{\footnotesize{Multiple Bloom filter summary}}
\end{figure}
%
With a well designed MHT the total number of bits for the MBF is 
%
\begin{equation}\label{eq:bitsmbf}
	\beta = n\ \log n
\end{equation}
%%%%%%%%%%%%%%%
\paragraph{{\bf Deletions.}}
%%%%%%%%%%%%%%%
The Bloom Filter based summaries only support inserts. To allow deletions significantly more effort is needed in terms of additional or modified data structures. Two deletion schemes are proposed in \cite{KirscM2008Simple}, the {\em lazy deletions} and {\em counter based deletions}
%%%%%%%%%%%%%%%
\paragraph{{\bf Lazy Deletions.}}
%%%%%%%%%%%%%%%%
A simple approach for adding deletion support is {\em lazy deletions}. Like the occupancy bitmap, a deletion bit array is kept in on-chip memory with one bit for every bucket in the MHT. When an item is deleted the corresponding bit is simply set to 1. During lookup, items in buckets with set deletion bit are simply ignored. Though being simply, it leads to inconsistencies in the MHT, since present and newly inserted items are placed further right, than needed. Thus after a certain threshold the whole MHT must be rebuilt, that is, all items must be examined for relocation.
%%%%%%%%%%%%%%%
\paragraph{{\bf Counter Based Deletions.}}
%%%%%%%%%%%%%%%
As with counting Bloom filters this schemes adds counters to the Bloom filter based summaries to keep track of the number of items inserted. The single filter summary must now contain one counter for each possible type in each of its cells. In the multiple Bloom filter summary the Bloom filters are replaced by counting Bloom filters. Since the number of items decreases throughout the sub-tables the counter-width can also decrease. No evaluation is given by the authors for the modified single filter summary but given $d$ choices and a counter-width $\upsilon$  it would require 
%
\begin{equation}\label{eq:bitssfd}
	\beta_{sfcounter} = \upsilon \cdot d \cdot n\ \log n
\end{equation}
%
bits in total. Generalizing the amount of bits needed for the modified multiple Bloom filter summary is not as straightforward since the choice of how many bits per counter and filter should be used depends on the type of application and also personal taste. However, the authors give some examples and state that the modified version occupies $3.3$ times more space than the simple multiple Bloom filter summary. This leads to a total number of bits equal to 
%
\begin{equation}\label{eq:bitsmbfd}
	\beta_{mbfcounter} = 3.3\ n\ \log{n}
\end{equation}
%