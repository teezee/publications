\section{Introduction}\label{sec:introduction}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
In 2007, sales for mobile microprocessors first surpassed that of desktop processors. Since then the trend tends towards higher mobility in computing. This is accompanied by the demand of an everywhere internet. Today, not only PCs but mobile phones, mp3 and multimedia players want to connect to the world wide web. In addition, flourishing economy in newly industrializing countries like China and India further leads to an explosion of the number of connected hosts.

The standard networking protocol used today is the Internet Protocol version 4 (IPv4). However, its address space is too small to serve the highly increased number of hosts. IPv6, proposed in 1998 by the Internet Engineering Task Force, promises to solve that problem by providing a virtually unlimited (in the sense of not to be expected to ever get exhausted) number of addresses. But efficient forwarding of IPv6 packets is still a major problem. That holds especially in the internet core where the routing tables contain millions of entries, and packets arrive on thousand high-speed links. Identifying the correct route in a $128$ bit address space is an extensive task that requires specialized hardware, efficient algorithms and optimized data structures to be able to process the packets at line speed. Satisfying these conditions is accompanied by incredibly high production costs.

%The main bottleneck is the number of memory accesses needed to successfully lookup the prefix. This number depends on the {\it longest prefix matching} technique and the underlying hash table. Table summaries which are kept in fast but expensive on-chip memory can be used to improve the lookup process.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Longest Prefix Matching}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
To forward a packet a router must search its forwarding table for the longest entry that matches the destination of the packet. This extensive task is known as {\it longest prefix matching} (LPM) and is also referred to as IP-lookup. It requires the router to search multiple prefixes of variable length. Common approaches for longest prefix matching include algorithms and content addressable memory (CAM), which can be queried for content and returns the addresses of its location. CAM is expensive in terms of chip size and operation. Thus, it is desirable to exchange it with algorithmic approaches. Here, the main bottleneck is the number of memory accesses needed to retrieve the prefixes. These algorithms require the prefixes to be stored in data structures that allow  lookup with a maximum of one memory access to prevent the overall process to degenerate. In this thesis we concentrate on improving these data structures in terms of hardware requirements to reduce router production and operation costs. LPM algorithms are shortly reviewed in \ref{sec:lpm}.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Hash Tables and Summaries for IP-Lookup}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
A natural approach is to store prefixes in hash tables that allow identification of the correct bucket in one memory access. However, due to hash collisions one can not guarantee that the correct entry can be retrieved immediately. Hashing with multiple choices can improve the lookup by allowing an item to be stored in multiple locations. While this helps to improve the distribution of items it requires parallel lookup of all possible locations. To eliminate the need for parallelism and improve lookups, small hash table summaries - usually based on Bloom filters - can be kept in very fast but expensive on-chip memory. A variety of techniques have been proposed which will be reviewed in section \ref{sec:related}. We propose multiple techniques to improve the efficiency of hash tables and their summaries in section \ref{sec:eht}.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Motivation}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Even more than ten years after its proposal, IPv6 is still sparsely used. The main reason is the high requirement IPv6 poses to the hardware. Current solutions require vast amounts of extremely expensive but fast on-chip memory. The economic pressure on providers to deploy an IPv6 ready network does not outweigh the high costs associated with it. However, the explosion of hosts, especially in the eastern world, leads to an omnipresent need for a higher address space. An evaluation of the $status\ quo$ and an evolution towards cost-efficient solutions that are deployable in the internet core are long overdue.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Position and Outline}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
This work analyzes state-of-the-art data structures, their applicability in IPv6 core routers, and suggests mechanisms to improve the efficiency of prefix-storing data structures in terms of memory requirements and router production cost. We are especially interested in aggressively reducing the number of bits needed for the summary representation to allow table sizes of millions of entries that are common in the internet core. Our design is based on four key-ideas which have been ignored so far. As we will show, these observations allow a decrease in size by a factor of \factor. Section \ref{sec:related} discusses approaches currently in use or suggested. First, a basic overview of LPM algorithms is given to demonstrate the complexity of this task, followed by an introduction of Bloom filters and derivates. The section concludes by reviewing different hash table structures and their summaries. Section \ref{sec:eht} starts with a hash table and summary analysis emphasizing their applicability in core routers. We then present our key-ideas and an improved and adjustable hash table design specialized for IP-lookup applications. Section \ref{sec:discussion} discusses the results of our simulations. A conclusion as well as a discussion on future work is given in \ref{sec:conclusion}.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Conventions}\label{sec:conventions}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Throughout this thesis we use the following conventions.
%
\begin{itemize}
	\item We strongly stick to the International System of Units (SI) \footnote{http://physics.nist.gov/cuu/Units/}, the SI prefixes and the approved prefixes for binary multiples. That is, we use the prefixes k,M,G \dots only for multiples of 10 and the prefixes Ki,Mi,Gi \dots for multiples of 2.
	\item 'Iff' with two 'f' denotes 'if and only if' throughout this document.
	\item Unless otherwise stated 'log' denotes the logarithm to base 2.
	\item 'ln' denotes the natural logarithm to base $e$.
	\item $|x|$ denotes the length of x.
	\item $[x]$ denotes the range of x. Unless otherwise stated it is defined as $[x] = \{0,\dots,x-1\}$.
	\item $==$ denotes equals.
	\item In pseudocode, $\leftarrow$ denotes assignment where $=$ denotes equals.
\end{itemize}
%