%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Discussion and Results}\label{sec:discussion}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
In this section we present and discuss results of a conceptual implementation of the EHT. The implementation is conceptual in the sense that it does {\em not} fully resemble the complex structure of the EHT but simulates it's behavior appropriately.

For simulations we use the following parameters: $$n = \{ 100,000; 1,000,000\} ; c = \{ 3.2;1.6 \}; \chi = \{ 4;5 \}; |\omega| = \{ 64;128 \}$$ for a total of $16$ different simulations. The number of hash functions $k$ is always chosen optimal, using normalization as mentioned in section \ref{sec:counter}. When referencing the different parameter configurations we use the following table.
%
\begin{table}[h!]
\begin{center}
\begin{tabular}{l@{\ \ \ }|c@{\ \ \ }c@{\ \ \ }c@{\ \ \ }c@{\ \ \ }r}
	\hline \hline
	ref & n & c & k & $\chi$ & $|\omega |$ \\
	\hline
	0 & $1,000,000$ & $1.6$ & 2 & $4$ & $128$ \\
	1 & $1,000,000$ & $1.6$ & 2 & $4$ & $64$ \\
	2 & $1,000,000$ & $1.6$ & 2 & $5$ & $128$ \\
	3 & $1,000,000$ & $1.6$ & 2 & $5$ & $64$ \\
	4 & $1,000,000$ & $3.2$ & 3 & $4$ & $128$ \\
	5 & $1,000,000$ & $3.2$ & 3 & $4$ & $64$ \\
	6 & $1,000,000$ & $3.2$ & 3 & $5$ & $128$ \\
	7 & $1,000,000$ & $3.2$ & 3 & $5$ & $64$ \\
	8 & $100,000$ & $1.6$ & 2 & $4$ & $128$ \\
	9 & $100,000$ & $1.6$ & 2 & $4$ & $64$ \\
	A & $100,000$ & $1.6$ & 2 & $5$ & $128$ \\
	B & $100,000$ & $1.6$ & 2 & $5$ & $64$ \\
	C & $100,000$ & $3.2$ & 4 & $4$ & $128$ \\
	D & $100,000$ & $3.2$ & 4 & $4$ & $64$ \\
	E & $100,000$ & $3.2$ & 4 & $5$ & $128$ \\
	F & $100,000$ & $3.2$ & 4 & $5$ & $64$ \\
	\hline \hline
\end{tabular}
	\label{tab:config}
	\caption{\footnotesize{Parameter configurations.}}
\end{center}
\end{table}
%

On each simulation we do ten trials, that is we instantiate the EHT and fill it with $n$ random keys and values. The structure is then pruned using algorithm \ref{algo:prune}. No updates are performed but the EHT is queried for all $n$ and additional $2n$ random keys to verify that every key can be retrieved and to analyze the false-positive probability. Simulations are very expensive in software, that is why only ten trials are performed. This may seem few but confidence is pretty high and it is assumed that the ten trials are representative for a larger set of tables. As summary a HCCBF is used. The compression rate $\gamma_h$ is calculated using algorithm \ref{algo:compress}. No hard compression is used, since we want to evaluate the quality of the compression algorithm. The cost of using hard compression can be derived by examining the resulting HCCBF and is included in the analysis.

For each try we calculate the size of the offline CBF, the size of a PCBF and the size of the online HCCBF. We count the frequency of all counters in the offline summary which allows to derive the number of overflown counters in the online summary. Every compressed word in the HCCBF is analyzed for the number of bits that are actually used to encode counters, resulting in a histogram of code-lengths per word. In addition the load of all online buckets is calculated and the number of CAM entries counted. We are especially interested in the effect of the on-chip word-size $|\omega|$ and the counter limit $\chi$ on compression quality and CAM size to evaluate the tradeoff. Not all the results will be presented since this would go beyond the scope of this thesis. Instead we concentrate on expected and obscure behavior and present these with selected examples.

The analysis follows the outline in section \ref{sec:eht}. We begin by evaluating the counter distribution and bucket loads. We proceed with an analysis of the effect of $\chi$ and the number of CAM entries. Then the achieved compression rate is discussed and a tradeoff for hard compression elaborated. Last, we evaluate the sizes of the summaries and weigh improvements and overhead.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Counter distribution}\label{sec:cdist}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Since the parameters $\chi$ and $|\omega |$ have no effect on the counter distribution we count the frequencies of the counters for $n = \{ 1E+6, 1E+5 \}$ with sizes of $c = \{ 1.6,3.2 \}$. Using equation \ref{eq:countervalue} presented in section \ref{sec:counter} we also calculate the expected frequency for each counter value. The probability of the counter values is multiplied with the number of counters $m$ to derive the expected frequency.
%
\begin{figure}[h!]
	\centering
	\label{fig:cdist}
	\includegraphics[scale=0.45]{\gfx cdist}
	\caption{\footnotesize{Real and expected counter frequencies}}
\end{figure}
%

The results are shown in figure \ref{fig:cdist}. The actual frequencies resemble the expected frequencies exactly. Thus, the structure behaves as predicted. The number of counter overflows in the online summary depends on $\chi$ and can be seen in table \ref{tab:coverflow}.
%
\begin{table}[h!]
\begin{center}
\begin{tabular}{l@{\ \ \ }|c@{\ \ \ }c@{\ \ \ }c@{\ \ \ }r}
	\hline \hline
		& \multicolumn{4}{c}{configuration} \\
	%\cline{2-5}
	$\chi$ & [0]-[3] & [4]-[7] & [8]-[B] & [C]-[F] \\
	\hline
	$4$ & $34106.85$ & $25868.12$ & $2033.25$ & $4068.850$ \\
	$5$ & $6277.50$ & $3594.20$ & $301.15$ & $602.225$ \\ 
	\hline \hline
\end{tabular}
	\label{tab:coverflow}
	\caption{\footnotesize{Number of counter overflows.}}
\end{center}
\end{table}
%

It is interesting that for $n = 1E+6$ the number of overflows is smaller for the larger table while for $n = 1E+5$ the frequencies are reversed. However, against first assumptions, this is expected and can easily be explained. Recall from section \ref{sec:counter} that the counter distribution depends on the fraction $\frac{m}{n}$ and the number of hash functions $k$. To explain this behavior, an analysis of these factors seems appropriate. Table \ref{tab:mn} shows the number of choices $k$ and counters\slash buckets per item for $n = \{ 1E+6, 1E+5 \}$ and $c = \{ 1.6, 3.2 \}$.
%
\begin{table}[h!]
\begin{center}
\begin{tabular}{l@{\ \ \ }c@{\ \ \ }c@{\ \ \ }r}
	\hline \hline
	n & c & k & m/n \\
	\hline
	$1E+06$ & $1.6$ & $2$ & $2.097152$\\
	$1E+05$ & $1.6$ & $2$ & $2.621440$\\
	$1E+06$ & $3.2$ & $3$ & $4.194304$\\
	$1E+05$ & $3.2$ & $4$ & $5.242880$\\
	\hline \hline
\end{tabular}
	\label{tab:mn}
	\caption{\footnotesize{Number of choices and buckets per item.}}
\end{center}
\end{table}
%

As can be seen tables with $n = 1E+5$ have an advantage in both the number of choices and the number of counters\slash buckets per item. Especially for $c = 1.6$ the number of hash functions $k$ is equal, but the fraction $\frac{m}{n}$ is higher for $n = 1E+5$. Hence, the probability that a counter receives more insertions is higher for $n = 1E+6$, explaining the numbers in table \ref{tab:coverflow}.

We will study the impact of the number of overflows on the number of CAM entries in section \ref{sec:chi}.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Bucket Load}\label{sec:bload}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
The maximum load depends on the number of choices $k$ and the number of items $n$. We aggregate the results of the combinations for $n$ and $c$ and count the number of entries in every online bucket. We then take the maximum of the frequencies to evaluate the worst-case behavior. The results are shown in table \ref{tab:load}.
%
\begin{table}[h!]
\begin{center}
\begin{tabular}{c@{\ \ \ }|c@{\ \ \ }c@{\ \ \ }c@{\ \ \ }c@{\ \ \ }c@{\ \ \ }r}
	\hline \hline
		& & \multicolumn{5}{c}{load} \\
	configuration & $E_{maxload}$ & 0 & 1 & 2 & 3 & 4 \\
	\hline
	$[0]-[3]$ & $3$ & $1184464$ & $837562$ & $80950$ & $684$ & $1$ \\
	$[4]-[7]$ & $2$ & $3204894$ & $980039$ & $10438$ & $1$ & $0$ \\
	$[8]-[B]$ & $3$ & $167662$ & $89728$ & $5327$ & $24$ & $0$ \\
	$[C]-[F]$ & $2$ & $424659$ & $99411$ & $369$ & $0$ & $0$ \\
	\hline \hline
\end{tabular}
	\label{tab:load}
	\caption{\footnotesize{Entry distribution.}}
\end{center}
\end{table}
%

For all the tables with $n = 1E+6$ there was one bucket overflow in the worst-case. That is, only one entry will be diverted to CAM. None of the buckets for tables with $n = 1E+5$ experienced an overflow. With these results the best solution to deal with bucket overflows is to artificially increase the counter of the affected bucket to $\chi$ and move the entries to CAM. This can be performed during pruning and by the update engine which adjusts the counters accordingly.

According to the numbers, the bucket loads do not scale with $n$. Compared to tables with $n = 1E+5$ the number of entries per bucket for $n = 1E+6$ are lower for $0$ entries, and much higher for $> 1$ entries. Like the counter distribution, bucket load distribution also depends on the fraction $\frac{m}{n}$ and the number of choices $k$. In the previous section we have shown that tables with $1E+5$ have an advantage in both factors over tables with $1E+6$ (see table \ref{tab:mn}). This also explains the behavior in table \ref{tab:load}.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{The power of $\chi$}\label{sec:chi}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
The counter limit $\chi$ bears a variety of effects. It affects the number of counter overflows in the online summary which have been evaluated in section \ref{sec:cdist}. Depending on the probability of the smallest counter value in $k'$ chosen counters it also influences the number of entries that must be stored in CAM. Finally, the achievable compression rate $\gamma$ directly depends on $\chi$.

We will start by evaluating the number of CAM entries and then move onward to inspecting the achieved compression.
%%%%%%%%%%%%%%%
\subsubsection{CAM requirements.}\label{sec:cam}
%%%%%%%%%%%%%%%
For CAM entries we aggregate the results for $\chi$ according to $n$ and $c$, calculate the average and take the minimum\slash maximum values encountered. Using equation \ref{eq:ecam} we also calculate the expected number of CAM entries. Table \ref{tab:cam} shows the results.
%
\begin{table}[h!]
\begin{center}
\begin{tabular}{l@{\ \ \ }c@{\ \ \ }c@{\ \ \ }c@{\ \ \ }c@{\ \ \ }c@{\ \ \ }r}
	\hline \hline
	n & c & $\chi$ & min & max & avg & expected\\
	\hline
	$1E+6$ & $4$ & $1.6$ & $5017$ & $5446$ & $5194.05$ & $5181$\\
	$1E+6$ & $5$ & $1.6$ & $236$ & $287$ & $258.20$ & $265$\\
	\hline
	$1E+6$ & $4$ & $3.2$ & $40$ & $61$ & $47.00$ & $47$\\
	$1E+6$ & $5$ & $3.2$ & $0$ & $0$ & $0.00$ & $0$\\
	\hline\hline
	$1E+5$ & $4$ & $1.6$ & $144$ & $209$ & $177.95$ & $178$\\
	$1E+5$ & $5$ & $1.6$ & $2$ & $11$ & $6.05$ & $6$\\
	\hline
	$1E+5$ & $4$ & $3.2$ & $0$ & $1$ & $0.15$ & $0$ \\
	$1E+5$ & $5$ & $3.2$ & $0$ & $0$ & $0.00$ & $0$ \\
	\hline \hline
\end{tabular}
	\label{tab:cam}
	\caption{\footnotesize{Number of CAM entries.}}
\end{center}
\end{table}
%

Once again, the results closely resemble the expectations. It can be observed, that the numbers do not scale with $n$. We have explained the reasons in previous sections and a recapitulation is omitted. However, one interesting fact can be extracted by analyzing table \ref{tab:cam}. That is, the quality of $\chi$ also depends on the fraction $\frac{m}{n}$. This does not come as a surprise, since with higher counters in general the probability to choose a higher counter as smallest counter value for any item is also increased. Still, it is worth mentioning. For tables with a sufficiently large number of counters\slash buckets per entry (like $n = 1E+5$,$c = 1.6$) $\chi$ can be set to $4$ without much overhead. Consulting table \ref{tab:mn} allows a rough estimate of the lower bound somewhere in $2 > \frac{m}{n} > 2.6$.

It can be expected at this point, that the achieved compression is more effective for tables with higher $\frac{m}{n}$. The next section analyses compression quality in detail.
%%%%%%%%%%%%%%%
\subsubsection{Compression.}\label{sec:compression}
%%%%%%%%%%%%%%%
To analyze the achieved compression we take the minimum, maximum and average $\gamma_h$ and compare that to $\gamma_p$ and the number of counters if no compression is used (denoted $\gamma_0$). We also include the maximum number of bits used to compress the counters. Table \ref{tab:gamma} shows the results.
%
\begin{table}[h!]
\begin{center}
\begin{tabular}{l@{\ \ \ }c@{\ \ \ }c@{\ \ \ }c@{\ \ \ }|c@{\ \ \ }c@{\ \ \ }c@{\ \ \ }|c@{\ \ \ }c@{\ \ \ }|r}
	\hline \hline
	n & c & $\chi$ & $|\omega|$ & min $\gamma_h$ & max $\gamma_h$ & avg $\gamma_h$ & $\gamma_p$ & $\gamma_0$ & max bits\\
	\hline
	$1E+6$ & $1.6$ & $4$ & $64$ & $22$ & $24$ & $22.8$ & $27$ & $21.3$ & $63.3$\\
	$1E+6$ & $1.6$ & $5$ & $64$ & $21$ & $22$ & $21.5$ & $24$ & $21.3$ & $63.3$\\
	\hline
	$1E+6$ & $1.6$ & $4$ & $128$ & $50$ & $53$ & $51.0$ & $55$ & $42.6$ & $126.4$\\
	$1E+6$ & $1.6$ & $5$ & $128$ & $47$ & $51$ & $49.5$ & $49$ & $42.6$ & $125.1$\\
	\hline
	$1E+6$ & $3.2$ & $4$ & $64$ & $23$ & $26$ & $24.6$ & $27$ & $21.3$ & $62.7$\\
	$1E+6$ & $3.2$ & $5$ & $64$ & $24$ & $25$ & $24.9$ & $24$ & $21.3$ & $63.2$\\
	\hline
	$1E+6$ & $3.2$ & $4$ & $128$ & $56$ & $59$ & $57.7$ & $55$ & $42.6$ & $126.3$\\
	$1E+6$ & $3.2$ & $5$ & $128$ & $55$ & $58$ & $56.9$ & $49$ & $42.6$ & $126.3$\\
	\hline\hline
	$1E+5$ & $1.6$ & $4$ & $64$ & $25$ & $27$ & $26.0$ & $27$ & $21.3$ & $62.6$\\
	$1E+5$ & $1.6$ & $5$ & $64$ & $24$ & $26$ & $25.4$ & $24$ & $21.3$ & $62.5$\\
	\hline
	$1E+5$ & $1.6$ & $4$ & $128$ & $57$ & $60$ & $58.8$ & $55$ & $42.6$ & $126.6$\\
	$1E+5$ & $1.6$ & $5$ & $128$ & $55$ & $60$ & $57.8$ & $49$ & $42.6$ & $125.7$\\
	\hline
	$1E+5$ & $3.2$ & $4$ & $64$ & $23$ & $26$ & $25.5$ & $27$ & $21.3$ & $63.0$\\
	$1E+5$ & $3.2$ & $5$ & $64$ & $23$ & $26$ & $24.6$ & $24$ & $21.3$ & $62.1$\\
	\hline
	$1E+5$ & $3.2$ & $4$ & $128$ & $57$ & $60$ & $58.3$ & $55$ & $42.6$ & $126.9$\\
	$1E+5$ & $3.2$ & $5$ & $128$ & $56$ & $59$ & $57.0$ & $49$ & $42.6$ & $125.8$\\
	\hline \hline
\end{tabular}
	\label{tab:gamma}
	\caption{\footnotesize{Compression rate.}}
\end{center}
\end{table}
%

The numbers provide a lot of useful information. With sufficiently large $|\omega|$ or larger $\chi$, Huffman compression always performs better than word packing, even without using {\em hard compression}. If $|\omega|$ is small and $\chi$ is also small, word packing is the better choice. The only exception to this rule is for tables with $n = 1E+6$ and $c = 1.6$. However, we have already seen that these have to be treated differently and we will ignore them for now. In all cases, compression yields an improvement over not using compression. The counter limit $\chi$ only slightly influences the compression rate $\gamma_h$. It's impact on $\gamma_p$ is greater by far.

We will now analyze the impact of a small $\frac{m}{n}$ on the HCCBF and show possible improvements at the cost of additional CAM. As examples we choose tables [2] for $n = 1E+6$ and [A] for $n = 1E+5$, because $|\omega|$ and $\chi$ are sufficiently large and Huffman compression does not scale for [2].

%
\begin{figure}[h!]
	\centering
	\label{fig:wdist}
	\includegraphics[scale=0.45]{\gfx wdist}
	\caption{\footnotesize{Frequency of used bits for n = 1e6, c = 1.6}}
\end{figure}
%

Figure \ref{fig:wdist} shows the frequencies of the number of bits per word used to compress the counters. The distribution again follows a binomial distribution, which is to be expected. The two graphs can not be directly compared. However, since all parameters except $n$ are equal, it can be concluded, that for smaller $\frac{m}{n}$ and equal $k$, the center of the distribution moves to the left, leading to a smaller average bit usage. Considering the counter distribution, which also leads to higher counters for smaller $\frac{m}{n}$ and equal $k$, this is a probable explanation.

The compression can be improved by reducing $|\omega|$ while keeping the same $\gamma_h$, thus resembling hard compression. For example, by reducing $|\omega|$ to $118$ bits, $10$ bits per word can be saved (see the blue lines in figure \ref{fig:wdist}). Of course, this leads to a number of word overflows. In this case approximately $160$ words will overflow with up to $10$ bits. Thus by providing CAM for an additional $160$ overflown words, $10$ bits per on-chip memory word can be saved.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Comparing sizes}\label{sec:size}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
So far we have analyzed and explained the effect of the different parameters on the performance of the EHT. It is now time to present the resulting sizes of the summary. Since we already proved that the results perform as expected no open questions should remain. Therefore we omit a discussion of the summary sizes.

The figures present the average sizes for all simulations made. They include the size of uncompressed filters (CBF), the packed filters (denoted P) and Huffman compressed filters (denoted H) for each $\chi$ and word-size $|\omega|$ grouped by the number of items $n$ and the size of the table.

%
\begin{figure}[h!]
	\centering
	\label{fig:s1m16}
	\includegraphics[scale=0.45]{\gfx size1e616}
	\caption{\footnotesize{Summary sizes for n = 1e6, c = 1.6}}
\end{figure}
%
%
\begin{figure}[h!]
	\centering
	\label{fig:s1k16}
	\includegraphics[scale=0.45]{\gfx size1e516}
	\caption{\footnotesize{Summary sizes for n = 1e5, c = 1.6}}
\end{figure}
%
%
\begin{figure}[h!]
	\centering
	\label{fig:s1m32}
	\includegraphics[scale=0.45]{\gfx size1e632}
	\caption{\footnotesize{Summary sizes for n = 1e6, c = 3.2}}
\end{figure}
%
%
\begin{figure}[h!]
	\centering
	\label{fig:s1k32}
	\includegraphics[scale=0.45]{\gfx size1e532}
	\caption{\footnotesize{Summary sizes for n = 1e5, c = 3.2}}
\end{figure}
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Summary}\label{sec:summary}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
The results fully meet the expectations and backup our theoretical analysis. We have shown that our initial assumptions allow fundamental improvements over previous work. In conclusion, when constructing an EHT, the following aspects must be considered.
\begin{itemize}
	\item Reducing the size $m$ is achieved by increasing the off-chip memory width. Analysis has shown, that the expected maximum load will not exceed $3$ as long as $\frac{m}{n}>2$. Bucket overflows are extremely rare, even for a large set of items. The off-chip memory width can be reduced at the cost of additional CAM.
	\item Performance does not scale with $n$. With equal $k$ but smaller $\frac{m}{n}$, performance will be worse. This holds especially if table sizes are very small such that $\frac{m}{n} \rightarrow 2$.
	\item Choosing $\chi$ depends on the fraction $\frac{m}{n}$. Starting with $\chi = 5$ for $2 < \frac{m}{n} < 2.5$, $\chi$ can be decremented by one each time $\frac{m}{n}$ is doubled for a small overhead in terms of CAM.
	\item Huffman compression is favorable over word packed compression, unless the word-size $|\omega|$ and the counter limit $\chi$ are small.
	\item At the cost of few additional CAM cells, the performance of Huffman compression can be improved.
\end{itemize}