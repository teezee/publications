% TODO:
% SRV request/autoconfiguration/… -> more traffic on a regular basis

% Information:
% DNScurve: http://dnscurve.org
% DNScrypt: http://www.opendns.com/about/innovations/dnscrypt/, http://lifehacker.com/how-to-boost-your-internet-security-with-dnscrypt-510386189, http://securityandrisk.blogspot.ch/2010/11/public-dns-servers-less-privacy-in.html
% DNS Prefetching: https://www.usenix.org/legacy/event/leet10/tech/full_papers/Krishnan.pdf
% Public DNS: http://security.stackexchange.com/questions/38953/what-are-the-privacy-pros-and-cons-of-using-a-public-dns-server-vs-your-own, http://arstechnica.com/security/2009/12/google-public-dns-service-not-ideal-for-everyone/
% Privacy abuses: http://arstechnica.com/gaming/2014/02/valve-dns-privacy-flap-exposes-the-murky-world-of-cheat-prevention/
% DNS Privacy Problem Statement I-D: http://tools.ietf.org/html/draft-bortzmeyer-dnsop-dns-privacy-01
% DNS confidentiality I-D: http://tools.ietf.org/html/draft-koch-perpass-dns-confidentiality-00
% Confidential DNS: http://tools.ietf.org/html/draft-wijngaards-dnsop-confidentialdns-00
% Library preloading to enhance security: https://netfuture.ch/tools/tls-interposer/

\documentclass{IEEEtran}
\usepackage{makeidx}
\usepackage{subfigure}
\newcommand{\subfigureautorefname}{\figureautorefname}
\usepackage[href]{htwg}
\usepackage{algorithm2e}
\begin{document}
\pagestyle{empty}
\title{Boost DNS Privacy, Reliability, and Efficiency\\with opDNS Safe Query Elimination}
\author{Marcel Waldvogel \qquad Thomas Zink\\University of Konstanz, Konstanz, Germany\\
$<$first$>$.$<$last$>$@uni-konstanz.de\vspace{-0.5cm}}
\maketitle
\thispagestyle{empty}

\begin{abstract}
SRV records, DNSSEC, and DANE among others fortify the Domain Name System as the central information hub behind the Internet.
Largely hidden from the end user, an increasing number of protocol and trust decisions are contingent on DNS.
Neglect or attacks on DNS have much more impact today than ever, now endangering security far beyond denial of service.
Opportunistic Persistent DNS (opDNS) addresses these problems by abandoning pessimistic caching and eliminating unnecessary traffic.
Today's DNS infrastructure relies on the hosts forgetting and refreshing DNS records in relatively short time.
In conjunction with TLS, opDNS greatly reduces the number of queries and in turn increases privacy, reliability, and efficiency.
Even with DNS lookups all but eliminated for frequently visited secure services, changes to the server addresses will be recognized almost immediately, unlike standard DNS.
We will show how end systems can take advantage of opDNS without having to wait for support by server operators or application developers, enabling the most effective way of deployment.
\end{abstract}

\section{Introduction}
% problems
When connecting to networks anywhere in the world, your end system sends its DNS queries through a server provided by the local operator. Neglect, misconfiguration, and active spying or modification of DNS records becomes disastrous, yet is trivial and extremely resource-efficient. One of the first things your device will do when connected to the new network is re-establishing connections to mail servers, web applications, instant messaging servers, and many more push services.

DNS security is a major problem, as DNS is so valuable, ubiquitous and important, but also vulnerable:
\begin{enumerate}
  \item The network provider immediately recognizes many users due to the highly unique signature created by the set of queries.
	Queries also provide most revealing information about professional and personal affiliations, as well as personal interests.
  \item DNS misconfiguration, attacks, or bugs will cause your applications and clients to fail or connect to the wrong site \cite{ChinaDNS}.
	DNSSEC could mitigate these effects, unfortunately the vast majority of end systems do not currently validate it \cite{Huston2013DNS}.
  \item Not only black-hats cause blackouts.
	Nation states all around the globe have used DNS blocking to restrict access to certain services and perform censoring.
	The structure of DNS allows easy censorship and outages.
  \item The number of requests, especially on slow or error-prone links, can significantly slow down operations, time better spent on the actual connection to the services.
\end{enumerate}

% solutions
The response to these and other threats \cite{Ragan2013Three,Rubens2013How} has been to authenticate the data using DNSSEC \cite{DNSSEC} and work toward encrypted channels through VPNs, Tor, DNSCurve \cite{dnscurve}, or Confidential DNS \cite{confidentialdns}.
DNS encryption, however, is unlikely to become an immediate solution for all problems.

% our approach
The best way to avoid exposure of DNS queries and be immune to wrong answers is not to perform any DNS queries.
This is the goal of our approach, \emph{Opportunistic Persistent DNS} (opDNS). Orthogonal to DNS encryption and building on top of existing TLS and optionally DNSSEC, it significantly reduces the common and most identifying -- as well as most vulnerable -- DNS queries without sacrificing accuracy.

By using a slightly modified local cache and name resolver, and collaborating with an enhanced TLS library, unmodified applications can talk to unmodified servers with greatly reduced privacy exposure and risk of being subject to malicious or accidental DNS problems.
DNS records are optimistically kept by the local cache for sites whose identity can be verified using TLS and which have been proven to be stable.
Applications and servers aware of opDNS can take advantage of additional mechanisms, such as pushing authenticated DNS updates.
In addition to increase privacy and reduce impact of outages, opDNS also greatly reduces the load on the DNS infrastructure.
% privacy with benefits
We strongly believe that increased privacy coupled with additional functionality are strong incentives for users and network operators alike.


\section{DNS Insights}

\begin{figure*}[t]
    \subfigure[Collecting DNS queries with date and time stamp allows tracking of users, their browsing behavior and can lead to user identification.]{\includegraphics[width=.35\textwidth]{fig/dnsprivacy}\label{fig-dnsprivacy}}\hfill
    \subfigure[Blocking authoritative name servers or eliminating specific records prevents successful lookup and ultimately leads to no connection to the target host.]{\includegraphics[width=.4\textwidth]{fig/dnsoutage}\label{fig-dnsoutage}}\hfill
	  \subfigure[opDNS uses a local cache to reduce DNS traffic. Records are updated securely by the web servers using TLS.]{\includegraphics[width=.2\textwidth]{fig/opdns}\label{fig-opdns}}%
	\caption{DNS problems and the opDNS approach\label{fig-miti}.}
\end{figure*}

The collection of a user's DNS queries provides a discriminating database of his interests, services used, and browsing behavior and allows user identification (\autoref{fig-dnsprivacy})\cite{DNSprefetch}.
Simply blocking name servers or certain DNS records makes name resolution impossible and prevents users from navigating to their target sites (\autoref{fig-dnsoutage}).
This is due to the lack of cache update and misuse for load balancing.
Server operators prefer frequent DNS lookups instead of keeping possibly outdated information in cache, which would render a service essentially unreachable.
In contrast, prominent servers tend to change their addresses only every few years, if at all.
Nevertheless, an address change could become unavoidable without notice in a few minutes or days.

The simplest method to make traffic less revealing is to not generate the traffic at all.
This applies even more to DNS, where low cache times (TTL) require the clients to send request again every few minutes to few hours.

\section{Basic opDNS operation}

An overview of the operation of opDNS is shown in \autoref{fig-opdns}.
A local cache on the user's machine retains a copy of recently queried DNS records.
During the lifetime of the record, as indicated by the TTL value, the local cache is used normally.

After the TTL expires, the behavior deviates from the current standards:
Records related to TLS services will be kept further, but with a special `expired' flag set.
When a TLS-enabled application asks for the record, the expired and flagged entry will be returned.
The application will then proceed to connect to the server with caution, using shorter timeouts and strong certificate test.
If this fails, the expired entry is flushed and the procedure is repeated.

\section{Transparent opDNS}

The above basic process requires modifications to the application.
This will unacceptably slow down deployment.
A connection setup for a TLS-enabled client application will do the following steps, among other things:
\begin{enumerate}
    \item Using the server name, obtain the address of the server using \textsf{gethostbyname()}, \textsf{getaddrinfo()}, or a call to the DNS resolver library
    \item Set up a socket connection using \textsf{connect()}
    \item Start the TLS handshake (e.g. \textsf{SSL\_connect()})
    \item Verify that the certificate actually belongs to the named server from step 1 (e.g. \textsf{X509\_check\_host()})
\end{enumerate}

By hooking into the system libraries \cite{TLSinterposer}, the application's privacy awareness can be increased fully transparently.
The opDNS library intercepts several of the above library functions and their relatives.
By observing the calls to e.g. \textsf{getaddrinfo()} and \textsf{X509\_check\_host()}, it can determine that the application uses TLS to verify the server.\footnote{Of course, failures between these calls, such as the server being unreachable for \textsf{connect()}, should be taken into account.}

An application that has proven to contact this host only using TLS, will in the future also be served expired DNS records.
A connection error or certificate verification failure will wipe the expired entry from the cache.

\section{Demonstration}

As part of the demonstration, we will use devices which have the current `pessimistic' DNS caching changed into the optimistic variant, opDNS, to
\begin{enumerate}
	\item enhance privacy by disclosing as little personal information as possible,
	\item reduce the effect of outages by localizing name resolving, and
	\item improve client and server performance by reducing overall DNS traffic.
\end{enumerate}

We will show that a device which has previously connected to a service in a \emph{good} environment will be able to continue working even in \emph{adverse} DNS environments with logging, blocking, or modification.
We will also show that a device operating in a \emph{good} environment will react quicker to changes in DNS, flushing its cache before the DNS record's TTL has expired, if necessary, while still eliminating most of the normal DNS lookups.
All the tests will be run with off-the-shelf applications and servers; the only addition being a small linux library, which can be enabled or disabled as necessary.
The library will be published under a free and Open Source license to enable instant deployment.
Participants are welcome to connect their devices to our environments and experience the effects firsthand.

%%% bibliography ##############################################################

\bibliographystyle{IEEEtranS}
\bibliography{opDNS}

\end{document}

