%MW Wie gross soll das Dokument sein?
% http://petsymposium.org/2014/hotpets.php sagt:
% "There is no page limit for the papers to be submitted to the HotPETs workshop. However, short papers (less than 6 pages) are highly appreciated."
% Und: "Submitted papers *must not be anonymized*."

% TODO:
% SRV request/autoconfiguration/… -> more traffic on a regular basis

% Information:
% DNScurve: http://dnscurve.org
% DNScrypt: http://www.opendns.com/about/innovations/dnscrypt/, http://lifehacker.com/how-to-boost-your-internet-security-with-dnscrypt-510386189, http://securityandrisk.blogspot.ch/2010/11/public-dns-servers-less-privacy-in.html
% DNS Prefetching: https://www.usenix.org/legacy/event/leet10/tech/full_papers/Krishnan.pdf
% Public DNS: http://security.stackexchange.com/questions/38953/what-are-the-privacy-pros-and-cons-of-using-a-public-dns-server-vs-your-own, http://arstechnica.com/security/2009/12/google-public-dns-service-not-ideal-for-everyone/
% Privacy abuses: http://arstechnica.com/gaming/2014/02/valve-dns-privacy-flap-exposes-the-murky-world-of-cheat-prevention/
% DNS Privacy Problem Statement I-D: http://tools.ietf.org/html/draft-bortzmeyer-dnsop-dns-privacy-01
% DNS confidentiality I-D: http://tools.ietf.org/html/draft-koch-perpass-dns-confidentiality-00
% Confidential DNS: http://tools.ietf.org/html/draft-wijngaards-dnsop-confidentialdns-00
% Library preloading to enhance security: https://netfuture.ch/tools/tls-interposer/

\documentclass{llncs}
%
\usepackage{makeidx}  % allows for indexgeneration
\usepackage{subfigure}
\newcommand{\subfigureautorefname}{\figureautorefname}
\usepackage[href]{htwg}
\usepackage{algorithm2e}
%
%%% document ##################################################################
\begin{document}
%

%%% title page ################################################################
\mainmatter % start of the contributions
%
\title{Optimistic Persistent DNS:\\Enhancing Privacy by Query Elimination}
\titlerunning{Optimistic Persistent DNS}

%\author{Thomas Zink\inst{1} \and Marcel Waldvogel\inst{1}}
\author{Marcel Waldvogel \and Thomas Zink}
\authorrunning{Waldvogel and Zink}
\tocauthor{Marcel Waldvogel, Thomas Zink}

\institute{University of Konstanz, Konstanz, Germany\\
\email{$<$first$>$.$<$last$>$@uni-konstanz.de}}

\maketitle % typeset the title of the contribution

%%% abstract ##################################################################
\begin{abstract}
%DNS privacy issues: Queries allow fingerprinting and user identification.
%Solution: reduce DNS traffic and queries by caching.
DNS packets create probably the most revealing traffic today. Not only does it track almost every of your activities, its patterns make you immediately recognizable, and the information is willfully disclosed to the operator's servers by default.
Common belief is that this problem should be attacked by encrypting or tunneling DNS messages. We believe that eliminating most DNS requests is the best way to boost privacy. Thanks to the widespread use of TLS, an important privacy booster, we show how to safely eliminate many DNS requests. By enhancing client-side standard libraries, this can be done fully transparent to applications and servers; and orthogonal to other security and privacy improvements by safely extending the time-to-live of DNS resource records. 
% This one-sided start breaks the chicken-and-egg problem common to the deployment of privacy technology.
This enables the interested users to improve their privacy without having to wait for application developers, infrastructure providers, or server administrators to do their part.
%, a good way to start a revolution. (Geht das nicht besser?)
% Once a critical mass been reached, developers and administrators can enhance their applications to take advantage of the new possibilities.

\keywords{DNS, privacy, cache}
\end{abstract}

%%% introduction ##############################################################
\section{Introduction}

% Enhancing privacy frequently lacks the big-business financial interests that help drive standardisation and deployment. We therefore need to start one-sided.

% problems
From a privacy perspective, the domain name system (DNS) is one of the most dangerous protocols today. By default, for every service you use or web site you visit, at least one query is sent to your operator and\slash{}or ISP. Whenever you open your laptop or use your smartphone and tablet, they will perform several DNS lookups to update and synchronize. These lookups willfully and eagerly reveal to the operator of your current Internet connection which mail servers you use, what kind of instant messaging servers you connect to, which social networks you are following, what VoIP services you use, which blogs you follow, and many more. This makes it easy to identify your interests as well as track and trace you and your devices.

But DNS security is a problem as well, as DNS is so valuable, ubiquitous and important, but also vulnerable.
Not surprisingly, hackers have exploited DNS in the past to attack services and users alike.
Recent incidents like the takedown of the New York Times and Twitter by the Syrian Electronic Army (SEA) \cite{Kerner2013SEA},
the attack on Twitter by the Iranian Cyber Army \cite{Keizer2009Twitter},
or the massive attack on Spamhaus \cite{Prince2013DDoS}
are prominent examples of different real-world attacks on DNS.
Be it DNS cache poisoning, hijacking, or DDoS amplification attacks, everything has been tried in the past, and succeeded.

Not only black-hats cause blackouts.
Nation states like Germany, Turkey, or China have used DNS blocking to restrict access to certain services and perform censoring.
The structure of DNS makes censorship and outages kids play.

% With reports on voluntary and involuntary DNS takedowns one other aspect of DNS is often forgotten.
% DNS queries themselves pose a serious privacy threat.
% Whenever you try to access any website, your DNS server gets a record of it.
% These records reflect the user's interests and his browsing behavior.
% Collecting and evaluating these records can be enough to reliably identify individuals and infer habits.

% solutions
The response to these and other threats \cite{Ragan2013Three,Rubens2013How} has been to authenticate the data using DNSSEC \cite{DNSSEC} and work toward encrypted channels through VPNs, Tor \cite{Tor}, DNSCurve \cite{dnscurve}, or Confidential DNS \cite{confidentialdns}. DNS encryption, however, is unlikely to become an immediate miracle cure-all (see \autoref{relwork}). 

% our approach
The best way to avoid exposure of DNS queries and be immune to wrong answers is not to perform any DNS queries.
This may sound silly at first, but is the goal of our approach, \emph{Opportunistic Persistent DNS} (opDNS). Orthogonal to DNS encryption and building on top of existing TLS and optionally DNSSEC, it significantly reduces the common and most identifying DNS queries without sacrificing accuracy.

By using a slightly modified local cache and name resolver, and working in cahoots with an enhanced TLS library, unmodified applications can talk to unmodified servers with greatly reduced privacy exposure and risk of being subject to malicious or accidental DNS problems \cite{ChinaDNS}.
DNS records are optimistically kept by the local cache for sites whose identity can be verified using TLS and which have been proven to be stable. In addition, applications and servers aware of opDNS can take advantage of additional mechanisms, such as pushing authenticated DNS updates. In addition to increase privacy and reduce impact of outages, opDNS also greatly reduces the load on the DNS infrastructure.
% privacy with benefits
We strongly believe that increased privacy coupled with additional functionality are strong incentives for users and network operators alike.


\section{Related Work}
\label{relwork}

DNS was designed as a distributed, hierarchical system for reliability and delegation. To achieve efficiency and scalability, it is heavily based on caching. A cache is only useful when it can answer future requests from the same client and its neighbors (spatial and temporal locality). This makes all requests and answers essentially public. While caching encrypted records could be envisioned, they would still need to reveal information about their position in the domain name hierarchy. Due to caching, a repeatable encryption mechanism would need to be used, allowing the reverse engineering of the names through probing, possibly with the aid of hostname databases (e.g. through search engines), zone transfers (AXFR), and NSEC3 walking \cite{nsec3walker}.

DNSCurve \cite{dnscurve} and Confidential DNS \cite{confidentialdns} do not use encryption in the cache, only at the `link' level. Therefore, DNS caches still have full access to requests and replies; they protect only against eavesdropping packets in flight. Both approaches are also limited to opportunistic encryption,\footnote{Delegation NS records are not signed in the parent domain even under DNSSEC.} providing no protection against Man-in-the-Middle attacks. DNSCurve, despite going back to 2009, is yet to see a substantial deployment.

%TZ we could leave out this paragraph to reduce length
%In this context, it is frequently proposed to run a personal DNS server outside the normal caching hierarchy for more privacy. While this achieves this goal, the resulting load surge on the other DNS servers is likely to create scalability problems. Also, the set of name remote servers a name server talks to is still highly revealing.

DNSSEC \cite{DNSSEC} only provides authenticity and no privacy to the requests and responses. Authentication in DNS does however help to avoid DNS redirection attacks and the resulting privacy loss.

% Namecoin
Namecoin \cite{Namecoin} introduces an alternative Peer-to-Peer Domain Name System based on the crypto currency Bitcoin.
Like Bitcoins, Namecoins are mined searching for specific hash values which can then be used to register and update names and associated data (like PGP keys) using transactions. For lookups, access to a full copy of the history-based database (blockchain) is required, which does not scale. Gateways from DNS to Namecoin could be envisioned, but would reintroduce the DNS drawbacks.


\section{DNS Insights}

The collection of a user's DNS queries provides a discriminating database of your interests, Internet services used, and browsing behavior and allows user identification (\autoref{fig-dnsprivacy})\cite{DNSprefetch}.

\begin{figure}[t]
    \subfigure[Collecting DNS queries with date and time stamp allows tracking of users, their browsing behavior and can lead to user identification.]{\includegraphics[width=.48\textwidth]{fig/dnsprivacy}\label{fig-dnsprivacy}}
    \hfill
    \subfigure[Blocking authoritative name servers or eliminating specific records prevents successful lookup and ultimately leads to no connection to the target host.]{\includegraphics[width=.5\textwidth]{fig/dnsoutage}\label{fig-dnsoutage}}
	\caption{DNS problems addressed by opDNS\label{fig-miti}.}
\end{figure}


The simplest method to make some traffic less revealing is to not generate the traffic at all.
% TZ leichte verkuerzung
%This applies even more so to DNS, where low cache times (TTL) require the clients to request the resource records for a given domain name again every few minutes to few hours.
%These (often ridiculously) low TTL values are due to the lack of cache update; therefore, server operators prefer to err on the short side instead of being stuck with wrong DNS information in caches for weeks or months, rendering a service essentially unreachable during that period.
This applies even more so to DNS, where (often ridiculously) low cache times (TTL) require the clients to request the resource records for a given domain name again every few minutes to few hours.
This is due to the lack of cache update; therefore, server operators prefer to err on the short side instead of being stuck with wrong DNS information in caches, rendering a service essentially unreachable.
Larger servers tend to change their addresses only every few years, if at all; nevertheless, an address change could become unavoidable without notice in a few minutes or days.
% TODO: Da ist noch zu viel Dopplung drin. Bessere Formulierung?

This on-demand resolution is prone to (un)controlled outages \cite{ChinaDNS}.
Simply blocking name servers or certain DNS records makes name resolution impossible and prevents users from navigating to their target sites (\autoref{fig-dnsoutage}).

\section{Enhancing Privacy}

% TZ kuerzung
The two main reasons to perform a DNS lookup are
(1) to follow a new hyperlink % in a mail message or web page (`document')
or (2) to re-obtain the address for a host or service that has been previously contacted.

In this paper, we address the latter problem, where every time you open your laptop or use your smartphone, it fetches a large number of DNS records related to your mail accounts, communication platforms, or open web pages. We will show how to safeguard this important and highly identifying part of your DNS traffic without the need of cooperation with applications, infrastructure, or servers. However, the mechanism is general enough to be extensible to the first problem, but this requires the cooperation of developers and administrators.

The key observations behind Opportunistic Persistent DNS are as follows.
\begin{itemize}
    \item Real servers rarely change their address.
    \item Therefore, the result of the previous DNS query is still valid with very high probability.
    \item For services based on TLS, an address change can be reliably detected at connection setup.
\end{itemize}
Exceptions to these rules, such as private servers with a dynamic IP address, or load balancing and content distribution networks, will be discussed in \autoref{sec:problems}.

\begin{figure}[t]
	\centering
	\includegraphics[width=.5\textwidth]{fig/opdns}
	\caption{opDNS uses a local cache to reduce DNS traffic. Records are updated securely by the web servers using TLS. \label{fig-opdns}}
\end{figure}


\subsection{Basic opDNS operation}

An overview of the operation of opDNS is shown in \autoref{fig-opdns}: 
A local cache on the user's machine retains a copy of recently queried DNS records. During the lifetime of the record, as indicated by the TTL value, the local cache is used normally.

After the TTL expires, the behavior deviates from the current standards: Records related to TLS-enabled applications will be kept further, but with a special `expired' flag set. When a TLS-enabled application asks for the record, the expired entry will be returned, flagged. The application will then proceed to connect to the server with caution, e.g. with shorter timeouts, strong certificate test and the like. If this fails, the entire procedure is repeated after flushing the expired entry.


\subsection{Transparent opDNS}

The above basic process requires modifications to the application. As discussed previously, this will unacceptably slow down deployment. A connection setup for a TLS-enabled client application will do the following steps, among other things:
\begin{enumerate}
    \item Using the server name, obtain the address of the server using gethostbyname(), getaddrinfo() or a call to the DNS resolver library
    \item Set up a socket connection using connect()
    \item Start the TLS handshake (SSL\_connect() when using OpenSSL)
    \item Verify that the certificate actually belongs to the named server from step 1 (X509\_check\_host() when using OpenSSL)
\end{enumerate}
By hooking into the system libraries \cite{TLSinterposer}, the application's privacy awareness can be increased fully transparently.

The opDNS library intercepts several of the library functions from the above process and their relatives. By observing the calls to e.g. \textsf{getaddrinfo()} and \textsf{X509\_check\_host()}, it can determine that the application uses TLS to verify the server. Simplified, any successful \textsf{getaddrinfo()} call needs to be followed by \textsf{X509\_check\_host()} for the application to be considered to connect to that host only using TLS.\footnote{Of course, failures between these calls, such as the server being unreachable for \textsf{connect()}, should be taken into account.}

An application that has thus proven to contact this host only using TLS, will in the future also be served expired DNS records. A connection error or certificate verification failure will wipe the expired entry from the cache.

Under some circumstances, the opDNS library could even try and transparently handle the failure case when the cache is wrong by sending the DNS query and reconnecting. However, as these changes are very rare and plenty of the connections handled like that are background fetches such as mail, RSS feeds, or Ajax requests, they are likely to self-heal, latest when the user hits the browser's \emph{Refresh} button, we currently believe the added complexity is not worth it.


\subsection{Dealing with dynamic addresses}
\label{sec:problems}
%TZ damit ist das Mapping von dynamischen Adressen gemeint?
%MW …und Adressen, welche sich zeit- oder kontextabhängig ändern: CDNs etc.
Hosts using a dynamic DNS service to map their name to their dynamic address and content distribution networks can return addresses which change much more often than `regular' servers. Because opDNS only uses expired entries for TLS-secured connections which will fail if the wrong host is contacted, no sensitive information is revealed and no impersonation can take place. However, the timeouts or necessity to hit `reload' should be minimized. Therefore, opDNS will not cache records with very short TTL and only put addresses in cache after they have shown stability. An opDNS-aware application will be able to fine-tune the caching.


\subsection{DNS push}

%TZ kuerzung
One of the fundamental problems of DNS,
%already before beyond-TTL caching of opDNS,
is that there is no way to actively update DNS records.
%One of the reasons this has not been done
A reason for this 
is the perceived abuse potential from DNS cache poisoning \cite{DNSpoisoning}.

However, the game has changed: With the advent of DNSSEC, it is now possible to accept unsolicited records and still trust them to be genuine; to prevent Denial-of-Service attacks, it still remains a good idea to be selective about what to cache. This opens up new perspectives: A server that has been contacted can now authoritatively declare that another host has taken over his duties or that the load balancing setup has changed. This could be done as part of the TLS handshake, similar to OCSP stapling \cite{OCSPstapling}.

DNS Push could also have uses in ensuring the location privacy of mobile devices. For example, the address of a device could point to a proxy, which would forward messages. As soon as high-volume data has to be transmitted (e.g. a VoIP call), the proxy could perform an application layer protocol independent redirect using DNS push.

% TZ do we really have to change gehostbyname? Isn't resolvconf able to use local caches like dnscache anyway?
% MW Yes, but we need to 

% TZ auskommentiert
%\begin{itemize}
%	%\item How long should a record be cached, how to deal with the TTL of records.
%	\item When and how to update records
%	\item How to deal with load balancing % (akamai et al)
%	\item How to verify the authenticity of the target host and prevent hijacking
%	\item How can this be done transparently to client applications and network infrastructure
%\end{itemize}
%If addresses of the records change, or the TTL is very low ($< 60 sec$), then chances are, that the target host is either highly dynamic or also load balanced using another scheme.

%%% Conclusion and Future Work ################################################
\section{Conclusion and Future Work}

By changing the current `pessimistic' DNS caching into a much more optimistic variant, opDNS
\begin{enumerate}
	\item enhances privacy by disclosing as little personal information as possible,
	\item reduces the effect of outages by localizing name resolving, and
	\item improves client and server performance by reducing overall DNS traffic.
\end{enumerate}

Due to the reliance on TLS, even if the optimism turns out to be unfounded, privacy and security are not endangered. Even though opDNS can enhance unmodified applications, behavior and performance can be further tuned by making application and server opDNS-aware.
DNS push enables DNSSEC-based secure updates of DNS records using encrypted channels and trusted hosts, without the need of a third party or dedicated name servers.
The techniques can be implemented in standard libraries making them transparent to client applications and the network infrastructure alike.

We are working on implementing, testing, and evaluating opDNS. We could imagine an extended version of `DNS push' to be helpful in optimizing the efficiency and privacy of future connections and thereby make the Internet a safer and better place.

%Optimistic Persistent DNS has the advantage of safety, security, and simplicity. By just changing a client library, great benefits can be achieved. Thanks to DNSSEC, TLS-enabled Applications and servers do not need to be changed for this significant step forward, but their involvement can further boost the privacy and efficiency of the system.

%%% bibliography ##############################################################

\bibliographystyle{splncs03}
\bibliography{opDNS}

\end{document}

%%% notes

%The cache keeps a security bit for each entry, that keeps track of the hosts that are verified by TLS. The identity of hosts is verified using TLS. If the certificate changes, we most certainly have another host. Hosts that are verified, set the security bit to 1. This can be done using a custom starttls.

%This method can also be used to update DNS records. If the target host knows about infrastructure changes (ip, dns server ...) it informs the connecting host by adding DNS records to the TLS hello / handshake (?) (DNS "push").

%If connection fails, or the host cannot be verified, the DNS record is retrieved using ordinary DNS. To deal with connection errors and changing hosts (dynamic IPs, load balancing) we must be able to get feedback from connect, since DNS itself provides no meaningful resolution errors.

%\paragraph{Library changes:}
%\verb+gethostbyname+ and \verb+connect+ in libstdc? \verb+starttls+ in libssl?
%\begin{itemize}
%	\item gethostbyname queries and checks cache
%	\item connect does what exactly?
%	\item starttls evaluates certificate, DNS push updates, can update cache / safety bit
%\end{itemize}
