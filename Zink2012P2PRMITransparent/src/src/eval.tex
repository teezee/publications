%!TEX root = ../p2prmi.tex
% ============================================================================
% EVAL
% ============================================================================
\section{Evaluation}

	We implemented the {\em Local RMI Registry} with a DHT adapter to evaluate the performance of a distributed RMI registry with respect to
%
\begin{itemize}
	\item \verb+bootstrapping+ a DHT peer
	\item \verb+rebinding+ a remote service with the registry
	\item \verb+looking up+ a remote service at the registry
\end{itemize}
%
Our analysis consists of two parts; we first examine the behavior and performance of \ptprmi\ servers and then independently examine \ptprmi\ clients. Each server evaluation runs the following sequence:
%	
\begin{enumerate}
	\item create and bootstrap a new peer
	\item create and register a new remote object instance
	\item rebind the remote object with the registry
	\item unbind the remote object from the registry
	\item destroy DHT instance and peer
\end{enumerate}
%
To evaluate client performance, we run the following sequence of client operations:
%
\begin{enumerate}
	\item create and bootstrap a new peer
	\item look up a remote object through the local \ptprmi\ registry
	\item invoke a method through the received stub
	\item destroy DHT instance and peer
\end{enumerate}
		
	Our implementation uses the DHT of the BitTorrent client Vuze\cite{vuze} as a storage backend. The Vuze DHT is a modified Kademlia \cite{Maymounkov2002Kademlia} implementation and even though it is mainly intended for distributed trackers and tracker backup, its API allows writing arbitrary key\slash value pairs. Vuze is a very popular BitTorrent client and the Vuze DHT is one of the mostly used DHTs world-wide. The high amount of Vuze users and DHT nodes allow testing on a world-wide distributed platform. We also take this as an opportunity to provide an evaluation of the Vuze DHT itself. In the following, we shortly review Vuze's Kademlia implementation and how it affects the \ptprmi\ platform, and then proceed to present the performance results.
	
%
% ----------------------------------------------------------------------------
\subsection{Vuze DHT}
% ----------------------------------------------------------------------------
%
	Apart from an incomplete wiki page\cite{vuzedht} and open source code there is no documentation about Vuze's DHT implementation. Being an extended Kademlia DHT, the Vuze DHT supports Kademlia's four essential message types:
	
\begin{itemize}
	\item \verb+PING+: Check nodes for availability and keep routing tables up-to-date.
	\item \verb+STORE+: Store a (key, value) or a (key, array of values) pair.
	\item \verb+FIND_NODE+: Find the nearest node for a given key.
	\item \verb+FIND_VALUE+: Find the nearest node for a given key and retrieve the associated value(s).
\end{itemize}
	
	 In addition to these standard Kademlia message types Vuze also features some extensions like anti-spoofing, verification and NAT-traversal. Please note that there is no message type for the removal of an entry. Since entries are stored on many nodes which can freely join and leave the network, each node has to periodically republish its key mappings. An entry automatically expires after a certain amount of time, and is effectively removed if it is not 
republished periodically. For that reason, we do not include \verb+unbind+ performance in our evaluation, since it does not bear significance. Standard Kademlia uses a republish period of one hour that can even be increased under certain circumstances \cite{Maymounkov2002Kademlia}. In \cite{Geambasu2009Vanish, Crosby2007Analysis} the authors claim that entries in the Vuze DHT remain valid up to 8 hours after removal and that the peer's local data store is republished every 30 minutes. However, our empirical analysis has shown that Vuze nodes republish their mappings once every minute and that the entries remain valid up to one hour. It is unclear why Vuze uses such short intervals between publications. We assume that the intention is to prevent entries from being manipulated by malicious peers and that this falls into Vuze's anti-spoofing mechanisms.
	
	Since there is no active removal of entries, the Vuze implementation simply removes the entries from the local data store, thus effectively stopping the republication of the entry. As a result, if a remote service becomes unavailable, replications of its stub will still reside in the DHT for a period of about an hour. Clients that lookup the remote service would retrieve a stale entry and try to connect to an unavailable service. The remote registry could check service availability prior to returning the stub and only satisfy the lookup request if the service is still available. An alternative is to push responsibility to the client side. This approach seems reasonable because in a distributed system, clients need to 
be equipped with mechanisms to deals with stale remote references anyway.
 
%
% ----------------------------------------------------------------------------
\subsection{\ptprmi\ Vuze DHT Adapter}
% ----------------------------------------------------------------------------
%
	Due to Vuze's extended implementation of Kademlia there are certain pitfalls that need to be addressed when implementing a storage adapter for the Vuze DHT. For one there is a maximum packet size of 512 bytes. A stub or class file that exceeds the maximum byte limit has to be split into multiple parts for storage in the DHT. 
Fortunately, the Vuze DHT implementation supports the storage of (key, array of values) pairs. We exploit 
this feature to split longer stubs into multiple parts and save them under the same key (To be specific, the key is also split to indicate the presence of a split stub and the need for reconstruction. For the sake of simplicity we abstract from this detail).

	Multiple values can also be stored under a common key by different nodes. As mentioned earlier, the Vuze DHT is used as a distributed tracker. That is, the stored keys correspond to a file's infohash value (an identifier for a shared file), while the stored values represent file sources. Any peer that is a file source adds itself to the value array. While this is a very nice feature that allows storing collections of stubs or classes under single keys, it raises the question of how to deal with multiple return values. Currently, our implementation neglects this fact and simply considers the first returned value only. A more sophisticated approach could make better use of value arrays. Examples include a service directory for \ptprmi\ registries. This directory could list all services that are present in the network (allow implementation of \verb+list()+). In addition, each listed service can be associated with a list of service providers. In this case the \ptprmi\ registry could also provide load balancing functionality, by only returning the service closest to the requesting client or that has the least load. Regarding the codebase, instead of storing each class file individually, affiliated classes could be stored under a common key. The class file server would then collect all affiliated classes with one lookup and cache them locally to reduce pressure on the network by multiple lookups. This would also allow independent codebases that are addressed by unique identifiers.

	Vuze also implements some security related features like spoofing protection and storage verification, although, it remains unclear what techniques are applied. We believe that in this context Vuze also limits the amount of store requests issued by a node within a specific time frame. Our evaluation has shown that multiple fast subsequent stores lead to a state in which publications are no longer possible for a period of time. In this case we had to force-close the affected node and start a fresh one. This made \verb+rebind+ evaluation rather difficult and unfortunately limits Vuze's applicability for \ptprmi. With theses restrictions a server can only offer a limited amount of remote services before its store quota is exhausted.
	
%
% ----------------------------------------------------------------------------
\subsection{Results}
% ----------------------------------------------------------------------------
%
	\Cref{tbl:results} summarizes  the times we measured for all evaluated \ptprmi\ registry operations. The server evaluation ran 100 iterations of \verb+bootstrap+ and \verb+rebind+ operations, while the client evaluation ran 400 iterations of \verb+bootstrap+ and \verb+lookup+ operations. This explains why we measured a total of 500  \verb+bootstrap+ operations.

	\begin{table}[ht!]
		\centering
		\caption{Measured minimum, maximum, average and standard deviation for all operations \label{tbl:results} }
		\begin{tabular}{ l | r r r r r}
		\hline\hline
			& {\bfseries N} & {\bfseries min} & {\bfseries max} & {\bfseries avg} & {\bfseries stdev} \\	
		\hline
		{\bfseries bootstrap} & 500 &   4.54 s &  87.43 s & 32.48 s & 11.50 s \\
		{\bfseries rebind}    & 100 &  39.95 s & 134.43 s & 94.28 s & 17.46 s \\
		{\bfseries lookup}    & 400 &   0.35 s &  44.23 s &  3.54 s &  8.42 s \\
		\hline\hline
		\end{tabular}
	\end{table}


	It can  be seen that the measurements have high variance. This is especially notable for rebinds, which vary between $40$ and $134$ seconds. Bootstraps and lookups show a similar, but less pronounced, behavior. Curiously, lookup operations perform much faster than rebinds. Intuitively, one would assume both to perform similarly, especially because the Vuze DHT documentation\cite{vuzedht} states that "... store is performed on [...] 20 nodes [...] lookup does request [...] data from 20 nodes". However, store messages are significantly larger and must be propagated to every node that is a storage candidate or that is close to one. When a requesting node receives a response, it will update its routing table to increase performance. Lookups can then be satisfied much faster, because the path leading to the storing nodes is more efficient. 

	\begin{table}[ht!]
		\centering
		\caption{Correlation matrix of operation performances \label{tab:cor}}
		\begin{tabular}{ l | r r r}
		\hline\hline
		 	& {\bfseries bootstrap} & {\bfseries rebind} & {\bfseries lookup} \\
		\hline
		{\bfseries bootstrap} & $ 1.000$ & $-0.231$ & $-0.047$ \\
		{\bfseries rebind}    & $-0.231$ & $ 1.000$ & $    NA$ \\
		{\bfseries lookup}    & $-0.047$ & $    NA$ & $ 1.000$ \\
		\hline\hline
		\end{tabular}
	\end{table}

	\Cref{tab:cor} shows the correlation matrix (using Pearson's correlation coefficient) of the three measured operations. As expected, there is no significant correlation neither between bootstraps and rebinds nor between bootstraps and lookups.

\smallskip	

% BOOTSTRAP
	\Cref{fig:bootstrap} shows the histogram, normal distribution as well as the estimated kernel density of the bootstrap operation.
%	
\begin{figure}[ht!]
		\centering
		\includegraphics[width=.7\linewidth]{\gfx bootstrap}
		\caption{Histogram, normal distribution and estimated kernel density of the bootstrap operation \label{fig:bootstrap}}
\end{figure}
%
The measured and the estimated bootstrap performance closely resemble a normal distribution with its peak at 32.5sec. 
The similarity between the two curves indicate the high statistical significance of our measurement results.
Because bootstrapping is a necessary step in P2P systems, improvement of its performance would certainly be 
beneficial; however, because bootstrapping is performed exactly once per peer --- in our context: once per RMI server and once per RMI client --, the measured performance seems acceptable for many applications.

\smallskip

% REBIND
	Analogously, \Cref{fig:rebind} shows the histogram, normal distribution as well as the estimated kernel density of the rebind operation.
%	
\begin{figure}[ht!]
		\centering
		\includegraphics[width=.7\linewidth]{\gfx rebind}
		\caption{Histogram, normal distribution and estimated kernel density of the rebind operation \label{fig:rebind}}
\end{figure}
%
Rebinds, like bootstraps, closely follow a normal distribution, with the peak at around 95sec. Rebinds have, however, a much 
greater variance, and can take quite a long time. This can pose a problem for servers that want to serve many remote objects. In such a case it would be best to aggregate store requests and reduce the number of messages. Naturally, store messages can also be sent in parallel, the server does not have to wait for a reply before registering the next remote object.

	A combined average bootstrap and rebind duration of approximately 2 minutes might be acceptable for the server side of many applications. However, faster DHT store operations would clearly be desirable, to make the rebind times acceptable for an even broader array of applications.	

\smallskip

% LOOKUP
	Finally, \Cref{fig:lookup} shows the histogram, distribution and estimated kernel density of the lookup operation.
%
	\begin{figure}[ht!]
		\centering
		\includegraphics[width=.7\linewidth]{\gfx lookup}
		\caption{Histogram, normal distribution and estimated kernel density of the lookup operation\label{fig:lookup}}
	\end{figure}
%
Compared to bootstraps and rebinds, lookups show quite a different behavior. While the great majority of about 80\% of 
lookup operations succeed within 1sec, the remaining values seems almost randomly scattered over a range of approximately 44sec, which is the longest measured lookup time. There seems to be a second, small peak at about 32secs whose statistical significance is not clear.

From an application and user perspective the lookup performance is most crucial. As can be seen, for a great lot of 
lookup requests, response times are very short. The few spikes, however, may pose serious problems to 
client applications, especially when many stubs are requested sequentially. While the reason remains unclear, we 
assume the long response times to be related to churn and the respective reorganization of the stored data. 
Vuze stores the data on the 20 nodes that are closest to the key space. Each time one of those nodes leaves the 
network, or another one that is closer to the key space arrives, the data has to be reorganized and routing tables have to be updated. Since Vuze requests the values from all those 20 nodes for verification reasons, lookup performance will suffer at times when data and routing tables are currently being restructured.

\subsection{Discussion}
	Performance of the Vuze DHT leaves room for improvement, both in terms of average response times and 
	in terms of predictability. Especially store requests can take quite long, but also lookups take a long time under certain, unpredictable circumstances. Vuze is a classic file sharing infrastructure, and thus it does not come as a surprise that its DHT is optimized towards distributed tracker lists. Considering this kind of application, performance of the Vuze DHT is certainly acceptable. For distributing applications, however, it might not be the best choice. While rebind duration is acceptable for most applications, a long lookup time may not be since this requires special care on the client side.
	
	A DHT targeted towards \ptprmi\ applications needs to specifically optimize lookup time. Unfortunately, this competes with storage verification. However, there might be other solutions than retrieving the value from all storage nodes. A subset of nodes might be enough, the stored values could be signed, or verification could be pushed to a higher layer where churn and network delay is not present. It is also worth mentioning that churn might not be as high in a \ptprmi\ network as in a file sharing network since the semantics of distributed computing demands a higher degree of availability for both servers and clients. To be of any use in a distributed application, servers must stay available at least
long enough to compute invoked methods while clients require the results for further processing. In this regard, a dedicated \ptprmi\ DHT that is optimized for distributed applications might be a better solution. On the downside, an exclusive DHT requires a critical mass of storage nodes to be effective and efficient. A solution would be to have alternative backup storage available, either in the form of dedicated database servers or other DHTs like Vuze.
	
	Even though the results suggest that the distribution of object-oriented applications on \ptprmi\ networks is not yet generally applicable, we strongly believe that the proposed methodology is sound, and that an optimized distributed storage can satisfy performance demands.

% END EVAL
% ============================================================================
