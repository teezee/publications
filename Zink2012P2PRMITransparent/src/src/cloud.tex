%!TEX root = ../p2prmi.tex
% ============================================================================
% CLOUD
% ============================================================================
\section{Java \ptprmi\ services}
\label{Java ptprmi services}
%
	The RMI registry is the central instance in the RMI infrastructure. Remote object servers export their services to the registry. Clients use the registry to lookup remote object stubs that include information about the object's server and class. Stubs can also include their own custom socket factories and information regarding the codebase.
In order to \emph{transparently} distribute RMI services, the following problems have to be solved.
\begin{enumerate}
	\item\label{lbl:registry} \emph{Lookup}: Both clients and servers must be able to use a remote RMI registry for lookup and export.
	\item\label{lbl:codebase} \emph{Retrieval}: Clients must be able to obtain class files for unknown classes from a {\em codebase}.
	\item\label{lbl:remote} \emph{Method Invocation}: Remote object services must be accessible even behind firewalls and NAT boxes.
\end{enumerate}

	Accessibility of remote objects (problem \ref{lbl:remote}) can be ensured by exploiting RMI's feature of transporting custom socket factories within the stub, which has been discussed in-depth in \cite{Haase2009Punching}\cite{Maier2011NAT}. It is reviewed in \Cref{sec:remote} for the sake of completeness.

	To provide a distributed registry and remote object service (problem \ref{lbl:registry}) we utilize a {\em Distributed Hash Table} (DHT). The basic idea is to store the service stubs in the DHT. The \ptprmi\ registry simply provides bindings -- or adapters -- to the DHT. Remote objects that are exported to the \ptprmi\ registry are bound to the DHT using a unique {\em URI} as key. They can be looked up by any \ptprmi-aware peer. Please note that we distinguish between two different types of peers: (1) {\em DHT peers} that provide the DHT service, and (2) {\em \ptprmi\ peers} that provide and consume RMI related services. It is \emph{not} a requirement for \ptprmi\ peers to also be DHT peers. It is also possible to join 
the \ptprmi\ network without participating in the DHT network through the use of public gateways that remotely 
provide the necessary binding.

	There is also the issue of where to store the class files and how to make them accessible for peers (problem \ref{lbl:codebase}). When a client deserializes a server stub, it first searches its local classpath, and if unsuccessful,  inspects the \verb+java.rmi.server.codebase+ property, which is set by the remote object server and included in the 
stub. Usually the codebase is a HTTP or FTP server that offers the class files for download. However, the mechanism
 can be exploited to distribute the codebase in P2P networks. 

	In the following, we briefly review \cite{Haase2009Punching} that addresses problem \ref{lbl:remote}, and then elaborate on problem  \ref{lbl:registry}. While the focus of this work is the distribution of the RMI registry we also discuss problem \ref{lbl:codebase} to provide a complete solution for a Java \ptprmi\ network.
	
%
% ----------------------------------------------------------------------------
\subsection{NAT-traversal with RMI}\label{sec:remote}
% ----------------------------------------------------------------------------
%
	In \cite{Haase2008Remote} the authors show how custom RMI socket factories can be used to enable NAT-traversal for remote object servers. The mechanism is illustrated in \Cref{fig:rmihp}.

\begin{figure}[ht!]
	\centering
	\includegraphics[width=.7\linewidth]{\gfx rmihp}
	\caption{NAT-traversal for remote objects.}
	\label{fig:rmihp}
\end{figure}

	The remote object server first connects to a publicly available rendezvous server (1). It then exports the remote object by registering its stub that includes the custom \emph{NAT-traversal socket factory} (2). A client receiving the stub (3) and trying to connect to the remote object transparently uses the custom socket factory. This socket factory 
directs the client to the rendezvous server (4), which helps start (5) the mutual hole punching process between client and server. It should be noted that the stub includes a socket factory \emph{instance}; the client either needs to have the class definition locally or download it from a remote codebase, see problem \ref{lbl:codebase} in section \ref{Java ptprmi services}.

	This scheme practically allows connectivity in any environment since any kind of NAT-traversal mechanism can be used and communicated with the clients through the stub. It requires, however, additional well-known rendezvous servers for relaying or negotiating the actual client to server connection. In this paper we will not evaluate NAT-traversal techniques and their performance since this has already been done in \cite{Maier2011NAT}.
	
%
% ----------------------------------------------------------------------------
\subsection{Private \ptprmi\ service} 
% ----------------------------------------------------------------------------
%
	To distribute the RMI registry in a P2P network, the most straight-forward approach is to equip all peers with a local \ptprmi\  registry that connects to the DHT. This design is depicted in \Cref{fig:p2prmi}. In this architecture, remote object servers simply use their local \ptprmi\ registry to bind the remote object to the DHT. A server behind NAT takes advantage of the NAT-traversal techniques described in \cite{Haase2009Punching} and includes the NAT-traversal socket factory in its exported stub. Clients, respectively, look up the stubs using their own local \ptprmi\ registry.

	This design basically eliminates the need for a central, publicly reachable registry service at the expense of excluding unmodified clients from looking up remote objects. The peer-local registries do not provide services but merely allow local utilization of the DHT as storage for RMI. Since there is no public RMI registry service, ordinary clients cannot use this \ptprmi\ network.

	Peers must also know the keys of remote objects prior to the lookup. This is true for any RMI architecture and not a new problem. Registries implement a \verb+list()+ function that lists all remote objects which have been exported using the queried registry. Here, \verb+list()+ can only be called locally and thus only returns all objects exported by the local peer. Listing all globally available remote objects requires the DHT to support approximate string searches like Cubit \cite{Wong2008Approximate}. Whether or not global listing is possible therefore depends on the DHT implementation.

\begin{figure}[ht!]
	\centering
	\includegraphics[width=.7\linewidth]{\gfx p2prmi}
	\caption{\ptprmi. All peers can export and lookup remote objects using their local registry to connect to the DHT network. Clients that are not peers in the DHT network cannot use the service.}
	\label{fig:p2prmi}
\end{figure}

%
% ----------------------------------------------------------------------------
\subsection{\ptprmi\ with dedicated Registry Servers}
% ----------------------------------------------------------------------------
%
	The reference Java RMI registry (\verb+rmiregistry+) does not persistently store its bindings and requires servers to run collocated with the registry.  \cite{Haase2008Remote} proposes a {\em Remote RMI Registry} that uses persistent storage to store stub information and can be used by remote servers. Instead of local file systems or data bases, the remote RMI registry can also utilize a DHT -- or any other distributed storage -- as storage backend. This {\em Remote \ptprmi\ Registry} participates in the DHT network and exposes the registry service to remote clients and object servers. It basically serves as a gateway to the \ptprmi\ network for legacy clients and servers. They can use this registry to lookup and export remote objects, thus effectively becoming \ptprmi\ peers without the need to participate in the DHT network themselves (\Cref{fig:p2prmidedicated}).

The use of \ptprmi\ gateways solves incompatibility issues with unmodified clients and servers at the expense of re-introducing dedicated registry servers. This solution is useful as an interim step during the roll-out of the architecture presented in the next section, until enough \speers\ become available to provide a reliable infrastructure.

\begin{figure}[ht!]
	\centering
	\includegraphics[width=.7\linewidth]{\gfx p2prmidedicated}
	\caption{\ptprmi\  with dedicated servers. The servers provide the RMI registry service as a gateway to the \ptprmi\ network, allowing external clients to lookup and export remote objects, effectively becoming a \ptprmi\ peer.}
	\label{fig:p2prmidedicated}
\end{figure}

%
% ----------------------------------------------------------------------------
\subsection{\ptprmi\ with \speers}
\label{ptprmi with speers}
% ----------------------------------------------------------------------------
%
	To eliminate the need for dedicated \ptprmi\ gateways, we suggest an approach that is influenced by well-known P2P networks like {\em Gnutella} and {\em Skype} in the form of {\em \speers}. Highly available peers can be promoted to \speers, which provide a number of services that are otherwise difficult to deploy in a distributed manner (\Cref{fig:p2prmisuperpeer}). In particular, a  \speer\ can take on the role of a remote \ptprmi\ registry, serving as a gateway to the \ptprmi\ network. In addition, \speers\ can also work as rendezvous or relay servers and thus provide NAT-traversal services for NAT'ed peers. Finally, \speers\ can provide a distributed codebase service as described in the next section.

	The use of \speers\ removes the need for central gateways and allows for purely self-organizing, highly robust, and scalable \ptprmi\ networks. The cost for decentralization, however, is bootstrapping. To participate in the \ptprmi\ network a client or server must either have access to the DHT or know at least one active \speer\ to make use of its remote registry service. This bootstrapping problem is present in any P2P network and is not unique to the \ptprmi\ network presented here. Therefore, multiple solutions exist, both centralized and decentralized, including hotlists, random address probing, or even utilizing service records in DNS. Since a RMI client needs to know the address of a RMI registry anyway, we assume that a potential client or peer is aware of an existing \speer\ or other gateway. This information can be provided as a hotlist, configuration file or even as a system property. For readers interested in decentralized bootstrapping approaches we refer to literature such as \cite{Bless2009Decentralized,GauthierDickey2008bootstrapping,Dinger2009Bootstrapping}.

\begin{figure}[ht!]
	\centering
	\includegraphics[width=.7\linewidth]{\gfx p2prmisuperpeer}
	\caption{\ptprmi\ with \speers. Highly available peers can be promoted to \speers, which offer RMI and connectivity services.}
	\label{fig:p2prmisuperpeer}
\end{figure}

%
% ----------------------------------------------------------------------------
\subsection{Distributing Class Files}
% ----------------------------------------------------------------------------
%
	As described earlier in this section, one challenge for highly distributed RMI systems is for clients to retrieve all
class definitions that are necessary to deserialize the server stubs they looked up at the registry.
If a client cannot find such a class definition in its local {\texttt classpath} -- which will mostly be the case in a highly distributed system -- the client retrieves it from the {\emph codebase} that is included in the remote object's stub. 
For the implementation of this codebase, several options can be thought of
 (\Cref{fig:codebases}):

\begin{enumerate}
	\item\label{lbl:cbs} Dedicated codebase server
	\item\label{lbl:dht} Local DHT-based codebase
	\item\label{lbl:cfs} Peer-based class file server
	\item\label{lbl:scb} \Speer-based codebase
\end{enumerate}

\begin{figure}[ht!]
	\centering
	\includegraphics[width=.7\linewidth]{\gfx codebases}
	\caption{Distributing class files using (1) a dedicated server (2) the DHT to store code (3) the remote object's server (4) a \speer-based codebase}
	\label{fig:codebases}
\end{figure}

	Solution \ref{lbl:cbs} is to use a globally reachable, dedicated codebase server. Peers that export objects upload the class definitions to the dedicated server that clients can download from. While this is the easiest solution, it is also the most centralized one; it should be noted, however, that a client needs to contact a codebase server \emph{per 
class definition}, as compared to registries that are contacted \emph{per object instance}. Hence, scalability is generally a lesser issue for codebase servers than it is for registries.
	
	A more decentralized solution is to use the DHT as storage for class files (solution  \ref{lbl:dht}). The exporting peer stores the class file in the DHT and sets the codebase property to a custom URL format, such as 
\verb+dht://package.classname+. Any client that is also a DHT peer can then lookup the class files directly in the DHT and load them from there. Thus \verb+POST+ and \verb+GET+ of code are both performed locally. This approach requires a custom {\em ClassLoader} with DHT bindings on the client side and only works for clients that are both \ptprmi\ and DHT peers. 

	As with the RMI registry, the codebase service can be directly provided by remote object servers (solution \ref{lbl:cfs}). That is, the peers exporting remote services function as a codebase server for their own exported objects. This requires serving peers to provide at least two services: the actual remote object(s), and the class file server. This can lead to a notable overhead, especially in the presence of NAT boxes. However, the overhead is constant since only one class file service is needed for all exported remote objects. The class file server basically is a stripped down HTTP server that only accepts \verb+GET+ requests for class files. These requests are satisfied by serializing the requested classes directly from the peer's local classpath, requiring no additional storage space.

	An enhanced class file service can also be outsourced to \speers\ (solution \ref{lbl:scb}). As in solution \ref{lbl:dht}, the class files are stored in the DHT but the codebase service is provided by \speers, that allow both the download and the upload of class files. Remote object servers that have access to the DHT can store class files directly in the DHT. Other remote servers use the \speer's class file service to upload the code that is then stored in the DHT by the \speer. In all cases, remote object servers set the \verb+codebase+ property to any known and available \speer. Clients then can use the \speer\ as if it were a dedicated codebase. This relieves ordinary peers from the need to provide an additional service at the cost of putting more load on the \speers. But it allows any \speer\ to locate and provide every class file stored in the DHT and as such provides a codebase service that is distributed over the whole \ptprmi\ network. In addition it is transparent to both clients and remote object servers, since they only need to know the address of any available \speer\ to distribute and retrieve class files.
	
%
% ----------------------------------------------------------------------------
\subsection{\ptprmi\ Architecture}
% ----------------------------------------------------------------------------
%
	\Cref{fig:components} shows an overview of the \ptprmi\ architecture. The framework is lightweight, modular and extremely flexible. Depending on the desired levels of transparency and scalability nearly all components can be individually implemented, deployed, extended, and even replaced on demand. A \ptprmi\ network can be very heterogenous with services being distributed among peers, \speers, gateways and agnostic servers in any desired combination. Non-transparent \ptprmi\ networks are possible through utilizing DHT-based, local-only registries and codebases.

\begin{figure}[ht!]
	\centering
	\includegraphics[width=0.5\linewidth]{\gfx components}
	\caption{\ptprmi\ architecture. Distributing RMI services can be divided into three challenges that can be individually addressed and implemented}
	\label{fig:components}
\end{figure}

	Distribution of the Registry service can be achieved through a {\em Local RMI Registry}, used by \ptprmi\ peers to bind and lookup remote objects to and from the DHT. The {\em Remote RMI Registry} exposes the registry service to \ptprmi\ agnostic clients and object servers. It can be deployed on \speers\ or dedicated gateways alike.

	Distributing the codebase is slightly more complex. \ptprmi\ peers could directly store class files to the DHT and load them using a {\em Custom ClassLoader} at the cost of losing transparency. Alternatively, peers store class files in the DHT but provide a \verb+codebase+ property set to known a {\em ClassFile GetServer}. This service can be provided by the remote object server itself, any \speer\ or other dedicated gateway. In addition, the class file service can offer both down- and upload functionality ({\em ClassFile Server}) to also allow \ptprmi\ agnostic remote object servers to upload class files. Again, this additional functionality can be provided by \speers\ or dedicate servers.

	Finally, reachability of exported remote objects is guaranteed by using {\em Custom Socket Factories}, which, depending on the environment and application demands, provide NAT-traversal methods. Since both the codebase as well as the custom socket factories are metadata of the remote object's stub, any client can transparently make use of them.
% END CLOUD
% ============================================================================
