%!TEX root = ../x509auth.tex
\section{Analysis}\label{sec:analysis}


Our solution perfectly fulfills the goals stated in \Cref{sec:model}, as follows:
\begin{enumerate}
	\item Unprivileged users or operations incur no extra burden.
	\item Privileged operations are performed with zero effort for the basic (\emph{software certificate}) case of \Cref{img:evolution} and only a little bit more effort for the advanced protection modes. This applies to both setup and actual use.
	\item The credentials that can be stolen do not provide any long-term access to privileged operations. If the computer is infected and under carefully targeted control, the current session may be hijacked; but there is no known general protection against this risk.
	\item The web application hidden behind the 2FA does not need to be modified. Minimal changes are required to the web server (or a proxy inserted) for a first strong protection step.
\end{enumerate}

As administrative commands can be identified by URL path or parameters, environment-specific definitions of what is considered a privileged operation can be easily applied. As access to these URLs is only granted to specific certificates, our 2FA solution even protects against several implementation or verification errors inside the application itself. 

As no on-line verification is needed with a central service (unlike other services like U2F or linOTP), this system does not increase the damage caused by a partial network or service outage at the university.

The user certificates distributed for this purpose can be used to secure other services as well, including email, or vice versa, if policy allows. Then two birds are caught with one stone.


\section{Evaluation}\label{sec:evaluation}

We successfully implemented our client certificate-based authentication in our support front-end application used by supporters in our public help desk center.
The workstations used to login are publicly exposed and leakage of passwords is a real threat.
We already use the DFN PKI for x.509 certificates which allowed us an easy transition towards the certificate authentication.

Each support staff member receives a USB cryptoken to store his personal certificate.
The workstations' browsers are configured to access the certificate on the token, which is secured by a person identification number (PIN).

To login to the support front-end the supporter has to provide the token, his PIN and then his personal password to successfully log into the application.

We conducted a user study among the support staff to evaluate the quality of the solution.
Unfortunately, only six staff members reacted.
However, according to a study by Nielsen \cite{Nielsen1993Mathematical} a small sample is enough to yield realistic results.
The following tables present our results which are promising and in favor of our solution.

\Cref{tbl:awareness} shows questions about the awareness of the user regarding the security of his password and the offered solution.

\begin{table}[]
	\centering
	\caption{Questions regarding user awareness}
	\label{tbl:awareness}
	\hrule
	\begin{tabular}{lll}
		{\bfseries Question} & {\bfseries Yes} & {\bfseries No} \\
		Do you have to enter your password on untrusted devices & 83\% & 17\% \\
		Do you fear that your password is in danger & 83\% & 17\% \\
		Do you think the solution is justified  & 66.7\% & 33.3\% \\
	\end{tabular}
	\hrule
\end{table}

We also wanted to know the impact the solution has on the users' daily work and how well it is perceived (\cref{tbl:experience}).
\begin{table}[]
	\centering
	\caption{Questions regarding user experience}
	\label{tbl:experience}
	\hrule
	\begin{tabular}{lllll}
		{\bfseries Question} & {\bfseries not} & {\bfseries slightly} & {\bfseries moderate} & {\bfseries very} \\
		How much does the solution disrupt your work & 16.7\% & 16.7\% & 16.7\% & 50\% \\
		How satisfied are you with the solution  & 16.7\% & 16.7\% & 16.7\% & 50\% \\
	\end{tabular}
	\hrule
\end{table}

Finally, we asked if the users know other methods for multi-factor authentication, and if they perceive our solution as better, worse oder equally good.

\begin{table}[]
	\centering
	\caption{Comparison to other solutions}
	\label{tbl:comparison}
	\hrule
	\begin{tabular}{lllll}
		{\bfseries solution} & {\bfseries don't know} & {\bfseries better} & {\bfseries worse} & {\bfseries equal} \\
		Smartcard & 16.7\% & 16.7\% & 0\% & 66.7\% \\
		SMS-TAN & 0\% & 16.7\% & 83.3\% & 0\% \\
		U2F & 16.7\% & 16.7\% & 0\% & 66.7\% \\
		HOTP/TOTP & 16.7\% & 16.7\% & 33.3\% & 33.3\% \\
	\end{tabular}
	\hrule
\end{table}

In summary, most users are aware that their credentials are in danger and that additional security measures are justified.
However, users feel that the additional security impedes their daily work.
Compared to other solutions our proposed scheme is always perceived as at least equally well or even better.

%\todo{Usability study mit 5 Leuten ist eigentlich genug \cite{Nielsen1993Mathematical}, weil es schon 80\% der Usability-Probleme findet; siehe \url{https://www.nngroup.com/articles/why-you-only-need-to-test-with-5-users/}}