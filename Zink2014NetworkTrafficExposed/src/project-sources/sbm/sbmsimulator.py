class SBM (object):
    ip = 0
    lut = []
    output = True

    def __init__(self, lut):
        self.lut = [op[::-1] for op in lut]
    
    def x01(self,i,o):
        return (i[0] == o[0]) and (i[1] == o[1])

    def x23(self,i,o):
        return (i[2] == o[2]) and (i[3] == o[3])

    def x45(self,i,o):
        return (i[4] == o[4]) and ((i[5] == o[5]) | o[8])

    def x67(self,i,o):
        return (i[6] == o[6]) and (i[7] == o[7])

    def xc(self,i,o):
        '''match case-insensitive'''
        return self.x01(i,o) and self.x23(i,o) and self.x45(i,o)

    def xb(self,i,o):
        '''match byte'''
        return self.xc(i,o) and (self.x67(i,o) or o[8] and o[6])

    def xx(self,i,o):
        '''match don't care'''
        return o[8] and o[7] #and (not o[6])

    def xs(self,i,o):
        return (o[8] and (not o[7]) and (not o[6]) and False)
    
    def xout(self,i,o):
        return self.xb(i,o) or self.xx(i,o) or self.xs(i,o)
        
    def encode(self,i):
        i = bin(ord(i))[2:]
        i = -len(i) % 8 * '0' + i
        i = [bool(int(x)) for x in i[::-1]]
        o = [bool(int(x)) for x in self.lut[self.ip]]
        return i,o

    def match(self,text):
        self.output = True
        self.ip = 0
        for c in text:
            i,o = self.encode(c)
            #print c, bin(ord(c))[2:], i
            self.output &= self.xout(i,o)
            if self.output:
                print "ok:", c
                self.ip += 1
            else:
                print "ko:", c
                break
        return self.output


class SBM1 (SBM):
    def xs0(self,i,o):
        '''encode set 0'''
        i75 = (not i[5]) and (not i[6]) and (not i[7])
        i43 = (not i[3]) and (not i[4])
        c0 = (not i[0]) and (not i[1]) and (not i[2]) 
        c1 = (i[0]) and (i[1]) and (not i[2])
        c2 = (i[0]) and (not i[1]) and (i[2])
        return ((not o[0]) and (c0 or c1 or c2) and i75 and i43) 

    def xs1(self,i,o):
        '''encode set 1'''
        # [D-S] == [\x44-\x53]
        # wildcards rightmost 2
        # nand i[3] i[2]
        i75 = (not i[7]) and i[6] and (not i[5])
        i42 = (not i[4] and (i[3] or i[2])) or (i[4] and not (i[3] or i[2]))
        return o[0] and i75 and i42
       
    def xs(self,i,o):
        return (o[8] and (not o[7]) and (not o[6]) and (self.xs0(i,o) or self.xs1(i,o)))


def test():
    #for i in range(ord('C'), ord('U')): print chr(i), hex(i), "0" + bin(i)[2:]
    lut = ['101000001','101000010','101000011','110000000','110000000','110000000','110000000','100000000','000110000','100000001']
    sbm = SBM1(lut)

    print sbm.match('Abc..,-\x000D') # True
    print
    print sbm.match('AbC..,-\x030S') # True
    print
    print sbm.match('AbC..,-\x050F') # True
    print
    print sbm.match('ABC..,-\x100R') # False (set)
    print
    print sbm.match('ABC..,-\x130R') # False (set)
    print
    print sbm.match('ABc..,-\x050C') # False (range)
    print
    print sbm.match('ABC..,-\x050T') # False (range)
    print

    

if __name__ == '__main__':
    test()
