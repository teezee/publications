/** Copyright (C) 2011 Thomas Zink, Uni Konstanz
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.unikn.disy;

import java.util.Arrays;

import com.turn.ttorrent.client.Client;

/**
 * @author zink
 */
public final class DisyTorrent {

	private static void usage() {
		System.out.println("Runs either a client, tracker or creates " +
				"a torrent metainfo file depending on arguments.\n");
		System.out.println("Usage:" + 
				"java -jar ttorrent<version>.jar <client|tracker" +
				"|create> <arguments>");
		System.out.println("\tclient\t-\tstart the client");
		System.out.println("\ttracker\t-\tstart the tracker");
		System.out.println("\ttorrent\t-\tcreate a torrent metainfo file");
		System.out.println("\targuments\t-\tadditional arguments passed " +
				"to mainclass used.");
	}
	
	/**
	 * Wrapper class to run different main classes.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		if (args.length < 1) {
			usage(); return;
		}
		
		String[] newargs = Arrays.copyOfRange(args, 1, args.length);
		
		// java fortunately supports switching string, ... wait, it doesn't
		String mainclass = args[0].toLowerCase();
		if (mainclass.equals("client")) {
			Client.main(newargs);
		} else if (mainclass.equals("tracker")) {
			RunTracker.main(newargs);
		} else if (mainclass.equals("torrent")) {
			MakeTorrent.main(newargs);
		} else {
			usage(); return;
		}
	}
}
