/** Copyright (C) 2011 Turn, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.turn.ttorrent.client;

import com.turn.ttorrent.common.Torrent;
import com.turn.ttorrent.client.peer.SharingPeer;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.nio.ByteBuffer;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** Incoming peer connections service.
 *
 * <p>
 * Every BitTorrent client, BitTorrent being a peer-to-peer protocol, listens
 * on a port for incoming connections from other peers sharing the same
 * torrent.
 * </p>
 *
 * <p>
 * This ConnectionHandler implements this service and starts a listening socket
 * in the first available port in the default BitTorrent client port range
 * 6881-6889. When a peer connects to it, it expects the BitTorrent handshake
 * message, parses it and replies with our own handshake.
 * </p>
 *
 * <p>
 * Outgoing connections to other peers are also made through this service,
 * which handles the handshake procedure with the remote peer. Regardless of
 * the direction of the connection, once this handshake is successfull, all
 * {@link IncomingConnectionListener}s are notified and passed the connected
 * socket and the remote peer ID.
 * </p>
 *
 * <p>
 * This class does nothing more. All further peer-to-peer communication happens
 * in the {@link com.turn.ttorrent.client.peer.PeerExchange PeerExchange}
 * class.
 * </p>
 *
 * @author mpetazzoni
 * @see <a href="http://wiki.theory.org/BitTorrentSpecification#Handshake">BitTorrent handshake specification</a>
 */
public class ConnectionHandler implements Runnable {

	private static final Logger logger = LoggerFactory.getLogger(ConnectionHandler.class);

	public static final int PORT_RANGE_START = 9990; //6881;
	public static final int PORT_RANGE_END = 9999; //6889;

	private SharedTorrent torrent;
	//< obfuscated handshake extension
	private byte[] id;
	private boolean forceObfuscation = false;
	//> obfuscated handshake extension
	private ServerSocket socket;
	private InetSocketAddress address;

	private Set<IncomingConnectionListener> listeners;

	private Thread thread;
	private boolean stop;

	/** Create and start a new listening service for out torrent, reporting
	 * with our peer ID on the given address.
	 *
	 * This binds to the first available port in the client port range
	 * PORT_RANGE_START to PORT_RANGE_END.
	 *
	 * @param torrent The torrent shared by this client.
	 * @param id This client's peer ID.
	 * @param address The address to bind to.
	 * @throws IOException When the service can't be started because no port in
	 * the defined range is available or usable.
	 */
	//<> obfuscated handshake extension
	ConnectionHandler(SharedTorrent torrent, byte[] id, InetAddress address, boolean force)
		throws IOException {
		this.torrent = torrent;
		this.id = id;
		this.forceObfuscation = force;

		this.socket = new ServerSocket();

		// Bind to the first available port in the range
		// [PORT_RANGE_START; PORT_RANGE_END].
		for (int port = ConnectionHandler.PORT_RANGE_START;
				port <= ConnectionHandler.PORT_RANGE_END;
				port++) {
			InetSocketAddress tryAddress =
				new InetSocketAddress(address, port);

			try {
				this.socket.bind(tryAddress);
				this.address = tryAddress;
				break;
			} catch (IOException ioe) {
				// Ignore, try next port
				logger.info("Could not bind to " + tryAddress + "!");
			}
		}

		if (!this.socket.isBound()) {
			throw new IOException("No available port for BitTorrent client!");
		}

		this.listeners = new HashSet<IncomingConnectionListener>();
		this.thread = null;
	}

	/** Return the full socket address this service is bound to.
	 */
	public InetSocketAddress getSocketAddress() {
		return this.address;
	}

	/** Register a new incoming connection listener.
	 *
	 * @param listener The listener who wants to receive connection
	 * notifications.
	 */
	public void register(IncomingConnectionListener listener) {
		this.listeners.add(listener);
	}

	/** Start accepting new connections in a background thread.
	 */
	public void start() {
		if (!this.socket.isBound()) {
			throw new IllegalStateException("Can't start ConnectionHandler " +
					"without a bound socket!");
		}

		this.stop = false;

		if (this.thread == null || !this.thread.isAlive()) {
			this.thread = new Thread(this);
			this.thread.setName("bt-serve");
			this.thread.start();
		}
	}

	/** Stop accepting connections.
	 *
	 * <b>Note:</b> the underlying socket remains open and bound.
	 */
	public void stop() {
		this.stop = true;

		if (this.thread != null && this.thread.isAlive()) {
			this.thread.interrupt();
		}

		this.thread = null;
	}

	/** The main service loop.
	 *
	 * The service waits for new connections for 250ms, then waits 750ms so it
	 * can be interrupted.
	 */
	@Override
	public void run() {
		try {
			this.socket.setSoTimeout(250);
		} catch (SocketException se) {
			logger.warn(se.getMessage());
			this.stop();
		}

		while (!this.stop) {
			try {
				this.accept();
			} catch (SocketTimeoutException ste) {
				// Ignore and go back to sleep
			} catch (IOException ioe) {
				logger.warn(ioe.getMessage());
				this.stop();
			}

			try {
				Thread.sleep(750);
			} catch (InterruptedException ie) {
				// Ignore
			}
		}

		try {
			this.socket.close();
		} catch (IOException ioe) {
			// Ignore
		}
	}

	/** Return a human-readable representation of a connected socket.
	 *
	 * This returns a <em>host:port</em> string representing the given socket.
	 *
	 * @param s The socket to represent.
	 */
	private String socketRepr(Socket s) {
		return new StringBuilder(s.getInetAddress().getHostName())
			.append(":").append(s.getPort()).toString();
	}

	/** Accept the next incoming connection.
	 *
	 * When a new peer connects to this service, wait for it to send its
	 * handshake. We then parse and check that the handshake advertises the
	 * torrent hash we expect, then reply with our own handshake.
	 *
	 * If everything goes according to plan, notify the
	 * <code>IncomingConnectionListener</code>s with the connected socket and
	 * the parsed peer ID.
	 */
	private void accept() throws IOException, SocketTimeoutException {
		Socket socket = this.socket.accept();

		try {
			logger.debug("New incoming connection ...");
			Handshake hs = this.validateHandshake(socket, null);
			//< obfuscated handshake extension
			this.sendHandshake(socket, hs.getPeerId(), hs.isObfuscated());
			//> obfuscated handshake extension
			this.fireNewPeerConnection(socket, hs.getPeerId());
		} catch (ParseException pe) {
			logger.info("Invalid handshake from " + this.socketRepr(socket) +
					": " + pe.getMessage());
			try { socket.close(); } catch (IOException e) { }
		} catch (IOException ioe) {
			logger.info("An error occured while reading an incoming " +
					"handshake: " + ioe.getMessage());
			try {
				if (!socket.isClosed()) {
					socket.close();
				}
			} catch (IOException e) {
				// Ignore
			}
		}
	}

	/** Connect to the given peer and perform the BitTorrent handshake.
	 *
	 * Connect to the peer using its defined IP address and port, then execute
	 * the handshake process to validate the remote peer Id.
	 *
	 * If everything goes according to plan, notify the
	 * <code>IncomingConnectionListener</code>s with the connected socket and
	 * the parsed peer ID.
	 *
	 * @param peer The peer to connect to.
	 */
	public boolean connect(SharingPeer peer) {
		boolean retry = false;
		boolean sentObfuscatedHandshake = false;
		
		do {
		Socket socket = new Socket();
		InetSocketAddress address = new InetSocketAddress(peer.getIp(),
				peer.getPort());

		logger.info("Connecting to " + peer + "...");
		try {
			socket.connect(address, 3*1000);
		} catch (IOException ioe) {
			// Could not connect to peer, abort
			logger.warn("Could not connect to " + peer + ": " +
					ioe.getMessage());
			return false;
		}

		try {
			//< obfuscated handshake extension
			byte[] remotePeerId = (peer.hasPeerId() ? peer.getPeerId().array() : null);
			if (this.forceObfuscation) sentObfuscatedHandshake = this.sendObfuscatedHandshake(socket, remotePeerId, this.forceObfuscation);
			else sentObfuscatedHandshake = this.sendHandshake(socket, remotePeerId, peer.supportsObfuscation());
			//> obfuscated handshake extension
			Handshake hs = this.validateHandshake(socket,
					(peer.hasPeerId() ? peer.getPeerId().array() : null));
			this.fireNewPeerConnection(socket, hs.getPeerId());
			return true;
		} catch (ParseException pe) {
			logger.info("Invalid handshake from " + this.socketRepr(socket) +
					": " + pe.getMessage());
			//< obfuscated handshake extension
			if (sentObfuscatedHandshake && peer.supportsObfuscation()) {
				peer.supportsObfuscation(false); // try without obfuscation
				retry = true;
			}
			//> obfuscated handshake extension
			try { socket.close(); } catch (IOException e) { }
		} catch (IOException ioe) {
			logger.info("An error occured while reading an incoming " +
					"handshake: " + ioe.getMessage());
			try {
				if (!socket.isClosed()) socket.close();
			} catch (IOException e) { /*ignore */ }
		} // try
		
		} while (retry);
		return false;
	}

	/** Validate an expected handshake on a connection.
	 *
	 * Reads an expected handshake message from the given connected socket,
	 * parses it and validates that the torrent hash_info corresponds to the
	 * torrent we're sharing, and that the peerId matches the peer ID we expect
	 * to see coming from the remote peer.
	 *
	 * @param socket The connected socket to the remote peer.
	 * @param peerId The peer ID we expect in the handshake. If <em>null</em>,
	 * any peer ID is accepted (this is the case for incoming connections).
	 * @return The validated handshake message object.
	 */
	private Handshake validateHandshake(Socket socket, byte[] peerId)
		throws IOException, ParseException {
		InputStream is = socket.getInputStream();

		//< obfuscated handshake extension
		// Read the handshake from the wire
		byte[] data = new byte[Handshake.BASE_HANDSHAKE_LENGTH + 20];
		is.read(data, 0, data.length);
		int pstrlen = data[0] == 0x13 ? data[0] : 0x13;
		byte[] pstr = Arrays.copyOfRange(data, 1, 20);

		// Parse and check the handshake
		Handshake hs = Handshake.parse(ByteBuffer.wrap(data));
		logger.debug("Validating handshake from " + socket + ": " + Torrent.byteArrayToHexString(hs.getBytes()));
		
		if (hs.isObfuscated()) {
			//byte[] clientPeerId = this.id.getBytes();
			byte[] clientPeerId = this.id;
			byte[] infoHash = this.torrent.getInfoHash();
			byte[] infoHashPeerId = Arrays.copyOf(infoHash, infoHash.length + clientPeerId.length);
			System.arraycopy(clientPeerId, 0, infoHashPeerId, infoHash.length, clientPeerId.length);
			try {
				byte[] sighash = Torrent.hash(infoHashPeerId);
				if (!Arrays.equals(sighash, hs.getSignature())) {
					throw new ParseException("Invalid obfuscated handshake", pstrlen);
				}
			} catch (NoSuchAlgorithmException nsae) {
				// fall through
			}
			// read padding message
			byte[] paddingheader = new byte[5];
			is.read(paddingheader);
			int paddingLen = ByteBuffer.wrap(paddingheader).getInt();
			byte[] paddingBytes = new byte[paddingLen];
			is.read(paddingBytes);
		} else if (!Handshake.BITTORRENT_PROTOCOL_IDENTIFIER.equals(new String(pstr, Torrent.BYTE_ENCODING))) {
			throw new ParseException("Invalid handshake signature", pstrlen);
		}
		//> obfuscated handshake extension
		
		if (!Arrays.equals(hs.getInfoHash(), this.torrent.getInfoHash())) {
			throw new ParseException("Handshake for unknown torrent " +
					Torrent.byteArrayToHexString(hs.getInfoHash()) +
					" from " + this.socketRepr(socket) + ".", pstrlen + 9);
		}

		if (peerId != null && !Arrays.equals(hs.getPeerId(), peerId)) {
			throw new ParseException("Announced peer ID " +
					Torrent.byteArrayToHexString(hs.getPeerId()) +
					" did not match expected peer ID " +
					Torrent.byteArrayToHexString(peerId) + ".", pstrlen + 29);
		}

		return hs;
	}

	/** Send our handshake message to the socket.
	 *
	 * @param socket The socket to the remote peer.
	 */
	//< obfuscated handshake extension
	private boolean sendHandshake(Socket socket, byte[] remotePeerId, boolean obfuscate) throws IOException {
		OutputStream os = socket.getOutputStream();
		Handshake hs = Handshake.craft(this.torrent.getInfoHash(),this.id, remotePeerId, obfuscate); 
		os.write(hs.getBytes());
		logger.debug("Send Handshake to " + this.socketRepr(socket) + ": " + Torrent.byteArrayToHexString(hs.getBytes()));
		return hs.isObfuscated();
	}
	
	private boolean sendObfuscatedHandshake(Socket socket, byte[] remotePeerId, boolean obfuscate) throws IOException {
		OutputStream os = socket.getOutputStream();
		Handshake hs = Handshake.craft_obfuscated(this.torrent.getInfoHash(), this.id, remotePeerId, obfuscate);
		os.write(hs.getBytes());
		logger.debug("Send Handshake to " + this.socketRepr(socket) + ": " + Torrent.byteArrayToHexString(hs.getBytes()));
		return hs.isObfuscated();
	}
	//> obfuscated handshake extension

	/** Trigger the new peer connection event on all registered listeners.
	 *
	 * @param socket The socket to the newly connected peer.
	 * @param peerId The peer ID of the connected peer.
	 */
	private void fireNewPeerConnection(Socket socket, byte[] peerId) {
		for (IncomingConnectionListener listener : this.listeners) {
			listener.handleNewPeerConnection(socket, peerId);
		}
	}
}
