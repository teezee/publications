#!/usr/bin/env python
'''Preprocesses the ruleset and calculates statistics on the preprocessed rules.'''

import ruleset
import sys

def main(argv):
        cmdparser = ruleset.Parser.add_cmdline_options()
        if len(argv)<=1:
                print cmdparser.print_help()
                sys.exit(0)
        opts,args = cmdparser.parse_args(argv[1:])
        

	rules = ruleset.parse_rules(opts.ruleset)
	proc = [ruleset.preprocess(rule) for rule in rules]
	
	partition = ruleset.partition(proc)
	partition['rules'] = rules
	partition['proc'] = proc
	
	# calculate stats
	rulestat = ruleset.RuleStats.calculate(rules)
	procstat = ruleset.RuleStats.calculate(proc)
    
	#print rulestat
	#print procstat
	return (rulestat,procstat), partition # rules, prules
    #return rules, prules        

if __name__ == '__main__':
	stat, partition = main(sys.argv)
	#rules, prules = main(sys.argv)
