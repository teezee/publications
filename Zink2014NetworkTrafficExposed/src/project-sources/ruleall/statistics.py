#!/usr/bin/env python
"""
Calculates ruleset statistics.
"""
import ruleset
import sys, optparse
        
def main(argv):
        # cmdline parser
        cmdparser = ruleset.Parser.add_cmdline_options()
        if len(argv)<=1:
                print cmdparser.print_help()
                sys.exit(0)
        opts,args = cmdparser.parse_args(argv[1:])
        
        # parse rule set
        print "Parsing rule set: %s" % opts.ruleset
        ruleparser = ruleset.Parser(opts.ruleset)
        rules = ruleparser.parse()
        print "Errors: %s" %ruleparser.errors
        print
        
        # calculate stats
        stat = ruleset.RuleStats.calculate(rules)
        
        print stat
        return stat, rules
        

if __name__ == '__main__':
        stat, rules = main(sys.argv)
