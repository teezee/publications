#!/usr/bin/env python
"""
Configures the pcapObject according to command line options.

REQUIREMENTS:
    python-libpcap: http://sourceforge.net/projects/pylibpcap/

"""

try:
    import pcap
except ImportError:
    print "Error: python-libpcap not found. Download from http://sourceforge.net/projects/pylibpcap/"
import sys
import optparse

all = ['PcapConfig']


class PcapConfig:
    def __init__ (self,handler=None,*args,**kwa):
        # set arguments
        self.rfile = kwa['rfile'] if kwa.has_key('rfile') else ''
        self.wfile = kwa['wfile'] if kwa.has_key('wfile') else ''
        self.dev = kwa['dev'] if kwa.has_key('dev') else PcapConfig.lookupdev()
        self.snaplen = kwa['snaplen'] if kwa.has_key('snaplen') else 1600
        self.promiscuous = kwa['promiscuous'] if kwa.has_key('promiscuous') else True
        self.to_ms = kwa['to_ms'] if kwa.has_key('to_ms') else 100
        self.filter = kwa['filter'] if kwa.has_key('filter') else ''
        self.count = kwa['count'] if kwa.has_key('count') else 0
        self.handler = handler
        self._config_pcapObject()

    def _config_pcapObject(self):
        self.pcapobj = pcap.pcapObject()
        if self.rfile != '':
            self.pcapobj.open_offline(self.rfile)
        elif self.dev != '':
            print self.dev,self.snaplen,self.promiscuous,self.to_ms
            self.pcapobj.open_live(self.dev,self.snaplen,self.promiscuous,self.to_ms)
        else:
            print >> sys.stderr, "No file or interface specified"
        self.pcapobj.setfilter(' '.join(self.filter),0,0)
        
    @staticmethod
    def lookupdev():
        """wrapper to catch exception"""
        try:
            return pcap.lookupdev()
        except:
            return ''

    @staticmethod
    def add_cmdline_options (parser=None):
        """adds tcpdump style argument checking to optionparser"""
        p = parser if parser else optparse.OptionParser()
        p.add_option(\
            '-r',dest='rfile',default='',\
            help='pcap input file.\ndefault: ""'\
        )
        p.add_option(\
            '-i',dest='dev',default=PcapConfig.lookupdev(),\
            help='listen interface.\ndefault: pcap_lookupdev()'\
        )
        p.add_option(\
            '-p',dest='promiscuous',action="store_false",default=True,\
            help='dont put interface into promiscuous mode.'\
        )
        p.add_option(\
            '-m',type='int',dest='to_ms',default=100,\
            help='read timeout in milliseconds.\ndefault: 100'\
        )
        p.add_option(\
            '-s',type='int',dest='snaplen',default=1600,\
            help='max number of bytes to capture for each packet. No limit: -1\ndefault: -1'\
        )

        p.add_option(\
            '-w',dest='wfile',default='',\
            help='pcap output file.\ndefault: ""'\
        )
        p.add_option(\
            '-c',type='int',dest='count',default='0',\
            help='number of packets to capture. No Limit: 0\ndefault: 0'\
        )
        p.add_option(\
            '-f',dest='filter',default='',\
            help='pcap filter string, enclose in "".\ndefault: ""'\
        )
        return p

    def loop (self):
        self.pcapobj.loop(self.count,self.handler)
        

def test (argv):
    parser = optparse.OptionParser()
    PcapConfig.add_cmdline_options(parser)
    opts,args = parser.parse_args(argv[1:])
    cap = PcapConfig(SimplePacketPrinter(),args,**opts.__dict__)
    try:
        cap.loop()
    except KeyboardInterrupt:
        pass
    return cap

if __name__ == '__main__':
    from packethandler import *
    cap = test(sys.argv)
