"""
Handles packets.

REQUIREMENTS:
    dpkt: http://code.google.com/p/dpkt/
    python-libpcap: http://sourceforge.net/projects/pylibpcap/
    
"""

try:
    import pcap
except ImportError:
    print "Error: python-libpcap not found. Download from http://sourceforge.net/projects/pylibpcap/"

try:
    import dpkt
except ImportError:
    print "Error: dpkt not found. Download from http://code.google.com/p/dpkt/"
    
import sys, itertools, os


all = ['PacketHandler', 'SimplePacketPrinter','PacketDecoder', 'RuleMatchHandler','RuleMatchCounter']

# TODO: need packethandler to count: see rulematcher object

class PacketHandler (object):
    '''
    abstract base class for packet handlers.
    '''
    def __init__ (self):
        raise NotImplementedError("Abstract Base Class")

    def __call__ (self, pktlen, buf, ts):
        raise NotImplementedError("Abstract Method")
    
    
class SimplePacketPrinter (PacketHandler):
    '''
    simply prints out the representation of the packet.
    '''
    def __init__ (self): pass
    
    def __call__ (self,pktlen,buf,ts):
        print pktlen, ts
        print repr(buf)


class PacketDecoder (PacketHandler):
    '''
    decode the packet accoding to the datalink using dpkt.
    '''
    def __init__ (self, capture):
        datalink = capture.datalink()
        if datalink == pcap.DLT_EN10MB:
            self.decode = self._decode_EN10MB
            self.getIPpkt = self._getIPpkt_EN10MB
        elif datalink == pcap.DLT_C_HDLC:
            self.decode = self._decode_C_HDLC
            self.getIPpkt = self._getIPpkt_C_HDLC
        elif datalink == pcap.DLT_RAW:
            self.decode = self._decode_RAW
            self.getIPpkt = self._getIPpkt_RAW
        else:
            raise Exception("DTL %s not supported!" % datalink)
        print "Using DTL %s" % datalink

    def __call__ (self,pktlen,buf,ts):
        decoded = self.decode(buf)
        print repr(decoded)
        return decoded

    def _decode_EN10MB(self,buf):
        decode = None
        try: decode = dpkt.ethernet.Ethernet(buf)
        except: pass
        return decode

    def _decode_C_HDLC(self,buf):
        decode = None
        try: decode = dpkt.chdlc.Chdlc(buf)
        except: pass
        return decode

    def _decode_RAW(self,buf):
        decode = None
        try: decode = dpkt.ip.IP(buf)
        except: pass
        return decode

    def _getIPpkt_EN10MB(self,buf):
        decoded = self.decode(buf)
        ippkt = None
        if decoded: ippkt = decoded.data
        return ippkt if isinstance(ippkt,dpkt.ip.IP) else None

    def _getIPpkt_C_HDLC(self,buf):
        decoded = self.decode(buf)
        ippkt = None
        if decoded: ippkt = decoded.data
        return ippkt if isinstance(ippkt,dpkt.ip.IP) else None

    def _getIPpkt_RAW(self,buf):
        ippkt = self.decode(buf)
        return ippkt if isinstance(ippkt,dpkt.ip.IP) else None



class RuleMatchHandler (PacketDecoder):
    def __init__ (self, capture, ruleset, matcher):
        self.ruleset = ruleset
        self.match = matcher
        self.pktcount = 0
        self.bytecount = 0
        self.pktid = 0
        self.byteid = 0
        self.packetmatch = {} # { ippkt : [ rules ] } store matching rules for each packet
        self.rulecounter = {} # { rule : [pkts,bytes] } counter for satisfied rules
        self.conditioncounter = {} # { rule : {condition : {match : count}} } counters for condition matches
        for rule in ruleset:
            self.rulecounter[rule] = [0,0] # [ pkt, byte ]
            self.conditioncounter[rule] = {} # { condition : {match:count} }
            for condition in rule:
                self.conditioncounter[rule][condition] = {} # { match : count } a dict for offsets and match counts
        PacketDecoder.__init__(self,capture)

    def __repr__ (self): return "%s()" % self.__class__.__name__
    def __str__ (self): return self.__class__.__name__
    
    def __call__ (self,pktlen,buf,ts):
        self.pktcount += 1
        self.bytecount += pktlen
        identified = False
        match = []
        ippkt = self.getIPpkt(buf)
        if ippkt:
            for rule in self.ruleset:
                rmatch = self.match(ippkt,rule)
                match.append(rmatch)
                #print rmatch, self.match.isSatisfied(rule,rmatch) # debug
                if rmatch and self.match.isSatisfied(rule,rmatch):
                    identified = True
                    self.rulecounter[rule][0] += 1
                    self.rulecounter[rule][1] += pktlen                    
                    for condition,cmatch in rmatch.items():
                        if not self.conditioncounter[rule][condition].has_key(cmatch):
                            self.conditioncounter[rule][condition][cmatch] = 1
                        else:
                            self.conditioncounter[rule][condition][cmatch] += 1
                    if not self.packetmatch.has_key(ippkt):
                        self.packetmatch[ippkt] = []
                    self.packetmatch[ippkt].append(rule)
                    # break # we could break here for more efficient classification, but want to know all satisfied rules
        if identified:
            self.pktid += 1
            self.byteid += pktlen
        # print progress
        os.write(1,"%s\r" % str(self))
        return match
            
            
    def __str__ (self):
        s = "packets processed %s,bytes processed: %s, " % (self.pktcount,self.bytecount)
        s += "packets identified: %s (%s %s), bytes identified: %s (%s %s)" % (self.pktid, round(self.pktid/float(self.pktcount)*100,2), '%',self.byteid, round(self.byteid/float(self.bytecount)*100,2), '%')
        return s
    
