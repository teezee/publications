#!/usr/bin/env python
"""
Software classifier for the rule sets.

REQUIREMENTS:
    dpkt: http://code.google.com/p/dpkt/
    python-libpcap: http://sourceforge.net/projects/pylibpcap/
"""

import sys, optparse
import packet
import ruleset



def main (argv):
    # parse cmdline
    cmdparser = optparse.OptionParser()
    cmdparser = packet.PcapConfig.add_cmdline_options(cmdparser)
    cmdparser = ruleset.Parser.add_cmdline_options(cmdparser)

    if len(argv)<=1:
        print cmdparser.print_help()
        sys.exit(0)
        
    opts,args = cmdparser.parse_args(argv[1:])

    print "Using"
    print "Options: %s" % opts
    print "Arguments: %s" % args
    print

    # parse rule set
    print "Parsing rule set: %s" % opts.ruleset
    print
    ruleparser = ruleset.Parser(opts.ruleset)
    rules = ruleparser.parse()

    # process packets
    capture = packet.PcapConfig(args, **opts.__dict__)
    capture.handler = packet.RuleMatchHandler(capture.pcapobj,rules,ruleset.RuleMatcher())

    print "Process packets ..."
    try:
        capture.loop()
    except KeyboardInterrupt as e:
        print >> sys.stderr, e

    # output results
    print '\npackets: %d received, %d dropped kernel, %d dropped interface,' % capture.pcapobj.stats(),
    print '%d processed\n' % capture.handler.pktcount
    print

    print satisfied_rules(capture)
    print matched_conditions(capture)
    #print rule_match_matrix(capture)
    #rmat = calc_rule_match_matrix(capture)
    
    print capture.handler
    
    # return values
    return opts, args, capture
        
def matched_rules_per_packet(capture):
    s = "Matched rules per packet\n"
    s += "id(ippkt),p,count\n"
    for k,v in capture.handler.packetmatch.items(): s += "%s,%s,%s\n" % (id(k),k.p,len(v))
    return s

def satisfied_rules (capture):
    s = "Satisfied Rules\n"
    s += "rid,pid,p,pkts,bytes\n"
    for k,v in capture.handler.rulecounter.items():
        if v[0]: s += "%s,%s,%s,%s,%s\n" % (k.RuleID,k.ProtocolID,k.Protocol,v[0],v[1])
    return s

def matched_conditions (capture):
    # TODO: combine the satisfied rule counter with the matched condition table.
    s = "Matched Conditions\n"
    s += "rid,cid,type,op,value,matched,count\n"
    for rk,rv in capture.handler.conditioncounter.items():
        for ck,cv in rv.items():
            for mk,mv in cv.items():
                s += "%s,%s,%s,%s,%s,%s,%s\n" % (rk.RuleID,ck.ConditionID,ck.Type,ck.Op,ck.Pattern,mk,mv)
    return s

def calc_rule_match_matrix (capture):
    # first create the table {rule: {rule:counter}}
    mat = {}
    for k,v in capture.handler.packetmatch.items():
        for ruleh in v:
            if not mat.has_key(ruleh):
                mat[ruleh] = {} # {rulev:counter}
            for rulev in v:
                if not mat[ruleh].has_key(rulev):
                    mat[ruleh][rulev] = 0
                mat[ruleh][rulev] += 1
    return mat

def rule_match_matrix (capture):
    mat = calc_rule_match_matrix(capture)
    s = "\t,"
    hrules = []
    for ruleh,v in mat.items():
        s += "%s," % ruleh.RuleID
        hrules.append(ruleh)
    s = s[:-1] + "\n"
    for rulev in mat.keys():
        s += "%s," % rulev.RuleID
        for ruleh in hrules:
            s += "%s," % mat[rulev][ruleh]
        s = s[:-1] + "\n"
    return s
        

if __name__ == '__main__':
    opts, args, capture = main(sys.argv)
