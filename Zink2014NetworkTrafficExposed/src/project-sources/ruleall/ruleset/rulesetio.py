#!/usr/bin/env python
"""
Reading and writing rules.
"""

import re, sys
import optparse
from ruleset import *

all = ['ConditionFactory','Parser','Serializer','CnSerializer','LatexSerializer','parse_rules']

def ConditionFactory (line,rid,method):
    '''
    Factory method to generate conditions
    from input lines
    '''
    typ = None
    op = None
    off = -1
    nbytes = 0
    val = None
    field = None
    buf = line.strip()
    # first replace all the "\\x" stuff
    while buf.find('\\x') != -1:
        i = buf.find('\\x')
        buf = buf[:i] + chr(int(buf[i+2:i+4],16)) + buf[i+4:]
    # extract the type
    splitted = buf.split(':',1)
    typ = splitted[0].lower()
    buf = splitted[1]
    # check method for http rules
    # if we got httptule, remove the field strings
    if method == "httprule" and ':"' in buf:
        splitted = buf.split(':"')
        buf = splitted[1][:-1]
        field = splitted[0].split(":")[-1].lower()
    # check offset
    match = re.findall("\s\d+",buf)
    off = -1 if match == [] else int(match[-1])
    if buf.endswith(str(off)):
        buf = buf[0:len(buf)-len(str(off))-1]
        off = int(off)
    else:
        off = -1
    # check operator
    if buf.startswith('[') and buf.endswith(']'):
        if buf.startswith('[-'):
            #op = LE()
            op = "LE"
            buf = buf.replace('[-','').replace(']','')
        elif buf.endswith('-]'):
            #op = GE()
            op = "GE"
            buf = buf.replace('[','').replace('-]','')
        elif '-' in buf:
            splt = buf.split('-')
            if len(splt) == 2 and len(splt[0]) == len(splt[1]):
                #op = RNG()
                op = "RNG"
                buf = buf.replace('-',',')
        elif ',' in buf:
            #op = SIN()
            op = "SIN"
            buf = buf
    if op == None:# and typ != 'regex':
        #op = EQ()
        op = "EQ"
    # check type and set val
    if typ in ["srcport","dstport","port"]:
        field = typ
        off = 0
        val = eval(buf)
        nbytes = 2
    elif typ in ["srcip","dstip","ip"]:
        field = typ
        off = 0
        nbytes = 4
        # transform ips to bytes
        f = lambda x: ''.join([chr(int(y)) for y in x.split('.')])
        #if op.__class__ == RNG or op.__class__ == SIN:
        if op == "RNG" or op == "SIN":
            val = map(f,buf[1:-1].split(','))
        else:
            val = f(buf)
    elif typ == 'regex':
        #op = REX()
        #op = "REX"
        #nbytes = -1
        nbytes = len(buf)
        try:
            val = re.compile(buf)
        except Exception as e:
            print >> sys.stderr, "RuleID %s, %s" % (rid,e)
            return None
    elif typ == 'str':
        #if op.__class__ == RNG or op.__class__ == SIN:
        if op == "RNG" or op == "SIN":
            val = buf[1:-1].split(',')
            if len(set(map(len,val))) != 1:
                print >> sys.stderr, "RuleID %s, unequal string lengths" % rid
            nbytes = len(val[0])
        else:
            #op = EQ()
            #op = "EQ"
            val = buf
            nbytes = len(val)
        if off == -1:
            typ = 'flt'
            #op = FLT()
            #op = "FLT"
    # that should do it
    return Condition(typ,op,off,nbytes,val)


class Parser (object):
    errors = 0
    '''
    Parser to parse rule files
    '''
    def __init__ (self,fname):
        self.file = None
        try:
            self.file = open(fname,'r')
        except Exception as e:
            print >> sys.stderr, e

    @staticmethod
    def add_cmdline_options (parser=None):
        p = parser if parser else optparse.OptionParser()
        p.add_option(\
            '-z', dest='ruleset',default='',\
            help='ruleset file name\ndefault: ""'\
        )
        return p

    def parse (self):
        if not self.file:
            raise AttributeError
        ruleset = []
        for line in self.file:
            line = line.strip()
            if line.startswith("{"):
                # start of record
                conditions = []
                pid = None
                rid = None
                p = None
                method = None
                reportflag = False
                ffflag = False
            elif line.startswith("ProtocolID:"):
                pid = int(line.split(':')[1])
            elif line.startswith("RuleID:"):
                rid = int(line.split(':')[1])
            elif line.startswith("PktReportFlag:"):
                reportflag = bool(int(line.split(':')[1]))
            elif line.startswith("FullFillFlag:"):
                ffflag = bool(int(line.split(':')[1]))
            elif line.startswith("Protocol:"):
                p = line.split(':')[1].strip().upper()
                if p == 'TCP':
                    p = 6
                elif p == 'UDP':
                    p = 17
                else:
                    raise AttributeError("Unsupported protocol " + str(p))
            elif line.startswith("RuleRecMethod:"):
                method = line.split(':')[1].strip().lower()
            elif line.startswith("}"):
                # end of record
                rule = Rule(pid,rid,method,p,conditions)
#                for r in ruleset:
#                    if rule == r:
#                        print >> sys.stderr, "RuleID %s, duplicate rule %s" % (rid, r.RuleID)
#                        self.errors += 1
                if rule in ruleset:
                    print >> sys.stderr, "RuleID %s, duplicate rule" % rid
                    self.errors += 1
                else:
                    ruleset.append(rule)
            elif line.startswith("#"):
                pass
            else:
                # need to factor Condition
                condition = ConditionFactory (line,rid,method)
                if condition != None:
                    if condition in conditions:
                        print >> sys.stderr, "RuleID %s, duplicate condition" % rid
                        self.errors += 1
                    else:
                        conditions.append(condition)
                else:
                    self.errors += 1
        print >> sys.stderr, "Parsing errors: %s" % self.errors
        return ruleset
    

class Serializer (object):
    '''
    Generic serializer to serialize rules
    to files
    '''
    def __init__ (self):
        raise NotImplementedError("Abstract Base Class")
    
    def serialize_ruleset (self,ruleset):
        serialized = []
        for rule in ruleset:
            serialized.append(self(rule))
        return serialized

    def serialize (self,rule):
        return self(rule)

    def __call__ (self, ruleset):
        raise NotImplementedError("Abstract Method")


# TODO: fix missing offsets in some cases
# TODO: fix non printables with using repr for values instead of str
class CnSerializer (Serializer):
    def __init__ (self): pass

    def __call__ (self,rule):
        if rule.Protocol == 6:
            p = "TCP"
        elif rule.Protocol == 17:
            p = "UDP"
        s = "{\nRuleID:%s\nProtocolID:%s\nProtocol:%s\nRuleRecMethod:%s\n" % \
            (rule.RuleID, rule.ProtocolID, p, rule.RuleRecMethod)
        for cond in rule:
            off = ''
            val = ''
            if cond.Off < 0:
                off = ''
            else:
                off = ' ' + str(cond.Off)
            typ = type(cond.Value)
            if typ == type(''):
                val = str(cond.Value)
            elif typ == type(int):
                val = str(cond.Value)
            elif typ == type([]):
                op = cond.Op.__class__.__name__
                if op == 'SIN':
                    val = str(cond.Value)
                elif op == 'RNG':
                    val = str(cond.Value).replace(',','-')
                elif op == 'LE':
                    val = str(cond.Value).replace('[','[-')
                elif op == 'GE':
                    val = str(cond.Value).replace(']','-]')
            s += "%s:%s%s\n" %(cond.Type,val,off)
        s += '}\n'
        return s

class LatexSerializer (Serializer):
    def __init__ (self): pass
    
    # TODO: replace backslash in cond.Value with \textbackslash
    def __call__ (self,rule):
        s = ""
        for cond in rule:
                    if cond.Type=='regex':
                        s += "%s & %s & %s & %s & %s & %s & %s & %s & %s \\\\\n" % \
            (rule.RuleID, rule.ProtocolID, rule.RuleRecMethod, rule.Protocol, cond.Type, cond.Op, cond.Off, cond.N, repr(cond.Value.pattern).replace("\\","\\textbackslash ").replace("_","\\_").replace("<","$<$").replace(">","$>$").replace("^","$\\wedge$").replace("|","$|$"))
                    else:
                        s += "%s & %s & %s & %s & %s & %s & %s & %s & %s \\\\\n" % \
            (rule.RuleID, rule.ProtocolID, rule.RuleRecMethod, rule.Protocol, cond.Type, cond.Op, cond.Off, cond.N, repr(cond.Value).replace("\\","\\textbackslash ").replace("_","\\_").replace("<","$<$").replace(">","$>$").replace("^","$\\wedge$").replace("|","$|$"))
        return s


def parse_rules(rules):
    # parse rule set
    print "Parsing rule set: %s" % rules
    ruleparser = Parser(rules)
    prules = ruleparser.parse()
    return prules


def test (argv):
    cmdparser = Parser.add_cmdline_options()
    if len(argv)<=1:
                print cmdparser.print_help()
                sys.exit(0)
    opts,args = cmdparser.parse_args(argv[1:])

    return parse_rules(opts.ruleset)

if __name__ == '__main__':
    rs = test(sys.argv)

