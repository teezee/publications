#!/usr/local/bin/python
"""
Ruleset partitioning
"""
all = ["partition"]

import ruleset # for rules and conditions

def partition(ruleset):
	partition = {}
	partition['rid'] = {} # lookup table for RuleID
	partition['pid'] = {} # lookup table for ProtocolID
	partition['ports'] = {} # for port conditions
	partition['ips'] = {} # for ip conditions
	partition['str'] = {} # for str conditions
	partition['flt'] = {} # for flt conditions
	partition['regex'] = {} # for regular expression conditions
	
	for r in ruleset:
		# build lookup tables for RuleID and ProtocolID
		partition['rid'][r.RuleID] = r
		if partition['pid'].has_key(r.ProtocolID):
			partition['pid'][r.ProtocolID].append(r)
		else:
			partition['pid'][r.ProtocolID] = []
		# build condition partitions based on (aggregated) types
		for c in r:
			# find correct partition
			if 'port' in c.Type: part = 'ports'		
			elif 'ip' in c.Type: part = 'ips'
			else: part = c.Type
			# add condition and ruleIDs
			if partition[part].has_key(c):
				partition[part][c].append((r.RuleID,r.ProtocolID))
			else:
				partition[part][c] = [(r.RuleID,r.ProtocolID)]
	return partition