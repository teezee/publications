#!/usr/local/bin/python
"""
Functions to match rules and condition in packet data
"""

import ruleset

all = ['MatchOperators', 'RuleMatcher', 'RuleSetMatcher','match_rule', 'match_condition']

# TODO: rewrite to reflect new operators for REX and FLT

class MatchOperators (object):
    '''
    The Operators class the actual matching.
    '''
    def __init__ (self): pass
    def __repr__ (self): return "%s()" % self.__class__.__name__
    def __str__ (self): return self.__class__.__name__

    # s: op string
    # x: text
    # y: pattern
    def __call__(self,s,x,y): return eval("self.%s(x,y)" % s.upper())

    @staticmethod
    def EQ (x,y): return x == y

    @staticmethod
    def LE (x,y): return  x <= y
	
    @staticmethod
    def GE (x,y): return x >= y

    @staticmethod
    def SIN (x,y): return x in y
	
    @staticmethod
    def RNG (x,y): return  y[0] <= x <= y[1]
	
    @staticmethod
    def REX (x,y):
        #m = y.match(x) # match only matches at _beginning_ of string!
        # this would be all idx
        #it = [m.start(0) for m in y.finditer(x)]
        m = y.search(x)
        return m.start() if m else False

    @staticmethod
    def FLT (x,y):
        m = x.find(y)
        return m if m != -1 else False

    
class RuleMatcher (object):
    '''
    A rule matcher matches a rule against an ip packet. Only if all conditions
    match, the rule is satisfied. The RuleMatcher matches the conditions and
    exits when the first mismatch occurs. The RuleMatcher requires an operator
    to perform the actual matching of conditions. It returns a matching object
    if the rule is satisfied, or None if not.

    Each RuleMatcher matches the conditions of exactly a single rule.
    Only if all conditions match, the rule is satisfied.

    A call to the RuleMatcher instance returns a list of match results. The
    results can be True, False, None, or an int for indexes.
    '''
    def __init__ (self, op=MatchOperators()):
        self.op = op
        
    def __repr__ (self): return "%s()" % self.__class__.__name__
    def __str__ (self): return self.__class__.__name__

    def __call__ (self,ippkt,rule):
        #print "match %s in %s\n" % (rule,repr(ippkt))
        match = {}
        if ippkt.p != rule.Protocol:
            return None
        for condition in rule:
            condition_match = self._match_condition(ippkt,condition)
            match[condition] = condition_match
        return match

    @staticmethod
    def isSatisfied(rule,match):
        ret = False
        if match and len(rule)==len(match):
            ret = True
            for k,v in match.items():
                ret &= v
        return ret

    def _match_condition (self,ippkt,condition):
        """
        matches a packet against a condition.
        This is actually a delegate that delegates matching
        to individual functions according to type.
        @arg condition: the condition to match
        @arg ippkt: the network (IP) layer packet
        """
        # this works not for all theoretically possible combinations of types and operators
        #return eval("self._match_%s(ippkt,condition)" % condition.Type)
        if condition.Type == 'str' or 'ip' in condition.Type or 'port' in condition.Type:
            return eval("self._match_%s(ippkt,condition)" % condition.Type)
        elif condition.Type == 'flt':
            if condition.Op == 'EQ':
                return self._match_flt(ippkt,condition)
            elif condition.Op in ['LE','GE','RNG','SIN']:
                try: payload = ippkt.data.data
                except AttributeError: return False
                # that's a very inefficient way to do that ...
                c = condition
                found = False
                for i in range(len(payload)-condition.N):
                    found = self.op(c.Op,payload[i:i+c.N],c.Value)
                    if found: break
                return i if found else found
        elif condition.Type == 'regex':
            if condition.Op == 'EQ':
                return self._match_regex(ippkt,condition)
            elif condition.Op == 'SIN':
                try: payload = ippkt.data.data
                except AttributeError: return False
                # a set of regexes, does not make much sense, should rather be individual conditions
                found = -1
                for regex in condition.Value:
                    found = self.op('REX',payload,regex)
                    if found: break
                return foundss
            elif condition.Op in ['LE','GE','RNG']:
                # that doesn't make sense and should not occur.
                return -1

    def _match_srcip (self,ippkt,condition):
        """match a src IP condition"""
        try: return self.op(condition.Op,ippkt.src,condition.Value)
        except AttributeError: return False

    def _match_dstip (self,ippkt,condition):
        """match a dst IP condition"""
        try: return self.op(condition.Op,ippkt.dst,condition.Value)
        except AttributeError: return False

    def _match_ip (self,ippkt,condition):
        """match any of src or dst ip"""
        return self._match_srcip(ippkt,condition) or self._match_dstip(ippkt,condition)

    def _match_srcport (self,ippkt,condition):
        """match src port"""
        #if ippkt.data: return self.op(condition.Op,ippkt.data.sport,condition.Value)
        try: return self.op(condition.Op,ippkt.data.sport,condition.Value)
        except AttributeError: return False

    def _match_dstport (self,ippkt,condition):
        """match dst port"""
        #if ippkt.data: return self.op(condition.Op,ippkt.data.dport,condition.Value)
        try: return self.op(condition.Op,ippkt.data.dport,condition.Value)
        except AttributeError: return False

    def _match_port (self,ippkt,condition):
        return self._match_srcport(ippkt,condition) or self._match_dstport(ippkt,condition)

    def _match_regex (self,ippkt,condition):
        """match a regular expression"""
        try: payload = ippkt.data.data
        except AttributeError: return False        
        if condition.Off == -1:
            return self.op(condition.Op,payload,condition.Value)
        else:
            return self.op(condition.Op,payload[condition.Off:],condition.Value)

    def _match_str (self,ippkt,condition):
        """match a string condition"""
        try: payload = ippkt.data.data
        except AttributeError: return False
        c = condition
        return self.op(condition.Op,payload[c.Off:c.Off+c.N],c.Value)

    def _match_flt (self,ippkt,condition):
        """match a floating string condition"""
        try: payload = ippkt.data.data
        except AttributeError: return False
        c = condition
        return self.op(condition.Op,payload,c.Value)

    @staticmethod
    def _get_payload (ippkt):
        try: return ippkt.data.data
        except AttributeError: return None


class RuleSetMatcher (object):
    '''
    A RuleSetMatcher matches packets against all rules in the rule set.
    It keeps track about matching statistics.

    The RuleSetMatcher keeps a record of every condition that matched,
    where it matched, and how often the rule was satisfied.
    
    For each condition the following statistics are kept:
    . total number of matches
    . number of matches per offset (for fstr and regex)
    
    For each rule we also keep record of
    . total number of matches
    . number of matches of conditions per offset that lead to complete rule coverage
    '''
    def __init__ (self, rules, matcher = RuleMatcher(MatchOperators())):
        self.rules = rules
        self.match = matcher
        self.matchcount = 0
        self.rulecounter = {} # a dict for satisfied rules
        self.conditioncounter = {}
        for rule in rules:
            self.rulecounter[rule] = 0
            for condition in rule:
                self.conditioncounter[condition] = {} # a dict for offsets and match counts

    def __repr__ (self): return "%s()" % self.__class__.__name__
    def __str__ (self): return self.__class__.__name__

    def __call__ (self, ippkt):
        m = []
        for rule in self.rules:
            rmatch = self.match(rule,ippkt)
            m.append(self.match(rule,ippkt))
            if self.match.isSatisfied(rule,rmatch):
                self.matchcount += 1
                self.rulecounter[rule] += 1
                for condition,cmatch in rmatch.items():
                    if not self.conditioncounter[condition].has_key(cmatch):
                        self.conditioncounter[condition][cmatch] = 1
                    else:
                        self.conditioncounter[condition][cmatch] += 1
        return m

    @staticmethod
    def isSatisfied(rule,match): return this.match.isSatisfied(rule,match)


def test (argv):
    '''Test method'''
    import ruleset
    import dpkt
    rule = ruleset.test(argv)
    print rule
    #pkt = '\x00\x00\x0c\x07\xac\x01\x00#2\xd2c\x88\x08\x00E\x00\x001`D\x00\x00@\x11E\x18\x86"\xa5\x93\x97A\x12i\x0fx\xa1\xbd\x00\x1dW`k\xdb\x02\x05/\xab\xc4\x9fj\xdd\x850\xb4\xd4\xd8k\x99\xd2\xc7p\xe1'
    pkt = '\x00\x00\x0c\x07\xac\x01\x00#2\xd2c\x88\x08\x00E\x00\x001`D\x00\x00@\x11E\x18\x86"\xa5\x93\x97A\x12i\x0fx\xa1\xbd\x00\x1dW`k\xdb\x02\x05/\xab\xc4habe\x20aber\x20abba'
    dp = dpkt.ethernet.Ethernet(pkt)
    ippkt = dp.data
    print repr(ippkt)
    m = RuleMatcher(MatchOperators())
    print m(ippkt,rule)
    mset = RuleSetMatcher([rule])
    print mset(ippkt)
    #print match_rule(ippkt, rule)
    return rule,ippkt, mset

if __name__ == '__main__':
    import sys
    rule,ippkt,mset = test(sys.argv)
