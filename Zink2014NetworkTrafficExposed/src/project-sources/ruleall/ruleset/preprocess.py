#!/usr/local/bin/python
"""
Rule Preprocessor
performs the following actions on the rule set:
- aggregate conditions
- eliminate shared bytes in sets and ranges

Aggregated rules save range conditions as xranges
and set conditions as sets.

The preprocessor does not optimize regular expressions. There
are possible implementations of regex optimizers, not for python
though:

- (perl) http://search.cpan.org/~dankogai/Regexp-Optimizer-0.15/lib/Regexp/Optimizer.pm



"""
all = ["aggregate","eliminate_shared","preprocess"]

import ruleset # for rules and conditions
import rulesetio
import sys,os # for os.path.commonprefix 

def aggregate (rule):
    # get all str conditions with fixed offset
    fstr = [cond for cond in rule if cond.Type=='str' and cond.Off>=0]
    aggregated = rule
    if len(fstr)>0:
        # find minimal offset and max length
        offset = min([x.Off for x in fstr])
        #l = max([x.Off+x.N for x in fstr]) + 1 # todo: check offset and length, leads to index out of range errors
        l = len(range(offset,max([x.Off+x.N for x in fstr])))
        buf = [xrange(0xff+1)] * l
        for cond in fstr:
            value = cond.Value
            idx = cond.Off - offset
            lng = idx+cond.N 
            if cond.Op == 'EQ':
                buf[idx:lng] = value
            elif cond.Op == 'SIN':
                # set([0x0000,0x103f]) != [set([0x00,0x10]),set([0x00,0x3f])]
                for i in range(cond.N):
                    tmp = []
                    for j in range(len(value)):
                        tmp.append(chr(ord(value[j][i])))
                    tmp = set(tmp) if len(set(tmp))>1 else list(set(tmp))[0]
                    buf[idx+i] = tmp
            elif cond.Op == 'RNG':
                # xrange([0x0000,0x103f]) == [xrange([0x00,0x10]),xrange([0x00,0x3f])]
                if value[0]=='': value[0] = '\x00'
                buf[idx:lng] = xrange(ord(value[0]),ord(value[1])+1)
            elif cond.Op == 'LE':
                for i in range(cond.N):
                    buf[idx+i] = xrange(ord(value[i])+1)
            elif cond.Op == 'GE':
                for i in range(cond.N):                
                    buf[idx+i] = xrange(ord(value[i]),0xff+1)
        condition = ruleset.Condition("str","eq",offset,l,buf)
        conditions = [condition]
        remaining = [c for c in rule if c not in fstr]
        if remaining:
            conditions.extend(remaining)
        aggregated = ruleset.Rule(rule.ProtocolID, rule.RuleID, rule.RuleRecMethod, rule.Protocol, conditions)
    return aggregated
    

def eliminate_shared (rule):
    '''Remove shared prefixes in range and set conditions. Does not aggregate.
        Does only eliminate prefixes, not suffixes. Sufficient for ruleset.'''
    conditions = []
    for cond in rule:
        if cond.Type=="str" and cond.Op in ['RNG','SIN'] and cond.Off >= 0 and cond.N > 1:
            value = cond.Value[:]
            prefix = os.path.commonprefix(value)
            # create new prefix condition
            if prefix != '':
                # remove the prefix (don't strip!)
                for i in range(len(value)):
                    value[i] = value[i][len(prefix):]
                conditions.append(ruleset.Condition("str","EQ",cond.Off,len(prefix),prefix))
            # adjust offset and nbytes
            conditions.append(ruleset.Condition(cond.Type,cond.Op,cond.Off + len(prefix),cond.N - len(prefix),value))
        else:
            conditions.append(cond)
    eliminated = ruleset.Rule(rule.ProtocolID, rule.RuleID, rule.RuleRecMethod, rule.Protocol, conditions)
    return eliminated
            

def preprocess (rule):
    processed = eliminate_shared(rule)
    processed = aggregate(processed)
    return processed


def main(argv):
    rules = rulesetio.parse_rules(argv)
    prules = [preprocess(r) for r in rules]
    return rules, prules


if __name__ == '__main__':
    rules,prules = main(sys.argv)
