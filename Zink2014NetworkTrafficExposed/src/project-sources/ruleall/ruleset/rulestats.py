"""
Calculate rule set statistics
"""

all = ['RuleStats']

class RuleStats (object):
        '''
        Represents rule set statistics.
        '''
        def __init__ (self):
                self.nrules = 0
                self.nconditions = 0
                self.type = {}
                self.op = {}
                self.type_op = {}
                self.offsets = {}
                self.lengths = {}
        
        @property
        def conditions_per_rule(self):
                return self.nconditions / float(self.nrules)
        
        @staticmethod
        def calculate (ruleset):
                '''
                calculate rule set statistics
                '''
                stats = RuleStats()
                
                # total counts 
                stats.nrules = len(ruleset)
                stats.nconditions = sum([len(rule) for rule in ruleset])
                stats.nprotocols = len(set([rule.ProtocolID for rule in ruleset]))
                
                # counts by type, by operator
                for rule in ruleset:
                        for cond in rule:
                                if not stats.type.has_key(cond.Type):
                                        stats.type[cond.Type] = 1
                                else:
                                        stats.type[cond.Type] += 1
                                if not stats.op.has_key(str(cond.Op)):
                                        stats.op[str(cond.Op)] = 1
                                else:
                                        stats.op[str(cond.Op)] += 1
                                kto = str(cond.Type)+str(cond.Op)
                                if not stats.type_op.has_key(kto):
                                        stats.type_op[kto] = 1
                                else:
                                        stats.type_op[kto] += 1
                                if not stats.offsets.has_key(cond.Type):
                                        stats.offsets[cond.Type] = {}
                                if not stats.offsets[cond.Type].has_key(cond.Off):
                                        stats.offsets[cond.Type][cond.Off] = 1
                                else:
                                        stats.offsets[cond.Type][cond.Off] += 1
                                if not stats.lengths.has_key(cond.Type):
                                        stats.lengths[cond.Type] = {}
                                if not stats.lengths[cond.Type].has_key(cond.N):
                                        stats.lengths[cond.Type][cond.N] = 1
                                else:
                                        stats.lengths[cond.Type][cond.N] += 1
                # return statistics
                return stats
                
        def __str__ (self):
                s = 'Number of protocol IDs: %s\n' % self.nprotocols
                s += 'Number of rules: %s\n' % self.nrules
                s += 'Number of conditions: %s\n' % self.nconditions
                s += 'Average number of conditions per rule: %s\n' % self.conditions_per_rule
                s += "\nNumber of conditions by type and op\n"
                s += "category,desc,count\n"
                s += "ruleset,rule,%s\n" % self.nrules
                s += "ruleset,condition,%s\n" % self.nconditions
                #s += 'Number of conditions by type:\n'
                for k,v in self.type.items():
                        s += "type,%s,%s\n" % (k,v)
                #s += 'Number of conditions by operator:\n'
                for k,v in self.op.items():
                        s += "op,%s,%s\n" % (k,v)
                #s += 'Number of conditions by type and operator:\n'
                for k,v in self.type_op.items():
                        s += "typeop,%s,%s\n" % (k,v)
                s += "\nNumber of conditions by type and offset\n"
                s += "type,offset,count\n"
                for typ,offs in self.offsets.items():
                        for off,count in offs.items():
                                s += "%s,%s,%s\n" % (typ,off,count)
                s += "\nNumber of counditions by type and length\n"
                s += "type,length,count\n"
                for typ,lengs in self.lengths.items():
                        for leng,count in lengs.items():
                                s += "%s,%s,%s\n" % (typ,leng,count)
                return s[:-1]
