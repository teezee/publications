#!/usr/local/bin/python
"""
Classes and functions to represent rules and conditions
"""

all = ['Operator','EQ','LE','GE','SIN','RNG','REX','Rule','Condition']

RulRecMethods = ["singlepacket", "multipacket", "httprule"]
ConditionTypes = ["srcip", "dstip", "ip", "srcport", "dstport", "port", "regex", "str", "fstr"]

#class Rule (list):
class Rule (tuple):
    """representation of Rule.
    ProtocolID:
        Each rule identifies a Protocol.
        Protocols can be identified by multiple rules,
        however, only one nedds to match. During
        identification rule match results are OR'ed.
        int
    RuleID:
        Each rule has a unique ID.
        int
    RuleRecMethod:
        Method the rule applies to. One of:
        singlepacket
        multipacket
        httprule
        str, .lower() applied
    Protocol:
        the transport layer protocol
        int
    Conditions:
        the list of conditions to satisfy the rule. AND'ed
        Condition
    """
    def __new__ (cls, pid,rid,method,proto,conditions = []):
        return super(Rule,cls).__new__(cls, tuple(conditions))
        #return super(Rule,cls).__new__(cls, conditions)
        
    def __init__ (self,pid,rid,method,protocol,conditions = []):
        self.ProtocolID = pid
        self.RuleID = rid
        self.RuleRecMethod = method
        self.Protocol = protocol
        #list.__init__(self,conditions)
        tuple.__init__(self,conditions)

    #@property
    #def RuleID (self):
        # obscure real ruleid
        #return id(self)

    def __repr__ (self):
        s = 'Rule('
        s += str(self.ProtocolID) + ',' +\
             str(self.RuleID) + ',' +\
             str(self.RuleRecMethod) + ',' +\
             str(self.Protocol) + ',' +\
            tuple.__repr__(self) + ')'
             #list.__repr__(self) + ')' 
        return s

    def __str__ (self):
        s =''
        for c in self:
            s += str(self.ProtocolID) + ',' +\
                str(self.RuleID) + ',' +\
                str(self.RuleRecMethod) + ',' +\
                str(self.Protocol) + ',' +\
                str(c) + '\n'
        return s[:-1]

    def __eq__ (self,other):
#            (self.RuleID == other.RuleID) and \
        return (self.ProtocolID == other.ProtocolID) and \
            (self.RuleRecMethod == other.RuleRecMethod) and \
            (self.Protocol == other.Protocol) and \
            tuple.__eq__ (self,other)
            #list.__eq__ (self,other)

    def __hash__ (self):
	    h = hash(self.ProtocolID) ^ \
	        hash(self.RuleID) ^ \
	        hash(self.RuleRecMethod) ^ \
	        hash(self.Protocol)
	    for c in self: h ^= hash(c)
	    return h

    def has_type(self,typ):
        has = False
        for c in self: has |= (c.Type==typ.lower())
        return has

    def has_op(self,op):
        has = False
        for c in self: has |= (c.Op==op.upper())
        return has


class Condition (object):
    """representation of a condition.
    Type:
        the type of condition, see
        ConditionTypes
        str
    Op:
        the comparison operator
        Operator
    Off:
        offset into the field, where comparison
        starts. If -1, no fixed offset present
        int
    Value:
        the value(s) to compare.
        can be:
        str: string, ip
        int: port
        list: range
        set: set
        pattern: a regular expression pattern
    """
    def __init__ (self,Type,Op,Off,N,Val):
        self.Type = Type.lower()
        self.Op = Op.upper()
        self.Off = Off
        self.N = N
        self.Value = Val

    @property
    def ConditionID (self):
        return id(self)
    
    @property
    def Pattern(self):
        ret = None
        if self.Type=='regex': ret = self.Value.pattern
        else: ret = self.Value
        return repr(ret)
    
    def __str__ (self):
#        s = str(id(self)) + ","
        s = ''
        s += str(self.Type) + "," + \
            str(self.Op) + "," + \
            str(self.Off) + "," + \
            str(self.N) + "," #+ \
#        if self.Type=='regex':
#            s += repr(self.Value.pattern)
#        else:
#            s += '"' + repr(self.Value) + '"'
        s += repr(self.Pattern)
        return s

    def __repr__ (self):
        s = 'Condition(' +\
            repr(self.Type) + "," +\
            repr(self.Op) + "," +\
            repr(self.Off) + "," +\
            repr(self.N) + "," +\
            repr(self.Pattern) + ')'
        return s

    def __eq__ (self,other):
        return self.Type == other.Type and \
               self.Op == other.Op and \
               self.Off == other.Off and \
               self.N == other.N and \
               self.Value == other.Value

    def __hash__ (self):
	    h = hash(self.Type) ^ hash(self.Op) ^ hash(self.Off) ^ hash(self.N)
	    if type(self.Value) == type([]) or type(self.Value) == type(set()) :
	        for item in self.Value:
	            if type(item) == type([]) or type(item) == type(set()):
	                for i in item:
	                    h ^= hash(i)
	            else:
	                h ^= hash(item)
	    else:
	        h ^= hash(self.Value)
	    return h

    def __len__ (self):
        return len(self.Value)


def test (argv):
    import re
    '''Test method'''
    conds = []
    # TODO: for complete test should include all condition types
    conds.append(Condition('ip',"EQ",0,4,'\x86"\xa5\x93'))
    conds.append(Condition('port',"SIN",0,2,[41405,3960]))
    conds.append(Condition('str',"EQ",2,3,'\x02\x05/'))
    conds.append(Condition('flt','FLT',-1,4,'abba'))
    conds.append(Condition('regex','REX',-1,3,re.compile('ab*')))
    rule = Rule(0,0,'singlepacket',17,conds)
    print rule    
    return rule

if __name__ == '__main__':
    import sys
    rule = test(sys.argv)
