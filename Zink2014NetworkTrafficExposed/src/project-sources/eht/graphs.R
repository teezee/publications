#########################################################
# READ FILES / RESULTS
# These are the sources used for evaluation of results
# for both the eht publication as well as the phd thesis
#
# HOWTO reproduce the results:
# 1. Set the working directory to the directory with the .csv result files
# 2. Read the 16 result files used
#    res = readresults()
#########################################################

#workingdir = '../results/'
workingdir = '/Users/tzink/Workspace/disy/Thesis/Zink/phd/projects/eht/results'
#workingdir = '/home/tzink/Workspace/disy/zink/phd/projects/eht/results'
setwd(workingdir)

# read files
# reads files including substring in name
# with values separated by separator
# @param: substring a substring in the filename
# @param: separator 
# @return data in the files as table
readfiles <- function(substring,separator) {
	files <- list.files()
	indices = grep(substring, files); # indices that contain subString
	if(length(indices)==0) stop("No file found!");
    read <- {};
    j <- 1;
    for (i in indices) {
		read[j] <- files[i];
		j <- j + 1;
    }
    data <- lapply(read, read.table, header=TRUE, sep=separator);
    return(data);
}

# read the result files
# with specific names
# creates a result matrix where table index [[i]] maps to the following
# parameter configurations in the paper / thesis (run param_conf to check):
# index : parameter
# 1  : 1001 9
# 2  : 1000 8
# 3  : 1011 B
# 4  : 1010 A
# 5  : 1101 D
# 6  : 1100 C
# 7  : 1111 F
# 8  : 1110 E
# 9  : 0001 1
# 10 : 0000 0
# 11 : 0011 3
# 12 : 0010 2
# 13 : 0101 5
# 14 : 0100 4
# 15 : 0111 7
# 16 : 0110 6
readresults <- function() {
	n1mc16 <- readfiles("n1000000c1.6",",");
	n1mc32 <- readfiles("n1000000c3.2",",");
	#return (c(n1mc16,n1mc32));
	n1kc16 <- readfiles("n100000c1.6",",");
	n1kc32 <- readfiles("n100000c3.2",",");
	return (c(n1mc16,n1mc32,n1kc16,n1kc32));
}

# calc the parameter configuration of the result table
# that corresponds to the configuration in the paper / thesis
param_conf <- function(res) {
  m = {};
  for (i in 1:length(res)) {
    p = c();
    n = mean(res[[i]]$n)
    c = mean(res[[i]]$c)
    climit = mean(res[[i]]$climit)
    word = mean(res[[i]]$word)
    # n
    if (n == 10**5) { p = cbind(p,0) }
    if (n == 10**6) { p = cbind(p,1) }
    # c
    if (c == 1.6) { p = cbind(p,0) }
    if (c == 3.2) { p = cbind(p,1) }
    # chi / climit
    if (climit == 4) { p = cbind(p,0) }
    if (climit == 5) { p = cbind(p,1) }
    # w / word
    if (word == 64) { p = cbind(p,0) }
    if (word == 128) { p = cbind(p,1) }
    # build matrix 
    m = rbind(m,p);
    #print(paste(i,":", toString(p)))
    #print (colMeans(subset(res[[i]],select=c(1,2,5,7))));
  }
  return (m)
}


# build a result table by row
# @param o negative offset from end of table
# @param w number of columns from there
# @param f function to apply
buildtab <- function(res,o,w,f) {
	# buckets at : length(res)-5, 5 cols
	# build the table
	tab <- rbind(
		# n1mc16
		f(rbind(
			res[[1]][,off(res[[1]],o):(off(res[[1]],o)+w)],res[[2]][,off(res[[2]],o):(off(res[[2]],o)+w)],
			res[[3]][,off(res[[3]],o):(off(res[[3]],o)+w)],res[[4]][,off(res[[4]],o):(off(res[[4]],o)+w)]
			)
		  ),
		# n1mc32
		f(rbind(
			res[[5]][,off(res[[5]],o):(off(res[[5]],o)+w)],res[[6]][,off(res[[6]],o):(off(res[[6]],o)+w)],
			res[[7]][,off(res[[7]],o):(off(res[[7]],o)+w)],res[[8]][,off(res[[8]],o):(off(res[[8]],o)+w)]
			)
		),
		# n1kc16
		f(rbind(
			res[[9]][,off(res[[9]],o):(off(res[[9]],o)+w)],res[[10]][,off(res[[10]],o):(off(res[[10]],o)+w)],
			res[[11]][,off(res[[11]],o):(off(res[[11]],o)+w)],res[[12]][,off(res[[12]],o):(off(res[[12]],o)+w)]
			)
		),
		# n1kc32
		f(rbind(
			res[[13]][,off(res[[13]],o):(off(res[[13]],o)+w)],res[[14]][,off(res[[14]],o):(off(res[[14]],o)+w)],
			res[[15]][,off(res[[15]],o):(off(res[[15]],o)+w)],res[[16]][,off(res[[16]],o):(off(res[[16]],o)+w)]
			)
		)
	);
	return (tab);
}

# calculate the negative offset realtive to length of table
# @param results the results
# @param o the desired negative offset from end of table
off <- function(results,o) {
	return (length(results)-o);
}

#########################################################
# BASIC CALCULATIONS / Buckets, Choices, sizes
#########################################################

# number of buckets of a bloom filter.
# @param c constant
# @param n number of items
# @return the number of buckets m
m_bf <- function(c,n) {
	m <- c*n
	return (m);
}

# number of buckets of an FHT summary
# @param c constant
# @param n number of items
# @return the number of buckets m
m_fht <- function(c,n) {
	m <- 2^(ceiling(log(c*n,2)));
	return (m);
}

# number of buckets for MHT
# @param c1 constant > 1
# @param c2 constant < 1
# @param d number of choices / subtables
# @param n number of items
# @return number of buckets
m_mht <- function(c1,c2,d,n) {
	m <- {};
	for (j in 1:d) {
		m[j] = c1 * c2^(j-1) * n;
	}
	m <- ceiling(sum(m))
	return (m);
}

# simplified number of buckets for MHT
# @param c a constant factor denoting number of buckets per item
# @param n the number of items
# @return number of buckets
m_mht_simple <- function(c,n) {
  m <- c * n;
  return (m);
}

# number of cells for sf summary
# @param n number of items
# @return number of cell of an SF summary
m_sf <- function(n) {
	m <- n * log(n,2);
	return (m);
}

# number of choices d for mht
# @para, n number of items
# @return number of choices, subtables
d_mht <- function(n) {
	d <- ceiling(log(log(n,2),2)) + 1;
	return (d);
}

# number of hash functions for bloom filter
# @param n number of items
# @param m number of buckets
# @return number of hash functions
k_bf <- function(n,m, ceil=T) {
	k <- (m/n) * log(2);
	if (ceil == T) k <- ceiling(k);
	return(k);
}

# calculates the number of buckets m
# for the segmented hash table
# according to the number of items and a 
# desired load factor
# @param load the load factor in % (i.e. 80)
# @param n the number of items to insert into table
# @return the number of total buckets m
m_seg <- function(load,n) {
  # load = m/n
  m = load * n;
  return (m);
}

# size of peacock bloom filter segments
# @param b number of bits per item as for segmented hash (16)
# @param r scaling factor as fraction (0.1)
# @param n number of items
# @return the size of all bloom filter segments combined
# paper: beta_peacock(16,0.1,10**6)
beta_peacock <- function(b,r,n) {
  s = 0;
  curr = m_bf(b,n);
  while(curr>=1) {
    curr = r * curr;
    s = s + ceiling(curr);
  }
  return (s)
}

# size of FHT CBF summary
# @param c buckets per item
# @param n number of items
# @param b bits per counter
# @return the size of an FHT CBF summary in bits
beta_fht <- function(c,n,b) {
  size = m_fht(c,n) * b;
	return (size);
}

# size of the MHT on-chip bitmaps
# @param c number of buckets per item
# @param n number of items
beta_mht <- function(c,n) {
  return (m_mht_simple(c,n));
}

# size of the SF summary
# @param n number of items
# @return size of SF summary in bits
beta_sf <- function(n) {
  # n log n
  # n logn (logloglogn) = m * log log log n
  m = m_sf(n);
  b = log(log(log(n,2),2),2);
  size = m * b;
  return (size);
}

# size of the SF summary with lazy deletions
# @param c number of buckets per item
# @param n number of items
# @return number of bits needed for SF with lazy deletions
beta_sf_lazy <- function(c,n) {
  size = 2*beta_mht(c,n) + beta_sf(n);
  return (size)
}

# size of SF summary with counters
# @param v counter width in bits
# @param c buckets per item
# @param n number of items
# @return number of bits for a counting SF
beta_sf_count <- function(v,c,n) {
  m = m_sf(n)
  d = d_mht(n);
  size = v * d * m + beta_mht(c,n);
  return (size);
}

# size of MBF summary 
# @param n number of items
# @return number of bits of the MBF summary
beta_mbf <- function(n) {
  size = n * log(n,2); # m
  return (size);
}

# size of lazy MBF
# @param c number of buckets per item
# @param n number of items
# @return number of bits needed for MBF with lazy deletions
beta_mbf_lazy <- function(c,n) {
  size = 2*beta_mht(c,n) + beta_mbf(n);
  return (size);
}

# size of counting MBF
# @param c number of buckets per item
# @param n number of items
# @return number of bits needed for counting MBF
beta_mbf_count <- function(c,n) {
  size = beta_mht(c,n) + 3.3 * beta_mbf(n);
  return (size);
}

# table of all sizes
# calculates the sizes of all summaries
# for the paper results run
# beta_mat(12.8,1,10**6,2,3)
# @param c_fht number of buckets per item for FHT (12.8)
# @param c_mht number of buckets per item for MHT (6,2,1)
# @param n number of items (10**6)
# @param v number of bits per sf cell (2)
# @param b number of bits per FHT counter (3)
# @return a matrix with names and associated sizes
beta_mat <- function(c_fht, c_mht, n, v, b) {
  unit = 1/8/1000;
  fht = ceiling(beta_fht(c_fht,n,b) * unit);
  fht_bpi = beta_fht(c_fht,n,b) / n;
  sf = ceiling(beta_sf(n) * unit);
  sf_bpi = beta_sf(n) / n;
  sfl = ceiling(beta_sf_lazy(c_mht,n) * unit);
  sfl_bpi = beta_sf_lazy(c_mht,n) / n;
  sfc = ceiling(beta_sf_count(v,c_mht,n) * unit);
  sfc_bpi = beta_sf_count(v,c_mht,n) / n;
  mbf = ceiling(beta_mbf(n) * unit);
  mbf_bpi = beta_mbf(n) / n;
  mbfl = ceiling(beta_mbf_lazy(c_mht,n) * unit);
  mbfl_bpi = beta_mbf_lazy(c_mht,n) / n;
  mbfc = ceiling(beta_mbf_count(c_mht,n) * unit);
  mbfc_bpi = beta_mbf_count(c_mht,n) / n;
  seg_bpi = 16;
  seg = ceiling(m_bf(seg_bpi,n) * unit);
  #peacock_bpi = seg_bpi * n * 0.1 / n;
  #peacock_bpi = 2;
  #peacock = ceiling(peacock_bpi * n * unit);
  peacock = ceiling(beta_peacock(seg_bpi,0.1,n) * unit);
  peacock_bpi = beta_peacock(seg_bpi,0.1,n) / n;
  dnd_bpi = 4;
  dnd = ceiling(dnd_bpi * n * unit);
  # build table and return
  return (rbind(
    c("FHT",fht, fht_bpi),
    c("SF", sf, sf_bpi),
    c("SF lazy", sfl, sfl_bpi),
    c("SF counting", sfc, sfc_bpi),
    c("MBF", mbf, mbf_bpi),
    c("MBF lazy", mbfl, mbfl_bpi),
    c("MBF counting", mbfc, mbfc_bpi),
    c("Segmented Hash", seg, seg_bpi),
    c("Peacock", peacock, peacock_bpi),
    c("Discriminator", dnd, dnd_bpi)
  ));
}

beta_eht <- function(res) {
  unit = 1/8/1000;
  # all EHT n=10**6 c=1.6, (8-B)
  eht_8 = ceiling(colMeans(res[[2]][9:11]) * unit); # 1000 (8) -> 2 chi 4 w 64
  eht_8_bpi = colMeans(res[[2]][9:11]) / 10**6;
  eht_9 = ceiling(colMeans(res[[1]][9:11]) * unit); # 1001 (9) -> 1 chi 4 w 128
  eht_9_bpi = colMeans(res[[1]][9:11]) / 10**6;
  eht_A = ceiling(colMeans(res[[4]][9:11]) * unit); # 1010 (A) -> 4 chi 5 w 64
  eht_A_bpi = colMeans(res[[4]][9:11]) / 10**6;
  eht_B = ceiling(colMeans(res[[3]][9:11]) * unit); # 1011 (B) -> 3 chi 5 w 128
  eht_B_bpi = colMeans(res[[3]][9:11]) / 10**6;
  # all EHT n=10**6 c=3.2, (C-F)
  eht_C = ceiling(colMeans(res[[6]][9:11]) * unit); # 1100 (C) -> 6 chi 4 w 64
  eht_C_bpi = colMeans(res[[6]][9:11]) / 10**6;
  eht_D = ceiling(colMeans(res[[5]][9:11]) * unit); # 1101 (D) -> 5 chi 4 w 128
  eht_D_bpi = colMeans(res[[5]][9:11]) / 10**6;
  eht_E = ceiling(colMeans(res[[8]][9:11]) * unit); # 1110 (E) -> 6 chi 5 w 64
  eht_E_bpi = colMeans(res[[8]][9:11]) / 10**6;
  eht_F = ceiling(colMeans(res[[7]][9:11]) * unit); # 1111 (F) -> 5 chi 5 w 128
  eht_F_bpi = colMeans(res[[7]][9:11]) / 10**6;
  return (rbind(
    c("EHT 8 ", eht_8, eht_8_bpi),
    c("EHT 9 ", eht_9, eht_9_bpi),
    c("EHT A ", eht_A, eht_A_bpi),
    c("EHT B ", eht_B, eht_B_bpi),
    c("EHT C ", eht_C, eht_C_bpi),
    c("EHT D ", eht_D, eht_D_bpi),
    c("EHT E ", eht_E, eht_E_bpi),
    c("EHT F ", eht_F, eht_F_bpi)
  ));
}

#########################################################
# COMBINATION STUFF
#########################################################

# factorial function
# @param x the value to compute the factorial
# @return !x
fact <- function(x) {
	result = 1;
	if (x <= 1) return(result);
	for (i in seq(2,x)) {
		result <- result * i;
	}
	return(result);
}

# permutations of drawing r from set n
# @param n the number of items to draw from
# @param r the number of items to draw
# @return permutations of drawing r from s
perm <- function(n,r) {
	if (r == 1) return(n);
	if (r == 0) return(1);
	if (r <= 0) return(0);
	result <- n - r + 1;
	for (i in seq(result+1,n)) {
		result <- result * i;
	}
	return(result);
}

# combinations of drawing r from set n
# @param n the number of items to draw from
# @param r the number of items to draw
# @return combinations of drawing r from s
comb <- function(n,r) {
	return(perm(n,r)/fact(r));
}

#########################################################
# PROBABILITIES
#########################################################

# false positive probability for bloom filters
# @param n number of items
# @param m number of buckets
# @param k number of choices
# @return the false positive probability
p_fpbloom <- function(n,m,k) {
	fp = (1-(1-1/m)^(n*k))^k;
	return (fp);
}

# probability that with nk insertions into m buckets 
# the number of insertions a bucket received == i
# fi in Song2005
# @param nk number of insertions
# @param m number of buckets
# @param i number of insertions into bucket
# @return probability of i
f_i <- function(nk,m,i) {
	p <- comb(nk,i) * (1/m)^i * (1 - 1/m)^(nk-i);
	return(p);
}

# probability of all p_i from 0 to maxv
# @param n number of items
# @param m number of buckets
# @param k number of choices
# @param maxv maximum value to calculate
p_i_max <- function(n,m,k,maxv) {
	probs = {};
	for (i in 0:maxv) {
		probs[i+1] = f_i(n*k,m,i);
	}
	return(probs);
}

# probability that the first r hashes produce j distinct values
# d(j,r) in Song2005
# @param j number of distinct values
# @param r number of hashes
# @param m number of buckets
# @return probability of j distincts in r hashes
p_distinct <- function(j,r,m) {
	if (j > r) return(0);
	if (j == 0 && r == 0) return(1);
	if (j == 0 && r > 0) return(0);
	result <- j/m * p_distinct(j,r-1,m) + ((m - j + 1)/m) * p_distinct(j-1,r-1,m);
	return (result);
}

# probability that a counter in a set of j non-empty buckets is i
# p(i,j) in Song2005
# @param i counter value
# @param j number of non-empty buckets
# @param nk number of insertions
# @param m number of buckets
# @return Pr{a countervalue in a set of j non-empty buckets is i}
p_countervalue <- function(i,j,nk,m) {
	result <- comb(nk-j,i-1) * (1/m)^(i-1) * (1 - 1/m)^((nk-j)-(i-1));
	return (result);
}

p_countervalue_seq <- function (n, c, maxv) {
	m <- m_fht(c,n);
	k <- k_bf(n,m,T);
	p <- {};
	for (i in 1:maxv) {
		j <- p_countervalue (i,k,n*k,m);
		p <- cbind(p,j);
	}
	return (p);
}

# probability that smallest counter in any r of j buckets is s
# q(r,s,j) in Song2005
# @param r number of buckets with counter value s
# @param j number of selected buckets
# @param s selected  counter value
# @param nk number of insertions
# @param m total number of buckets
# @return Pr{smallest counter value in any r of j buckets is s}
p_smallest <- function(r,s,j,nk,m) {
	result <- 0;
	for (i in 1:r) {
		x <- comb(r,i) * p_countervalue(s,j,nk,m)^i;
		y <- 0;
		for (h in 1:s) {
			y <- y + p_countervalue(h,j,nk,m);
		}
		result <- result + (x * (1-y)^(r-i));
	}
	return (result);
}

p_smallest_seq <- function(n,c,l) {
	m <- m_fht(c,n);
	k <- k_bf(n,m,T);
	p <- {};
	for (i in 1:l) {
		j <- p_smallest (k,i,k,n*k,m);
		p <- cbind(p,j);
	}
	return (p);
}


# probability that any bucket has number of items s 
# @param s the number of items in the bucket
# @param n number of items
# @param m number of buckets
# @param k number of choices
# @return Pe{linkedlist length == s}
p_linkedlist <- function(s,n,m,k) {
	x <- 0;
	for (j in 1:k) {
		x <- x + p_distinct(j,k,m) * p_smallest(j,s,j,n*k,m);
	}
	return (x);
}

p_linkedlist_seq <- function(n,c,l) {
	m <- m_fht(c,n);
	k <- k_bf(n,m,T);
	p <- {};
	for (i in 1:l) {
		j <- p_linkedlist (i,n,m,k);
		p <- cbind(p,j);
	}
	return (p);
}

# binomial distribution with n trials, m buckets, num items > j
# @param n the number of trial
# @param m the number of buckets
# @param j the number of items
# @return probability 
binom <- function(n,m,j) {
	p <- 0;
	for (i in 0:j) {
		p <- p + fi(n,m,i);
	}
	return (1-p);
}

#########################################################
# GENERATE PLOTS
#########################################################

# calcluate and plot counter distributions
# @param results the results as generated by readresults
# @return a counter distribution table
plot_counter_distribution <- function(results, color=F) {
	# counters are at : (length(res)+1)-16
	# 10 cols
	# build the counter dist table
	tab <- buildtab(results,15,9,mean)
	# calculate expected dist
	p <- {};
	for (n in c(1e6,1e5)) {
		#for (c in c(1.6,3.2,6.4)) {
		for (c in c(1.6,3.2)) {
			m <- m_fht(c,n);
			k <- k_bf(n,m,T);
			p <- rbind(p,p_i_max(n,m,k,9)*m);
		}
	}
	x <- 0:9
	#tab <- rbind(tab,p);
	upper <- max(tab);
	lower <- min(tab);
	range <- seq(lower,upper,(upper-lower)/(9));
	cols <- gray(0:(length(tab[,1])-1)/(length(tab[,1])+3));
	if (color) cols <- rainbow(length(tab[,1]), start=0.7, end=0.1);
	plot(x,range,type="n",xlab="counter value",ylab="frequency",log="y");
	for (i in 1:length(p[,1])) {
	  #lines(x,tab[i,],col=cols[i],lty=(i%%5)+2,lwd=2);
	  lines(x,p[i,],col="gray",lty=(i%%5)+2,lwd=2);
	}
  for (i in 1:length(tab[,1])) {
		#lines(x,tab[i,],col=cols[i],lty=(i%%5)+2,lwd=2);
	  lines(x,tab[i,],col="black",lty=(i%%5)+2,lwd=2);
	}
	txt = c(
		"n = 1e6, c = 1.6","n = 1e6, c = 3.2","n = 1e5, c = 1.6","n = 1e5, c = 3.2",
		"exp n = 1e6, c = 1.6","exp n = 1e6, c = 3.2","exp n = 1e5, c = 1.6","exp n = 1e5, c = 3.2"
	);
	#legend(0,1e3,txt,col=cols,lwd=2,lty=((1:length(p[,1]))%%5)+2);
  cols=c("black","black","black","black","gray","gray","gray","gray")
	legend("bottomleft",txt,col=cols,lwd=2,lty=((1:length(p[,1]))%%5)+2);
	return(tab);
}

# plots the frequencies of used bits for huffman compression
# @param results the results tables as returned by readresults
# @return the frequency tables
# @return frequency tables
plot_bits_frequency <- function(results, color=F) {
	tab128 <- {};
	tab64 <- {};
	for (i in 1:length(results)) {
		if (i %% 2) {
			tab128 <- rbind(tab128, mean(results[[i]][18:(18+(128-32))]));
		}
		else{
      print(i);
			tab64 <- rbind(tab64, mean(results[[i]][18:(18+(64-16))]));
		}
	}
	# plot for 128 bits
	upper <- max(tab128);
	lower <- 0;
	range <- seq(lower,upper,(upper-lower)/(dim(tab128)[2]-1));
	x <- 32:128;
	print(range);
	print(x);
	cols <- gray(0:(dim(tab128)[1]-1)/(dim(tab128)[1]+3));
	if (color) cols <- rainbow(dim(tab128)[1], start=0.7, end=0.1);
	plot(x,range,type="n",xlab = "number of used bits", ylab = "frequency");
	for (i in 1:dim(tab128)[1]) {
		lines(x,tab128[i,],col=cols[i],lty=(i%%8)+1,lwd=2);
	}
	txt <- c("p = 9", "p = B", "p = D", "p = F", "p = 1", "p = 3", "p = 5", "p = 7");
	legend(35,4000,txt,col=cols,lwd=2,lty=((1:dim(tab128)[1])%%dim(tab128)[1])+1);
	return (c(tab128, tab64));
}

# plot the probability of drawing the smallest counter value
# depending on different c
# @param n number of items
# @param cvec vector of different c
# @param l maximum counter value to include
# @return probabilities
plot_p_smallest <- function(n,cvec,l, color = F) {
	p <- {};
	for (c in cvec) {
		p <- rbind(p,p_smallest_seq(n,c,l));
	}
	# plot
	upper <- max(p);
	lower <- 1e-15; #min(p);
	range <- seq(lower,upper,(upper-lower)/(l-1));
	x <- 1:l
	cols <- gray(0:(length(p[,1])-1)/(length(p[,1])+3));
	if (color) cols <- rainbow(length(p[,1]));
	plot(x,range,type="n",log = 'y',xlab = "smallest counter value", ylab = "probability");
	for (i in 1:length(cvec)) {
		lines(x,p[i,],col=cols[i],lty=(i%%5)+2,lwd=2);
	}
	text((l+1)/2,max(p),paste("n = ",as.character(n),sep = ""));
	txt <- c();
	for (c in cvec) {
		txt <- cbind(txt, paste("c = ",as.character(c),sep = ""));
	}
	legend(1,1e-10,txt,col=cols,lwd=2,lty=((1:length(cvec))%%5)+2);
	return (p);
}

# plot the probability of any non-0 counter value
# depending on different c
# @param n number of items
# @param cvec vector of different c
# @param l maximum counter value to include
# @return probabilities
plot_p_countervalue <- function(n, cvec, l, color = F) {
	p <- {};
	for (c in cvec) {
		p <- rbind(p,p_countervalue_seq(n,c,l));
	}
	# plot
	upper <- max(p);
	lower <- min(p);
	range <- seq(lower,upper,(upper-lower)/(l-1));
	x <- 1:l
	cols <- gray(0:(length(p[,1])-1)/(length(p[,1])+3));
	if (color) cols <- rainbow(length(p[,1]));
	plot(x,range,type="n",log = 'y',xlab = "smallest counter value", ylab = "probability");
	for (i in 1:length(cvec)) {
		lines(x,p[i,],col=cols[i],lty=(i%%5)+2,lwd=2);
	}
	text((l+1)/2,max(p),paste("n = ",as.character(n),sep = ""));
	txt <- c();
	for (c in cvec) {
		txt <- cbind(txt, paste("c = ",as.character(c),sep = ""));
	}
	legend("bottomleft",txt,col=cols,lwd=2,lty=((1:length(cvec))%%5)+2);
	return (p);
}

# calc expected cam entries
plot_expected_cam <- function(n,cseq,lseq) {
	e <- {}
	for (c in cseq) {
		m <- m_fht(c,n);
		k <- k_bf(n,m,T);
		el <- {};
		for (l in lseq) {
			s <- 0;
			for (i in l:10) {
				j <- p_linkedlist(i,n,m,k) * n;
				if (j >= 1) { s <- s+j; }
			}
			el <- cbind(el,s);
		}
		e <- rbind(e,cbind(c,el));
	}
	df <- as.data.frame(e);
	mat <- as.matrix(df[2:(length(lseq)+1)])
	barplot(mat,col=rainbow(length(cseq)),beside=TRUE,names.arg=as.character(lseq),xlab="chi",ylab="CAM entries",legend.text=paste("c = ",cseq,sep=""),ylim=c(0,1.3*max(mat)));
	text(e,mat+0.3*max(mat),round(mat));
	return (mat);
}
