# Kolloquium "Network Traffic Exposed & Concealed"

## 01 Titel
Willkommen zu meinem Kolloquium mit dem Thema ...
Es geht um Netzwerk Ueberwachung und wie man sich davor schuetzen kann.

Aber es geht im Allgemeinen auch um Sicherheit und Privatsphaere und da moechte ich einen Friedensnobelpreistraeger zitieren, der sagte:

## 03 Obama

"Man kann nicht 100% Sicherheit und dabei auch 100% Privatsphaere haben."
Das impliziert man kann 100% Sicherheit haben und 100% Privatsphaere haben, aber halt nicht beides. 
Man muss sich entscheiden.
Und mit solch einem Modell kann man dann gut Politik machen.

Noch deutlichere Worte findet der 2. Mann im Justizministerium

## 04 Cole

Cole sagte kuerzlich sinngemaess zu Vertretern von Apple:

"Verschluesselungstechnologien toeten Kinder"

Hintergrund ist, dass Apple und Google Datenverschluesselung auf Telefonen einfuehren wollen. 
Laut Cole koennen die Strafverfolgungsbehoerden dann ihre Arbeit nicht machen und deswegen werden Kinder sterben.

## 05 Obama Cole

Also man sieht das Thema ist politisch brisant, aber Technologie ist politik-agnostisch und ich moechte ueber Techniken zum Klassifizeren und Verschleiern von Nertwerkverkehr reden.

## 06 These

Und meine These ist, dass die gaengigen Technologien sowohl Konzeptionell als auch technisch fehlgeleitet und mangelhaft umgesetzt sind.

*3 mins*

## 07 Motivation

Aber wieso will man Klassifikation und Verschleierung ueberhaupt machen.
Und ich meine jetzt nicht staatliche Ueberwachung sondern bei Unternehmen, Netzwerk Provider und auch Benutzer.
Da gibt es vor allem vier grosse Bereiche.

Natuerlich gehts umsGeld.
Netzwerke sind teuer und man moechte, dass sie moeglichst optimal verwendet werden.
Hinzu kommt ein Konflikt auf Grund von assymetrischen Kostenmodellen.
Benutzer zahlen idR einen Festpreis, waehrend Provider untereinander Transitkosten haben.
Hier moechte man natuerlich hohes Transferaufkommen vermeiden.

Ein weiterer Grund ist Qualitaetssicherung.
Anwendungen haben verschiedene Ansprueche an das Netzwerk
Um Qualitaet gewaehrleisten zu koennen muss man evtl anwendungsorientiert Ressourcen zur Verfuegung stellen

Rechtliche Vorgaben koennen sonderbehandlung von Verkehr verlangen.
Zb Zensur.
Aber auch Rechteverweter wie Film und Musik Industrie versuchen rechtlich einfluss zu nehmen um bestimmten Verkehr zu unterbinden.

Aber der fuer uns wichtigste Punkt ist Sicherheit.
Wir wollen die Netze schuetzen vor Angreifern oder gefaehrlichem Verkehr.
Dazu brauchen wir Klassifikationstechniken.
Wir wollen aber auch unsere Daten schuetzen nicht nur innerhalb des Netzes sondern auch im Transfer.
Und dafuer brauchen wir Verschleierungstechniken die gewaehrleisten, dass die Daten vor Beobachtern geschuetzt sind, dass sie beim richtigen Empfaenger ankommen.

Man sieht also, dass Sicherheit und Privatsphaere sich nicht ausschliessen sondern im Gegenteil ergaenzen.
Man braucht also beides.

*5:30 mins*

## 08 Background

Jetzt kommt noch ein kurzer technischer Hintergrund und dann gehen wir auf die Techniken ein.

## 09 Stack

Ein Modell wie man sich Kommunikation im Internet vorstellen kann ist der sog. Internet Protokoll Stack von Kurose und Ross.
Sie beschreiben 5 Schichten von denen uns aber nur die drei obersten interessieren.
Ganz oben ist die Anwendungs-Schicht, hier sind die Anwendungen (bsp eMail Client) die Nachrichten austauschen wollen.
Und hierfuer muessen sie sich nach aussen irgendwie bemerkbar machen, und das machen sie ueber Ports, sowas wie Tueren in der Transport Schicht.
Hier werden Tueren aufgemacht durch die die Nachrichten verschickt werden koennen.
Das reicht aber noch nicht ganz, man benoetigt auch die die Absender und Empfaenger Adressen von den beteiligten Rechnern und diese stellt die Netzwerk-Schicht zur verfuegung.

Moechte man also Nachrichten verschicken so werden braucht man erst eine Tueren durch die die Nachrichten durchgehen und dann die Adressen.
Man schnuert also ein Paket aus Daten und Kontaktinformationen (Header).
Das ist im Prinzip aehnlich wie bei der Post.
*6:40 mins*

Redet man von Schichten hoert man oft auch vom Ende-Zu-Ende-Prinzip.
Das besagt, dass Geraete im Netz nur die Schichten implementieren sollen, die sie wirklich brauchen.
Also jemand der das Paket weiterleitet der muss nichts ueber die Applikation oder den Inhalt der Pakete wissen.
Interessant sind nur die Adressen.
Man stelle sich vor die Post macht Pakete und Briefe aus und entscheidet dann anhand des Inhalts was damit gemacht werden soll.

Zur Zeit auch haefiger in der Presse ist die Netz Neutralitaet.
Diese sagt, dass die Netzwerke sich neutral gegenueber den Paketen verhalten sollen.
Also jedes Paket hat das gleiche Recht auf Zustellung.

Praktisch gibt es aber u.U. Gruende Pakete zu diskriminieren.
Die Post hat unterschiedliche Kostenmodelle z.B. in Abhaengigkeit von Groesse und Gewicht.

Amazon geht noch einen Schritt weiter.
Als vor einigen Wochen der Streit mit den Verlagen war, hat Amazon Pakete auch einfach mal liegen lassen.

Was aehnliches wuerde die Telekom auch gerne tun.
Die Telekom ist naemlich nicht nur Netzanbieter sondern auch Inhaltsanbieter
VoIP oder Entertain z.B.
Das sind dann die sog. Managed Service.
Und diese moechte die Telekom natuerlich gerne bevorzugt behandeln.
Da gibt es also durchaus diskussionsbedarf was ist ok und was nicht.

*8:20 mins*

## 10 Router

Fuer die Zustellung von Paketen zustaendig sind Router.
Diese haben viele Ein- und Ausgaenge wo die Pakete reinkommen bzw wieder raus sollen.

Wenn Pakete reinkommen muessen die erstmal zwischengelagert und bearbeitet werden.
D.h. man muss nachschauen, wo das Paket hin soll und es zum richtigen Ausgang bringen.

Zusaetzlich moechte man noch Pakete klassifizieren, um dann bestimmte Aktionen durchfuehren zu koennen.

Wenn Geraete kommunizieren verschicken die nicht nur einzelne Pakete sondern die laufen in Stroemen hin und her.
Das nennt man dann Flow.
Man kann sich Aufwand sparen, wenn man Pakete den richtigen Flows, zuordnet und die Aktionen auf den Flow anwendet. 
Allerdings sieht man nicht unbedingt die ganze Kommunikation - das haengt auch davon ab, wo im Netz man sich befindet - und man muss weiss nicht wirklich wann genau ein Strom zu Ende ist und der naechste anfaengt.
Man kann sich also der Kommunkation nur annaehern.

Das Problem ist, dass ununterbrochen Pakete mit sehr hoher Geschwindigkeit reinkommen.
Bei schnellen Netzen ist es durchaus gaengig, dass pro Eingang 1mio Pakete/s ankommen.
D.h. man hat gerade mal 1ns Zeit diese zu bearbeiten.
Denn wir haben ja nicht unendlich viel Speicher zur Verfuegung, um Pakete zwischenzulagern.

D.h. was wir hierfuer brauchen ist spezialisierte Hardware.
Aber selbst dann ist der Speicher den wir zur Verfuegung haben vergleichsweise langsam und klein.
Weil schneller Speicher kostet viel Geld.

*10:00 mins*

## 11 State of The Arts

Jetzt kommen wir zu den Techniken und warum sie mangelhaft sind.

## 12 TC

Klassifizerung kann man in vier grosse Bereiche einteilen

## 13 Point Location

Der erste in Point Location.
Hier stellt man sich den Suchraum als Multidimensionalen Raum vor, der durch Metadaten und Daten aufgespannt wird.
Ein Paket beschreibt in diesem Raum dann einen Vektor.
Das Regelwerk beschreibt Hyperrechtecke.
Rechtecke deswegen, weil wir Wertebereiche in den Regeln haben.
(Und die Aufgabe ist jetzt das kleinste Hyperrechteck zu finden, in dem das Paket noch drin liegt.)
Und die Aufgabe ist jetzt das Hyperrechteck zu finden, das das Paket am genauesten beschreibt.
Und man sieht das Paket kann in mehreren Rechtecken liegen.
Selbst bei dem Gruenen liegt es in 3 von vier Grenzen.
Die Anzahl dieser Grenzen steigt exponentiell in abhaengigkeit der Dimension
Also haben wir eponentiellen Aufwand und weder Platz noch Zeit.

*12:00 mins*

## 14 Pattern matching

Beim Pattern Matching sucht man im Paket nach Schluesselwoertern, oder Signaturen.
D.h. man hat ein Regelwerk aus vielen Signaturen und muss diese im Paket suchen.
Manmal weiss man wo man suchen muss, manchmal nicht.
Dann muss man das ganze Paket durchsuchen.
Bei tausenden von Signaturen ist das aufwaendig.

## 15 Machine Learning

Beim machine learning macht man sich statistische Eigenschaften von Flows zunutze.
Dazu gehoeren bspw durchschnittliche Paketgroessen oder Intervalle in denen Pakete ankommen.
Die Idee dahinter ist, dass Anwendungen bestimmte Kommunikationseigenschaften besitzen.
Man erstellt also einen statistischen Fingerabdruck von Anwendungen.
Allerdings braucht man hier immer erst ein paar Pakete bis man genug Information zusammen hat, um sagen zu koennen um was fuer einen Flow es sich wahrscheinlich handelt.

## 16 BA

Relativ neu ist die Behavior Analyse.
Hier geht man davon, dass man von Kommunikationsprofilen die Rechner aufweisen auf die Anwendungen schliessen kann.
Bspw verhaelt sich ein Rechner, beim Surfen anders als beim Filesharing.
Hier werden also Kommunikationsmuster ausgewertet.
Das ist nochmal eine Abstraktionsschicht ueber den Flows.

*14:00 mins*

## 17 TC Issues

Man also einige unterschiedliche Techniken zur Verfuegung und muss entscheiden, welche man wie fuer seine Zwecke einsetzen kann.
Und das ist nicht so einfach.

Man kommt naemlich nicht einfach an vergleichsdaten ran.
Gleiches gilt fuer die Werkzeuge an sich.
Auch bei der Bewertung von Ergebnissen gibt es auch keine Standards.

Aber das schwerwiegendste ist dass es keine Baseline gibt.
Wir kennen global nicht alle Anwendungen, die es gibt und wissen nicht wie sie miteinander kommunizieren.
Also kann man schlecht verlaesslich feststellen, wie gut Klassifizierer funktioniern.

Insgesamt bedeutet das, wir haben keine Transparenz, keine Reproduzierbarkeit und keine Vergleichbarkeit.

*17:00 mins*

## 18 TO

Bei der Verschleierung kommt das meiste, was man heutzutage benutzt aus der Kryptographie. 
D.h. man versucht den Inhalt von Paketen vor Beobachtern zu schuetzen.
Das ist leider oft mangelhaft implementiert.
Funktioniert bei inhaltsbasierter Klassifikation versagt aber bei statistischen Analysen.

Da kann man sich ueberlegen das man die statistischen Eigenschaften von Flows veraendert.
Die Frage ist hierbei allerdings wie sich das auf die Kommunikation und die Netze auswirkt.

Eine andere Moeglichkeit ist sich in anderem Verkehr zu verstecken, also so zu tun, als ob es sich um etweas anderes handelt.
Findet wenig Verbreitung und beschraenkt sich auf Onion Routing und Tunneln.

Eine Weitere Verschleierungstechnik zeige ich nachher noch.

*18:00 mins*

## 19 Multi-classification

Vorher schauen wir uns an, wie man Klassifikation besser machen kann.
Ein Vorschlag ist Multi-classification.
Die Idee ist, sich nicht nur auf eine Klassifizierungsmethode zu verlassen, sondern mehrere zu verwenden. 

Da bin ich nicht der 1. der das vorschlaegt, aber tatsaechlich findet man in der Literatur hauptsaechlich Arbeiten dazu, wie man die Ergebnisse mehrerer unabhängiger Klassifizierer aggregieren kann.

Mein Ziel ist dagegen unterschiedliche Techniken moeglichst geschickt miteinander zu kombinieren, um die Klassifikation insgesamt effizienter und aussagekraeftiger zu machen.

*15:00 mins*

## 20 Ruleset

Das haengt auch mit dem Regelwerk zusammen, das uns zur Verfuegung stand.
Denn Wir haben mit dem Regelwerk eines chinesischen Routerherstellers gearbeitet.
Und hier wurden viele Unterschiedliche Felder des Pakets untersucht.
Ports, Protokol, IPs aber auch der Inhalt.
Und beim Inhalt gab es noch unterschiedliche Arten von Signaturen, feste Zeichenketten, fliessende Strings, und regex
Dazu gab es unterschiedliche Operatoren, von Gleichheit uber Wertebereiche bis hin zu sets.
Im Regelwerk befanden sich tausende von Regeln, die auch noch Abhaengigkeiten untereinander haben.

So ein komplexes Regelwerk kann man nicht einfach in Hardware uebersetzen.
Koennen schon, das wird aber gross und damit teuer, ineffizient und unflexibel.
Man moechte ja auch auf Regelaenderungen eingehen koennen.
Also braucht man einen Workflow um aus dem Regelwerk effiziente und wiederverwendbare Hardware zu machen.

## 21 Rules2Circ

Dazu analysieren und optimieren wir zuerst werden die Regeln.
Man entfernt also Redundanzen und Konflikt, und reduziert Abhaengigkeiten.
Und man sortiert die Regeln nach Wichtigkeit,
es zeigt sich, dass ein Bruchteil Regeln einen Grossteil des Verkehrs identifizieren.

Dann werden Regeln mit aehnlichen Eigenschaften zu Partitionen grupiert.

Und diese kann man dann im naechsten Schritt uebersetzen in maschinen die auf genau diese Eigenschaften optimiert sind.
Und die wiederum koennen unabhaengig voneinander laufen, das ist wichtig fuer die Parallelitaet.

## 22 Classify

Wenn ein Paket reinkommt machen wir eine mehrstufige Klassifikation.
Zuerst wird der Verkehr aufgeteilt in unterschiedliche Kategorien.
as machen wir um die Last zu moeglichst frueh aufzuteilen und damit den Durchsatz auf den folgenden Maschinen zu erhoehen.

Als naechsten suchen wir ihm header des pakets und parallel nach fixen Strings im Paketinhalt.
Das sind die, von denen wir wissen wo sind sind.
Diese zwei sind also vergleichsweise einfach.

Im Idealfall haben wir dann schon ein Ergebnis und sind fertig.
Anderfalls muessen wir die teuern Maschinen anschmeissen und den gesamten Paketinhalt durchsuchen.

Wenn dann immer noch nichts rausgekommen ist, muessen wir auf Software ausweichen.

Im folgenden schauen wir uns Einzelteile hieraus genauer an und fangen an mit der Klassifikation in Kategorien.

*20 mins*

## 23 picdfi

Und hierbei geht es um das klassifizieren von P2P Verkehr.
P2P ist interessant.
Es verursacht viel Verkehr mit potentiell hohen transitkosten.
Wird oft fuer illegales filesharing verwendet.
Aber auch die meisten botnetze sind P2P netze.


## 24 how to find
Aber die Frage ist, wie findet man P2P
Wir haben es hier mit einer Vielzahl dynamischer selbstorganisierenden Peers zu tun, die staendig wechseln und versuchen sich zu verstecken.
Dann gibt es unterschiedliche Arten von Verkehr.
Zum einen Signalverkehr, zum suchen von Peers und Ressourcen.
Und dann der eigentliche Datentransfer.
Und all das passiert ziemlich schnell.

Da habe ich gedacht, warum Nutzen wir diese Eigenschaften nicht aus.
Konkret die Tatsache, dass auf dem gleichen Port kurz aufeinander unterschiedlicher Verkehr zu sehen ist.
Zuerst Signalverkehr ueber UDP und transferverkehr ueber TCP.

## 25 weakness
Und da kann man sich jetzt einen einfachen Algorithmus denken und diesen um wenig Ports und kurze Signaturen ergaenzen.

D.h. ein Paket kommt rein, wir schauen, ob es ueber typische P2P ports geht oder ob wir eindeutige Signaturen am Paketanfang haben.
Falls ja handelt es sich um P2P.
Ansonten pruefen wir das Transportprotokoll.
Sieht man UDO markiert man den Flow als moeglicherweise P2P.
Falls man dann kurz darauf TCP sieht, setzen wir den Flow auf P2P.
In allen anderen Faellen sagen wir, es ist kein P2P.

Dieser Algorithmus ist sehr einfach auch in Hardware zu implementieren.
Aber die Frage ist, funktioniert er auch und wie gut.
Und da kommen wir zu dem Problem, wie will man das messen wenn es keine Baseline gibt.

## 26 Baseline
Also habe ich eine gebaut.
Und zwar nehme ich zwei oeffentlich Verfuegbare klassifikationstools.
zum einen nDPI, das Paketinhalte durchsucht, und SPID, das statistische Analyse auf Flows macht.
Und davon bilde ich die Vereinigungsmenge, wobei bei Konflikt nDPI bevorzugt behandelt werden.
Und zwar gehe ich davon aus, dass bei Klartext nDPI perfekt funktioniert.

Dann erhaelt man eine Moegliche Grundwahrheit von der man ausgeht dass sie stimmt.

## 27 Results

Und gegen die kann man jetzt Vergleichen.
Ich will jetzt hier gar nicht genau auf die einzelnen Ergebnisse eingehen.
Was ich verdeutlichen moechte ist, wie sehr sich die Ergebnisse unterscheiden, jenachdem was man misst.
Deutlich wird das bspw bei den FP, also den daten, die von meinem Algorithmus als P2P klassifiziert wurden , in der Baseline aber nicht P2P ist.
Und da sieht ...
Sichtbar wird das auch drueben bei precision und accuracy.
...

Die False Positives wollte ich mir noch etwas genauer ansehen, denn schliesslich moechte man vermeiden unbedarfte faelschlicherweise benachteilt werden.

Es sind ja auch nur 2 Flows.

## 28 FP
Und wenn man die sich anschaut sieht das erstmal wie HTTP aus.
Steht sogar drin und das stimmt auch.
Das ist tatsaechlich HTTP.
Nur wenn man weiter schaut sieht man, dass der Verkehr von eine P2P Applikation stammt, naemlich Limewire, dass ein Gnutella Client ist.
Gnutella verwendet naemlich HTTP zum Download von Dateien.
Und das ist eine normale Anwendung von HTTP.

Und jetzt stellt sich die Frage: Wer hat recht?
Die Baseline sagt, das ist HTTP, und das stimmt ja auch.
Ich sage aber, das ist P2P, und das stimmt auch.
Das ist ein Beispiel dafuer, dass syntax basierte Klassifikation von Anwendungsprotokollen vielleicht nicht unbedingt die Informationen liefert, die man haben moechte.

Und jetzt rufen wir uns ins Gedächtnis, dass die Amerikaner ihre nofly und kill listen auf Grund solcher Ergebnissen befueelen.

*26 mins*

## 29 SBM

Nach der Kategorie-Klassifizierung machen wir Stringmatching
Hier hat man traditionell 2 verschiedene Moeglichkeiten.
Entweder man weiss genau wonach ich suche dann mache ich exact matches
Das geht einfach und schnell.
Oder aber ich weiss nicht genau wonach ich suche, weil ich Wertebereiche habe, oder Sets, oder Quantifizierer u.ae.
Dafuer braucht man regulaere Ausdruecke die komplexe Automaten hervorbringen.
D.h. es dauert lang und braucht viel Speicher.


## 30 Simple Regex

Wenn man sich jetzt die Regeln genauer ansieht, bemerkt man, dass die meisten Regeln eigentlich nur ein subset der Maechtigkeit von regex benoetigen.
Eigentlich muesste man jetzt die ganzen Werte in der Zustandtabelle speichern.

Und wir haben uns gefragt, ob es nicht eine Moeglichkeit gibt, das so zu codieren, dass wir eine Komplexitaet nahe der exact matches erreichen aber eine Maechtigkeit, die sich an regulaere Ausdruecke annaehrt.

## 31 Logic
Und da sind wir auf folgende Schaltung gekommen.
Wir haben 7 bit input, was der zu pruefende charakter ist.
8 bit opcode, was codiert, was wie gematcht werden soll
Die Schaltung selbst codiert dann, bytes, don't care's, case.

Sets muessen extra implementiert werden.
Aber auch hier kann man optimieren.
Man muss nicht jeden Wert kodieren. 
Denn sowohl bei den Wertebereichen als auch bei den meissten sets, teilen sich die einzelnen Werte viele Bits.
bsp. prefixe, und don;t cares.
Also kann man hier potentiell viel wiederverwenden und bei der kodierung sparen.

## 32 Comp8
Jetzt kann man das mit einem gewoehnlichen 8bit Komparator vergleichen, was der standard Baustein beim string matchen ist.
Und man sieht, dass man beim SBM etwas meht Logik brauchen, aber dafuer viel flexibler ist und dafuer viel Speicher sparen kann.

## 33 Engine

Und dann bekommt man einen Baustein den man recht flexibel Einsetzen kann, um unterschiedliche  Klassifizierungsmaschinen zu bauen.

*32 mins*

## 34 Bittorrent

Nachdem wir viel ueber Klassifizierung geredet haben kommen wir jetzt zur Verschleierung und fangen mit Bittorrent an.

Bittorrent ist das beliebteste P2P Netzwerk und deswegen natuerlich auch Ziel von diskriminierung.

## 35 Peer Wire

Und Bittorrent macht eine Erkennung recht einfach, denn nach dem Verbindungsaufbau tauscht es den sog. BitTorrent Handshake aus in dem drinsteht, dass es sich um Bittorrent handelt.
Zudem gibt es verschiedene statistische Eigenschaften (varianz in den Feldern) sowie die feste Paketlaenge die eine identifikation trivial machen.

Wenn man Bittorrent verstecken moechte, muesste man diese eindeutigen Eigenschaften loswerden.

## 36 Payload obfuscation

wir generieren eine zufallszahl, konkatenieren die mit der Peer ID des Peers mit dem wir uns verbinden wollen.
Diese bekommen wir ueber einen anderen Kanal.
Davon bilden wir den SHA1 hash Wert, welcher dann ein Schluessel fuer eine symmetrische kryptographische Funktion dient.
Den Sallt muessen wir klar reinschreiben zum tauschen, damit der andere die nachricht mit seiner Peer ID dekodieren kann. 
Jetzt haben wir aber immer noch das Problem, dass der Handshake immer noch 68 byte lang ist.

## 37 Flow obfuscation

Um auch das zu verschleiern gibt 2 moeglichkeiten.
Man kann zufaellige Daten einfach an die Nachricht anhaengen.
Dafuer definiere ich eine Bittorrent Padding Nachricht.
Wobei hier die Felder analog zu eben verschleiert werden.

Die zweite Moeglichkeit sind zufaellige Flushs.
Also man schneidet Die Nachrichten an zufaelligen Stellen auseinander bevor man sie verschickt.

Und das kann man auch beliebig kombinieren.

## 38 observed

Schaut man sich jetzt den entstandenen Verkehr an, so sieht man, man sieht eigentlich gar nichts.
Nur rauschen.

## 39 Flush impact

Interessant ist allerdings noch wie sich das flushen auf das Netzwerk auswirkt.
Hier sieht man, dass die Zahl der Pakete sowie die gesamte Dauer um ein Vielfaches ansteigen.
Das ist natuerlich sowohl fuer Benutzer als auch die Netzwerke.
Aber aus Sicht eines Nutzers ist schlechter Durchsatz immer noch besser als gar keiner.
Aber das bedeutet, flushing sollte man nur Verwenden, wenns nicht anders geht.

## 40 Id

Aber funktioniert das ganze auch.
Das habe ich anhand von 3 Klassifizieren getestet, naechmlich OpenDPI, SPID, und picDFI, das ist mein Algorithmus von vorhin.
was zuerst auffaellt ist der grosse balken hier, das liegt daran, dass picdfi unidirektionale flows verwendet und die anderen beiden bidirektionale.
abgesehen davon sieht man, dass nur openDPI einen kleinen Teil als Bittorrent erkannt hat, das ist der Tracker Verkehr und identisch zu dem HTTP Verkehr beo SPID.
picDFI konnte aber fast die haelfte korrekt als P2P klassifizieren.

Das zeigt jetzt zwei Dinge.
Zum einen, dass nur geringe Aenderungen an der Applikation ausreichen, um komplexe und teure Klassifikationssysteme zu ueberlisten.
Zum anderen ist es ein weiteres Beispiel, wie ein simpler semantik-basierter Klassifikationsalgorithmus den komplexeren syntax basierten Systeme klar ueberlegen ist.
* 38 mins*

## 41 opDNS

Aber ausser Verkehr zu verschleiern kann man auch einer Klassifizierung entgehen, indem man den Verkehr gar nicht erst produziert, oder zumindest auf ein mindestmass beschraenkt.

Und als Beispiel wo das moeglich und sinnvoll ist nehmen wir DNS.
DNS ist wie das Telefonbuch im Internet.
Wenn man zu einem Server Verbindung aufnehmen will, muss man im DNS nachfragen wie seine Adresse ist.

## 42 DNS Problems

Das fuehrt zu einer Menge von Privacy Problemen.
Zum einen macht es Zensur einfach, indem man einfach bestimmte Eintraege oder DNS komplett sperrt.
Aber es erlaubt auch Interessensprofile von Anwendern zu erstellen indem man einfach die Anfragen auswertet.
Das kann man dann verwenden um Benutzer zu identifizieren und sogar ueber Netze hinweg zu verfolgen.

## 43 Hosting vs TTL

Der Grund dafuer ist, dass es keine automatischen Updates gibt.
D.h. die Adressen koennen sich theoretisch staendig aendern, ohne dass man es mitbekommt.
Also muss man staendig nachfragen.

Wenn ich mich also bsp mit Facebook verbinden will, bekomme ich vom DNS eine Adresse mit dem Hinweis, dass diese 1 Tag gueltig ist.
Also muss man jeden Tag einmal nachfragen, ob sich die Adresse geaendert hat.

Die Adresse von Facebook aendert sich allerdings im durchschnitt nur 1. alle 1.5 Jahre.
Eigentlich koennte man die Adresse weiter verwenden.

Das ist Problem ist nur, kann man feststellen, ob sich hinter der Adresse immer noch Facebook verbirgt.

## 44 TLS
Und das geht.
Mit TLS.
Hat man die Adresse eine Servers bekommen und baut eine Verbindung auf, so schickt der Server ein Zertifikat, das ihn eindeutig ausweist.
Und wenn man die authentizitaet eines Server pruefen kann, braucht man ja eigentlich keine DNS Anfrage mehr machen.

## 45 OpDNS
Das ist die Idee von opDNS.
Sobakkd wir einmal die Adresse des Servers haben behalten wir die einfach.
Und wenn eine Applikation die Adresse haben moechte bekommt sie sie, auch wenn die TTL abgelaufen ist.
Kommt vom Server das richtige Zertifikat ist alles in Ordnung.
Gibt es einen Zertifikatsfehler, dann muss man die Verbindung abbrechen und per DNS eine neue Adresse holen.

Das tolle daran ist, das geht vollkommen transparent.
Die Applikation muss nichts wissen, der Server muss nichts wissen, man braucht nur einen lokalen cache der die Eintraege behaelt und die Zertifikatspruefung macht.
Und schon kann man DNS fast komplett vermeiden.
Bis auf den ersten und die Anfragen im Fehlerfall.

Schoen waere es allerdings, wenn man die auch noch wegbekaeme.
Eigentlich koennte der Server die update Adressupdates selbst machen.

## 46 DSpush

D.h., wenn wir uns zu dem Server verbinden und er weiss, dass sich bald die Adresse aendert, dann schickt er ueber eine TLS Erweiterung die neue Adresse einfach mit.
Und damit kann dann der Cache aktualisiert werden.

Das erfordert natuerlich Unterstuetzung von den Servern, ist aber nicht schwer umzusetzen.
Damit koennte man dann DNS bis auf die 1. Anfrage ueberfluessig machen.

*46 mins*

## Conclusion

Kommen wir zum Ende.

## Contributions

Was haben wir gelernt.

Wir haben Klassifikation und Verschleierungstechniken kennengelernt und gesehen, was fuer Probleme sie haben.

Wie man unterschiedliche Techniken miteinander kombinieren kann um auch komplexe Regelwerke effizient zu implementieren.

Semantische Klassifikation ist aussagekraeftiger und effizienter, als syntax basierte.
Und man muss generell bei der Bewertung von Klassifikationssystemen aufmerksam sein.

Ich habe einen Prozessor fuer String matching Probleme vorgestellt, der mit wenig zusaetzlicher logik viel Speicher sparen kann.

Ich habe verschiedene verschleierungsTechniken fuer BitTorrent vorgestellt und gezeigt wie man mit wenig Aufwand komplexe Klassifikationsysteme ueberlisten kann.

Und ich habe gezeigt, wie es moeglich ist unnoetigen DNS Verkehr zu eliminieren um Privatsphaere zu schuetzen.

*48 mins*

## Future

Aber das sind leider nur ein paar Puzzleteile im Kampf um das Netz.
Es bleibt noch einiges zu tun.

Z.Bsp. muesste man die semantische Klassifikation vorantreiben indem man weitere Klassen und deren Eigenschaften beschreibt.

Wir haben an mehreren Beispielen gesehen, dass es sich lohnt gaengige Webprotokolle zur Kommunikation zu verwenden.
Man koennte sich eine zusaetzliche Protokollschicht vorstellen, die jeglichen Verehr in harmlosen Webtraffic uebersetzt.

Bei DNS gibt es noch weiter offene Fragen, zBsp bei Service Discovery.
Aber das kann man dann in der Diss von Daniel lesen.

Vielen Dank!





