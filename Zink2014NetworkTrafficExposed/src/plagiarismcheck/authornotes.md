# Author Notes for PlagCheck

This document lists all the matches identified by PlagCheck and the authors reaction / explanation for them.

## Check 2a

### Abstract

1. _good or bad lies in the eye of the beholder_

    Common expression imho

2. _Traffic classification is an important tool for network_

    Again a general observation and way of expression.


### Chapter 1

1. _can be used to promote certain services or_ 

    I have never seen the mentioned paper before. Promotion of services is also again a common practice and I this is a natural way of expressing this.
    
2. _DPI is a very expensive task both in space and time that only works on well-known signatures._

    Taken from my own paper, which is stated many times in the thesis.

3. _DPI is a very expensive task both in space and time that only works on well-known signatures._

    _It is prone to obfuscation and encryption but works reliably on plain text information._
    
    _Statistical/behavior analysis uses statistical information to evaluate the behavior of interesting flows or hosts and give estimates about possible application layer protocols._

    _Methods range from simple port matching to bayesian analysis and other machine learning techniques._

    _Though accurate results cannot be guaranteed, it is possible to identify obfuscated and encrypted protocols._

    _The more accurate the results have to be, the more expensive and sophisticated traffic classification gets._

    _Updating signatures and fingerprints for identification is retroactive and requires contin- uous monitoring and analysis of communication protocols._
    
    _Since traffic identification has to be done on wire-speed these systems are highly integrated into the network infrastructure and cannot be changed easily._
    
    _A change of rules usually results in a change of hardware that needs to be verified and distributed._
    
    _We argue, that future traffic classification must not rely on local communication syntax to identify exact applications but must instead exploit global communication patterns and semantics of application domains in order to be able to keep pace with rapid application changes._

    Taken from my own paper, which is stated many times in the thesis.

4. _Analysis and efficient classification of P2P file sharing traffic._

    _Exploiting Degrees of Freedom for Efficient Hashing in Network Applications._
    
    Reference Titles, no plagiarism

5. _Parts of [..] found their way into this thesis [...]_

    This also qualifies as a common expression imho.
    
6. _In Proceedings of the First Workshop on P2P and Dependability, P2P-Dep ’12, pages 2:1–2:5, New York, NY, USA, 2012._

    Reference data

7. _BitTorrent Traffic Obfuscation : a Chase towards Semantic Traffic Identification._

    Reference title, of my own paper btw.
    
### Chapter 2

1. _to communicate with each other, to exchange and_

    Again I think that qualifies as a common way of expression. How else to talk about how to communicate with each other? Oh I did it again.
    
2. _Data that flows from the source to the_

    Again a common expression. Data flows and it has to flow from somewhere to womewhere else.
    
### Chapter 3

1. _is difficult due to a large variety_

    General way of expression.

2. _depends on the point of observation as well [as ...]_

    Common expression. Also the _as well_ has a totally different meaning in my sentence.
    
3. _This continues until a complete match is found._

    I am not aware of the referenced document. It is completely off-topic and unrelated to anything in my thesis. Continuing until meeting a condition like a complete match ist also fairly general in computer science.

4. _Based on their evaluation the authors suggest three protocol design improvements to effectively avoid traffic classification_

    _Skype can use both TCP and UDP as well as a range of different codecs to transport and encode the VoIP messages and dynamically adjust to different network environments and to make identification harder_

    Taken from my own paper, which is stated many times in the thesis.
    
### Chapter 4

1. _how this knowledge can be exploited to design_

    Well, knowledge can be exploited, and in many cases it can be exploited to design something. This is a general way of thinking and expression. Again unaware of the referenced publication with is apparently some medical paper and totally unrelated to my thesis.
    
2. _provide a better understanding of how the network_

    General way of expression.
    
### Chapter 5

1. _BitTorrent also features a form of centralization in terms of trackers, though this has been loosened by introducing decentralized trackers._
    
    References a web collection of BitTorrent related articles that in turn contains excerpts of my own paper. Like this one.
    
2. _Content is not placed randomly but at specific locations._

    Ok. This one looks suspicious even to me. I cannot recall ever reading the referenced Master Thesis. However, it is related to P2P networks like this chapter. To be on the safe side, this sentences will be removed from the published thesis.

3. _Transfer of vast amounts of data at high speeds._
    
    The context is different of the referenced article. Also this is really common and highly general in broadband and P2P networks.
    
4. _algorithm we compare it to two_

    Well, in CS it is common to present algorithms and compare the to others.
    
5. _does not mean, that there are no false_

    _this also applies to many of the_
    
    Imho too general, especially in the context of evaluation and statistics.
    
### Chapter 6

1. _At the end of the l iterations, the result_

    Possibly common way of expressing the inner workings of algorithms. It's funny that it also is 'L' iterations in the referenced patent. However, it deals with som key algorithm controller, is off-topic, and I have never seen it before.
    
2. _This is only an educated guess_

    This is only a common expression. Like this. And this.
    
### Chapter 7

I checked all disputable sentences. Nearly all of them matche exclusively with my own two related publications. This is clearly stated at the beginning of the chapter and cannot be viewed as plagiarism. Other sentences are:

1. _wait a random amount of time_

    Way too general.

2. _There would actually be no point in trying to_

    Unaware of the referenced publication which is off-topic and deals with some lexicalization stuff. Also I think this is a general way of expression.
    
3. _shows the standard deviation and mean of _

    General expression in statistical analysis.
    
4. _the same port within a short period of time_

    Again unaware of the referenced article. The context is also completely different. My text describes the algorithm which just works that way.
    
### Chapter 8

1. _is that there is no way to actively_

    Imho again a general way of expressing something.
    
2. _“NSA collecting phone records of millions of Verizon customers daily”_

    This is a citation and clearly marked as such.
    
3. _It is a more precise way of describing the_

    Unaware of reference political publication. Off-topic. Completely different context. General expression.
    
4. _based on the observation that the number of unique_

    This might looks suspicious. The referenced article also discusses packet classification although I am unaware of it. When discussing the ideas of related work like here, it is common to talk about observations. Expressions like that can be found in many publications and are common.
    
### Bibliography

Everything in here is reference metadata which is suspected to be found in other publications. No plagiarism here.

