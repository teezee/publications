#!/bin/sh
# sanitize images
# remove certain effects that do not display on PDFKit-based viewers like Mac.

# list of images
#FIG="architecture archoverview effective_obfuscation_alt effective_obfuscation tcchallenges tcissues tcmotivation tctechniques"
#FIG="dns dnscurve dnsoutage dnssec dnstracking namecoin"
#FIG=`ls fig/tctech*.pdf | cut -d'/' -f2 | cut -d'.' -f1`
FIG=`ls fig/to*.pdf | cut -d'/' -f2 | cut -d'.' -f1`

echo "Trying to sanitize pdf transparency to display correctly on crappy Mac PDFkit ..."

if test -x $(shell which ${ps2pdfwr});
	then echo "Found converter, running...";
		d="fig"
		for i in $FIG; do
			echo "sanitizing $d/$i.pdf";
			ps2pdfwr -dCompatibilityLevel=1.4 -dHaveTransparency=false $d/`echo $i`.pdf $d/`echo $i`.sanitized;
			mv $d/`echo $i`.sanitized $d/`echo $i`.pdf;
		done;
	else echo "No converter found, skipping";
fi;