# Porposal Network Traffic Exposed and Concealed

## 1
How classify traffic and how prevent classification. What are the reasons
and challenges

## 2
Typical network, endnodes and routers / network infrastructure
Take granted that many packets reach destination

## 3
But routers have to perform many functions
has to be really fast
ex. 100 Gbps -> 12.5 GB -> minpkt 64B -> 2.7m p/s
(14 bytes (MAC Header) + 46 bytes (Data) + 4 bytes (CRC) = 64 bytes)
since not unlimited memory, need to put out packets on line speed
that is 4 ns

### 4
expensive limited infrastructure
large traffic, not able to handle it equally well all the time
transit costs over isps, P2P problem
Copyright holders push responsibility of content to ISPs
War on Terror / National Security issues, surveillance
Security

### 5
urged to classify and shape
classify traffic and do some action on it

### 6
Different goals based on granularity
most try fine grained might not be the best, see later

### 7
oldest headers, does not work well for newer applications ut still surprisingly good
next payload, look for known signatures
flow newer, assign each pkt to flow based on connection, compute statistics

### 8
exact match, if i know what to look for
heuristics, behavior patters
machine learning, statistics

### 9
nearly 2 decades of research
established algorithms exponential either in space or time, not suitable for high-speeds

### 10
high dimens packet size
packets arrive at high speeds, no time
due to speed not time to use memory, only small and expensive
need dedicated hardware, nowadays reprogrammable, changes of rulesets

### 11
still problem of large rulesets
designed from software architect point of view
multiple conditions, hierarchies
different types of rules
need to transform rules (regexes with fixed string parts, anchor strings ...)
find partitions with common characteristics
find localities that can be exploited
compile them to efficient specialized hardware engines
even then, traffic on engines to high

### 12
do prefiltering on pics
based on simple heuristics and small pattern matches.
devide the traffic into categories that are handled by individual engines, with hierarchy

### 13
first fixed string, then more expensive matching
from user and net designer point of view
violates net neutrality / end-to-end
threatens privacy

### 14
try to obfuscate
- payload using encryption, helps prevent dpi but not flow based
- flow with random padding / flushing, changing connection patterns
- hide inside well-known protocols, ssh, ssl, ipsec
contemporary approaches use payload obfuscation
minimal and defective flow obfuscation (MSE)
and onion routing

### 15
taking bittorrent as example we examined how small changes can break
all contemporary classification systems

bittorrent becaus it is responsible for large amounts of traffic and is the
most prominent application (besides web)
mention how MSE is broken, complicated not supported everywhere
uses handshake with clearly identifyable patterns, signature, options and size
shared file can be used for number of attacks, as can client version

### 16
generate random salte (noonce)
take target peer ID from different channel
use sha and encryption functions to encrypt fields
use random padding for variance in packet sizes
random flushing for larger packets

### 17
explain, straight forward, information encoded in the checksum

### 18
traffic before obfuscation and afterwards
ordinary easily shows signature, options, file as well as client version
no pattern in obfuscated traffic
small changes effectively breaks classification and can make it extremely hard.
requires keeping state
last resort: don't know it, drop it.

### 19
that why we're working on TASP
encode information in variable field of well known protocols, independent of
the protocol itself
use checksums to transport information
use reordering of fields
example bittorrent, encode options and files, send chunks using multiple messages

### 20
arms race expensive, customer has to pay for identification
fine-grained leads not to wanted results, example HTTP can be much more than web browsing
rather search for application-domain patterns and optimize infrastructure usage by
identifying bottlenecks etc
hiding in well-known protocols, changing flow features, keeping state
makes live on-line classification impossible to do




30 mins


All information hiding techniques that may be used to exchange steganograms in telecommunication networks can be classified under the general term of network steganography. This nomenclature was originally introduced by Krzysztof Szczypiorski in 2003

higher level als network 
15 Folien Inhalt + Meta 5 (Titel, Conclusion ...)


Meta

Motivation for both Packet ID
And Motivation for Hiding
(provider, government, china, arabischer fruehling, people)


Related work, oberflaechlich, paar bilder von datenstrukturen.
Gegen Steganographie abgerenzen
- 1:1 mapping loswerden (grosse herausforderung, codierung und transportmittel)
-- steganographien domain-abhaengig, bandbreiten limitiert (e.g. 1 bit pro pixel)
wir wollen ganze protokolle abbilden, nicht nur messages, transportunabhaengig,
sender und empfaenger unabhaengig voneinander evolvieren / synchronisieren.




single packet
vs state
vs flow


eht (2-3)
PC (kurz abhandeln)
- rulespace dimensions and cuts
- DPI
- DFI
- behavior
- probabilistisch
erkennungsupdate zyklus sehr lange


bis hierher breite
ab hier tiefe


Syntax vs Semantic (~2)
bsp BitTorrent Obfuscation (2-3)

TASP (2-3)


Conclusion + Future Work

- Publiste


TASP
Typische Protokolle, allgemeine Definitionen,
IMAP + SMTP + HTTP (kommandos wechseln etc)
- im einfachsten fall im POST / GET body
Felder mit Informationen zum Ausnutzen
Generische Vorabinformation (vs bekannte signaturen)
1:1 matching unflexibel (also von known proto zu unknown lookalike)
- variable informationsdichten
-- felder mit hoher informationsdichte (bsp base64)
- fliessende "pruefsummen" (auch selbst synchronisierend), ueber mehrere felder mit offsets + lengths
"Cookies" (keep state over multiple flows ZRTP)
Vorteil von nicht nur GET und POST, luecken erlauben, inject jpegs eg.

Email mit Info in Header vs Info in Body vs Info in Attachment


Sec FB
Soziale Netzwerke als sichere Kommunikationskanalaele
- kurz abhandeln


OLD PROPOSAL TEXT FOR EASY REFERENCE

\begin{abstract}
	The amount of network traffic, security and QoS demands, legal threats by
copyright holders, and economic pressure on Internet Service Providers (ISPs) is
steadily increasing. To satisfy demands, network providers employ traffic
analysis and shaping techniques. 
Established means of traffic analysis and resource planning are highly
integrated into the network infrastructure and thus doomed to
collapse under these ever-growing and ever-changing requirements. With traffic
increasingly abusing firewall-friendly ports and being obscured by encryption
and obfuscation, traditional port- or content-based analysis is doomed to fail.
%Additionally, they have legal issues and violate both the net neutrality and
%end-to-end principles. 
	
	Users on the other hand see their freedom and privacy endangered. With daily
communication shifting more and more into the network large scale network
traffic surveillance empowers service providers and governments to censor
unwanted information, threatening the very foundations of democracy.
Naturally, users, application programmers, and even
commercial service providers strive to hide their intentions and circumvent
traffic classification systems. While changing network infrastructure is
difficult, updating and distributing software is trivial.

	We design a traffic analysis system targeted towards highly resource
restricted environments that is capable of classifying vast amounts of traffic
in high-speed networks. The system is adaptable and robust to slight changes in
the ruleset. We then break all contemporary classification efforts by proposing
{\em The Any Signature Protocol (TASP)}, a generic self synchronizing and
adaptable communications protocol that can be applied to any application. We
argue that future traffic analysis must shift from fine-grained syntax-based to
coarse-grained semantic-based techniques to detect the traffic's application
domain based on communications behavior instead of the specific protocol.
	
	%2. Collaborative strengthening of distributedly collected weak evidence.
	%Even though individual flows, even more so individual packets, will be almost impossible to classify directly, essentially all protocols require at least some of the communication partners to exhibit a consistent long-term behavior. This behavior can be accumulated similar to anti-malware and anti-spam evidence and made available to members of the detection community. Our initial research on identifying VoIP and P2P traffic even under harsh conditions, where the applications tried to evade detection, has established confidence in the establishment of a reliable global distributed service directory using this approach.
\end{abstract}

%%%
%%% MOTIVATION / PROBLEM
\section{Problem Statement}
% Traffic Classification

Network routers perform a wide variety of functions of which the actual
routing is merely one (\cref{fig:rfun}). They have to deal with vast amounts of packets
arriving at high speeds. These packets have to be queued, cached, scheduled,
classified, and finally forwarded to the correct link.

\begin{figure}[ht!]
	\includegraphics[width=\linewidth]{fig/routerfunctions}
	\label{fig:rfun}
	\caption{Example router functions.}
\end{figure}


Network service providers have legitimate interest in knowing how their networks
are used. Multiple, sometimes conflicting, demands, both economical and legal,
force network providers to optimize their resources.



To satisfy all these demands network providers apply traffic classification and
traffic shaping techniques. Traffic classification can be categorized
by {\em Classification Goal}, observed {\em Traffic Features}, and {\em Method}
\cite{Zhang2009State}. The goal can be fine-grained to identify the exact
application or coarse-grained to classify traffic according to predefined
Contemporary. Features include information retrieved from packets (headers
and payload) or from flows (including bitrate, packetrate, or amount of data).
Methods include exact matches, machine learning (statistical analysis)
and heuristics. Taylor \cite{Taylor2005Survey} categorizes classification
techniques into the four areas {\em Exhaustive Search}, {\em Decision Tree},
{\em Decomposition}, and {\em Tuple Space}.

Contemporary traffic classification systems utilize a combination of Deep Packet
Inspection (DPI) and statistical or behavior analysis. DPI is a very expensive
task both in space and time and only works on well-known signatures while being
prone to obfuscation and encryption. Statistical\slash behavior analysis
(SA\slash BA) uses statistical fingerprints to evaluate the behavior of flows or
hosts and give estimates about possible application layer protocols. 

Updating signatures and fingerprints requires constant monitoring and evaluation
of communication protocols. While this poses no problem for low-speed
software-based classification systems, it is extremely difficult for high-speed
classification hardware. Those systems cannot be changed easily and a change of
rules usually results in a change of hardware that needs to be verified and
distributed.

% image with router functions
% queueing, forwarding, classification (firewall, QoS, QoS routing; e.g. latency vs. throughput vs. jitter), policies

%\smallskip

% Traffic Obfuscation
From a user perspective traffic classification systems are intrusive to privacy
and threaten freedom of speech. From a network designer's point of view they
violate both the end-to-end and net neutrality principles. As a result, users
try to circumvent traffic classification and prevent eavesdropping by applying
traffic obfuscation and encryption techniques. Common practices include
randomizing ports, exploiting well-known ports, and encrypting packet payloads.
While defeating port matching and DPI these obfuscation techniques are
vulnerable towards flow-based statistical and behavior analysis. Following
\cite{John2010Breaking-techreport} effective traffic obfuscation consists
of {\em Payload Obfuscation}, {\em Flow Obfuscation} and {\em Traffic Hiding}.

We demonstrate how easily contemporary traffic classification systems can be
fooled by applying simple payload and flow obfuscation mechanisms. We then
suggest a communication protocol that hides information within the syntax of
well-known protocols. {\em The Any Signature Protocol} can be applied to any
application and effectively hides communication within well-known protocol
signatures, thus appearing as ordinary HTTP or SMTP traffic. We conclude
that future traffic classification systems must refrain from syntax-based
application identification and rather exploit application-domain specific
communication semantics.

%%%
%%% APPROACH
\section{Traffic Classification}
% Traffic classification system
% * Rule preprocessing
% -- rule anatomy (fixed strings, variable strings, ranges, regex)
% -- extended hicuts
% * PICflow
% -- behavior based coarse-grained pre classification
% * FPGA Accelerated
% -- 'adaptive' DPI with modified PBV matchers
% -- parallel AC matchers
We propose a traffic classification system for resource restricted,
reprogrammable
environments in high-speed networks. We define traffic classification
system as a system that takes a given ruleset and transforms it into
hardware that is capable of classifying the traffic on line speed. The
system consists of multiple modules and matching engines that can be
used independently and in parallel. This is to early divide the traffic
and route it through different datapaths to reduce the amount of data
to be processed on each stage. The components include
{\em data structures} to store the actual information,
{\em rule preprocessing}, {\em pre-filtering} on physical interface
cards, and multiple {\em matching engines}.

\begin{figure}[ht!]
	\includegraphics[width=\linewidth]{fig/preprocess}
	\label{fig:preprocess}
	\caption{Rule preprocessing}
\end{figure}


\begin{figure}[ht!]
	\includegraphics[width=\linewidth]{fig/classify}
	\label{fig:classify}
	\caption{High-speed traffic classification component overview}
\end{figure}

% consider fig 8 from fpga dpi report here (slight modification to protocol parser)
%Rule preprocessing.
%Compile rules to engines and DPI module.
%Protocol parsing.
%Fix string matching.
%Variable string and regex matching.

\subsection{Data structures}

Many networking applications, including classification, require 
potentially large tables that must provide line speed lookup performance.
Hashing seems a natural candidate due to constant lookup complexity.
However, performance degrades in presence of collisions and resulting
chaining. Perfect hashing can guarantee a perfect mapping of the input
unverse but only works for static sets and is expensive to calculate.
We propose an {\em Efficient Hash Table}\cite{Zink2011Exploiting}
that uses compressed on-chip
bloom filters as summaries and wider off-chip buckets that can hold
multiple entries.

\subsection{Rule Preprocessing}
A ruleset can be seen as a multidimensional space where each individual
rule describes a vector. Packets with up to 1.500 bytes describe a
high dimensional space. Given that rulesets are potentially very large
with thousands of rules and conditions, the space to explore is
incredibly large.
Contemporary classification algorithms operate on few dimensions.
Rule pre-processing is required to greatly reduce the number of dimensions
and optimize the ruleset to be suitable for hardware processing.

%Analysis of real-world rulesets shows that rules can be categorized
Rules can be catergorized 
as being strings or regular expression, both either with fixed or
floating (variable) offset.
Each can include ranges or sets of values. A rule can
be composed of multiple conditions that need to be true for the
rule to fire.

Regular expressions are the most difficult to match in hardware,
since they require potentially large and slow state machines. On the other
hand fixed strings are the easiest to match. To reduce the number of rules
and dimensions and thus allow efficient hardware processing a number of 
transformations must be performed. First, rules are analysed and sorted
by matching frequency and correlations. Thus redundant, duplicate, and
competitive rules can be eliminiated. Sorting for matching frequency
also allows partitioning the rules into different sets according to
significance. Then regular expressions are transformed into fixed strings
wherever possible and quantifiers are reduced. Floating strings are anchored
or at least the offset ranges reduced to real world occurances.
Finally significant dimensions with high entropy are identified and the
ruleset is partitioned.
The partitions are then compiled into hardware matching engines.

\subsection{Coarse-Grained pre-filtering}

Incoming traffic is pre-filtered directly on physical interface cards
into a small number of pre-defined categories using very simple
behavior-based heuristics and port-based matching \cite{Zink2010Analysis}.
Traffic is then fed
into different matching engines based on category.

\subsection{Fixed String matching}

To support matching ranges and sets as well as a high number of dimensions
we suggest a modified {\em parallel bit vector (PBV)} engine. The PBV
consists of iterative and atomic byte matchers that can be configured
during runtime to compute a specific match function using opcodes.
Thus as the packet bytes traverse the matchers the engine can adapt to
the values encounter. Partial reconfiguration allows to easily adapt to
ruleset changes by exchanging individual matchers.  

\subsection{Floating string matching}

Matching floating strings is performed using multiple parallel enhanced 
Aho-Corasick automatons. Depending on clock rate and throughput
the AC engines use wider input words. Special states that use upper
and lower bound values can match ranges and wildcards but require
byte-wise comparison.

\subsection{Regular Expression matching}

Regular expressions are decomposed and compiled into several small,
dedicated NFA engines that can be fed individually and in parallel.
Only if a packet or flow could not be classified using the other
classification engines, NFAs that are potential classifier candidates
will be activated.

\section{Traffic Obfuscation}

Using BitTorrent as example application we show that contemporary
classification systems can be fooled. by applying simple payload and
flow obfuscation techniques. Payload encryption using a shared secret
and inpacket information 
randomizes the packet content and defeats DPI systems. Using random
padding and flushing to introduce variance in packet lengths aggravates
flow inspection \cite{Zink2012BitTorrent,Zink2012Efficient}.

Another possibility is to exploit well known plain text protocols to
hide arbitrary information. Here, floating checksums with variable
lengths and offsets encodes the actual information. State is kept over
flow boundaries and allows temporal decoupling. Without thorough
analysis of application and host behavior, traffic classification systems
will only be able to see ordinary web traffic.
