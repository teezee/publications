\documentclass{IEEEtran}
\usepackage[draft,href]{htwg}
\DeclareGraphicsExtensions{.pdf,.png,.jpg,.svg,.eps}

\begin{document}
%%%
%%% TITLE
\title{Network Traffic Exposed and Concealed}
\author{Thomas Zink ({\tt tz@uni.kn})\\Distributed Systems Lab\\Supervisers: Marcel Waldvogel, Oliver Haase}
\maketitle
%%%
%%% MOTIVATION / PROBLEM
\section{Problem Statement}
% Traffic Classification

Network routers perform a wide variety of functions of which the actual
routing is merely one (\cref{fig:rfun}). They have to deal with vast amounts of packets
arriving at high speeds. These packets have to be queued, cached, scheduled,
classified, and finally forwarded to the correct link.

\begin{figure}[ht!]
	\centering
	\includegraphics[width=\linewidth]{fig/routerfunctions}
	\label{fig:rfun}
	\caption{Routers must perform a multitude of functions. Examples include
	queueing, classification and forwarding.}
\end{figure}

Network Service providers have legitimate interest in knowing how their networks
are used. Multiple conflicting demands, both economical and legal,
force network providers to optimize their resources.

To satisfy all these demands network providers apply traffic classification and
traffic shaping techniques. Traffic classification can be categorized
by {\em Classification Goal}, observed {\em Traffic Features}, and {\em Method}
\cite{Zhang2009State}. The goal can be fine-grained to identify the exact
application or coarse-grained to classify traffic according to predefined
categories. Features include information retrieved from packets (headers
and payload) or from flows (including bitrate, packetrate, or amount of data).
Methods include exact matches, machine learning (statistical analysis)
and heuristics. Taylor \cite{Taylor2005Survey} categorizes classification
techniques into the four areas {\em Exhaustive Search}, {\em Decision Tree},
{\em Decomposition}, and {\em Tuple Space}.

Contemporary traffic classification systems utilize a combination of Deep Packet
Inspection (DPI) and statistical or behavior analysis. These are very expensive
tasks both in space and time. DPI only works on well-known signatures while being
prone to obfuscation and encryption. Statistical\slash behavior analysis
(SA\slash BA) uses statistical fingerprints to evaluate the behavior of flows or
hosts and give estimates about possible application layer protocols. 

Updating signatures and fingerprints requires constant monitoring and evaluation
of communication protocols. While this poses no problem for low-speed
software-based classification systems and forensics, it is extremely difficult for high-speed
classification hardware. Those systems cannot be changed easily and a change of
rules usually results in a change of hardware that needs to be verified and
distributed.

% image with router functions
% queueing, forwarding, classification (firewall, QoS, QoS routing; e.g. latency vs. throughput vs. jitter), policies

%\smallskip

% Traffic Obfuscation
From a user perspective traffic classification systems are intrusive to privacy
and threaten freedom of speech. From a network designer's point of view they
violate both the end-to-end and net neutrality principles. As a result, users
try to circumvent traffic classification and prevent eavesdropping by applying
traffic obfuscation and encryption techniques. Common practices include
randomizing ports, exploiting well-known ports, and encrypting packet payloads.
While defeating port matching and DPI these obfuscation techniques are
vulnerable towards flow-based statistical and behavior analysis. Following
\cite{John2010Breaking-techreport} effective traffic obfuscation consists
of {\em Payload Obfuscation}, {\em Flow Obfuscation} and {\em Traffic Hiding}.

%We demonstrate how easily contemporary traffic classification systems can be
%fooled by applying simple payload and flow obfuscation mechanisms. We then
%suggest a communication protocol that hides information within the syntax of
%well-known protocols. {\em The Any Signature Protocol} can be applied to any
%application and effectively hides communication within well-known protocol
%signatures, thus appearing as ordinary HTTP or SMTP traffic. We conclude
%that future traffic classification systems must refrain from syntax-based
%application identification and rather exploit application-domain specific
%communication semantics.

%%%
%%% APPROACH
\section{High-Speed Traffic Classification}
% Traffic classification system
% * Rule preprocessing
% -- rule anatomy (fixed strings, variable strings, ranges, regex)
% -- extended hicuts
% * PICflow
% -- behavior based coarse-grained pre classification
% * FPGA Accelerated
% -- 'adaptive' DPI with modified PBV matchers
% -- parallel AC matchers
We propose a traffic classification system for resource restricted,
reprogrammable
environments in high-speed networks. To be able to classify traffic on
line speed a first step is rule preprocessing.
A ruleset can be seen as a multidimensional space where each individual
rule describes a vector. Packet content describes a
high dimensional space. With rulesets consisting of thousands of rules and
conditions, the space to explore is potentially incredibly large.
Established classification algorithms operate on few dimensions.
Rule pre-processing is required to greatly reduce the number of dimensions
and optimize the ruleset to be suitable for hardware processing.
The ruleset is cut into several partitions according to rule characteristics,
significance and dimension. These partitions can then be compiled into
individual and specialized modules that can operate in parallel (\cref{fig:preprocess}).

\begin{figure}[ht!]
	\centering
	\includegraphics[width=\linewidth]{fig/preprocess}
	\label{fig:preprocess}
	\caption{Rulesets describe high-dimensional spaces. They are partitioned
	and compiled into speciliazed classification modules.}
\end{figure}

Classification, like many networking applications, requires 
potentially large tables that must provide line speed lookup performance.
Hashing seems a natural candidate due to constant lookup complexity.
However, performance degrades in presence of collisions and resulting
chaining. Perfect hashing can guarantee a perfect mapping of the input
unverse but only works for static sets and is expensive to calculate.
We propose an {\em Efficient Hash Table}\cite{Zink2011Exploiting}
that uses compressed on-chip
bloom filters as summaries and wider off-chip buckets that can hold
multiple entries.

The classification system itself is composed of multiple classification
stages (\cref{fig:classify}). 
Incoming traffic is pre-filtered directly on physical interface cards
into a small number of pre-defined categories using very simple
behavior-based heuristics and port-based and signature matching \cite{Zink2010Analysis}.
Traffic in different categories is
then handled independently and parallel by dedicated classification engines.
This divides the traffic early and reduces the pressure on later stages.

The classification engines are optimized for different types of rules.
Fixed strings are matched using a modified {\em parallel bit vector (PBV)}
that consists of iterative and atomic byte matchers that can be configured
during runtime to compute a specific match function using opcodes. This
allows matching ranges, sets and even wildcards.

Matching floating strings is performed using multiple parallel enhanced 
Aho-Corasick automatons. Depending on clock rate and throughput
the AC engines use wider input words. Special states that use upper
and lower bound values can match ranges and wildcards.

Regular expressions are decomposed and compiled into several small,
dedicated NFA engines that can be fed individually and in parallel.
Only if a packet or flow could not be classified using the other
classification engines, NFAs that are potential classifier candidates
will be activated.

\begin{figure}[ht!]
	\centering
	\includegraphics[width=\linewidth]{fig/classify}
	\label{fig:classify}
	\caption{High-speed traffic classification must use multiple stages and
	components that can run in parallel.}
\end{figure}

\section{Effective Traffic Obfuscation}

Using BitTorrent as example application
we demonstrate how easily contemporary traffic classification systems can be
fooled by applying simple payload and flow obfuscation mechanisms.
Payload encryption using a shared secret and inpacket information 
randomizes the packet content and defeats DPI systems (\cref{fig:bittorrent}).
Using random
padding and flushing to introduce variance in packet lengths aggravates
flow inspection \cite{Zink2012BitTorrent,Zink2012Efficient}.

\begin{figure}[ht!]
	\centering
	\includegraphics[width=\linewidth]{fig/bittorrent}
	\label{fig:bittorrent}
	\caption{BitTorrent obfuscation. The handshake message is obfuscated
	using a shared secret, sha1 hashing, and symmetric encryption \cite{Zink2012BitTorrent}.}
\end{figure}

We suggest a communication protocol that hides information within the syntax of
well-known protocols. {\em The Any Signature Protocol (TASP)} can be applied to any
application and effectively hides communication within well-known protocol
signatures, thus appearing as ordinary web traffic. 
Here, floating checksums with variable
lengths and offsets encode the actual information. State is kept over
flow boundaries and allows temporal decoupling. Without thorough
analysis of application and host behavior, traffic classification systems
will only be able to identify these obfuscated flows as ordinary web traffic.
We conclude
that future traffic classification systems must refrain from syntax-based
application identification and rather exploit application-domain specific
communication semantics.
%%%
%%% REFERENCES
\bibliographystyle{plain}
\bibliography{proposal.bib}

\end{document}
