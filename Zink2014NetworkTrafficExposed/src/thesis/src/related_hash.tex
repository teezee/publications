%!TEX root = ../phdthesis.tex
% from EHT Introduction
Efficient hashing in network applications is still a challenging task, because tremendously increasing line speed, demand for low power consumption and the need for performance predictability pose high constraints on data structures and algorithms. At the same time, memory access speed has almost stayed constant, especially because of the latency and waiting time between sequential repeated accesses. Hashing has yet to be widely accepted as an ingredient in hard real-time systems and hardware implementations, as prejudices concerning the unpredictability of size and time requirements due to collisions still persist.

Modern approaches make use of multiple choices in hashing \cite{Broder2001Using, Vocking2003How} to improve load and the number of memory accesses. Unfortunately, \emph{d-ary} hashing requires $d$ independent parallel lookups. To mitigate the need for high parallelism, table summaries \cite{Song2005Fast, Kirsch2008Simple}, based on (counting) Bloom filters \cite{Bloom1970Space/Time, Fan1998Summary} and derivates, further reduce the number of table accesses to one with high probability (w.h.p.) at the cost of fast but expensive on-chip memory (SRAM). The summaries allow set membership queries with a low false positive rate and some approaches also reveal the correct location of an item if present.

\smallskip

% from EHT related work
A hash function $h$ maps items of a set $S$ to an array of buckets $B$. Their natural application are hash tables, or dictionaries, that map keys to values. In theory, a \emph{perfect hash function} that is injective on $S$ \cite{Hagerup2001}, could map $n$ items to $n$ buckets. While perfect hashing for static sets is relatively easy \cite{Fredman1984Storing}, finding a suitable hash function that requires constant space and time to perform the mapping of a dynamic set is infeasible in practice. As a result, hashing has to deal with collisions, where multiple items are hashed into the same bucket. Na\"ive solutions anchor a linked list or an array of items to the overflown bucket or probe multiple buckets according to a predefined scheme. The need for collision resolution led to the persisting myth that hashing has unpredictable space\slash time requirements.

In 1994, Dietzfelbinger et al.\ \cite{Dietzfelbinger1994Dynamic} extended the scheme of Fredman et al.\ \cite{Fredman1984Storing} to store dynamic sets. Their dynamic perfect hashing resolves collisions by random selection of universal hash functions \cite{Carter1977Universal} for a second-level hash table.

Azar et al.\ \cite{Azar1994Balanced} observed, that allowing more possible destinations for items and choosing that destination with lowest load, both, the average as well as the upper bound load, can be reduced exponentially. This effect became popular as the ``power of two choices'', a term coined by Mitzenmacher in \cite{Mitzenmacher1996Power}. Vöcking \cite{Vocking2003How} achieved further improvements by introducing the ``always-go-left'' algorithm, where the items are distributed asymmetrically among the buckets. Broder and Mitzenmacher \cite{Broder2001Using} suggest using multiple hash functions to improve the performance of hash tables. The $n$ buckets of the table are split into $d$ equal parts imagined to run from left to right. An item is hashed $d$ times to find the $d$ possible locations. It is then placed in the least loaded bucket. Ties are broken by going left (\emph{d-left hashing}). A lookup requires examining the $d$ locations. Since the $d$ choices are independent, lookups can be performed in parallel or pipelined. A survey of multiple-choice hashing schemes and their applications can be found in \cite{Mitzenmacher2001power}.

\emph{Bloom Filters} \cite{Bloom1970Space/Time} represent set memberships of a set $S$ from a universe $U$. They allow false positives, that is, they can falsely report the membership of an item not in the set, but never return false negatives. Basically, a Bloom filter is a bit array of arbitrary length $m$ where each bit is initially cleared. For each item $x$ inserted into the set, $k$ hash values $\{ h_0, \dots, h_{k-1} \}$ are produced while $\forall{h} \in \mathbb{N} : 0 \leq h < m$. The bits at the $k$ corresponding positions are then set. A query for an item $y$ just checks the $k$ bits corresponding to $y$. If all of them are set, $y$ is reported to be a member of $S$. A false positive occurs, if all bits corresponding to an item not in the set are $1$. The probability that this happens depends on the number of items $n$ inserted, the array length $m$, and the number of hash functions $k$ as shown in \Cref{eq:fpbloom}.
%
\begin{equation}\label{eq:fpbloom}
	\epsilon = (1-(1-\frac{1}{m})^{kn})^k.
\end{equation}
%

The major drawback of Bloom filters is that they do not allow deletions. Fan et al. \cite{Fan1998Summary} address this issue by introducing a \emph{counting Bloom filter (CBF)}. Instead of a bit array, CBF maintains an array of counters $C = \{\varsigma_0, \dots, \varsigma_{m-1}\}$ to represent the number of items that hash to its cells. Insertions and deletions can now be handled easily by incrementing and decrementing the corresponding counters. Later, Bonomi et al.\ presented an improved version of CBF based on \emph{d-left hashing} \cite{Bonomi2006Improved}.

In \cite{Mitzenmacher2001Compressed} Mitzenmacher proposes arithmetic coding for Bloom filters used for exchanging messages (web cache information) in distributed systems. Recently, Ficara et al. \cite{Ficara2008MultiLayer} introduced a compression scheme for counting Bloom filters based on Huffman coding. They name their structure \emph{MultiLayer Compressed Counting Bloom Filter (ML-CCBF)}. The compressed counters are stored in multiple layers of bitmaps. Indexing requires perfect hash functions since collisions must be avoided. The structure provides near optimal encoding of the counters but retrieval is extremely expensive. The authors propose splitting the bitmaps into equal sized blocks and using an index structure to lower the cost of a counter lookup.

Bloom filters have since gained a lot of attention especially in network applications \cite{Broder2002Network}. Today, Bloom filters can be used as histograms \cite{Cohen2003Spectral} and represent arbitrary functions \cite{Chazelle2004Bloomier}.
% Newer applications make use of Bloom filters as summary structures.
In 2005 Song et al.\ \cite{Song2005Fast} suggested using Bloom filters as a hash table summary. This idea was later refined in \cite{Kirsch2005Simple}. Bloom filter-based summaries are also used for minimal perfect hashing \cite{Lu2006Perfect}.

% from EHT review
%-----------------------------------------------------------------------------
\subsection{Fast Hash Table}
%-----------------------------------------------------------------------------

Song et al.\ \cite{Song2005Fast} presented a new hash table design, named \emph{Fast Hash Table}, based on hashing with choices and counting Bloom filter summaries that targets hardware implementations and provides fast lookups by utilizing on-chip memory to optimize performance. Their scheme eliminates the need for parallel lookups usually required by multiple-choice hashing. Each $b$-bit counter ($b = 3$) in CBF summary corresponds to a bucket in the hash table and represents the number of items hashed into it. Note, that if $b$ is small, the probability of counter overflows can't be neglected. Song et al.\ propose using a small CAM for overflown counters. There are a total of $m$ counters\slash buckets where $m$ is calculated using \Cref{eq:mfht}.
%
\begin{equation}\label{eq:mfht}
	m_\textrm{FHT} = 2^{\lceil \log{c\ n}\rceil}
\end{equation}
%
The constant $c$ needs to be sufficiently large to provide low false positive and collision probabilities. It is set to $12.8$ which is considered optimal. $k$ independent hash functions, where $k$ is derived by \Cref{eq:kbloom}, are used to index both CBF and the hash table. 
%
\begin{equation}\label{eq:kbloom}
	k = \lceil \frac{m}{n} \ln{2} \rceil
\end{equation}
%

%
\begin{figure}[!t]
	\centering
	\label{fig:fht}
	\includegraphics[scale=0.5]{\fig fht}
	\caption[Fast Hash Table]{The two Fast Hash Tables. The Basic FHT (top) replicates every item. The Pruned FHT (bottom) only keeps the `left'most (`Left' refers to the table entry with the least index)}
\end{figure}
%

The \emph{Basic Fast Hash Table (BFHT)} simply replicates all inserted items to all $k$ locations in the table and increments the counters. As an improvement the table can be \emph{pruned} leading to a \emph{Pruned Fast Hash Table (PFHT)}. All replicas are removed except for the leftmost with the lowest counter value (\Cref{fig:fht}). A lookup only requires examining the least loaded bucket, i.e., the one with the lowest counter value. While pruning improves lookup time by reducing bucket loads, updates require an additional offline BFHT since items need to be relocated when their associated counters change.

Following \Cref{eq:mfht} the total amount of bits $\beta$ needed for the on-chip summary is dependent on the number of items and defined as
%
\begin{equation}\label{eq:bitsfht}
	\beta_\textrm{FHT} = 2^{\lceil \log{c\ n}\rceil} \cdot b
\end{equation}
%

%-----------------------------------------------------------------------------
\subsection{Multilayer Hash Table}
%-----------------------------------------------------------------------------

The rather high requirement of SRAM has later been addressed by Kirsch and Mitzenmacher \cite{Kirsch2005Simple, Kirsch2008Simple}. Their key idea is to separate the hash table from its summary to allow individual optimizations. They propose using a \emph{Multilevel Hash Table (MHT)} \cite{Broder1990Multilevel} consisting of $d = \log \log n + 1$ individual tables geometrically decreasing in size. An occupancy bitmap is kept in on-chip memory that allows efficient queries for empty buckets (see \Cref{fig:mht}).

%
\begin{figure}[!t]
	\centering
	\label{fig:mht}
	\includegraphics[scale=0.5]{\fig mht}
	\caption[Multilevel Hash Table]{Multilevel hash table with on-chip occupancy bitmap}
\end{figure}
%

The bitmap requires a number of bits equal to the number of buckets $m$ which is defined as
%
\begin{equation}\label{eq:mmht}
	\beta_\textrm{MHT} = m_\textrm{MHT} = \sum_{i=1}^{d} (c1 \cdot c2^{i-1} \cdot n)
\end{equation}
%
with the constants $c_1$, $c_2$ chosen such that $c_1 > 1$, $c_2 < 1$, $c_1 c_2 > 1$. Considering only the number of buckets per item the equation boils down to
%
\begin{equation}\label{eq:mmhtprime}
	\beta_\textrm{MHT'} = m_\textrm{MHT'} = c \cdot n
\end{equation}
%
The authors argue that $c=6$ buckets per item suffice. Later \cite{Kirsch2010Power} the authors refine the MHT by limiting the amount that items are allowed to be moved during insertions. In the most aggressive optimization scheme this can reduce the number of buckets per item to $c<2$ for $n=10^4$ at the cost of additional complexity. Note, that this does not affect the on-chip requirements of the MHT summaries, since they are deliberately separated from the actual hash table and their size only depends on the number of items. It has, however, an impact on the size of the occupancy (and deletion) bitmap.

Following Song et al.\ to eliminate parallel lookup overhead, Kirsch and Mitzenmacher present three summary structures, the \emph{interpolation search (IS)}, \emph{single filter (SF)} and \emph{multiple Bloom filter (MBF)} summaries. Since IS is not applicable in our targeted environment we will cover only the latter two summaries which are based on Bloom filters. They are depicted in \Cref{fig:sfmbf}.

%
\begin{figure}[!t]
	\centering
	\label{fig:sfmbf}
	\includegraphics[scale=0.6]{\fig sfmbf}
	\caption[Single Filter, Multiple Bloom Filter]{Single filter (SF) and multiple Bloom filter (MBF) summaries. The SF is a single Bloomier filter representing the type of an item. The MBF is an array of Bloom filters decreasing in size}
\end{figure}
%

The SF summary is a single Bloomier filter\cite{Chazelle2004Bloomier} representing the type $t$ of an item where $t$ corresponds to the sub-table of the MHT where the item is located. In addition to false positives, it can also return type failure. To keep the probability small the filter must be sufficiently large. The number of cells $m$ is defined as
%
\begin{equation}\label{eq:msf}
	m = n\ \log n
\end{equation}
%
With $\log \log \log n$ bits per cell the number of bits needed is
%
\begin{equation}\label{eq:bitssf}
	\beta_\textrm{SF} = n\ \log n\ (\log \log \log n)
\end{equation}
%

The MBF summary is constructed of an array of  Bloom filters $B = \{ B_0,...,B_{t-1} \}$. Each filter $B_i$ represents the set of items with type of at least $i+1$. Thus, a false positive on $B_i$ is equal to a type $i$ failure. This leads to the need of extremely small false positive probabilities to guarantee successful lookup. For a well designed MHT the number of bits the MBF requires is
%
\begin{equation}\label{eq:bitsmbf}
	\beta_\textrm{MBF} = n\ \log n
\end{equation}
%

Both, the SF and MBF summaries, support only inserts. To allow deletions significantly more effort is required. Kirsch and Mitzenmacher suggest two approaches. For \emph{lazy deletions} a deletion bitmap is kept alongside the occupancy bitmap in on-chip memory with one bit for every bucket in the MHT. On deletion, the corresponding bit is set to 1. During lookup, items in buckets that have a set deletion bit are ignored. The \emph{counter based deletions} add counters to the SF and MBF summaries to keep track of the actual number of items. The authors do not suggest specific values for the counter width nor provide evaluation. They state however, that a counting MBF requires about $3.3$ times more space than a simple MBF, that is 
%
\begin{equation}\label{eq:bitsMCBF}
	\beta_\textrm{MCBF} = 3.3 \cdot n \log n
\end{equation}
%
With $d$ choices and $\upsilon$ wide counters the modified SF requires
%
\begin{equation}\label{eq:bitssfc}
	\beta_\textrm{SFc} = \upsilon \cdot d \cdot n\ \log n
\end{equation}
bits.

%-----------------------------------------------------------------------------
\subsection{Segmented Hash}
%-----------------------------------------------------------------------------

A predecessor to the MHT is the \emph{Segmented Hash Table} \cite{Kumar2005Segmented} that also divides the hash table into multiple segments. Unlike the MHT, however, segments are equal sized. Each segment uses a Bloom filter to support membership queries for an item. The false positive probability needs to be extremely low to prevent sequential or parallel probing of multiple segments. A novel \emph{selective filter insertion} algorithm minimizes the number of non-zero counters by selecting that segment for insertion that leads the most empty counters. Thus false positive probability can be reduced. The authors argue that $16$ bits per item of on-chip memory and 16 or more segments suffice to provide good performance. To also support deletions an additional counting Bloom filter must be kept offline.

%-----------------------------------------------------------------------------
\subsection{Peacock Hash}
%-----------------------------------------------------------------------------

The authors later refine segmented hashing in \cite{4509624}. As with the MHT the idea is to have multiple segments that geometrically decrease in size according to a so called {\em scaling factor}. Each table, except the biggest main table, has an on-chip Bloom filter for membership queries. When an item is searched the filters of the subtables are queried. If lookup is unsuccessful the main table is probed. Again, the false positive probability needs to be extremely low to prevent multiple table accesses. With a scaling factor of 10 (each successive table has a size of $10\%$ of the former) and following the observations in \cite{Kumar2005Segmented}, about $2$ bits per item are needed for the on-chip Bloom filters.

%-----------------------------------------------------------------------------
\subsection{Discriminator Table}
%-----------------------------------------------------------------------------

The problem of non-deterministic lookup performance is addressed in \cite{Ficara:2009:DDA:1882486.1882519}. Here each item is associated with a {\em fingerprint} that is cut into chucks and stored in a small {\em discriminator} table. This table is used to index the main table and is stored on-chip. Fingerprints must be unique to prevent collisions. A genetic algorithm is suggested to find the perfect mapping. The authors show that a discriminator table with $4$ bits per item can be found in a reasonable amount of time. While it is possible to "build a perfect match [\dots] with fewer [2] bits per item [\dots] the effort [\dots] greatly exceeds the advantages." (\cite{Ficara:2009:DDA:1882486.1882519}, p.141.) Also, being a perfect hashing scheme, it works only on static sets and the discriminator table can only be built if the set of items is known a priori.
