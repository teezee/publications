\emph{There are numerous algorithms for packet classification based on many different aspects on packets or flows. This chapter briefly reviews many of the prominent, classical approaches of header-based classification. The list of algorithms is not exhaustive and descriptions are superficial. The purposes is not to deliver an in-depth discussion of proposed algorithms (this has already be done elsewhere multiple times) but merely to present a crude overview of possible solutions and the key observations they utilize.}

\subsubsection{Exhaustive Search}

A na\"ive approach is to search through every entry in the rule set. This can be achieved by using {\em linear search} through the list of rules or by using parallel searches through partitions of the rules. Clearly the storage requirement for linear searches is $O(n)$ while the search complexity depends on the number of partitions $p$ and is $O(\frac{n}{p})$. 

A massively parallel hardware-based approach uses {\em Ternary CAM (TCAM)} \cite{Montoye1994Apparatus} to search all $n$ rules in parallel ($n = p$). However, TCAM is highly expensive due to storage overhead and high power consumption. It is therefore not universally available.

\subsubsection{Hierarchical}

Hierarchical techniques utilize a hierarchical data structure like a tree or a trie to store and traverse the rule set. Each leaf in the tree then corresponds to a single rule or a small list of rules that can then be searched linearly. Since 5-tuple rule sets contain not only exact values but also ranges, sets and prefixes, tree construction might require a conversion of these different values and also leads to replication of rules. A na\"ive construction simply build a trie using the $w$ bits of each rule. A general observation of lookup and storage complexity states that lookup time is $O(w)$ where $w$ is the number of bits in the rule set used for the lookup, and that storage requirement is $O(2^{w+1})$ \cite{Taylor2005Survey}. However, many approaches improve upon these bounds by applying certain optimizations.

\paragraph{Grid-of-Tries (GoT).} 
Srinivasan et al. \cite{Srinivasan1998Fast} introduced the {\em Grid-of-Tries (GoT)} that facilitates fast packet classification by building a decision tree based on two dimensions of the rule set, usually the source and destination addresses. GoT stores rules only in one node and precomputes switch pointers to switch between subtrees and thus is able to eliminate rule redundancy. This leads to a storage requirement of $O(nw)$ and a lookup performance of $O(w)$. Unfortunately, GoT is only suitable for 2d classification and prefix matching. However, the authors also propose a simple extension for multiple field classification by partitioning the rule set and building multiple instances of the GoT.

\paragraph{Extended Grid-of-Tries (EGT)}
introduced by Baboescu {\em et al.} \cite{Baboescu2003Packet} extends the GoT to support multiple field classification without requiring multiple instances. This is achieved by storing lists of best matching rules instead of single rules and altering the switch pointers to point to candidate lists. The lists, however, must be searched exhaustively. EGT has a lookup complexity of $O(w^2)$ in the worst case.

\paragraph{Hierarchical Intelligent Cuttings (HiCuts) and Hyper Cuts.} 
Gupta and McKeown \cite{Gupta1999Packet} introduced the HiCuts algorithm. It
maps the packet classification problem to a $d$-dimensional space where the number of dimensions corresponds to the number of header fields in the classifier used in the classification process. HiCuts works by cutting the $d$-dimensional space along the $d$ axis of the hyperspace, thereby, dividing the search space into smaller partitions. The dimension to cut is chosen at each node such that the depth of the tree is minimized. Simplified, the primary goal of the cutting heuristics is to separate as many rules as possible into distinct partitions. While cutting the search space the number of rules in the partitions will decrease subsequently, however, it is possible that a single rule is part of more than one partition, leading to rule replication. The leaves then contain the list of rules that must be searched exhaustively.

Hyper Cuts \cite{Singh2003PCU} is an improvement of HiCuts. Instead of using one-dimensional cuts at each node, HyperCuts allows multidimensional cuts to reduce the height of the decision tree.

\paragraph{Modular Packet Classification (MPC)} \cite{Woo2000MPC} uses a similar approach like HiCuts to partition the search space. However, it presents a 3-stage architecture to store the rule set. First, the index jump table uses selected bits to find a pointer to a search tree. The corresponding tree is the searched to find filter buckets, that can contain multiple filters. The buckets are then search either linearly, binary, or using CAM.


\subsubsection{Decomposition}

Decomposition techniques take a multiple-field problem and break it into multiple single-field problems. This allows parallel independent searches and optimization. The results must be aggregated, doing this efficiently is the main challenge of these methods. The complexity of this problem arises from the fact, that simply locating the best match on one dimension is not necessarily representative for the best matching rule on all dimensions, since a rule might contain search values on individual fields that are less specific. Generally, decomposing the problem into individual searches allows parallelism and thus can achieve better lookup performance.

\paragraph{Parallel and Aggregated Bit-Vectors.}
Parallel Bit-Vectors (BV) \cite{Lakshman1998HPP} map filters, sorted by priority, to a $d$-dimensional space. Each axis is partitioned into intervals depending on the rules. Each interval is assigned a $n$-bit Bit-vector, where each bit corresponds to a rule, with 1 indicating a match and 0 indicating no match. Independent data structures are used to search the dimensions. A search returns the corresponding bit vector. Aggregation is then simply performed using a bit-wise AND.

The {\em Aggregated Bit-Vector (ABV)} \cite{Baboescu2001SPC} improves upon BV by partitioning the vectors into chunks. Each chunk is represented with a single bit in an aggregated bit-vector, where each bit is set to one, if the associated bit-vector chunk contains at least one set bit. A search then returns the aggregated bit-vectors which are ANDed to find the chunk containing the best matching rules. This eddectively reduces the numbe of memory accesses needed, since the information to retrieve is smaller. A downside of this approach is that it can't handle wildcards well. If many wildcards are present (leading to many 1's in the bit-vectors) the improvement over BV will decrease. 

\paragraph{Crossproducting}

\cite{Srinivasan1998Fast} is based on the observation that the number of unique search values in the fields are significantly smaller than the number of rules in the whole set. All uniques values of each field are combined in a set leading to $d$ independent sets, which are searched with independent data structures. To aggregate the results a crossproduct table is precomputed. This table contains the best matching filters for all possible combinations of the independent searches and is an associative array indexed by the concatenated $d$ search results. While being very lookup efficient, crossproducting has large memory requirements of $O(n^d)$.

\paragraph{Recursive Flow Classification (RFC)} \cite{Gupta1999PCM} also exploits structure in the rule set, specifically the observation that the number of overlaps is much smaller in real-world rule sets than the worst-case. The idea is that packet classification can be reduced to the process of mapping $S$ bit packet headers to $T$ bit {\em classID}s where $T\ll\ S$. Precompute all possible mappings would require too much memory so RFC does this mapping recursively and reduces the search space through multiple phases. The fields are split into chunks (multiple fields per chunk possible), which are used to index multiple independent tables. Their content is precomputed such that the space becomes narrower on every phase. Lookup results are concatenated, leading to a combined result in the last phase. % RFC paper also includes a large study of filter sets to be recognized later.

\paragraph{Tuple Space Search} \cite{Srinivasan1999PCU} is a 2D classification scheme that uses source and destination prefix tuples. It exploits the observation that the number of number of distinct tuples in the rule set are much lower than the number of rules. The tuple set is pruned and the resulting tuples used as keys to a hash table.

\paragraph{Distributed Crossproducting of Field Labels (DCFL).}

The original crossproducting idea is refined by Taylor {\em et al.} \cite{Taylor2004DCFL}. The key idea is to use simple encoding to assign labels to match conditions. Intermediate search results are combined to form sets and aggregation can be performed distributedly using set intersection.

\paragraph{Summary.}

Many of the proposed algorithms exploit structure in the rule set to optimize performance. The key observations of rule sets are (a) rule sets are relatively small with only a few thousand rules, (b) the number of unique values is small, (c) the number of values matched by any packet is small, (d) number of overlaps is small, (e) number distinct tuples is much less than the number of rules. The number of header fields used varies between only considering the source and destination addresses, considering the 5-tuple, or including other fields like ToS. Classification on more than two dimensions requires parallel lookups and sometimes dedicated hardware.
