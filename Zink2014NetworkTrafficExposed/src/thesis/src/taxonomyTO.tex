% Port knocking

There is little to no scientific work dedicated to obfuscate network traffic and avoid classification. Techniques used to obfuscate real-world traffic have their roots in general information security areas like cryptography, steganography and network security. The scientific community seems to be clearly biased towards the identification of network traffic. 

\begin{figure}[ht!]
	\centering
	\includegraphics[width=\linewidth]{effective_obfuscation_alt}
	\caption[Effective Traffic Obfuscation]{Effective Traffic Obfuscation as suggested by Hjelmvik and John \cite{John2010Breaking-techreport}. Encrypting payload data, obfuscating flow features, and hiding within other traffic.}
	\label{fig:toimproving}
\end{figure}

Effectively avoiding traffic classification primarily depends on the type of classification systems in place. With increasing effort to classify and discriminate all traffic, obfuscation itself must constantly evolve. An early na\"ive approach is to {\em randomize port} numbers or to exploit firewall-friendly ports in order to counter port-based identification. This simple method is effective for primitive classifiers and firewalls that do not employ content or flow inspection. {\em Encryption} can further protect the content from unauthorized observation. However, content encryption does not prevent statistical flow identification or behavior-based classification. {\em Disguising flow features} through varying packet sizes, transmission intervals, and directions can hinder flow-based classification.

% Bittorrent Paper
A report by Hjelmvik \cite{John2010Breaking-techreport} argues that obfuscation techniques are not sufficiently well implemented and that sophisticated statistical methods can identify even obfuscated and encrypted traffic with high accuracy. Based on their evaluation the authors suggest three protocol design improvements to effectively avoid traffic classification (\Cref{fig:toimproving}).

% Bittorrent Paper / 
\pagebreak
\subsection{Encrypt Payload}
\begin{wrapfigure}{r}{.2\textwidth}
	\includegraphics[width=.2\textwidth]{toenc}
\end{wrapfigure}
Encrypting payload data requires the communicating parties to exchange key information in advance to negotiate an encrypted connection.
The most prominent key exchange protocol used in many network applications including BitTorrent's Message Stream Encryption\slash Protocol Encryption (MSE\slash PE)  \cite{VuzeMSE}, eDonkey \cite{EDKObfuscation,eMuleProtocolObfuscation}, and SSL/TLS, is Diffie-Hellman-Merkle (D-H) \cite{diffie1980cryptographic}.
It allows the exchange of a secret over an untrusted medium without exposing it to an unauthorized observer.
More specifically, the cipher key is computed at each party individually, using public values, a private random value, and the received value of the communication partner.
An observer only sees parts of the secret and is unable to compute the cipher.
D-H does not, however, protect against man-in-the-middle attacks.

Other forms of key exchanges, for example using other media like email or phone, is often impractical for network applications that are expected to work without manual intervention.

An alternative to exchanging keys is to use a pre-shared common secret.
In this case it is imperative that the value cannot be intercepted or guessed, which would brake encryption.
A pre-shared secret has been used by Skype \cite{Berson2005Skype,Fabrice2005Skype} in the past before being acquired by Microsoft.

Unfortunately, even encrypted traffic can be detected, including the key exchange itself \cite{John2010Breaking-techreport}.
Most implementations do not sufficiently disguise the key exchange or the transmitted data and as a result the generated traffic exhibits statistical features which can be exploited for identification.

\subsection{Obfuscate Flow Features}
\begin{wrapfigure}{r}{.2\textwidth}
	\includegraphics[width=.2\textwidth]{toflow}
\end{wrapfigure}
Encryption alone cannot reliably protect against statistical fingerprinting.
Many applications like file download / sharing, streaming, and VoIP have strong flow features that can be detected even if the data itself is encrypted \cite{Waldvogel2006Stealth}.
To protect against identification flow features must also be protected.

To achieve this, the packet flows should appear to be random in at least length and direction.
The packet length can be altered by randomly flushing packets and randomly add garbage information to the packet content.
This, however, has impact on overall network performance, since it increases the number of packet and data sent.
Also, some applications like streaming or VoIP depend on a continuous stream of medium sized packets to guarantee a decent user experience.
In this case, randomly flushing packets or adding garbage is not feasible.
Skype can use both TCP and UDP as well as a range of different codecs to transport and encode the VoIP messages and dynamically adjust to different network environments and to make identification harder \cite{Biondi2006Silver}.

Another revealing feature is the direction of the flows.
Hjelmvik \cite{John2010Breaking-techreport} suggests to randomly change packet directions.
In practice this might often be not possible since certain types of conversations require a specific direction.

At the time of this writing we do not know about any applications actually implementing effective flow feature obfuscation.

\subsection{Traffic Hiding}
\begin{wrapfigure}{r}{.2\textwidth}
	\includegraphics[width=.2\textwidth]{tohide}
\end{wrapfigure}
Finally, hiding the traffic inside well-known traffic is another option.
Security protocols like IPsec or SSH can be used to tunnel information.
However, \cite{Dusi2009THD} has shown, that even tunneled traffic can be classified, again based on statistical fingerprinting.
As with encryption, tunneling does not suffice to avoid sophisticated classification.

Another means of traffic hiding is Onion routing.
Here, the traffic is routed through a multitude of routers, each adding a new layer of encryption (like an onion, hence onion routing).
This approach is taken by the Tor project \cite{Tor} and I2P \cite{I2P}.
