%!TEX root = ../phdthesis.tex

\emph{This chapter presents a survey of traditional and contemporary traffic classification techniques.
Parts of this chapter appear in \cite{6335792,Zink2012EBH}, especially the last two thirds of \Cref{sec:taxto} have been taken almost verbatim from these sources.
However, the author of this thesis is also the original author of that research.}

\bigskip

Contemporary traffic classification is a complex field that encompasses many different disciplines including data structures, algorithms, statistics, cryptography, hardware design, and even bio-inspired computing. Over the years there have been multiple approaches to classify traffic classification algorithms and produce a general taxonomy. However, the fast development of applications and increasing efforts in content encryption and flow obfuscation forces the classification community to constantly adapt and evolve. As a result, traffic classification has substantially changed over the years, adopting many new techniques from different research areas. This lead to a high fragmentation in the field and several issues remain to date that prevent fair comparisons between techniques due to the lack of common terminology, shareable traces and filters, and -- a most importantly -- an established ground truth. That is why some authors consider the field of traffic classification to be rather a form of art than ``real'' science \cite{Dainotti2012Issues}.

\bigskip

% Gupta
An early survey by Gupta and McKeown \cite{Gupta2001APC} covers algorithms that use multiple fields of network and transport layer headers for classification, specifically the source and destination addresses and ports. A rule associates values of the four dimensions with an action like ``deny'' or ``permit''. A set of rules is called a {\em classifier}. The example classifier is a typical representative of the kind of rules one can find in network security applications like firewalls. The authors identify four distinct classes of classification algorithms.

\bl
	\item {\em Basic Data Structures} encompass algorithms based on data structures like linked lists or tries.
	\item {\em Geometry-based} algorithms cut the search space into pieces and either perform hierarchical or independent searches.
	\item{\em Heuristics} exploit structure and redundancy in classifiers to  narrow the search space.
	\item{\em Hardware-based} algorithms require the use of special dedicated hardware, like TCAM, to match rules.
\el

% Taylor
Later, David Taylor \cite{Taylor2005Survey} also reviewed algorithms for packet classification that examine multiple header fields. A filter defines values in the header fields source and destination address, IP protocol type, and destination port. These fields are associated with an action that is a tuple of an assigned flowID and priority. The whole set of filters is called filter set. Taylor's taxonomy again proposes four classes, however, boundaries are fuzzy and hybrid techniques are possible. His taxonomy and the surveyed algorithms are depicted in \Cref{fig:taxonomyTaylor}.

\begin{figure}[ht!]
	\centering
	\includegraphics[width=.6\linewidth]{\fig taxonomy_taylor}
	\caption[Taxonomy of Internet traffic classification (Taylor)]{Taxonomy of Internet traffic classification as proposed by Taylor \cite{Taylor2005Survey}. Image taken from the original paper.}
	\label{fig:taxonomyTaylor}
\end{figure}

\bl
	\item {\em Exhaustive Search}: Examine all entries in the filter set.
	\item {\em Decision Tree}: The header fields are used to traverse a decision tree.
	\item {\em Decomposition}: The single fields of the filter set are searched independently and the individual results are aggregated. % vertical cuts
	\item {\em Tuple Space}: The filter set is partitioned and exact matches are performed on each of the partitions. % horizontal cuts
\el

% Nguyen 2008 4738466
Nguyen and Armitage published the first survey on traffic classification based on machine learning techniques \cite{Nguyen2008ITCML}. They use a performance metric to compare the different approaches based on false and true positives and negatives, which is a standard measurement in machine learning and classification. Machine learning uses a set of statistical traffic features usually based on packet sizes, inter-packet arrival time, duration and so forth. The extensive survey states, that most machine learning approaches for traffic classification focus on the following areas.

\bl
	\item {\em Clustering:} unsupervised techniques that cluster traffic into groups that exhibit similar traffic properties. Similarity is expressed with a similarity function, based for example on the distance in an euclidean space. Depending on the clustering technique used the identified clusters could overlap or exhibit some form of hierarchy.
	
	\item {\em Classification:} Supervised techniques that require training to build a knowledge base, or classification model, of pre-classified samples. Previously unknown samples are then compared to the model to find the correct classes. 
\el

% Kim et al 2008 
Kim {\em et al} are the first to compare traffic classification approaches that examine different traffic features \cite{Kim2008Internet}. The approaches are put into the classes {\em port-based}, {\em payload-based}, {\em host behavior-based}, and {\em flow feature-based}. The authors use a metric based on accuracy, precision, recall, and f-measure for evaluation.

% Callado et al 2009 
Callado {\em et al} use similar classes to describe Internet traffic classification approaches \cite{Callado2009Survey}. They evaluate {\em port-based}, {\em packet-based} and {\em flow-based} classification techniques using a metric based on accuracy and completeness. 

\bigskip 

% zhang et al 2009
A recent study \cite{Zhang2009State} encompasses over 60 papers spanning the last 15 years of traffic classification research. 
Their taxonomy is depicted in \Cref{fig:taxonomyCaida} and classifies the approaches according to classification goals, characteristics, methods, and datasets. Goals are either fine-grained to identify the application layer (5) protocol, or coarse-grained, to classify the traffic into pre-defined traffic categories like bulk, interactive, or P2P. Observed methods use packet- or flow-based features and include exact matches, heuristics, or statistical analysis. The authors note that the roughly 60 papers use more than 80 different datasets and conclude that this does not allow a systematic comparison. Other results of the review show that the amount of observed P2P traffic varies between 2\% and 98\%.

\begin{figure}[ht!]
	\centering
	\includegraphics[width=\linewidth]{\fig classification_caida}
	\caption[Taxonomy of Internet traffic classification (Caida)]{Taxonomy of Internet traffic classification as analyzed and proposed by Zhang {\em et al.} \cite{Zhang2009State}.}
	\label{fig:taxonomyCaida}
\end{figure}


\clearpage
\begin{table}[H]
\begin{center}
\begin{tabular}{ | l | c | l | l | }
	\hline 
	Survey & Year & Classes & Contribution \\
	\hline \hline
	\cite{Gupta2001APC} & 2001 & %
		\parbox{3.8cm}{%
			Basic Data Struct.\\
			Geometry-based\\
			Heuristics\\
			Hardware-based
		} & %
		\parbox{8cm}{
		First survey of algorithms for traffic classification on multiple fields. Introduction of a performance metric for memory requirements, update performance, scalability, flexibility, live applicability among others.} \\
	\hline \hline
	
	\cite{Taylor2005Survey} & 2005 & %
		\parbox{3.8cm}{%
			Exhaustive Search\\
			Decision Tree\\
			Decomposition\\
			Tuple Space
		} & %
		\parbox{8cm}{
		Extensive overview of $~20$ algorithms. States that performance comparison is difficult due to a large variety of implementation options and parameters. Concludes that future algorithms must use hybrid techniques and that the development of reprogrammable hardware allows cheap implementations.}\\
	\hline \hline
	
	\cite{Nguyen2008ITCML} & 2008 & %
		\parbox{3.8cm}{ %
			Clustering\\
			Classification
		} & %
		\parbox{8cm} { %
			First survey and evaluation of machine learning traffic classification. Evaluation metric based on true and false positives and negatives, and flow / byte accuracy. Concludes that ML for offline classification can achieve high accuracy, however, real-time ML classification is still in it's infancy. Combinations of ML models must be explored as well as parallel processing.
		}\\
	\hline \hline
	
	\cite{Kim2008Internet} & 2008 & %
		\parbox{3.8cm}{ %
			Port-based\\
			Payload-based\\
			Host-behavior-based\\
			Flow features-based
		} & %
		\parbox{8cm} { %
			First comparison study of algorithms using different features. Metric based on accuracy, precision, recall and f-measure. Concludes that port-based classification is still significant, that approaches are optimized for certain applications and combinations can increase accuracy, and that to establish a scientific ground truth it is essential to share tools, algorithms and data sets. 
		}\\
	\hline \hline
	
	\cite{Callado2009Survey} & 2009 & %
		\parbox{3.8cm}{ %
			Port-based\\
			Packet-based\\
			Flow-based
		} & %
		\parbox{8cm} { %
			Compares prominent techniques using a metric based on accuracy and completeness. Argues that port- and packet-based classification is predominant although prone to the exploitation of well-known and requirement on payload access. Shows that accuracy depends on the point of observation as well as applications to identify. There is no perfect classifier and validation is problematic.
		}\\
	\hline \hline

	\cite{Zhang2009State} & 2009 & %
		\parbox{3.8cm}{ %
			Goals\\
			Characteristics\\
			Methods\\
			Datasets
		} & %
		\parbox{8cm} { %
			Vast study on 15 years of traffic classification research. Shows that comparison of methods and results is impossible due to dependence on capture location (dataset), the unwillingness to share data, and the lack of standards for evaluation and classification goals.
		}\\
	\hline
			
\end{tabular}
	\label{tab:taxonomyGupta}
	\caption[Various taxonomies of Internet traffic classification]{Various taxonomies of traffic classification.}
\end{center}
\end{table}
