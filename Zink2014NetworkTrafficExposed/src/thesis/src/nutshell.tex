The Internet is a network of billions of devices ({\em nodes}) communicating with each other.
To join one must have a {\em carrier} -- an {\em Internet Service Provider (ISP)} or some other authority -- that is able to hand out addresses and carry data.
Communication in the Internet pretty much resembles real world postal services.
The sender packs the information into packets, sticks the source and destination addresses on them, and takes them to the nearest {\em router}, a device that keeps a record of addresses and routes, the {\em routing table}.
The router inspects the destination address and either delivers the packets directly to the designated destination (if it is in the routing table), or sends them to the next closest router he can find.
This process iterates until the packets are delivered.

For different reasons one or more of the carriers might be interested in what the contents of the packets or what the intentions of the senders are.
To gain that knowledge they can simply look at the stickers and stamps, the {\em headers}, to learn about the sender or receiver.
They can open the packets and look at the contents, that is, perform {\em Deep Packet Inspection (DPI)}.
Or they can look at the size and weight of the packets, and observe the rate that packets arrive.
They infer behavior and intentions from {\em statistical} properties of packet {\em flows}.
This packet and flow inspection is known as {\em Traffic Identification} or {\em Traffic Classification}, which is generally used interchangeably; although identification has a tendency to be more exact (find the identity) while classification implies a more coarse-grained approach based on classes or categories.

From a user point of view traffic identification invades privacy.
It violates the {\em end-to-end principle} \cite{Saltzer1984EAS}, which roughly states that functions should reside where they are required, i.e. a carrier should only carry packets and not open them, that function is only required at the end nodes.
Its applications to filtering also violate the {\em net neutrality principle}, which argues that the network should be neutral against the data it transports.
Naturally, the more privacy concerned users try to hide their data and intentions.
They can do this by putting locks on the packets and prevent others to open them ({\em encryption}).
They can add garbage to the packet content, or cut the content into pieces to change the statistical properties ({\em obfuscation}).
They can put the packets into other packets along with different content ({\em tunneling}).
And they can use different intermediate proxies to disguise true source and destination ({\em onion routing}).
All these techniques can be described as {\em Traffic Obfuscation}, the process of hiding the true content and intention from an observer or adversary.

This thesis explores how contemporary traffic classification and obfuscation are performed and how they can be applied in future and high-speed networks.