\section{Traffic Classification Challenges}

Multiple issues make high-speed live classification extremely difficult. They are depicted in \Cref{fig:tcchallenges}

\begin{figure}[ht!]
	\centering
	\includegraphics[width=\linewidth]{tcchallenges}
	\caption[Challenges of Traffic Classification]{The challenges of traffic classification. High dimensional search space, high frame rates, and small amounts of comparatively slow memory make real-time traffic classification extremely difficult.}
	\label{fig:tcchallenges}
\end{figure}

A significant challenge for efficient classification is the high dimensional search space. It can be deduced from computational geometry that searching a multi-dimensional space requires either exponential time with linear space or exponential space with linear time \cite{Gupta2001APC,varghese2004network}. Unfortunately, both space and time is rather scarce in todays network devices. To make matters worse, header-based rules include address prefixes and ranges of addresses and ports. Payload signatures can practically appear anywhere in the packet and also include regular expressions.

Line speed has experienced vast increases throughout the years while memory access times decrease slowly in comparison. The \emph{memory wall} \cite{Wulf1995HMW} is well present in packet processing. Considering contemporary high-speed networks with a throughput of 100 Gb/s and only maximum sized frames with 1,500 bytes of payload would require matching more than 8 million packets per second (p/s). In general, the number of frames that arrive on the link depends on several parameters.

\bl
	\item The size of frame header and footer $f_h = 26\ {\tt [B]}$
 	\item The size of frame payload, which is at minimum equal to the network layer (IP) header of $f_p = 46\ {\tt [B]}$ and at maximum equal to the maximum transmission unit of the network layer, which is $mtu = 1,500\ {\tt [B]}$. 
	\item The interframe gap, which is $ifg = 96\ {\tt [b]} = 12\ {\tt [B]}$\footnote{Actually, the interframe gap is measured in [$\mu$s] and represents the recovery time, needed to receive another frame without risking collisions. However, the standard ifg is equivalent to a time which would allow sending 96 bits, or 12 bytes, which is therefore the additional amount of (empty) data for a frame.}
	\item The throughput of the link $t\ {\tt [bps]}$
\el

This results in a minimum frame size of $84\ {\tt [B]}$ and a maximum frame size of $1,538\ {\tt [B]}$. The actual frame rate $f_r$ on a Gigabit link can then be computed as
$$f_r = t * 10^9 / ((f_h+f_p)*8)\ {\tt for}\ 84 \le f_p \le 1,538$$
\Cref{fig:framerates} shows the resulting frame rates for different Gigabit links.

\begin{figure}[ht!]
	\centering
	\includegraphics[width=.7\linewidth]{\fig framerates}
	\caption[Frame rates on Gigabit links]{Frame rates on different Gigabit links.}
	\label{fig:framerates}
\end{figure}

Header-based classification is independent of the packet's payload, here the worst case is actually minimum sized packets, since this leads to more packets to be classified per second. Payload-based classification suffers from large packets, where large amounts of data need to be searched. However, the frame rate drops with large packets, leading to an overall constant number of bytes to be matched. Even then, matching millions of packets per second leaves only a few nanoseconds processing time for each packet. Even with contemporary SRAM on-chip memory, matching the whole packet payload against a large database of signatures can simply not be done on line speed in high-speed networks. Therefore, custom hardware is required to do the actual matching. The problem with this approach is, customized hardware is expensive, especially if large amounts of high-speed memory are required. In the last decade a lot of research has been dedicated to optimize hardware requirements and provide efficient traffic classification implementations on FPGAs. These techniques often exploit structure in rulesets to find an efficient encoded representation.
