%!TEX root = ../phdthesis.tex

\emph{This chapter briefly describes the technical background of computer networking and traffic classification and obfuscation. \Cref{sec:proto,sec:packet,sec:router} are influenced by \cite{Kurose2009ComputerNetworking}, a book highly recommended for any reader interested in computer networking.}

% All content ORIGINAL

%-----------------------------------------------------------------------------
\section{Protocols, Layers, and Encapsulation} \label{sec:proto}
%-----------------------------------------------------------------------------

Managing the transmission of data through a network of billions of interconnected devices is not a small task. To achieve this requires well defined responsibilities and rules. Specific nodes in the network perform specialized functions. This is the concept behind {\em network protocols} and {\em protocol layers}.

A {\em network protocol} is a set of rules that define how communication partners actually communicate with each other. This includes how the messages should be formatted, in which order they are transmitted, and what actions should be taken during transmission and on receipt of a message \cite{Kurose2009ComputerNetworking}. 

%More formally:

%\newtheorem{protocol}{Def.}
%\begin{protocol}
%	A {\em network protocol} defines format and order of messages between two or more communicating entities as well as actions taken on transmission or receipt \cite{Kurose2009ComputerNetworking}.
%\end{protocol}

Network protocols are organized in {\em protocol layers}, which are {\em stacked} on top of each other. Each layer has a specific and well defined function. Network devices operate on multiple layers in a bottom-up fashion, that is, the more functionality a device exhibits the higher the layer it operates on. Each layer adds a new header (sometimes also footer) to the data from upper layers, which contains control information. This is known as {\em encapsulation} and allows clear separation of layers and functionality such that each device only has to check the headers of a specific layer. The data is also commonly referred to as {\em payload}. Thus the payload on one layer consists of the header plus payload of the layer above. End nodes usually need to implement all the layers, while intermediate nodes only require lower layer functionality. 

There exist multiple formal descriptions, or models, of protocol layers. The most prominent is the {\em OSI Reference Model} \cite{1094702}, which describes seven (7) layers. A simpler model is the {\em Internet Protocol Stack} as proposed by \cite{Kurose2009ComputerNetworking}, which we will use as reference throughout this thesis. It defines five (5) layers and is depicted in \Cref{fig:layers}. 

\begin{figure}[ht!]
	\centering
	\label{fig:layers}
	\includegraphics[width=.9\linewidth]{\fig layers}
	\caption[Protocol Layers and Encapsulation]{Protocol Layers and Encapsulation. The application layer adds data to a message. Each following layer adds its own header information (indicated by the initial letter of the layer followed by ``H''). The link layer also adds a footer (LF) containing additional control information. Influenced by \cite{wiki:UDPencapsulation} and \cite{Kurose2009ComputerNetworking}, p.55.}
\end{figure}

\paragraph{Application Layer.}

Responsible for all functionality required for network applications to communicate with each other, to exchange and understand  {\em messages}. Enables {\em process-to-process} communication between the processes running on the source and destination devices. Typical well-known application layer protocols include HTTP, SMTP, IMAP, FTP or SSH. Traffic classification usually tries to identify the application layer protocol used, without knowledge of the application itself.

\paragraph{Transport Layer.}

Implements {\em host-to-host} communication. This layer is responsible to transport {\em segments} between the communicating end nodes. There are two prominent transport protocols that offer different services. TCP offers a {\em connection-oriented} and reliable service that guarantees in-order delivery. UDP is a connection-less {\em best-effort} service, that does not offer guarantees. The transport layer header at least adds the source and destination {\em port} fields, that identify the processes on the end systems.

\paragraph{Network Layer.}

Delivers {\em datagram}s from the source to the destination, it offers {\em source-to-destination} communication. The {\em Internet Protocol (IP)} is the dominant network layer protocol. It defines IP addresses that are used to address hosts and networks. It is on this layer that routing is performed.

\paragraph{Link Layer.}

Implements {\em node-to-node} communication. The link layer transmits data only from one node to the next hop, it's neighbor. The connections between two nodes is the link. {\em Ethernet} is the most commonly used link layer protocol and addressing on this layer is done with {\em Media Access Control (MAC)}.

\paragraph{Physical Layer.}

Embodies the actual media over which communication occurs. This could be a cable or the air for wireless transmissions.

%-----------------------------------------------------------------------------
\section{Router Functions}  \label{sec:router}
%-----------------------------------------------------------------------------

Routers have to perform a multitude of functions. Those functions can be viewed as logically separated on different planes \cite{rfc3746}. The {\em forwarding plane} -- also data plane -- is responsible for specialized operations directly on packets and their data path, like examining headers and determining the outgoing link. Functions in the forwarding plane must be executed extremely fast, optimally at line-speed, to prevent congestion and packet drop. For that reason hardware is usually customized. The {\em control plane} provides generic control functionality. This includes executing routing protocols to build and maintain routing tables, performing network management and signaling. This separation of data and control functionality allows for more customizable hardware. It is depicted in \Cref{fig:routerfn}.

\begin{figure}[ht!]
	\centering
	\includegraphics[width=.9\linewidth]{\fig router_functions}
	\caption[Router Functions]{Example of router functions.}
	\label{fig:routerfn}
\end{figure}

The main function of a router is to move a packet from the input link to the correct output link, a process commonly known as {\em packet forwarding}. An industrial router will have hundreds, maybe even thousands of input and output links with possibly different line speeds. To find the output link a router must lookup the packet's destination address in the routing table, which contains {\em prefixes} of known networks and hosts associated with their corresponding links. These tables can grow potentially large with millions of entries. This prefix lookup is called {\em longest prefix matching} and is a challenging task due to the large amount of lookups in a large search space. A well known solution is {\em binary search on prefix length} \cite{Waldvogel1997SHS}. An overview of prefix match algorithms can also be found in \cite{varghese2004network}. After successful lookup the packet is switched from the input link to the output link. Since lookup can take some time, links can have different line speeds, and there might be collisions, routers must also provide queueing on input and output links. Unfortunately, memory is limited, when buffers run full packets have to be dropped. To prevent congestion and packet loss router functions need to perform at line speed.

The control plane of the router is responsible to calculate routes and build and update the routing tables by executing routing protocols. In addition to packet forwarding and routing modern routers also implement flow analysis and traffic classification.

%-----------------------------------------------------------------------------
\section{Of Packets and Flows}  \label{sec:packet}
%-----------------------------------------------------------------------------

The term {\em packet} is used ambiguously and in a variety of contexts. It commonly serves as a synonym for all packaged data sent over a packet switched network. Kurose and Ross describe packets as ``chunks of data [that] travels through [...] packet switches'' (\cite{Kurose2009ComputerNetworking}, p. 30). The OSI Reference Model \cite{1094702} reserves the term for layer 3 (network layer), which is the equivalent to Kurose's datagram. We will usually adopt the terminology established in \cite{Kurose2009ComputerNetworking} but will occasionally use the term packet for any unit of data that is composed of control information and payload, that is packets can be observed on any layer adding a header.

\bigskip

The definitions for a {\em traffic flow}, or simply {\em flow}, again varies.  In general a flow is sequence of packets between a source and a destination that share predefined properties and where the inter-packet arrival-time -- or idle time -- does not exceed a certain threshold. Flows are observed on devices in the network itself. They try to approximate end-to-end streams like TCP connections or UDP sessions, but are not necessarily equal to them \cite{rfc3697}. A common property traditionally used to define a flow is a 5-tuple consisting of header information from the network and transport layers \cite{rfc3697}:

\bl
	\item layer 3 source and destination addresses
	\item layer 4 source and destination port
	\item IP protocol field
\el

Note, that using layer 4 information on network layer (3) devices already violates the end-to-end principle. It might also be impractical to do so, since network layer packets could be fragmented and the payload could be encrypted, for example when using IPsec. An IPv6 flow is therefore defined as a 3-tuple based only on a label and the layer 3 IP addresses \cite{rfc3697}.

A traffic flow can be unidirectional or bidirectional. A bidirectional flow ignores the order of source and destination addresses and ports. Data that flows from the source to the destination and backwards is considered to be one single flow. The unidirectional flow considers the direction of a flow. A flow from source to destination is different to the flow from destination to source. An example is Cisco's {\em Netflow} \cite{rfc3954}, which is the industries {\em de-facto} standard for network flow monitoring and management. It defines a unidirectional flow using 7 values that include the 5-tuple in addition to the IP type of service field and the ingress interface.

\bigskip

The lack of consistent terminology has -- among other issues -- also been subject to recent studies \cite{Dainotti2012Issues,Salgarelli2007CTC,Callado2009Survey}. Dainotti {\em et al.} \cite{Dainotti2012Issues} define {\em flow objects}, which represent network traffic between hosts with different granularities.  Most of the terminology has been used in literature before, however, sometimes with different meanings. The relation between packets and flow objects is shown in \Cref{fig:flows}

\bl
	\item {\em Hosts:} A host is an endsystem node identified by its IP address.
	
	\item {\em Services:} Hosts communicate with each other via services. Services are combinations of the host address and the port number. A service is not necessarily unique for either individual process, nor individual applications, since processes can start multiple conversations and multiple different applications can be used to consume or offer a service with the same identifier. 
	
	\item {\em Conversations:} Communication between hosts can either be TCP connections or a UDP sessions. The conversation encompasses all traffic generated by the communicating applications and flows in either direction. \cite{Dainotti2012Issues} only defines Connections as flow objects.
	
	\item {\em Flows:} A flow is defined by the 5-tuple, is unidirectional, and terminates after a specific idle time (timeout). A conversation therefore usually consists of multiple flows, generally at least two. 
	
	\item {\em Biflows:} A biflow is a bidirectional flow. Basically it uses the same definition as a flow but the traffic flows in both directions thus ignores the ordering of source and destination addresses and ports.
\el

\begin{figure}[ht!]
	\centering
	\includegraphics[width=.9\linewidth]{\fig flows2}
	\caption[Packets and Flow Objects]{Packets and flow objects as perceived by \cite{Dainotti2012Issues}.}
	\label{fig:flows}
\end{figure}



Statistical information about flows, like number of packets, number of bytes, and timestamps and intervals are stored in flow tables, which are indexed using the flow identifier (e.g. the 5-tuple). An entry in the table is also called {\em flow record}.

%-----------------------------------------------------------------------------
\section{Traffic Classification and Obfuscation} \label{sec:tco}
%-----------------------------------------------------------------------------

Traffic classification has its origins in network security applications like firewalls and intrusion detection systems \cite{varghese2004network} ({\em IDS}). These systems are responsible to protected the network from malicious or unwanted packets. Firewalls enforce a relatively simple security policy that consists of rules that deny or allow traffic to and from hosts, networks and ports. IDS is more sophisticated and usually implements packet inspection and pattern matching techniques to locate malicious strings inside the packet content. Packet classification with the explicit purpose of ensuring Quality of Service (QoS) has also been considered in the design of the Internet Protocol. The type-of-service field (ToS) historically allowed adjusting packet individual priorities regarding delay, throughput, reliability and monetary cost \cite{rfc791,rfc1349}. ToS eventually evolved into Differentiated Services \cite{rfc2474}, which allow individual definitions of service classes and policies. 

Traffic classification today has more ambitious goals and tries to classify or identify the type of all traffic and then take action individually. The actions have not substantially changed. However, the scope has shifted from security or QoS related traffic shaping towards discrimination of specific applications or application categories. The evolution of traffic classification has in recent years been fueled by multiple factors like constant classification evasion efforts and increase in line speeds and user available bandwidth. Recent studies \cite{Zhang2009State,Dainotti2012Issues} present trends and evolution of traffic classification. A digest is shown in \Cref{fig:tchistory}.

\begin{figure}[ht!]
	\centering
	\includegraphics[width=.9\linewidth]{\fig tchistory}
	\caption[Arms Race]{The Traffic Classification and Obfuscation Arms Race. Influenced by \cite{Zhang2009State}}
	\label{fig:tchistory}
\end{figure}

Early classifiers relied on {\em port} numbers \cite{Zhang2009State,Dainotti2012Issues} to identify the application. RFC 739 \cite{rfc739} first introduced the {\em Assigned Numbers} that maps well-known network protocols to specific ports and thus enabled accurate and standardized port-based classification. A natural evolution is to include other header fields in the identification process, like source and destination addresses in case the provider of a specific service is known (e.g. directory servers of early P2P networks).

In order to avoid classification, applications began to randomize the port numbers (although mainly within a specific port range). The classifiers than countered with integration of {\em payload inspection}, which was already common in IDS. Here, the content of the packets is matched against a pre-defined set of application specific {\em signatures}. The discovery of these signatures is a lengthy process that requires monitoring, evaluating and sometimes reverse engineering network protocols. Signature databases can grow large and matching all the signatures against potentially large packets is highly expensive in space and time and does not scale well with increasing line speeds. Applications on the other hand can render content-based classification ineffective by regularly changing the communication protocol and signatures, or by using proprietary encoding, and implementing encryption.

With port- and content-based classification increasingly failing, traffic classification yet took another turn and adopted machine learning (ML) methods based on flow features (see \cite{Nguyen2008ITCML} for a survey). Statistical information about flows, like number of packets, packet rate, number of bytes, bit rates, and even the variances of byte values in the packet payload, form statistical {\em fingerprints} that can be used to identify applications or clusters of applications. Since these fingerprints are usually not -- or less -- dependent on packet content, ML can potentially classify encrypted protocols. While less intrusive on privacy, machine learning is computationally quite expensive and memory intensive \cite{Kim2008Internet}. Identification is not immediate due to the use of flow features that requires multiple packets. Obfuscating flow features through changing packet rates and sizes can avoid classification.

A recent development is to evaluate host behavior to infer the type of conversations they are engaged in \cite{Karagiannis2004TLI,Karagiannis2005BMT,Iliofotou2007NMU,Ngiwlay2008BPI}. Behavior analysis uses connection patterns based on network layer information like source and destination addresses and ports. Changing application behavior is more difficult than encrypting the content or obfuscating the flows, because to work as expected certain applications need to exhibit a specific behavior, e.g. P2P applications will always require many connections to multiple different hosts and per definition are both servers and clients.
