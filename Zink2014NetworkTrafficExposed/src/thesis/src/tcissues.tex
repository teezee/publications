%!TEX root = ../phdthesis.tex

In recent years several studies have revealed many issues predominant in contemporary traffic classification \cite{Salgarelli2007CTC,Kim2008Internet,Callado2009Survey,Zhang2009State,Dainotti2012Issues}. These issues are mainly caused by a high fragmentation of the classification community which in turn is the result of the historical evolution of the field. A significant effect is that the quality and results of classification systems cannot be compared nor validated. The observed issues can roughly be summarized as follows (\Cref{fig:tcissues}).

\begin{figure}[ht!]
	\centering
	\includegraphics[width=\linewidth]{tcissues}
	\caption[Traffic Classification Issues]{Traffic Classification issues: Lack of sharable data and public tools; different classification goals and no common metrics; no baseline; scalability problems.}
	\label{fig:tcissues}
\end{figure}

\subsection{No publicly available or shareable Data and Rulesets.}
\begin{wrapfigure}{r}{.17\textwidth}
	\includegraphics[width=.17\textwidth]{tcissuesdata}
\end{wrapfigure} 
Classification systems are evaluated using many different, not publicly available, data sets, which limits representativeness and validity.
Zhang {\em et al.} \cite{Zhang2009State} identified more than 80 different data sets used in less than 70 papers to evaluate classification techniques.
Traffic traces are hard to obtain due to privacy regulations and company policies.
Moreover, the recording location of traces is significant regarding the type and amount of observable traffic.
Traffic observed on e.g. university links is inherently different to that on access networks which again differs greatly to that on backbone networks.
The composition of traces also differs greatly.
Traces can be limited to flow information, or include sampled or individual packets with or without full or truncated payload.
The same problem applies to filter or rule sets.
Real-world (carrier) filter sets are rarely shared since their creation is costly and they directly affect the accuracy of traffic classification.
There are some rules freely available, but they are usually integrated into the actual classification tool.
Examples include OpenDPI \cite{opendpi}, nDPI \cite{ndpi}, L7-Filter \cite{l7filter}, or Snort \cite{Snort}.
Many rulesets used in academia are fictitious or randomly generated and rather small.

\subsection{No standard Measurement and evaluation Metrics.}
\begin{wrapfigure}{r}{.17\textwidth}
	\includegraphics[width=.17\textwidth]{tcissuesmetric}
\end{wrapfigure} 
Many traffic classification papers use different metrics or different proprietary definitions of seemingly equal metrics \cite{Salgarelli2007CTC}.
There are no common procedures to evaluate classification results, which further aggravates comparability. But not only the metrics differ, but also the objects that are measured. Many authors measure flow-based classification accuracy, although this does not lead any information about the actual amount of traffic classified, since the majority of flows are responsible for only a small number of bytes while the minority of flows amounts to a large number of bytes ({\em mice and elephants}). Depending on the application and classification intend a better metric could be to use byte-based accuracy, since this shows the actual amount of data that is classified \cite{Nguyen2008ITCML}.

\subsection{Different Classification Scope and Goals.}
\begin{wrapfigure}{r}{.17\textwidth}
	\includegraphics[width=.17\textwidth]{tcissuesgoal}
\end{wrapfigure} 
Some classification systems try to identify the precise application layer protocol or a subset of protocols.
Others cluster traffic into application categories or classes or again focus on a subset of classes like P2P.
Furthermore, there is no common definition of application classes \cite{Zhang2009State}.
The result is that different classifiers are optimized for specific network traffic classes and traces.
A combination of different techniques -- a {\em multiclassifier} -- could potentially outperform individual solutions \cite{Dainotti2012Issues}, a possibility that has yet to be thoroughly explored in research.
% what errors are allowed ? QoS, security, ...

\subsection{Few publicly available Classification Tools.}
\begin{wrapfigure}{r}{.17\textwidth}
	\includegraphics[width=.17\textwidth]{tcissuestools}
\end{wrapfigure}
Most researchers do not willingly share their tools and implementations making, validation even harder \cite{Kim2008Internet}.
It hinders comparability with other approaches. Commercial products are also not easily publicly available. There are some open source tools and variants of commercial products worth mentioning like L7-Filter \cite{l7filter}, nDPI \cite{ndpi} (the successor to Ipoque's now unavailable OpenDPI \cite{opendpi}), TIE \cite{TIE,Dainotti2009TCT}, or WEKA \cite{WEKA}.

\subsection{No Baseline or Ground Truth.}
\begin{wrapfigure}{r}{.17\textwidth}
	\includegraphics[width=.17\textwidth]{tcissuesbase}
\end{wrapfigure}
Since classification systems are inherently different and not comparable, and since there exist neither a perfect classifier nor annotated sharable traces, it is impossible to define a baseline or ground truth for proving the correctness of classifiers \cite{Dainotti2012Issues}.
This basically means that there is no way to effectively and correctly define a baseline and verify and compare traffic classification approaches.

\pagebreak
\subsection{Increasing Line-Speeds and Scalability.}
\begin{wrapfigure}{r}{.17\textwidth}
	\includegraphics[width=.17\textwidth]{tcissuesscale}
\end{wrapfigure}
While line speeds are permanently increasing, clock speeds and memory access times have stagnated in the last years, obscured by increasing parallelism.
Unfortunately, traffic classification approaches based on payload inspection or statistical features have high computational complexity and memory requirements \cite{Dainotti2012Issues,Nguyen2008ITCML}. This poses no problem for offline and forensic analysis but is impractical for online classification and requires either expensive custom hardware or sacrificing accuracy and features. Moreover, classification algorithms are not designed or optimized for parallel and multicore systems.
