\begin{center}
``NSA collecting phone records of millions of Verizon customers daily''\cite{theguardian2013NSA}
\end{center}

\bigskip

This is the headline Glenn Greenwald wrote for The Guardian on June, 6th 2013.
It was the first in an avalanche of articles unravelling the totalitarian and ubiquitous mass surveillance by the Five Eyes in general and the USA in particular.
The many top-secret documents Edward Snowden disclosed to the press have kept the media, the public, corporations, and governments all over the globe busy for nearly every day of the last 14 months.
This has sensitized (some) peoples to security and privacy issues of their digital life.

But we have not only witnessed the fall of privacy \cite{sterner2014secvspriv} for the sake of false security.
The last months also brought bad news for network security in general.
We have seen how Heartbleed \cite{heartbleed} and untrustworthy certification authorities \cite{google2014Maintain} compromise SSL\slash{}TLS security, without any reliable means to revoke certificates.
We had to stand by and watch while hacker's stole more than a billion unique user records distributed on tens of thousands of sites all over the world \cite{cybervor}.
On top of it all, many of those who strive towards data security are forced to shutdown \cite{theguardian2014Lavabit} or disappear for unknown reasons \cite{truecrypt}.

With all these devastating news, improving digital security feels like Sisyphean labor.
It is still a long journey towards secure and trustworthy networks.
This thesis aims to shorten the route, by providing insight into the motivation and techniques of both the classification and obfuscation communities, and by using this knowledge to device better ways to increase both security and privacy.

The goal we strive for is as pretentious as it seems inconceivable: A global communication network that is secure and privacy-preserving.
A network that provides services to everybody, without having to be paranoid about who might be stalking you, stealing your data or identity, trying to manipulate you, or exploiting your weaknesses. 

This cannot be achieved easily, if at all.
Misguided concepts and technical deficiencies of security and privacy techniques have prevented major breakthroughs in these fields.
This thesis tries to identify and eliminate some of these conceptual and technical flaws.

\section{Traffic Classification versus Obfuscation: An Arms Race?}

On a superficial level it seems inappropriate and inconsistent to endorse both traffic classification as well as obfuscation.
Both worlds often do not play well together and actively oppose each other.
Depending on where you stand, traffic classification and obfuscation have completely different and opposing functions. 

For the classification community, traffic classification is the savior, preventing the bad guys from doing harm who use obfuscation techniques to disguise their evil deeds.

\smallskip
{\it Traffic classification provides security}.
\smallskip

For the obfuscation community, traffic classification is an invasive and unacceptable privacy threat, spying on our communication and denying us our right of freedom of speech.
Traffic obfuscation techniques are required to preserve these rights by countering unauthorized classification.

\smallskip
{\it Traffic obfuscation provides privacy.}
\smallskip

It seems like classification and obfuscation are enemies and that you can either have security or privacy, but not both.
The truth is not as easy.
Classification as well as obfuscation can both be used for good and for evil.
To protect private and sensitive data from unauthorized access security measurements must be taken.
We need traffic classification to secure our networks with firewalls and intrusion detection systems.
We need it to protect against malicious programs, like worms, viruses, and trojan horses, that aim to steal our data and private information.

\smallskip
{\it Security provides privacy}.
\smallskip

However, in a global world, storing sensitive information in one location does not suffice.
Data needs to be exchanged.
Transmissions of sensitive data need to be protected against unauthorized observers and adversaries.
In order to communicate privately, we need traffic obfuscation techniques.

\smallskip
{\it Privacy provides security}.
\smallskip

In this context it becomes apparent that privacy and security do not exclude each other, but actually require and enhance each other.
It is probably one of the major misconception of our time that you have to choose between those two.

\section{Contributions}

This thesis aims to increase and enhance privacy and security in computer networks.
To achieve this goal we contributed to the two fields of traffic classification and traffic obfuscation.

\bnum
	\item {\bf State of the Arts} (\Cref{chp:taxonomyTC}).\\
Both traffic classification and obfuscation suffer from misguided concepts and technical flaws.
Traffic classification is a fragmented over many different disciplines and approaches. Several issues make traffic classification research difficult and prevent reproducibility and comparability.
The community slowly starts to react to these issues.
A clear definition of classification parameters, like goal and location, are paramount for selecting the appropriate classification tools.
On the other hand real-world traffic obfuscation is often insufficiently implemented and can be detected with sophisticated classification systems.

	\item {\bf Multiclassification} (\Cref{chp:rules}).\\
Traffic classification has to deal with a number of challenges like high line speeds, high dimensional search spaces, and very limited logic and memory.
Rulesets grow in size and complexity.
Contemporary traffic classification systems are not well suited for low-cost, high-speed classification and require large amounts of resources.
Analysis, preprocessing, and partitioning of rulesets allow the design of optimized classification engines that can run in parallel and require little space.

	\item {\bf Efficient Flow Classification} (\Cref{chp:picdfi}).\\
Using a very simple behavior-based algorithm and combining it with small sets of well-known ports and short application signatures it is possible to design a highly efficient classification engine.
The design is capable of dividing even obfuscated traffic into a small number of categories with high accuracy.
It is easy to implement and requires very little resources, making it a candidate for early classification in highly restricted components like physical interface cards.

To deal with the lack of ground truth we propose an algorithm that uses classification results of multiple classifiers to build a baseline.
We use only well known metrics to evaluate results and also reveal why classification results have to be taken with care.

	\item {\bf Enhanced String Matching} (\Cref{chp:pbv}).\\
Contemporary real-world classification rulesets contain a larger number of rules with few amounts of simple regular expressions.
Character sets, ranges, and wildcards are common, as well as defined quantifiers.
We designed a single-byte matching engine that is capable of matching a large subset of regular expressions.
The single-byte matcher is the base building block to build parallel matching engines that potentially can match all rules in the ruleset against the packet payload in parallel.
Through efficient encoding it is more space efficient than comparable solutions based on finite automatons.

	\item {\bf BitTorrent Obfuscation} (\Cref{chp:bittorrent}).\\
We show how only small changes in applications can render contemporary traffic classification ineffective. 
We present new obfuscation extensions to BitTorrent and apply and evaluate multiple different methods to disguise flow features.
The techniques are easy to implement, fully backwards compatible and fairly efficient.
They break even sophisticated statistical identification systems and make adaptation difficult and expensive to maintain and execute.
We show that one source of misclassification is the misguided concept of trying to identify exact application protocols.
	
	\item {\bf Opportunistic persistent DNS} (\Cref{chp:opdns}).\\
DNS is a highly revealing and privacy threatening system.
Associating DNS requests with IP addresses and geolocations allows a large number of attacks.
Blocking specific records or DNS as a whole completely prevents communication between hosts.
We propose two new extensions to DNS and TLS, named opDNS and DNSpush, that can transparently increase privacy, security, and reduce DNS traffic.
By eliminating unnecessary traffic, and associating DNS records with trusted applications and hosts, we eliminate user profiling and tracking, as well as prevent DNS outages.
The suggested extensions are orthogonal to existing solutions and work seamlessly with other security enhancing techniques.
They can be implemented in standard libraries making them transparent to client applications and the network infrastructure alike.
\enum

\section{Open Issues and Future Work}

As is the nature of research, once you try to answer one question, others keep popping up.
We believe that security and privacy are nonnegotiable and should be build into networks and operating systems by default.
Based on the result of this thesis we are working hard on many new ideas to reach this ambitious goal.

\subsection{Application Domains versus Protocol}

% Cisco Proposal 2011
We have shown that identifying the application layer protocol does not necessarily lead to information about the actual application domain.
Though it seems tempting to accurately identify the application layer protocol it might not be applicable or even essential to do so.
Several applications can use different protocols which have common or different application domains (file sharing, web browsing, VoIP, IM).
In general a set of application layer protocols provide a certain application domain.
Thus, there exist $m$ to $n$ relationships between {\em applications}, {\em domains} and {\em protocols}.

Instead of trying to identify the specific {\em protocol} it is more meaningful to classify the {\em domain} and only revert to lower level identification on demand, e.g. to extract domain specific information.
Every application domain has specific attributes and requirements on the network which can be exploited for classification.
Although the exact protocol is not identified, knowledge about the traffic's domain is actually more valuable to the network provider.
It is a more precise way of describing the traffic's purpose than the type of the protocol.

This \emph{domain behavior classification} is superior to DPI or BA on multiple levels. It is not prone to protocol\slash{}application changes and even encryption\slash{}encapsulation.
Unknown applications and protocols can be classified without the need to identify specific protocol signatures.
The traffic behavior of any domain is not likely to change fast or often.
Classification will be stable over a long period of time and requires few updates and little maintenance.

While there are promising approaches for statistical and behavior analysis, current efforts still aim to classify application protocols, or classes of protocols, and they still suffer from a wrong concept.
A paradigm shift has to take place to acknowledge the use of application domains instead.
PICDFI (\Cref{chp:picdfi}) is a first step in the right direction, but is limited to two classes with very different behavior.
More application domains need to be identified, described, and algorithms for classification need to be designed.

\subsection{Distributed Traffic Classification}

% Cisco Proposal 2011
Contemporary traffic classification is powerful but expensive.
This is due to classification being a highly local process, limited to specific areas in the network.
Traditionally, network devices perform individual and independent traffic classification and monitoring.
To gain limited global knowledge a dedicated centralized instance collects and aggregates traffic records on a regular basis (e.g. \emph{netflow}).
Human interaction is required to evaluate traffic patterns and take action.
These classification systems require vast resources to be able to deal with the high amount of high-speed traffic.

We argue that distributed sensor networks can significantly reduce classification complexity and furthermore lead to scaleable global information retrieval.
Our goal is to provide network service providers with a scaleable \emph{global service directory}, a directory that allows classification of hosts, their relationships, traffic, and services.
Network service providers will be able to utilize this global knowledge to ensure security, provide better quality of service and tighten customer relations.
By collaboration, the directory can even be extended to a much larger scale.
Similar to P2P networks, a sensor network can be created to exchange information on network level devices.
Sensors use their local information and information retrieved from peers in the sensor network to monitor and classify traffic.

\subsection{TASP - The Any Signature Protocol}

We explore possibilities to introduce more randomness into the traffic obfuscation process and apply more sophisticated flow feature obfuscation.
A promising approach seems to disguise traffic to appear to originate from other applications and protocols.
This tricks traffic classification systems into classifying the traffic as harmless, ordinary web traffic.

A protocol can be envisioned that hides information within the syntax of well-known protocols.
{\em The Any Signature Protocol (TASP)} can be applied to any application and effectively hides communication within well-known protocol signatures, thus appearing as ordinary web traffic.

Floating checksums with variable lengths and offsets encode the actual information, similar to the magic peer ID proposed in our BitTorrent obfuscation scheme (\Cref{chp:bittorrent}).
State is kept over flow boundaries and allows spatial and temporal decoupling.
Without thorough analysis of application and host behavior, traffic classification systems will only be able to identify these obfuscated flows as ordinary web traffic.

We are working on translators that transform BitTorrent traffic into ordinary HTTP and SMTP traffic.
An observer will not be able to detect thus obfuscated traffic without evaluating global connection patterns.
The ultimate goal is to provide TASP on an operating system library level to translate any traffic into harmless web traffic.

\subsection{DNS Privacy}

We have established that DNS is a major privacy threat.
This is especially true for DNS-based service discovery protocols like {\em Bonjour} and {\em Zeroconf}.

We are working on improving the whole DNS infrastructure.
The extensions suggest in \Cref{chp:opdns} (OpDNS and DNSpush) need to be implemented and tested.
Apart from that, we also work on private service discovery methods \cite{dkmw2014adding,dkmw2014efficient}.
These methods could also potentially be used for global scale, peer-to-peer service discovery.
But this is material for yet another thesis.
