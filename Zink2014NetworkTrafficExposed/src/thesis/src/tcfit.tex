The multitude of different taxonomies shows that traffic classification research still suffers from a high fragmentation of approaches, definitions, terminology, and used metrics. While we do not want to introduce yet another taxonomy we feel that it might be useful to present an encompassing overview of network traffic classification characteristics and terms that are used throughout this document. It is depicted in \Cref{fig:taxonomyComplete}. The presented taxonomy is not entirely new, it is more of a combination of results and discussions proposed in earlier surveys, specifically \cite{Taylor2005Survey,Kim2008Internet,Nguyen2008ITCML,Callado2009Survey,Zhang2009State} enriched with some personal insights.

\begin{figure}[h!]
	\centering
	\includegraphics[width=\linewidth]{\fig taxonomy_complete}
	\caption[Taxonomy of Internet traffic classification (Unified)]{A unified taxonomy of Internet traffic classification.}
	\label{fig:taxonomyComplete}
\end{figure}

\subsection{Purpose}

The purpose of applying traffic classification is crucial for for determining the actual classification technique. It determines the type of rule set and actions which are provided for the classifier. It also sets requirements for both the actual application of classification as well as the scope, goal, and expected accuracy. Subsets of theses purposes have been discussed in \cite{Taylor2005Survey,Callado2009Survey}.

\paragraph{QoS} tries to classify traffic into predefined type of service or traffic classes that can individually be shaped to ensure the quality of certain applications with specific network requirements. In general it can be said that QoS does not require very high accuracy. While it would be beneficial to be able to dynamically adjust the quality of all traffic, it is not a requirement for providing the service. Thus, QoS related traffic classification can improve the quality of identified services, but it does not degrade the quality of unidentified services.

\paragraph{Billing} uses traffic classification to identify classes of specific services or applications ti allow application- / service-based accounting. It also includes discriminating services or applications that might produce high transit costs or that a carrier might want to downgrade (competition) or promote (own services). From an economic point of view the carrier or service provider has high interest in a high classification accuracy, since unclassified traffic means either a loss of revenue or additional transit costs.

\paragraph{Security} includes anomaly detection like worms or spam, as well as intrusion detection, and network / service access to prevent unauthorized and malicious traffic. Since malicious traffic can pose a serious threat to providers and consumers alike it must be identified accurately and very early, thus requires highly accurate live classification. However, another security related classification purpose is crime investigation in which the traffic is not classified live but 

\paragraph{Prognosis} tries to identify trends in application usages to infer the impact on the network. This can provide a better understanding of how the network might be used in the future and enables better more optimized traffic shaping and network development.

\subsection{Goal}

The classification goal describes the granularity of classes in which the traffic should be categorized. Following \cite{Zhang2009State} the goals can either be fine-grained or coarse-grained. A fine-grained classification goal tries to identify the exact {\em application} layer protocol responsible for the observed traffic. Coarse-grained classification goals use predefined and arbitrary categories to group the traffic. There is no consensus in how to define theses traffic classes, however, it can be observed that there is a difference between {\em Application Classes} and {\em Service Classes}, also referred to as traffic profiles \cite{Dainotti2012Issues}. {\em Application Classes} group applications based on their intended usage, like ordinary web traffic, instant messaging, VoIP, or P2P. {\em Service Classes} group network traffic based on the type of service they require. This includes best-effort, bulk transfers (up/downloads), interactive (user interaction), realtime (streaming, VoIP), or transactional (request/response based). A great discussion of service classes can be found in \cite{Cisco2008QoS}. The desired classification goal strongly depends on the purpose. For example for QoS a coarse-grained classification based on service classes proves much more suitable and efficient than directly identifying the application. However, billing might require identifying the application or specific application classes. It is also important to note, that application layer protocols can be used in different contexts, and different traffic using the same application protocol can fall into different application or service classes. An example is HTTP that can be both classified as transaction but also bulk transfer. BitTorrent, a prominent P2P application, also uses HTTP for tracker communication. Here, trying to identify the application layer protocol would clearly be misguided. It is essential to clearly define the classification goal in order to deploy the optimal classification solution.

\subsection{Scope}

While the goal describes how traffic should be categorized the scope is what kind of traffic, applications, or classes the classifier can actually classify. Some techniques try to classify all traffic ({\em general}) while others focus on {\em specific} applications or classes, like identify Skype, or identify all P2P traffic. These approaches often achieve high accuracy in their domain but obviously fail to classify other traffic. General techniques often sacrifice accuracy for the sake of being generic. Depending on the classification purpose and goal, the scope is invaluable for choosing the right techniques.

\subsection{Application}

This states how traffic classification should be applied and therefore put constraints on the applicability of the methods. {\em Realtime} application is a live classification of traffic on line-speed. {\em Monitoring} requires a network operator, who uses the classification system to monitor the network and who can interactively perform actions. Although monitoring systems also need to deliver a timely view on network usage the requirements are much lower than for realtime classification since they do not need to identify traffic on the wire but mostly use sampled flow information, an example being Cisco's netflow \cite{rfc3954}. Obviously, monitoring is only applicable on long-lived flows. Finally, {\em Forensics} is used offline and a posteriori to analyze captured traffic traces. This is used for crime investigation, surveillance and also prognosis.

\subsection{Link Location}

The location of the link where observation occurs vastly influences the amount and type of data available to use for classification. On the {\em network edge} all flows entering and leaving the network are visible, while in the {\em network core} only subsets of flows, even unidirectional in case of asymmetrical routing, can be observed. Here we also distinguish between a {\em carrier} and a {\em provider}. The {\em carrier} is responsible to carry packets to many attached networks and thus will have many {\em edge} routers and a strong {\em core}. A {\em provider} can be any instance that offers Internet access including company sites or universities. Obviously the type and amount of traffic is inherently different on the different level of {\em carriers} or {\em access providers}. This highly influences the purpose and goal of classification.

\subsection{Data}

This describes the type of data available for the classification system and is mostly identical to the dataset  set property of \cite{Zhang2009State}. However, we further distinguish flows into unidirectional flows and bidirectional flow ({\em biflows}) using the terminology established in \cite{Dainotti2012Issues}, since this greatly affects the applicability of certain classification approaches.

\subsection{Method}

{\em Method} describes the methods used by the classification system and is a combination of the methods presented in \cite{Zhang2009State} and the taxonomy of \cite{Taylor2005Survey} with the classes {\em decision tree} and {\em tuple space} unified under {\em hierarchical}.

{\em Point Location} is used to find the best matching entry in a rule set and usually uses values in headers or payload composed of exact values, ranges, sets or prefixes. This can be achieved by searching through all the entries ({\em exhaustive search}), by building a {\em hierarchical} data structure which is traversed, or by {\em decomposing} the problem into individual searches and then aggregate the results. {\em Pattern matching} specifically describes techniques to find specific strings or regular expressions in the packet's payload and is primarily used for Deep Packet Inspection.

{\em Machine Learning} encompasses techniques that use statistical information to cluster or classify the traffic. The classes can be predefined or even automatically deducted through ML. Supervised techniques require a manually defined training set as rule set while unsupervised techniques can identify similarities within unseen traffic.

Finally, {\em Behavior Analysis} includes all approaches that exploit assumptions based on the properties of applications, that is, how traffic and connection patterns presumably look like if hosts are engaged in conversations using specific applications.

\subsection{Traffic Features}

This describes which traffic features a classification method uses. It is based on feature descriptions in \cite{Zhang2009State,Nguyen2008ITCML,Kim2008Internet,Callado2009Survey}. It is important to understand that features might be observed on different network flow objects.

One feature used predominantly in traffic classification is the {\em packet headers}. This includes information from multiple layers, especially the source and destination ports and addresses, but also signal information like the TCP control bits. Another field often used is the IP protocol or type field. {\em Packet headers} are observed on individual packets.

The {\em application payload} is mainly used for deep packet inspection and intrusion detection. However, some machine learning and behavioral approaches use statistical information extracted from the payload, like variance in byte values. The application payload is usually observed on individual packets, but also might span multiple packets if fragmentation is present and therefore might require reordering and reassembly.

{\em Flow features} includes statistical information about flows, like packet sizes, bitrates, duration and burstiness. These features are mainly used for machine learning approaches. Note, that flow features might also include information about ports and addresses, however, they are aggregated and observed on a flow level, i.e. valid throughout the lifetime of the whole flow, rather than on individual packets.

{\em Behavior} describes features that are based on the observed behavior of applications and hosts in terms of connection patterns over time. Observations on behavior are usually performed on ports and host addresses. It is, however, different from the {\em packet header} feature above, since it operates not on packet-level information but on aggregated flows, i.e. the {\em service object} as described in \cite{Dainotti2012Issues}.

\subsection{Metrics}

This is a compressed combination of the metrics discussed in \cite{Nguyen2008ITCML,Kim2008Internet,Taylor2005Survey}. The purpose is to be able to evaluate both the performance in terms of classification quality but also in terms of complexity, applicability, and scalability. It includes the standard classification metrics of precision, recall, and f-measure (on the flow level), accuracy of identified flows and bytes (calculated from the flow volumes), and the ration of unknown or unidentified traffic (both flows and bytes). Furthermore, performance metrics include the complexity of a method and the update performance, i.e. how easily is it to update the rule set.
