%!TEX root = ../phdthesis.tex

% More recent related work
% Multidimensions:
% \cite{Song2013ABC}
% The extensions to AC:
% \cite{Jiang2010PipelineAC}
% 

% Other NFA / DFA Optimizations:
% http://www.mendeley.com/research/improved-dfa-fast-regular-expression-matching/
% \cite{Ficara2008DeltaDFA} delta DFA
% Reorganized and Compact DFA for Efficient Regular Expression Matching 2011
% NFA split architecture for fast regular expression matching 2010
% A fast regular expression matching algorithm for Deep Packet Inspection 2010
There are numerous techniques for traffic classification that emanate from different research areas.
In the last decade several authors proposed different taxonomies to classify traffic classification algorithms either based on their high-level approach to the problem \cite{Gupta2001APC,Taylor2005Survey,Nguyen2008ITCML}, the traffic features that are examined \cite{Kim2008Internet,Callado2009Survey}, or a combination enriched with the point of observation and the actual classification goal \cite{Zhang2009State}.
Nevertheless, the techniques used can roughly be classified into the four classes point location, pattern matching, machine learning, and behavior analysis as depicted in \Cref{fig:tctechniques}.

\begin{figure}[ht!]
	\centering
	\includegraphics[width=\linewidth]{tctechniques}
	\caption[Traffic Classification Techniques]{Traffic Classification Techniques. Point location, pattern matching, machine learning, and behavior analysis are predominant techniques used in network traffic classification.}
	\label{fig:tctechniques}
\end{figure}

\newpage
\subsection{Point Location}
\begin{wrapfigure}{r}{.2\textwidth}
	\includegraphics[width=.2\textwidth]{tctechlocation}
\end{wrapfigure} 
Searches on one or multiple header fields can be seen as a {\em geometric} problem \cite{Lakshman1998HPP}. Each rule in the ruleset has multiple fields to search. These fields effectively describe a search space with a number of dimensions $d$ equal to the number of fields to search.
Each value in a field corresponds to a vector in that dimension of the space. A ruleset with $d$ fields thus describes a $d$-dimensional space and each rule in the set corresponds to a specific point or hyper-rectangle (in case of ranges or wildcards). Rules can intersect or form hierarchies such that a rule is contained in another rule. Finding a matching rule then boils down to locating the point described by the rule or the smallest hyper-rectangle containing the point.

With a geometric view of the traffic classification problem algorithms from computational geometry can be applied \cite{Lakshman1998HPP,varghese2004network}. Unfortunately, multi-dimensional {\em point location} has rather high complexity and is still not well understood for high dimensions \cite{Berg2008CGA}. It can be proven that with $n$ rectangles and $d \geq 3$ dimensions the lower bound to locate the smallest rectangle requires either $O(\log n)$ time and $O(n^{d})$ space, or $O((\log n)^{d-1})$ time and $O(n)$ space \cite{Gupta2001APC,varghese2004network}.
In the general case, $d$-dimensional traffic classification has either exponential space or time requirements, which is impractical for high-speed online classification.

As if searching in multidimensional spaces is not a difficult task in itself, the search is further complicated by the fact, that search values in the ruleset can include ranges, sets, wildcards and prefixes (in case of IP addresses). 

There are are multitude of algorithms that perform searches on one or more dimensions. %that are shortly reviewed here for completeness.
Interested readers are referred to the surveys \cite{Gupta2001APC,Taylor2005Survey} and the book \cite{varghese2004network}.

% algorithms moved to appendix

\subsection{Pattern Matching}
\begin{wrapfigure}{r}{.2\textwidth}
	\includegraphics[width=.2\textwidth]{tctechpattern}
\end{wrapfigure}
Pattern matching, or the more specific form of string matching, is well studied especially in automaton theory. Great introductions into the topic can be found in \cite{Knuth1998The,Hopcroft1979IAT,Hopcroft2006IAT}. Pattern matching is required to find signatures in the packet payload. The problem is that signatures can include ranges, quantifiers, wildcards, and other regular expression, and that they can practically appear anywhere in the packet payload. An overview of string matching for deep packet inspection can be found in \cite{varghese2004network,Lin2008StringDPI}

A na\"ive approach simply moves the search pattern along the text and compares each character sequentially. On a mismatch the search pattern is moved to the next position in the text. This continues until a complete match is found. Clearly this is highly inefficient, requiring $O(n*m)$ time, with $n$ being the length of the text and $m$ being the length of the search string. It also does not work on regular expressions. Several improvements exist, that use hashing \cite{Karp1987ERP}, precomputed prefix tables \cite{KnuthMorrisPratt1977FPM}, or an inverted search (right to left) with shift heuristics \cite{BoyerMoore1977FSS} to reduce the search time to (sub)linear.

Pattern matching can be described using regular languages, a subset of formal languages. The {\em alphabet $\Sigma$} constitutes a finite set of symbols. The empty language $\emptyset$ and all singleton languages $a \in \Sigma$ as well as their concatenation and union are regular languages. A pattern, or string, is a finite sequence of symbols where each symbol $a \in \Sigma$. Thus, pattern matching can be realized using finite state machines (FSM).

%An FSM is a directed graph with a finite set of states and transitions between those states. Each transition is associated with an input symbol, that activates the transition from one state to another. Formally an FSM is a 5-tuple $D(Q,\Sigma,\delta,q_o,F)$ where
%\bl
%	\item $Q$ is a finite set of states
%	\item $\Sigma$ is the alphabet
%	\item $\delta$ is the transition function
%	\item $q_0$ is the start state
%	\item $F$ is a finite set of accepting states
%\el

Two types of FSM are prominent in string matching, the {\em Non-Deterministic Finite Automaton (NFA)} and the {\em Deterministic Finite Automaton (DFA)}. A DFA is deterministic, that is, each input symbol triggers exactly one transition from one state to the next. An NFA is non-deterministic because a state can have any number of transitions to next states for a single input symbol. It can therefore be in multiple active states simultaneously.

Using an NFA for pattern matching is straight forward. The start state has transitions to itself for every input symbol. In addition, for each keyword of length $k$ there exist $k$ states with transitions for each symbol in the keyword \cite{Hopcroft2006IAT}. The text is then supplied to the NFA symbol-wise. If an accepting state is reached, a match occurred.  Unfortunately, an NFA cannot easily be implemented due to non-determinism and multiple active states. A na\"ive solution is to compute all the sets of active states and follow each path either sequentially or in parallel. Fortunately, each NFA can be converted to a DFA suitable for implementation. However, this usually results in many more transitions, the need to cope with backtracking, and can theoretically result in exponential state explosion. Several approaches exist to minimize a DFA, that is, find an equivalent DFA that has the minimum number of required states \cite{Hopcroft2006IAT}.

A well known improvement of a DFA for string matching is the Aho-Corasick-Algorithm \cite{Aho1975Efficient}. Aho-Corasick builds a trie using the input symbols. In addition, {\em failure functions} are added that allow transitions to subtrees with common prefixes. This eliminates the need for backtracking and reduces search complexity.


\subsection{Machine Learning}
\begin{wrapfigure}{r}{.2\textwidth}
	\includegraphics[width=.2\textwidth]{tctechlearning}
\end{wrapfigure}
Machine Learning (ML) is prominent in pattern recognition and data mining but has also gained early attention in network security to detect unusual behavior like SPAM messages. In the last decade, ML has been adopted for network traffic classification, due to increasing efforts of network applications to hide the traffic and render classical port-/payload-based classification useless. A survey by Nguyen and Armitage \cite{Nguyen2008ITCML} evaluates numerous ML techniques used for traffic classification grouped into {\em supervised} and {\em unsupervised} approaches. Again, we will not review individual approaches, but rather give an overview about general ML application in the field of network traffic classification.

In general, the assumption is that traffic classes or network application exhibit certain unique statistical features. ML tries to find patterns of these features in a given dataset composed of individual examples or instances. For traffic classification such an instance is generally a flow, a number of consecutive packets that have a common flow identifier usually composed of the 5-tuple source and destination addresses and ports as well as the IP protocol type, although other flow definitions exist. Common flow features include statistics about packet lengths, number of packets, inter-packet arrival time, number of bytes, idle time, flow duration, or bandwidth. 

Supervised ML uses training data that has been labeled with predefined classes, also known as model. To further improve classification accuracy the training data can also include examples that are not members of the predefined classes. Supervised ML is also often described simply as classification, because it outputs classes for previously unseen examples. Since the classes are known a-priori the classification can be based on decision trees or rulesets. 

Unsupervised ML does not require any training data. It clusters data based on similarities between observed examples. Similarities are calculated using a similarity function, which is often based on distances in the search space. A difference between classes and clusters is that the latter do not have crisp boundaries. While classes are distinct, clusters can overlap and can form hierarchies. As a result, the membership of examples in clusters can only be predicted with certain probabilities.

ML is primarily suitable for off-line classification where it can achieve high accuracy \cite{Nguyen2008ITCML}. Although there is some work on on-line ML traffic classification, those techniques usually have a limited scope, i.e. are specific for a small set of applications or single applications, and with only few exceptions do not allow classifications of unfinished flows. 


\subsection{Behavior Analysis}
\begin{wrapfigure}{r}{.2\textwidth}
	\includegraphics[width=.2\textwidth]{tctechbehavior}
\end{wrapfigure}
Behavior Analysis (BA) is the newcomer in traffic classification. It is based on the observation, that hosts, which engage in certain conversations, show a specific behavior, that can be recognized in the network traffic.

The first approach to exploit connection patterns to identify P2P traffic is \cite{Karagiannis2004TLI}, described as {\em P2P Traffic Profiling (PTP)}. PTP uses a heuristic based on transport layer information, specifically the relation of TCP/UDP usage and the relation between the number of unique ports and number of unique connected IPs. Two observations lead to these heuristics. First, many P2P protocol use both TCP as well as UDP for data transfers and signaling. Second, assuming P2P application choose their ports randomly, than the number of distinct ports will be equal to the number of distinct IP addresses. 

The approach is later refined in BLINC (BLINd Classifiaction) \cite{Karagiannis2005BMT}. BLINC uses flow information to observe host behavior on three different levels. The {\em social level} includes the interaction of host, i.e. with how many peers the host communicates. The {\em functional level} describes the role of a host, i.e. if it is consumer, provider, or both. The {\em application level} observes service interactions on the transport layer and compares that to predefined graphlets, representations of known traffic. BLINC is suitable for real-time classification on flow collectors but requires biflows and suffers from the presence of NAT devices, since NAT aggregates and obfuscates multiple hosts behind a single address. The authors state, that implementing BLINC is not straight-forward and that defining and matching graphlets is complicated and order-dependent.

The social behavior is also the basis of {\em Traffic Dispersion Graphs (TDG)} \cite{Iliofotou2007NMU}. However, TDG is does not constitute a classification method based on host behavior, but instead are described as the ``next step in the progression of packet, flow, and host level aggregation'' \cite{Iliofotou2007NMU}. TDGs aggregate multiple hosts just as BLINC aggregates multiple flows. Nevertheless, the authors show that TDGs can uncover application specific patterns across multiple hosts and give examples on how to perform application identification using ports and TDGs. However, computation and evaluation of TDGs is relatively expensive and is done on a flow collector level.It requires many flows and is not suitable for real-time on-wire classification but rather aimed at being a new network monitoring tool.

An approach specific to BitTorrent is \cite{Ngiwlay2008BPI}. The authors exploit behavior patterns of BitTorrent's {\em choke algorithm} to identify BitTorrent traffic. Roughly, the choke algorithm aims at balancing the up/download rate, which has impact on the relation of active transfers and connected IPs. The approach only considers TCP and requires a bi-directional view on the traffic.

In summary, behavior analysis is still in it's infamy. While host connections patterns and TDGs can obviously be used for classification, the behavior of many applications must still be explored and efficient mechanisms for real-time designed. Practical BA uses heuristics specific for certain applications (BitTorrent) or application domains (P2P). There is yet not general approach to classify many applications using a concise behavior description. 
