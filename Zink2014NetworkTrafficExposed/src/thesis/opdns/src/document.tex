%
%%% INTRODUCTION %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
\section{Introduction}

% problems
From a privacy perspective, the domain name system (DNS) is one of the most dangerous protocols today.
By default, for every service you use or web site you visit, at least one query is sent to your operator and\slash{}or ISP.
Whenever you open your laptop or use your smartphone and tablet, they will perform several DNS lookups to update and synchronize.
These lookups willfully and eagerly reveal to the operator of your current Internet connection which mail servers you use, what kind of instant messaging servers you connect to, which social networks you are following, what VoIP services you use, which blogs you follow, and many more.
This makes it easy to identify your interests as well as track and trace you and your devices.

But DNS security is a problem as well, as DNS is so valuable, ubiquitous and important, but also vulnerable.
Not surprisingly, hackers have exploited DNS in the past to attack services and users alike.
Recent incidents like the takedown of the New York Times and Twitter by the Syrian Electronic Army (SEA) \cite{Kerner2013SEA},
the attack on Twitter by the Iranian Cyber Army \cite{Keizer2009Twitter},
or the massive attack on Spamhaus \cite{Prince2013DDoS}
are prominent examples of different real-world attacks on DNS.
Be it DNS cache poisoning, hijacking, or DDoS amplification attacks, everything has been tried in the past, and succeeded.

Not only black-hats cause blackouts.
Nation states in Europe as well as Turkey, or China have used DNS blocking to restrict access to certain services and perform censoring.
The structure of DNS makes censorship and outages child's play.

Standard DNS lacks all aspects of security by design. Transmitted queries and responses can be read by anybody (confidentiality), they can easily be altered and forged (integrity), and they can be prevented altogether(availability).

% solutions
The response to these and other threats \cite{Ragan2013Three,Rubens2013How} has been to authenticate the data using DNSSEC \cite{DNSSEC} and work toward encrypted channels through VPNs, Tor \cite{Tor}, DNSCurve \cite{dnscurve}, or Confidential DNS \cite{confidentialdns}.
DNS encryption, however, is unlikely to become an immediate miracle cure-all (see \Cref{sec:opdnsrelwork}).
While it protects against a number of attacks, and adds some security features, it does not solve the problems of censorship or user fingerprinting.

% our approach
The best way to avoid exposure of DNS queries and be immune to wrong answers is not to perform DNS queries at all.
%This may sound silly at first, but is the goal of our approach, \emph{Opportunistic Persistent DNS} (opDNS).
This is the goal of our approach, \emph{Opportunistic Persistent DNS} (opDNS).
Orthogonal to DNS encryption, and building on top of existing TLS and optionally DNSSEC, it significantly reduces the common and most identifying DNS queries without sacrificing accuracy.

By using a slightly modified local cache and name resolver, and working in cahoots with an enhanced TLS library, unmodified applications can talk to unmodified servers with greatly reduced privacy exposure and risk of being subject to malicious or accidental DNS problems \cite{ChinaDNS}.
DNS records are optimistically kept by the local cache for sites whose identity can be verified using TLS and which have been proven to be stable.
Applications and servers aware of opDNS can take advantage of additional mechanisms, such as pushing authenticated DNS updates.
In addition to increase privacy and reduce impact of outages, opDNS also greatly reduces the load on the DNS infrastructure.
We strongly believe that increased privacy coupled with additional functionality are strong incentives for users and network operators alike.


%
%%% RELATED WORK %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
\section{Related Work}
\label{sec:opdnsrelwork}

DNS \cite{rfc1034,rfc1035} was designed as a distributed, hierarchical system for reliability and delegation \Cref{fig-dns}.
To achieve efficiency and scalability, it is heavily based on caching.
A cache is only useful when it can answer future requests from the same client and its neighbors (spatial and temporal locality).
This makes all requests and answers essentially public. While caching encrypted records could be envisioned, they would still need to reveal information about their position in the domain name hierarchy.
Due to caching, a repeatable encryption mechanism would need to be used, allowing the reverse engineering of the names through probing, possibly with the aid of hostname databases (e.g. through search engines), zone transfers (AXFR), and NSEC3 walking \cite{nsec3walker}.

\begin{figure}[h!]
	\centering
	\includegraphics[width=.5\textwidth]{dns}
	\caption[DNS]{DNS allows iterative and recursive queries to lookup names. A caching recursive nameserver stores the retrieved records for a limited amount of time. No paths are secured.\label{fig-dns}}
\end{figure}

% DNSSEC
The Domain Name System Security Extensions (DNSSEC) \cite{DNSSEC} are proposed extensions to DNS that aim to add security.
It uses public key cryptography to sign DNS records and is backwards compatible to DNS.
Key Authentication is done using a chain-of-trust, similar to PGP \Cref{fig-dnssec}.
DNSSEC has a number of shortcomings \cite{rfc3833} and is, unfortunately, not widely implemented due to implementation complexity and required changes to infrastructure and client software alike.
It only provides authenticity and integrity, not privacy to the requests and responses. Authentication in DNS does, however, help to avoid DNS redirection attacks and the resulting privacy loss.

\begin{figure}[h!]
	\centering
	\includegraphics[width=.5\textwidth]{dnssec}
	\caption[DNSSEC]{DNSSEC uses a chain of trust to sign and authenticate records. Potentially, DNSSEC allows end-to-end security (integrity), if all participating entities are trusted and have the required keys. In practice, the local resolver usually does not validate DNSSEC signed records.\label{fig-dnssec}}
\end{figure}

% DNScurve and Confidential DNS
DNSCurve \cite{dnscurve} and Confidential DNS \cite{confidentialdns} do not use encryption in the cache, only at the `link' level.
Therefore, DNS caches still have full access to requests and replies; they protect only against eavesdropping packets in flight.
Both approaches are also limited to opportunistic encryption,\footnote{Delegation NS records are not signed in the parent domain even under DNSSEC.} providing no protection against Man-in-the-Middle attacks.
DNSCurve encrypts all DNS packets using public key encryption based on the \emph{Curve25519} \cite{Bernstein2006Curve25519} elliptic curve.
It requires two participants, a cache and a forwarder (\Cref{fig-dnscurve}).
The cache resides on the client side and is responsible to encrypt lookups and decrypt responses.
It automatically detects if a DNS server is DNScurve aware, making it backwards compatible.
The forwarder listens on the server side and decrypts the clients' requests and encrypts the servers responses.
The usage of DNScurve caches and forwarders allows deployment without the need to change client or server software.
DNScurve adds confidentiality and integrity to DNS through encryption, but does not have end-to-end security.
It also increases availability by protecting against certain types of DoS attacks, but it cannot protect against deliberate takedown of DNS servers.
Despite going back to 2009, it is yet to see a substantial deployment, which is partially due to the lack of a trustworthy key management.

\begin{figure}[h!]
	\centering
	\includegraphics[width=.5\textwidth]{dnscurve}
	\caption[DNScurve]{DNScurve provides an encrypted channel between the recursive resolver and the authoritative name server. It does not provide end-to-end encryption, nor do the root servers participate due to unresolved key management issues. \label{fig-dnscurve}}
\end{figure}

% Namecoin
Namecoin \cite{Namecoin} introduces an alternative Peer-to-Peer Domain Name System based on the cryptocurrency Bitcoin (\Cref{fig-namecoin}).
Like Bitcoins, Namecoins are mined searching for specific hash values which can then be used to register and update names and associated data (like PGP keys) using transactions.
For lookups, access to a full copy of the history-based database (blockchain) is required, which does not scale.
Gateways from DNS to Namecoin could be envisioned, but would reintroduce the DNS drawbacks.

\begin{figure}[h!]
	\centering
	\includegraphics[width=.5\textwidth]{namecoin}
	\caption[Namecoin]{Namecoins are a cryptocurrency that must be mined and can then be associated with a name. To participate, all nodes need a copy of the whole database and then perform secure lookups locally. Names can be transferred by exchanging namecoins.\label{fig-namecoin}}
\end{figure}

% TOR and DNS
Another approach to increase privacy in DNS is onion routing through an anonymity network like Tor \cite{Tor}. Tor acts as a SOCKS \cite{socks4} proxy and routes queries through the anonymity network where they are redirected and encrypted multiple times, concealing the original sender in the process.
Unfortunately, this requires either an exclusive Tor-aware forwarder or application-specific SOCKS proxy configuration.
Since only a specific protocol (SOCKS4a \cite{socks4a}) is supported by Tor \cite{TorFAQ}, this narrows down applicability.
In addition, the Tor forwarder does not support all kinds of DNS queries \cite{TorDns}.
This makes it hard to guarantee that an application actually uses Tor for DNS queries, again leading to leaked information and privacy concerns.
Similar restrictions exist when using virtual private networks (VPNs).
If configured improperly, local DNS configuration can still be in effect and therefore lookups are not routed through the private network.

In this context, it is frequently proposed to run a personal DNS server outside the normal caching hierarchy for more privacy.
While this achieves this goal, the resulting load surge on the other DNS servers is likely to create scalability problems.
Also, the set of remote servers a name server talks to is still highly revealing.

% LUNA
Reducing DNS traffic by caching resource records beyond the TTL has been addressed in \cite{luna2012system}.
The proposed solution uses an additional entity called harmonization server, which polls DNS servers and informs devices when a DNS entry has changed, allowing devices to update their DNS cache without themselves asking the DNS servers.
We consider the harmonization server disadvantageous, because it complicates  deployment, and it is able to monitor the DNS queries.

% DANE
While opDNS enhances DNS with the help of TLS, DANE (DNS-Based Authentication of Named Entities) \cite{rfc6698}
enhances TLS with the help of DNS. Both techniques augment the verification of associations; opDNS allows to verify the
associations of domain names and IP addresses, whereas DANE allows to verify associations of certificates and domain names.
Using DNSSEC, DANE serves a special DNS record containing the fingerprint of a certificate, allowing clients to verify the certificate has been set by the domain owner prior to initiating a TLS connection.
DANE and opDNS work seamlessly together giving the benefit of both improved certificate verification and privacy.
Mechanisms similar to DANE, which are used to verify keys for IPsec and SSH, are defined in \cite{rfc4025} and \cite{rfc4255}, respectively.
%\pagebreak
%
%%% DNS INSIGHTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
\section{DNS Privacy Threats}

The Domain Name System is a highly revealing and chatty system, that exposes a number of privacy threats that we want to mitigate. 

\begin{figure}[ht!]
	\centering
	\begin{subfigure}{.6\textwidth}
		\centering
		\includegraphics[width=\textwidth]{dnsprivacy}
		\caption{Collecting DNS queries with date and timestamp allows tracking of users, their browsing behavior, and can lead to user identification.}
		\label{fig-dnsprivacy}
    \end{subfigure}%
	\qquad

	\begin{subfigure}{.6\textwidth}
		\centering
		\includegraphics[width=\textwidth]{dnstracking}
		\caption{DNS allows tracking the movement of users over devices, networks, and locations. Multiple parties can easily aquire the information.}
		\label{fig-dnstracking}
    \end{subfigure}%
	\qquad

	\begin{subfigure}{.6\textwidth}
		\centering
		\includegraphics[width=\textwidth]{dnsoutage}
		\caption{Blocking authoritative name servers or eliminating specific records prevents successful lookup and ultimately leads to no connection to the target host.}
		\label{fig-dnsoutage}
	\end{subfigure}
	\caption[DNS problems addressed by opDNS]{DNS problems addressed by opDNS.}
	\label{fig-dnsproblems}
\end{figure}

\subsection{User Fingerprinting and Identification}

The collection of DNS queries provides a discriminating database of your interests, Internet services used, and browsing behavior (\Cref{fig-dnsprivacy})\cite{DNSprefetch}. By collecting DNS queries, associating it with date, time, IP addresses, and queried domains, a user can be fingerprinted and identified.
% TODO: cannot find any references to that, would also make a nice paper maybe?
Frequencies of queries also reveals the users' browsing behavior, i.e. when does a user check his mail and how many different addresses does he have. DNS, by design, is a highly revealing protocol that announces queries not only to the users' DNS servers but to anyone able to observe the network.

Also, DNS not only reveals information of individual users, but also of organizations as a whole.
Imagine an organization like a university or a company engaged in research.
Observing queries from and responses to the organizations DNS server tells an adversary in what kind of research the organization is interested, and more importantly \emph{how much} interested the organization is (using frequencies of queries).
These observations can happen on a number of locations not limited to the actual network access provider.

\subsection{User Tracking}

Mobility is also problematic, since changing location and network providers can also lead to new DNS configuration.
Whenever a user enters a new network, additional parties can use DNS information for profiling.
However, even keeping the DNS configuration poses threats!
By associating IP addresses of queriers with geo locations it is also possible to track user movement \cite{guha2007identity} (\Cref{fig-dnstracking}).

\subsection{Outages and Cencorship}

Simply blocking name servers or certain DNS records makes name resolution impossible and prevents users from navigating to their desired target sites (\Cref{fig-dnsoutage}).
These outages can be mandated on purpose (\cite{ChinaDNS}), be provoked by malicious parties, or can happen by accident.
While DNS outages are not a privacy issue per se, they often are the result of censorship of content that is considered illegal by the legislative and even to query such defined illegal content might lead to persecution in certain states.

\subsection{Not a Problem?}

One could argue that all this is not a problem, since DNS is usually followed by either a TCP connection or UDP session from the querier to the response target, and that can also be observed on the network, possibly revealing the same information.
This is, however, only partially true.
First, the DNS server operator is not necessarily equal to the carrier providing network access.
In this case, DNS reveals information to an additional - possibly not trustworthy - party.
Second, a connection setup or session initiation does not reveal the actual domain the host is contacting.
A web server can host any number of domains, from very different areas of interest; observing traffic on the network layer does not reveal which domains are browsed to.
The domains a host browses are still transmitted, e.g. within GET requests or TLS server name indications.
However, to find these, the packet's content needs to be captured and parsed, which is much harder to do than evaluating DNS requests.
Connection information is less meaningful regarding browsing behavior than the actual DNS queries which contain the full domain basically for free.

It is also much more difficult to collect and mine information out of it.
In order to extract all connections, the observer needs to capture and analyze all network layer packets.
This requires significant amounts of resources, both in terms of processing power and memory and is especially hard to do on line rate.
In fact, flow collectors like Cisco's netflow basically require a sampling rate (an example of 1\% is given in the manual), to reduce impact on performance \cite{SampledNetflow}.
Flow records are usually only available after completion; information can only be extracted forensic.
In contrast, intercepting and parsing DNS packets in real time is fairly easy to do, even on high speeds.
But capturing DNS packets is not even necessary: The DNS server's log files can provide all interesting information in an easy to evaluate manner.

DNS discloses privacy relevant information to additional parties.
While the information disclosed can also be gained through other means, it still makes sense to make DNS traffic less revealing to increase the effort necessary .

\section{Enhancing Privacy}

The simplest method to make some traffic less revealing is to not generate the traffic at all.
This applies even more so to DNS, where (often ridiculously) low cache times (TTL) require the clients to request the resource records for a given domain name again every few minutes to few hours.
This is due to the lack of cache update; therefore, server operators prefer to err on the short side instead of possibly being stuck with wrong DNS information in caches, rendering a service essentially unreachable.
Larger servers tend to change their addresses only every few years, if at all; nevertheless, an address change could become unavoidable without notice in a few minutes or days.

%This on-demand resolution is prone to (un)controlled outages \cite{ChinaDNS}.

While there are several (bad) uses for DNS \cite{vixie2009dns}, the two main reasons to perform a DNS lookup are

\begin{enumerate}
	\item to follow a new hyperlink in a mail message or web page (`document')
	\item to re-obtain the address for a host or service that has been previously contacted.
\end{enumerate}

In this paper, we address the latter problem, where every time you open your laptop or use your smartphone, it fetches a large number of DNS records related to your mail accounts, communication platforms, or open web pages.
We will show how to safeguard this important and highly identifying part of your DNS traffic without the need of cooperation with applications, infrastructure, or servers.
However, the mechanism is general enough to be extensible to the first problem, but this requires the cooperation of developers and administrators.

The key observations behind Opportunistic Persistent DNS are as follows.
\begin{itemize}
    \item Real servers rarely change their address.
    \item Therefore, the result of the previous DNS query is still valid with very high probability.
    \item For services based on TLS, an address change can be reliably detected on connection setup.
\end{itemize}
Exceptions to these rules, such as private servers with a dynamic IP address, or load balancing and content distribution networks, will be discussed in \Cref{sec:opdnsproblems}.

\subsection{Hosting Stability and TTLs}

% TODO: IP hosting stability survey.
% http://toolbar.netcraft.com/stats/topsites
% http://toolbar.netcraft.com/site_report

The key observation for opDNS is that real servers rarely change their address. Rarely in this case is to be understood in direct comparison to TTL values.

Since 1995, British network company Netcraft, performs a monthly web server survey, associating IP addresses with active sites and the specific web server software used \cite{NetcraftWebServer}.
Unfortunately, the data is not publicly available but their site report portal \cite{NetcraftSiteReport} can be used to retrieve the last ten IP addresses a domain was hosted under.
While granularity is coarse and only the last ten addresses are returned this still allows a valid estimate about the hosting stability of selected domains.

\begin{figure}[h!]
	\centering
	\includegraphics[width=.8\textwidth]{facebookip}
	\caption[Facebook IP stability]{Facebook IP stability. Facebook has changed it's IP address only once within 1.5 years. Image generated using \url{http://toolbar.netcraft.com/site_report} \label{fig-dnsfacebookip}}
\end{figure}

For example, from December 2013 to June 2014 Facebook has changed it's IP address only a single time, after more than a year of operating under the same address \Cref{fig-dnsfacebookip}. Compared to the TTL of 900 seconds (15 minutes) this is a factor of more than $35.000$.
In other words, instead of refreshing the record once every 15 minutes, it could have been cached for a year saving more than $35.000$ redundant external lookups (for each requesting cache).

Another example for highly stable hosting is \url{wikipedia.org}.
The page has changed IP addresses only two times from April 2011 to July 2014 and it's previous IP address was valid for over 2 years. 
Nevertheless, the authoritative TTL is one hour.

There are of course exceptions.
Youtube, Google, Twitter, and Amazon use DNS load balancing and return a randomized set of possible IPs.
This inevitably distorts IP stability statistics, since records change on each query, although at least subsets of returned IPs remain equal and all records are likely to be valid for an extended period of time.
Dealing with dynamic hosts is discussed in \Cref{sec:opdnsproblems}.

\subsection{Basic opDNS operation}

\begin{figure}[t]
	\centering
	\includegraphics[width=.5\textwidth]{opdns}
	\caption[Basic opDNS]{opDNS uses a local cache that stores records for an extended period of time to reduce DNS traffic. Records are updated securely by the web servers using TLS. They are only fetched from the recursive cache for bootstrapping and if host verification fails. \label{fig-opdns}}
\end{figure}

An overview of the operation and components of opDNS is shown in \Cref{fig-opdns}: 
A local cache on the user's machine retains a copy of recently queried DNS records. During the lifetime of the record, as indicated by the TTL value, the local cache is used normally.

After the TTL expires, the behavior deviates from the current standards: 
Records related to TLS-enabled applications will be kept further, but with a special `expired' flag set.
When a TLS-enabled application asks for the record, the expired entry will be flagged and returned to the application.
The flag informs the application, that the returned entry is actually expired and that connection should be established with caution, i.e. using shorter timeouts, strong certificate tests, and the like.
If the connection fails for whatever reason, the expired entry is flushed from the cache and the entire process is repeated after the entry has been renewed using standard DNS lookup.

\subsection{Transparent opDNS}

A requirement for optimistic caching is the application's usage of TLS to connect to the target host.
Unfortunately, DNS does not provide different lookup mechanisms for insecure or secure hosts.
Therefore, on the DNS layer, there is no simple means to detect secure connection setups.
The above basic process requires some sort of signaling between applications and the opDNS cache.
A naive approach requires modifications to the application.
As discussed previously, this will unacceptably slow down deployment.

\begin{algorithm}[t]
	\sf
	\textbf{addr = getaddrinfo(name, service)}\;
	sock = socket()\;
	connect(sock, addr)\;
	tls = SSL\_new()\;
	SSL\_set\_fd(tls, sock)\;
	\textbf{SSL\_connect(tls)}\;
	cert = SSL\_get\_peer\_certificate(tls)\;
	\textbf{X509\_check\_host(cert, name)}\;
	Read and write data as desired\;
	\caption{Simplified POSIX/OpenSSL connection setup without error checking (boldface explained in the text)}
	\label{alg:OpenSSL}
\end{algorithm}

To enable transparent opDNS operation, it suffices to observe calls to connection setup related functions (\Cref{alg:OpenSSL}).
A connection setup for a TLS-enabled client application will do the following steps, among other things:
\begin{enumerate}
    \item Using the server name, obtain the address of the server using \texttt{gethostbyname()}\footnote{Although this function is actually obsolete and should not be used anymore, it still has to be intercepted and handled}, \texttt{getaddrinfo()} or a call to the DNS resolver library.
    \item Set up a socket connection using \texttt{connect()}.
    \item Start the TLS handshake (\texttt{SSL\_connect()} when using OpenSSL)
    \item Verify that the certificate actually belongs to the named server from step 1 \\(\texttt{X509\_check\_host()} when using OpenSSL)
\end{enumerate}
By hooking into the system libraries \cite{TLSinterposer}, the application's privacy awareness can be increased fully transparently.
The opDNS library intercepts several of the library functions from the above process and their relatives to determine if communication between client and server can be trusted to control how records are served and put into the cache (\Cref{fig-opdnsflow}).

\begin{figure}[t]
	\centering
	\includegraphics[width=.5\textwidth]{opdnsflow}
	\caption[Transparent opDNS]{Transparent opDNS. By intercepting system library calls opDNS can verify if an application connects securely to remote services and can serve DNS records appropriately.\label{fig-opdnsflow}}
\end{figure}

By observing the linked libraries and calls to functions like \texttt{getaddrinfo()}, \texttt{connect()}, \texttt{SSL\_connect()}, and \texttt{X509\_check\_host()}, it can be determined, that the application uses TLS to connect to and verify the server.
The set of mapped libraries and the port numbers passed to \texttt{getaddrinfo()} or \texttt{connect()} can be used to infer that an application is TLS enabled, albeit unreliably.
Simplified, any successful \texttt{getaddrinfo()} call needs to be followed by \texttt{X509\_check\_host()} for the application to be considered to connect to that host only using TLS.
An application that has thus proven to contact this host only using TLS, will in the future also be served expired DNS records.

Any connection error or certificate verification failure will flush the expired record from the cache.
The record needs then to be retrieved again using ordinary DNS and the whole process starts fresh.

The opDNS library could try to transparently handle the failure case when the cache is wrong by sending the DNS query and initiating a reconnect.
However, many connections handled like that are background fetches such as mail, RSS feeds, or Ajax requests, and they are likely to self-heal, either through the applications' failure handling or the users' intervention.
Changes themselves are also very rare.
We believe that the added complexity to opDNS to handle these rare cases of not self-healing changes to be not worth the effort.

OpDNS is able to vastly increase privacy by reducing external DNS queries to cases, when secure communication with known hosts fails.

\subsection{DNSpush}

One of the fundamental problems of DNS is that there is no way to actively update DNS records.
A reason why this has not been done before is the perceived abuse potential from DNS cache poisoning \cite{DNSpoisoning}.

However, with the advent of DNSSEC, it is now possible to accept unsolicited records and still trust them to be genuine; to prevent Denial-of-Service attacks, it still remains a good idea to be selective about what to cache.
This opens up new perspectives:
A server that has been contacted can now authoritatively declare that another host has taken over his duties or that the load balancing setup has changed.
This could be done as part of the TLS handshake, similar to OCSP stapling \cite{OCSPstapling}.

In our proposed ``server address indication'' extension, the server embeds the DNSSEC records in the extended server hello.
This works similar to the server name indication client hello extension.
In fact, similar to OCSP stapling, the server name indication should precede a server address indication, to ensure that the correct certificate is received, although this is not a requirement.
Since only DNS server names are supported the process mimics ordinary DNS lookups, but using TLS and not requiring a third party except for initial lookup and contact.

DNSpush could also have uses in ensuring the location privacy of mobile devices.
For example, the address of a device could point to a proxy, which would forward messages.
As soon as high-volume data has to be transmitted (e.g. a VoIP call), the proxy could perform an application layer protocol independent redirect using DNSpush.

DNSpush eliminates the need for a third party to update DNS records. Ordinary DNS queries are only required for initial bootstrapping. This vastly reduces DNS traffic as well as exposed information. The initial bootstrapping can still be monitored easily, which gives some information about the user's interests. However, since subsequent queries are eliminated by DNSpush, browsing behavior and profiling are also eliminated.

Utilizing both TLS and DNSSEC, and combining them in a new and clever way, DNSpush unifies their security features and enhances them with increased privacy and decreased traffic.
The change from poll to push not only reduces traffic but also relieves pressure on Servers and the Domain Names System.
This is an important step towards securing Internet traffic as a whole.
These benefits can be achieved without any change to the network infrastructure at all.

\subsection{Dealing with dynamic addresses}
\label{sec:opdnsproblems}

Hosts using a dynamic DNS service to map their name to their dynamic address and content distribution networks can return addresses which change much more often than `regular' servers.
Because opDNS only uses expired entries for TLS-secured connections which will fail if the wrong host is contacted, no sensitive information is revealed and no impersonation can take place.
However, the timeouts or necessity to hit `reload' should be minimized.

%MW was fuzzy here on purpose, as the actual values need to be evaluated (e.g., by one of the students). A simple mechanism might be to have multiple identical addresses returned over several attempts.
%Therefore, opDNS will not cache records with very short TTL and only put addresses in cache after they have shown stability.
% An opDNS-aware application will be able to fine-tune the caching.
There are multiple possible solutions to deal with quickly changing addresses, that depend on the actual cause of address changes. 

A conservative approach is to simply not cache records with very short TTL and only put addresses in cache after they have shown stability.
An opDNS-aware application will be able to fine-tune the caching.
This increases cache accuracy at the expense of privacy and network latency.

For load balanced sites, queries usually return multiple randomly ordered IP addresses that also have very low TTLs.
The client than decides which of the addresses is used (usually the first in the set).
Despite the low TTL (for balancing reasons), all records in the response are likely to be valid for an extended period of time, so all of them are candidates for opportunistic caching.
In this case opDNS can cache all returned records and return them randomly (or sequentially) to querying applications.
Of course, checking for stability is still a requirement for caching.

Using this mechanism, opDNS can also increase efficiency of load balancing.
Since the entries are cached locally opDNS does not require additional DNS queries to do the actual load balancing.
Records are fetched from the local cache without stressing the DNS and network infrastructure. 

\section{Conclusion and Future Work}

We presented two new extensions to DNS and TLS, named opDNS and DNSpush, that can transparently increase privacy, security, and reduce DNS traffic.

By changing the current `pessimistic' DNS caching into a much more optimistic variant, opDNS
\begin{enumerate}
	\item enhances privacy by disclosing as little personal information as possible,
	\item reduces the effect of outages by localizing name resolving, increassing availability, and
	\item improves client and server performance by reducing overall DNS traffic.
\end{enumerate}

Due to the reliance on TLS, even if the optimism turns out to be unfounded, privacy and security are not endangered.

By abandoning pull-based updates, DNSpush enables DNSSEC-based secure updates of DNS records using encrypted channels and trusted hosts, without the need of a third party or dedicated name servers.
DNSpush provides integrity and confidentiality by eliminating third parties and adding encryption.

The techniques can be implemented in standard libraries making them transparent to client applications and the network infrastructure alike.
Even though opDNS and DNSpush can enhance unmodified applications, behavior and performance can be further tuned by making application and server opDNS-aware.

Of course, opDNS and DNSpush rely on a wide availability of TLS and DNSSEC.
It combines and enhances these techniques in new ways to offer more privacy and better security.
The proposed extensions are significant in insuring pervasive protection of all Internet traffic.

We are working on implementing, testing, and evaluating opDNS.
%We could imagine an extended version of `DNSpush' to be helpful in optimizing the efficiency and privacy of future connections and thereby make the Internet a safer and better place.
But opportunistic persistent caching and DNSpush are only small steps in the long process of taking back the web into the hands of the users.
%We believe that network communication should be private by default.
We envision optimized system libraries that provide secure connections by default.
An extended version of `DNSpush' can be helpful in optimizing the efficiency and privacy of future connections and thereby make the Internet a safer and better place.
