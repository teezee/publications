\NeedsTeXFormat{LaTeX2e}[1995/12/01]
%% \ProvidesClass{tdp}[2008/02/25 v1.00 LaTeX document class for Transactions on Data Privacy]
\ProvidesClass{tdp}[2013/12/31 v2.00 LaTeX document class for Transactions on Data Privacy]

\LoadClass[twoside]{article}
\RequirePackage{fancyhdr}
\RequirePackage{lastpage}           %% Number of pages of the paper

\usepackage[a4paper,twoside,centering]{geometry}  %% twoside: simetriques les pagines
\geometry{
  layoutwidth=17cm, 
  layoutheight=24cm,
  %top=0mm,            %% Top margin of the page
  %bottom=0mm,         %% Top margin of the page
  inner=15mm,         %% Costat interior  
  outer=15mm,         %% Costat interior  
%%  bindingoffset=0mm,   (per si fa falta enquadernar)
  top=5mm,
  bottom=5mm,
  layouthoffset=15mm,
  layoutvoffset=23mm,
  includeheadfoot
}

%% la pagina d'a4 es 210 x 297
%% amplada del text del TDP 17cm de text + 1.5 + 1.5 dels costats = 20
%% llargada del text del TDP 24cm de text + 0.5 + 0.5 dalt i baix = 25
%% tenim 1cm dels costats i 4.7 cm a dalt i baix
%% layouthoffset = 5mm
%% layoutvoffset = 23mm

\setlength{\headsep}{5mm}
\setlength{\footskip}{15mm}

%\usepackage[cam,a4,center]{crop}
%\usepackage[cam,a4,center,dvips]{crop}

\def\TDPAbstract{Abstract.}
\def\TDPKeywords{Keywords.}
\def\TDPAuthors{}
\def\TDPTitle{}
\def\TDPVolume{}
\def\TDPYear{}
\def\TDPFirstPage{}
\def\TDPLastPage{}


\pagestyle{fancy}



\fancyhead{} 
%%\fancyhead[LO,RE]{\thepage}
%%\fancyhead[LE]{\TDPTitle}
%%\fancyhead[RO]{\TDPAuthors}
\fancyhead[LE,RO]{\thepage}
\fancyhead[LO]{\TDPTitle}
\fancyhead[RE]{\TDPAuthors}
\fancyfoot{} 
\fancyfoot[CO,CE]{{\sc Transactions on Data Privacy~\TDPVolume~(\TDPYear)}}
\renewcommand{\headrulewidth}{0.4pt}
\renewcommand{\footrulewidth}{0.4pt}


\renewenvironment{abstract}{%
  \list{}{
    \advance\topsep by0.35cm\relax\small
    \labelwidth=\z@
    \listparindent=\z@
    \itemindent\listparindent
    \leftmargin=0cm
    \rightmargin\leftmargin}
\item[\hskip
  \labelsep
  \bfseries\TDPAbstract]}
		 {\endlist}
		 
\newenvironment{keywords}{%
      \list{}{\advance\topsep by0.35cm\relax\small
	\leftmargin=0cm
      \labelwidth=\z@
      \listparindent=\z@
      \itemindent\listparindent
      \rightmargin\leftmargin}\item[\hskip\labelsep
                                    \bfseries\TDPKeywords]}
    {\endlist}


\RequirePackage{amsthm}

\theoremstyle{plain}
\newtheorem{theorem}{Theorem}
\newtheorem{proposition}[theorem]{Proposition}
\newtheorem{corollary}[theorem]{Corollary}
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem{axiom}[theorem]{Axiom}
\newtheorem{hypothesis}[theorem]{Hypothesis}
\newtheorem{assumption}[theorem]{Assumption}
\theoremstyle{definition}   %% 
\newtheorem{definition}[theorem]{Definition}
\newtheorem{example}[theorem]{Example}
\newtheorem{problem}[theorem]{Problem}


\def\TDPReferences{References}

\def\refname{\TDPReferences}


\renewenvironment{thebibliography}[1]%
  {\section*{\refname}%
    \@mkboth{\MakeUppercase\refname}{\MakeUppercase\refname}%
    \list{\@biblabel{\@arabic\c@enumiv}}%
         {\settowidth\labelwidth{\@biblabel{#1}}%
           \leftmargin\labelwidth
           \advance\leftmargin\labelsep
	   \setlength{\parsep}{0pt}%
	   \setlength{\itemsep}{3pt}%
	   \@openbib@code
	   \usecounter{enumiv}%
	   \let\p@enumiv\@empty
	   \renewcommand\theenumiv{\@arabic\c@enumiv}}%
	 \small
	 \sloppy
	 \clubpenalty4000
	 \@clubpenalty \clubpenalty
      \widowpenalty4000%
      \sfcode`\.\@m}
     {\def\@noitemerr
       {\@latex@warning{Empty `thebibliography' environment}}%
      \endlist}

\setlength{\parindent}{0.5em}

\newcommand{\TDPRunningAuthors}[1]{
  \def\TDPAuthors{#1}
}
\newcommand{\TDPRunningTitle}[1]{
  \def\TDPTitle{#1}
}
\newcommand{\TDPThisVolume}[1]{
  \def\TDPVolume{#1}
}
\newcommand{\TDPThisYear}[1]{
  \def\TDPYear{#1}
}

\newcommand{\TDPFirstPageNumber}[1]{
  \def\TDPFirstPage{#1}
  \setcounter{page}{#1}}
\def\TDPLastPage{\pageref{LastPage}}

\fancypagestyle{plain}{%
\fancyhf{} 
\fancyfoot[C]{\bfseries \thepage} 
\fancyhead[R]{\sc Transactions on Data Privacy~\TDPVolume~(\TDPYear)~\TDPFirstPage--\TDPLastPage} 
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0pt}}


\def\@TDPAuthorAddress{}
\def\@TDPDates{}
\def\TDPAuthorFont{\bf {\small}}
\def\affiliationfont{\footnotesize}
\def\author#1{\expandafter\def\expandafter\@TDPAuthorAddress\expandafter
  {\@TDPAuthorAddress
    \noindent\ignorespaces
	{\TDPAuthorFont{#1}}\relax\par
  }
}
\def\address#1{\expandafter\def\expandafter\@TDPAuthorAddress\expandafter
  {\@TDPAuthorAddress
    \noindent\ignorespaces
	{\affiliationfont{{{#1}}}}\relax\par 
	\vspace*{5pt}
  }
}
\def\TDPSubmissionDates#1{\expandafter\def\expandafter\@TDPDates\expandafter
  {\@TDPDates
    \noindent\ignorespaces
	{\affiliationfont{{{#1}}}}\relax\par 
	\vspace*{10pt}
  }
}

\newcounter{author}               
\newcount\n@author                

\def\thanks#1{\footnotemark
  \protected@xdef\@thanks{\@thanks
    \protect\footnotetext[\the\c@footnote]{#1}}%
}
\def\footnotemark{%
   \@ifnextchar[\@xfootnotemark
     {\stepcounter{footnote}%
      \protected@xdef\@thefnmark{~}%
      \@footnotemark}}

\renewcommand\maketitle{\par
  \begingroup
  \setlength{\parskip}{0cm}
  \renewcommand*{\baselinestretch}{1}
  \renewcommand\thefootnote{\@fnsymbol\c@footnote}% Aixo es pel numero que hi ha al peu de la pagina
  \def\@makefnmark{\rlap{\@textsuperscript{\normalfont\@thefnmark}}}%
  \long\def\@makefntext##1{\parindent 1em\noindent
    \hb@xt@1.8em{%
      \hss\@textsuperscript{\normalfont\@thefnmark}}##1}% Aixo escriu el text de la footnote corresponent al titol
  \global\@topnum\z@   % Prevents figures from going at top of page.
  \@maketitle
  \thispagestyle{plain}\@thanks 
  \endgroup
  \setcounter{footnote}{0}%
  \global\let\thanks\relax
  \global\let\maketitle\relax
  \global\let\@maketitle\relax
  \global\let\@thanks\@empty
  \global\let\@author\@empty
  \global\let\@date\@empty
  \global\let\@title\@empty
  \global\let\title\relax
  \global\let\author\relax
  \global\let\date\relax
  \global\let\and\relax
}
\def\@maketitle{%
  \null
  \vskip 2em%
  \noindent
       {\huge {\bf \@title \par}}%
       \vskip 1.5em%
	      {\large
		\@TDPAuthorAddress       
		\par}%
	      \vskip 1em%
	      \noindent {\small \@TDPDates \par}%
	      \par
	      \vskip 1.5em}

\usepackage{palatino}
