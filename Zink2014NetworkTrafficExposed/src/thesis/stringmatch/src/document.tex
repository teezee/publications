%
%%% INTRODUCTION %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
\section{Introduction}

% problems
String matching is an important field with a vast array of practical applications, spanning different areas like computational biology, signal processing, data compression, file comparison and of course text processing and retrieval \cite{Navarro2001Guided}, to only name a few.
Due to this, string matching is well studied and understood, and efficient algorithms are standard in many applications like word processing software (search and replace) or text processing tools like 'grep' or 'diff'.

String matching also plays an important role in network traffic classification, especially - but not exclusively - in access and threat control.
Here, known signatures of applications or attacks are searched in the packet content to identify the network application or to find embedded exploits and attacks.
Firewalls, Virus Scanners, and Network Intrusion Detection Systems (NIDS) all use string matching algorithms to search for specific patterns.

% maybe remove this whole paragraph
Formally, string matching is the process of searching occurrences of a finite pattern of symbols out of an alphabet $\Sigma$ in a search text comprised of symbols of the same alphabet.
Strictly, we have to distinguish between algorithms that search for a single pattern and algorithms that can search for a finite set of multiple patterns.
Though multiple parallel instances of single pattern algorithms can also search for multiple patterns it might not be efficient to do so.

% Visualization Tools
% http://www.regexper.com
% https://www.debuggex.com/
% http://regexvisualizer.apphb.com/
% http://blog.ivank.net/aho-corasick-algorithm-in-as3.html
% http://artlives.ru/ahocorasik/en/

\begin{figure}[ht!]
	\centering
	\includegraphics[width=\linewidth]{stringmatch}
	\caption[String Matching Problems]{Different kinds of string matching problems and example solutions. Exact matches of fixed strings can efficiently be done using Aho-Corasick tries (image generated using \cite{acimpl}). Regular expressions require either DFAs or NFAs and are expensive in space and time (image generated using \cite{debuggex}). Approximate string matching with distance or resemblance is often addressed using dynamic programming (image taken from \cite{van2007families}) but can often also be solved through regular expressions or explosion and exact matching.}
	\label{fig:stringmatch}
\end{figure}

% solutions
There are basically three types of string matching problems (see \Cref{fig:stringmatch}).
%Applied string matching can itself be categorized into three classes.
Exact matching searches for exact instances of the pattern in the searched text.
Naive approaches simply compare each character of the input pattern with each character in the searched text.
Complexity can be reduced to sub linear time through omitting characters.
Approximate string matching allows for a finite number of errors in the searched text, that is, the matches can be different from the pattern up to a certain distance.
Depending on the type and number of possible errors, approximate string matching can have high variations in complexity \cite{Navarro2001Guided}.
Finally, regular expressions introduce meta characters that allow the formation of expressions that specify qualities the search string must have.
Regular expressions are expensive to match requiring at least linear time.

% our approach
While there are many solutions to string matching problems they are specific to a certain type of matching problem.
Contemporary network related classification rulesets contain many search patterns of different types.
Due to rapidly changing application signatures, approximate string matching and regular expression matching become increasingly popular.
However, matching both approximate strings and regular expressions is hard to do; it can take a lot of time and also requires potentially large transition tables.
Often, the search patterns are actually composed of exact strings mixed with few and short regular expressions to express character sets or ranges.
That is, for many patterns, only a small subset of regular expressions are really required.

This observation allows for optimization.
%A naive approach is to simply split the patterns into their exact and regular parts and independently match each of them. 
We suggest a generic byte matching engine, that is capable of matching not only single characters, but also sets of characters and ranges, according to a reprogrammable instruction table.
The engine fits into a single logical unit of an FPGA and can be used to implement many matching problems present in contemporary packet inspection rulesets.
It makes use of the potentially high parallelism of reprogrammable hardware.
Since the engine itself is so small, many of them can easily run in parallel to match whole packets at once.
For the same reason it is also very flexible towards ruleset changes.
New rules can simple be added while changing rules can be reflected through simply changing the small lookup tables.
The resulting pattern matching engine is therefore very efficient in space and time, flexible, and robust to ruleset changes.

%
%%% RELATED WORK %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
\section{Related Work}
\label{sec:stringmatchrelwork}

Approximate string matching is a problem present in many research and application areas.
It is becoming increasingly important and it can be shown, that implementing string matching algorithms in reprogrammable hardware like FPGAs leads to significant performance improvements over software-based solutions \cite{Irwin2011FPGA}.

A study by Navarro \cite{Navarro2001Guided} identifies several application areas for approximate string matching and also gives a vast overview of different approaches to the problem.
Not all of these solutions are equally well suited for hardware implementation.
Network applications like packet inspection have high restrictions on hardware resources, execution time, and therefore further limit the applicability of string matching algorithms.
They also often do not require matching strings with errors, instead exact matches or regular expressions are more common, and more efficient.

A still widely used approach for pattern matching at high speeds is using TCAM \cite{yu2004Gigabit}.
It is fast and easy to implement but it requires high amounts of power and is only suited for strings with fixed patterns and length, and not for regular expressions, which must be exploded.

Aho-Corasick \cite{Aho1975Efficient} is another algorithm to match fixed patterns that is well suited for network applications.
Aho-Corasick builds a deterministic finite automaton using the input patterns.
Failure transitions allow fast and easy backtracking in case matching fails.
Aho-Corasick can match all input patterns simultaneously with one pass, however, it cannot match regular expressions.
Tuck et al \cite{tuck2004deterministic} present modifications on Aho-Corasick to improve the performance in hardware implementations.
Yu et al \cite{yu2004Gigabit} use parallel string matching engines and introduce precomputed default transition pointers to further speed up the matching and reduce memory impact.

Fixed string matching itself does not allow searching for patterns with gaps or wild cards.
To search for these kinds of signatures required using regular expressions.
This problem is addressed in \cite{rahman2006finding}. The authors slightly modify the Aho-Corasick pattern matching machine to keep track of starting positions and ranges to calculate matches.

Another approach for fixed string matching based on Bloom filters is presented in \cite{dharmapurikar2004design}.
The authors use multiple Bloom filters (first introduced in \cite{Bloom1970Space/time}) to store the patterns grouped by length.
The packet bytes are then fed into each of the Bloom filters to check if a corresponding pattern has been stored.
Multiple of these packet scanning engines can be used in parallel with shifted byte offsets to improve scanning speed.
Using Bloom filters has several advantages. They require very little space and lookup can be achieved with small constant effort.
This allows packet scanning and high speeds.

Matching regular expressions practically requires the usage of finite state machines (a.k.a. finite automatons, FA).
Due to the implementation complexity of non-deterministic FAs (NFA) early approaches concentrated on deterministic FAs (DFA). 

The first approach to implement an NFA on an FPGA is presented in \cite{sidhu2001fast}.
The main difference between DFA and NFA implementations is that DFAs usually use memory to store transition tables and retrieve the next state, while NFAs require logic in the form of flip-flops to encode active states.
This is because NFAs can have multiple states active at the same time and the linear and non-parallel nature of memory makes this difficult to achieve.
The authors of \cite{sidhu2001fast} show how to construct, prune, and implement an NFA on reprogrammable hardware through clever encoding of states and introducing $\epsilon$ transitions into logic.
% note: maybe use this paper for evaluation, it shows logic and memory requirements of DFAs and NFAs very nicely.
Other authors also try to improve NFA encoding on FPGAs.
They differ in the amount of states and transitions needed, and thus the memory and logic requirements, the capability of matching meta characters, and the achieved throughput.
A nice overview and evaluation of different approaches is given in \cite{Sourdis2008Regular}.

%
%%% Method %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
\section{Generic Single-Byte Matching engine}

Traffic classification and especially the complex processes of packet inspection or intrusion detection suffer from several constraints.
To avoid congestion the packets need to be analyzed at line speed.
Unfortunately, logic, memory, and clock speed is scarce, while packet rates keep increasing.
To make matters worse, due to ever increasing numbers of network applications, contemporary rulesets use large numbers of regular expression signatures to search for patterns in the packet stream.

However, if examined closely, it appears that many rules do not require sophisticated regular expressions but contain only small numbers of character sets or ranges, including wildcards. 
These patterns behave more like fixed strings with small portions of rather light-weight regular expressions added.
Interestingly, this problem of hybrid fixed strings and light regular expressions has to the best of our knowledge not been addressed to date.

This kind of strings does not require full-fledged regular expression matching automatons but rather an extended fixed string matching engine.
This is the problem we aim to solve.

\noindent\begin{minipage}{\linewidth}
\lstset{language=c, caption={\footnotesize{Examples of Regular Expression Rules}}, label=lst:stringruleshuawei, frame=tb, breaklines=true, basicstyle=\footnotesize, float=T}
\begin{lstlisting}
\x7f#[*Z\x1a].\xffC
\x05.\x00[\x01\x03\x04]
\x80.\x01[\x00\x03][\x01\x00\x02]
/2.0\x20\d{3}\x20
\end{lstlisting}
\end{minipage}

\Cref{lst:stringruleshuawei} shows an excerpt of rules from a real world router ruleset that demonstrates the kind of matching problem.
The rules include fixed character values as well as character sets and ranges.
It also includes quantifiers of a fixed number of repetitions (e.g. \texttt{\{3\}}).
The ruleset itself contains thousands of these rules.

This problem can certainly be solved using the regular expressions shown, however, the resulting automaton would be rather big and possibly exceed resource requirements.
Using fixed string matching algorithms like Aho-Corasick instead requires all sets to be exploded into multiple patterns, and while AC can be modified to match wildcards (\cite{rahman2006finding}), matching arbitrary ranges is a problem still not solved.
Furthermore, speed is another problem in both AC as well as NFA/DFA due to excessive use of comparatively slow memory.

\subsection{Regular Expression Encoding}

The observation we exploit is that most rules do not use regular expression metacharacters excessively.
Instead, regular expressions are often limited to sets of characters or ranges and wildcards.
The idea is to build a very simple and generic single-byte matching processor, that can match not only characters and exact bytes values but also the required sets and ranges.
This byte matching 'nano' processor is then used as the main building block to design the engine capable of matching the whole ruleset in parallel on one pass.

To achieve this goal, the single-byte matcher (SBM) must be able to match an input byte against several matching types or operators.
The following operators can be identified.

\bl
	\item Exact Value.
The input byte must match a specific value.
It includes case-sensitive characters, numbers, but also any other possible byte value.
This covers many regular expressions, including the character \texttt{\textbackslash xHH}, where `HH` denotes a 2-digit hexadezimal value, any printable character, as well as certain control characters like \texttt{\textbackslash 0}, \texttt{\textbackslash t}, \texttt{\textbackslash r}, \texttt{\textbackslash v} among others. 
	
	\item Set of Values.
The input byte must match a set of possible values.
This includes ranges, that basically also constitute value sets.
In regular expression syntax sets and ranges are expressed using brackets \texttt{[]}.
E.g. \texttt{[xyz]} matches the characters in the brackets, while \texttt{[A-Z]} matches the range between the characters concatenated by \texttt{-}, including the boundaries.
Some metacharacters also denote sets, like \texttt{\textbackslash d} for single digits (\texttt{[0-9]}), or \texttt{\textbackslash w} for alphanumeric characters (\texttt{[A-Za-z0-9\_]}).

	\item Case-insensitive Character.
The input byte must match a specific case-insensitive character.
Case-insensitive characters can be expressed using sets.
However, they are very common and they can indeed be matched very efficiently, that is why they have an operator on their own.

	\item Don't care.
The input byte can match any possible byte value.
The value of the input byte does not matter at all for the matching.
This happens, when the rule includes gaps or wildcards.
It is equivalent to the regular expressions \texttt{[\textbackslash x00-\textbackslash xff]},\texttt{[\textbackslash .\textbackslash n]}.
\el

\Cref{tbl:strregex} shows regular expression patterns that can be matched using the single-byte matcher.
The descriptions are directly taken from the perl documentation on regular expressions \cite{perlre}.

\begin{table}[ht!]
	\centering
\begin{tabular}{l l}
	{\bf Pattern} & {\bf Description}\\\hline
	\texttt{.} & any character except newline\\
	\texttt{[\textbackslash.\textbackslash n]} & any character including newline (wildcard)\\
	\texttt{\{n\}} & match exactly n times\\
	\texttt{[ ]} & set of characters or ranges\\
	\texttt{[\^{} ]} & characters not in the set\\
	\texttt{\textbackslash t} & horizontal tab\\
	\texttt{\textbackslash n} & newline\\
	\texttt{\textbackslash r} & carriage return\\
	\texttt{\textbackslash f} & form feed\\
	\texttt{\textbackslash a} & alarm bell\\
	\texttt{\textbackslash e} & escape\\
	\texttt{\textbackslash cK} & control character\\
	\texttt{\textbackslash xnn} & character whose ordinal is given by hexadecimal number nn\\
	\texttt{\textbackslash nnn} & character whose ordinal is given by ordinal number nnn\\
	\texttt{\textbackslash w} & "word" character (alphanumeric plus "\_")\\
	\texttt{\textbackslash w} & non-"word" character\\
	\texttt{\textbackslash s} & whitespace character\\
	\texttt{\textbackslash S} & non-whitespace character\\
	\texttt{\textbackslash d} & decimal digit character\\
	\texttt{\textbackslash D} & non-decimal digit character\\
	\texttt{\textbackslash v} & vertical whitespace\\
	\texttt{\textbackslash V} & not vertical whitespace\\
	\texttt{\textbackslash h} & horizontal whitespace\\
	\texttt{\textbackslash H} & not horizontal whitespace\\
	\texttt{\textbackslash R} & linebreak\\
\end{tabular}
	\caption[Regular Expression Patterns]{\label{tbl:strregex}Regular expression patterns that can be matched using the single-byte matcher processor. Descriptions taken from perldoc on regular expressions \cite{perlre}.}
\end{table}

The single-byte matcher can match many regular expressions, with the exception of most quantifiers and assertions.
Dealing with some quantifiers is still possible by rewriting or splitting regular expressions into multiple conditions at the expense of accuracy.

An 8-bit match value and four different match operators would normally require a 10-bit encoding.
However, since all types except the exact byte value ignore some of the input  bits, these can actually be reused leading to a 9-bit operator encoding.
\Cref{tbl:strmenc1} shows a possible encoding.
The mapping has been chosen in order to require little logic for decoding.

\begin{table}[ht!]
	\centering
\begin{tabular}{l | l}
	{\bf Operator} & {\bf Encoding}\\\hline
	Exact Value & 0 \,vvvv \,vvvv\\ % 0 00-ff
	Specific set (see below) & 1 \,00xx \,rrrr\\ % 1 00-(0f | 3f) ? 
	Case-independent & 1 \,01xv \,vvvv\\ % 1 40-5a (A-Z, a-z)
	Don't care (wildcard) & 1 \,10xx \,xxxx\\ % 1 c0-
\end{tabular}
	\caption[Match Operator Encoding]{\label{tbl:strmenc1} Encoding for all operator options. The encoding is in binary with the following special values. x: Don't care bit; v: Copy bit from corresponding bit of the actual value, r: Reserved for hardware decoding.}
\end{table}

This encoding is used to build the instruction set of a single-byte matcher.
The first bit (from left to right) switches operation between matching an exact value and the three other operators.
A cleared bit indicates an exact value to be matched.
In this case the following 8 bits simply denote the byte value to be matched.
If the first bit is set, the next two bits (borrowed from the byte value encoding) encode the match operator.
Sets and ranges are assigned free values of the reserved part of the code.
That is, each distinct set that needs to be matched is assigned the next free code in the table.
Case-independent ASCII characters ignore the three highest bits, which means that only the lower five bits need to be considered.
The last operator is `don't care`, which simply ignores all input bytes and is equal to the regular expression \texttt{[\textbackslash.\textbackslash n]}.

\subsection{Rule Encoding}

A naive implementation would use one single-byte matcher for each byte in the ruleset to be matched and hardcode the match function.
While this would eliminate the need for an instruction LUT and enable all bytes to be matched in parallel, it would consume extreme amounts of logic.

A more space efficient solution uses one SBM for each rule in the ruleset.
This ``iterative matcher'' keeps a local instruction LUT and iterates over the incoming input bytes.
An instruction pointer keeps track of the next instruction to be fetched.
This results in an individualistic encoding for each SBM.

Every SBM must at least support matching a case-insensitive character, an exact value, and the wildcard.
Matching sets requires individual hardware, which is indicated in \Cref{tbl:strmenc1} with the bits reserved for hardware decoding.
A simple circuit can be devised, that is shared by all SBMs to support the three basic operators.
It is depicted in \Cref{fig:sbmlogic}.

\begin{figure}[ht!]
	\centering
	\includegraphics[width=\linewidth]{sbmlogic}
	\caption[Circuit of a Single-Byte Matcher]{\label{fig:sbmlogic} The circuit of a single byte matcher that supports the operators for matching case-insensitive characters, exact byte values, and the wildcard.}
\end{figure}

To demonstrate the encoding process we use an example regular expression rule (shown in \Cref{lst:strregex} and \Cref{fig:regexvis}) that can be matched by a single-byte matcher engine. 
This rule has two sets (\texttt{[\textbackslash 0\textbackslash x03\textbackslash x05]} and \texttt{[\textbackslash x80-\textbackslash xbf]}), some wildcards with fixed quantifiers, some case-insensitive characters (\texttt{[Aa][Bb][Cc]}) and an exact byte value (\texttt{0}).
The two sets are assigned codes from the \emph{reserved} part of \Cref{tbl:strmenc1}, e.g. the first one \texttt{1\,00xx\,xxx0} and the second one \texttt{1\,00xx\,xxx1}.
This mapping is encoded as the individualistic part of this particular rule matcher engine.
That is, each SBM has its own instruction set, which basically is a simple array of opcodes kept in available on-chip memory, and a some individual decoding hardware.
This leads to an instruction LUT as shown in \Cref{tbl:enc3}.

%http://www.regexper.com/#[Aa][Bb][Cc].{4}[\0\x03\x05].0.[D-S]
\lstset{language=bash, caption={\footnotesize{Example regular expression rule with sets, wildcards, ranges, and exact values.}}, label=lst:strregex, frame=tb, breaklines=true, basicstyle=\footnotesize, float=T}
\begin{lstlisting}
[Aa][Bb][Cc][.\n]{4}[\0\x03\x05][.\n]0[.\n][D-S]
\end{lstlisting}

\begin{figure}[ht!]
	\includegraphics[width=\linewidth]{regexvis}
	\caption[Regex Visualization]{\label{fig:regexvis}A visualization of the example regular expression rule in \Cref{lst:strregex}. Image created using \cite{regexper}.}
\end{figure}

\begin{table}[ht!]
	\centering
	\begin{tabular}{r|r|l}
		Offset & Compressed set  & Comment\\\hline
		0 & 1\,0100\,0001 & case-insensitive \texttt{[Aa]}\\
		1 & 1\,0100\,0010 & case-insensitive \texttt{[Bb]}\\
		2 & 1\,0100\,0011 & case-insensitive \texttt{[Cc]}\\
		3\ldots 6 & 1\,1000\,0000 & don't care\\
		7 & 1\,0000\,0000 & \texttt{[\textbackslash 0\textbackslash x03\textbackslash x05]}\\
		8 & 1\,1000\,0000 & don't care\\
		9 & 0\,0011\,0000 & single byte \texttt{0}\\
		10 & 1\,1000\,0000 & don't care\\
		11 & 1\,0000\,0001 &\texttt{[D-S]}\\
	\end{tabular}
	\caption{\label{tbl:enc3}Instruction LUT for \Cref{lst:strregex}}
\end{table}

This kind of encoding works well as each individual rule only has few sets to take care of, even if the total number of sets is significant.
The actual encoding was chosen such that a very small number of bits only need to be compared.

Encoding sets seems to require significant amounts of logic, especially for a large number of distinct values.
However, in many cases the number of gates required for comparison can be minimized by sharing logic for common bits.
For example, the values in the set \texttt{[\textbackslash 0\textbackslash x03\textbackslash x05]} only change in the rightmost three bits, the leftmost 5 bits never change and need only be negated once.
The code to match this specific set is show in \Cref{lst:strset0}.

\noindent\begin{minipage}{\linewidth}
\lstset{language=c, caption={\footnotesize{Match function for the set \texttt{[\textbackslash 0\textbackslash x03\textbackslash x05]}.}}, label=lst:strset0, frame=tb, breaklines=true, basicstyle=\footnotesize, float=T}
\begin{lstlisting}
function match_set0(in,op) {
	i73 <- !in[3...7]
	v0 <- !in[0] && !in[1] && !in[2]
	v1 <- in[0] && in[1] && !in[2]
	v2 <- in[0] && !in[1] && in[2]
	return i73 && v0 || v1 || v2
}
\end{lstlisting}
\end{minipage}

Similarly, the logic for matching ranges can often be significantly reduced.
In the naive case, comparing the range requires two 8-bit word comparators that take the input byte and the lower\slash{}upper bounds respectively.
However, there are several optimizations that can be applied.
First, similar to sets, common prefixes do not need to be compared using a comparator but can simply be tested for equality.
In the example set (\texttt{[A-S]}) this includes the leftmost three bits, which are always \texttt{010}.
Second, each rightmost bit that is set in the upper bound but cleared in the lower bound can be ignored, since these positions are actually wildcards. 
In the example set, these are the two rightmost bits that can either be set or cleared in any case.
Only the remaining middle bits need actually be compared.
\Cref{lst:strset1} shows the full function to match the set \texttt{[A-S]}.

\lstset{language=c, caption={\footnotesize{Match function for the range \texttt{[A-S]}.}}, label=lst:strset1, frame=tb, breaklines=true, basicstyle=\footnotesize, float=T}
\begin{lstlisting}
function match_set1(in,op) {
	i75 = !in[7] && in[6] && !in[5]
	i42 = (!in[4] && (in[3] || in[2])) || (in[4] && !(in[3] || in[2]))
	return op[0] && in75 && in42
}
\end{lstlisting}

\subsection{Rule Matching}

For $n$ rules of $l$ bytes each to be checked, $n$ single-byte matchers are required, which are then fed sequentially with $l$ bytes.
The matcher is depicted in \Cref{fig:pbvloop}.

\begin{figure}[ht!]
	\centering
	\includegraphics[width=0.9\linewidth]{pbvloop} 
	\caption[Single-Byte Matcher]{\label{fig:pbvloop} A single-byte matcher unit. The SBM takes an input byte and an opcode that encodes the match to be performed. It iterates over the input bytes and the opcodes and returns whether a rule matches or not.}
\end{figure}

Before the first byte of a new packet is processed, a flip-flop is set using the \texttt{first byte?} input.
The instruction pointer, which simply is the index into the LUT, is set to 0.
During the matching process, that continues for $l$ iterations, a data byte and a match opcode are fed to the matcher, whose output is used to reset the flip-flop and the instruction pointer if any mismatch occurs.
If a byte matches, the flip-flop is untouched and the instruction pointer is incremented, pointing to the next opcode in the LUT.
At the next clock, the next instruction and input byte are fetched and the match continues.

At the end of the $l$ iterations, the result of the SBM is written to a register.
A modified priority encoder needs to identify the bits set among these $n$ registers to identify which of the rules has matched.
This is depicted in \Cref{fig:pbvvector}

\begin{figure}[ht!]
	\centering
	\includegraphics[width=.7\textwidth]{pbvvector}
	\caption[Iterative Byte Matches]{\label{fig:pbvvector}Iterative single-byte matchers. Each SBM matches a single rule. The output is fed into a priority encoder that identifies which rules matched.}
\end{figure}

\subsection{Constraints and Limitations}

\subsubsection{Multibyte Values}

As the name implies the single-byte matcher can only match byte values.
It is not well suited to match multibyte characters or values (such as unicode characters), which breaks the suggested encoding.
Sets or ranges with values that span multiple bytes also pose a problem.
While such sets cannot easily be expressed with regular expression, real-world rulesets with multibyte values and sets can be observed.
Matching those with a SBM is possible, however, it introduces false positives.

For example, using the SBM to match the set \texttt{[\textbackslash x0372\textbackslash x14a6]} requires matching the two single-byte sets \texttt{[\textbackslash x03\textbackslash x14][\textbackslash x72\textbackslash xa6]}, which also matches combinations of values not allowed in the original set.

This can be mitigated by extending the SBM to a width appropriate for the encoding and changing the instructions (at the expense of additional logic required).
However, the engine still cannot handle variable length encodings (like UTF-8) well.

\subsubsection{Ruleset Changes}

When reprogramming an FPGA on rule changes is possible, all conditions of a particular rule can be combined into one engine with minimal effort.
Each SBM keeps it's individual instruction set, on changes SBMs can be modified, added, or removed as required.

However, it might not be possible to reprogram the FPGA logic, only memory contents.
This is not a problem regarding the common decoders that are shared among all SBMs (exact values, don't cares, and case-insensitive) since the instruction LUT can simply be rewritten.
But set matches are decoded in hardware and specific for an individual SBM.
They cannot be changed on rule changes.

To address this issue all SBMs could be equipped with all set decoders.
Unfortunately this still does not solve the problem of adding new sets and it even introduces the problem that the SBMs grow significantly in size, which will certainly exceed space limitations, especially for large rule sets.
To reduce the logic requirements similar sets could be combined into a joint subset.
This would reduce the number of codewords to handle at the cost of additional false positives.

Another possibility is to use set match engines tailored for the rules that will be deployed initially and integrate a few more flexible set matching engines as spares.
These spare engines would consist of some 8-bit word comparators to encode ranges and sets. 

%
%%% EVALUATION %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
\section{Evaluation}

To evaluate the proposed single-byte matching engine we estimate the resource requirements using an Altera Stratix-IV FPGA as target platform.
The Stratix-IV offers two basic building blocks, the Logic Array Block (LAB) and the Adaptive Logic Module (ALM).
Each LAB consists of ten ALMs that can be used to implement functions.
Each ALM offer 8 data inputs in addition to certain control inputs.
ALMs can also be configured to be used as dual-port SRAM memory blocks of up to 640 bits.
For an in-depth discussion of the Stratix-IV building blocks see \cite{stratixiv}.
 
A possible mapping of the shared decoders into actual ALMs is shown in \Cref{fig:lutalm}.
This is only an educated guess, it is expected that the HDL compiler produces a more efficient mapping.
The 8-bit input byte and the 9-bit opcode require the use of 3 ALBs for the shared encoders.
Internal registers as well as the instruction lookup table require another ALB configured as SRAM, which is enough to encode more than 70 opcodes.

\begin{figure}[ht!]
	\includegraphics[width=\linewidth]{lutalm}
	\caption[Generic Matcher Mapping]{\label{fig:lutalm} Possible mapping of a the shared encoders into Stratix-IV ALMs. Functions with identical color combinations can be put into the same ALM. Image created using OmniGraffle.}
\end{figure}

Resource requirements of the specific set functions depend on the actual sets and ranges that need to be encoded.
The two set functions of the example rule in \Cref{lst:strregex} require approximately four more ALMs, two for the first set, one for the second set, and one for splitting the match function that has too many inputs.
Although it is quite possible that through rerouting the overall ALM usage is smaller. 

\begin{figure}[ht!]
	\includegraphics[width=\linewidth]{regexample}
	\caption[Regex DFA]{\label{fig:regexample}The example regular expression rule \Cref{fig:regexvis} as DFA.}
\end{figure}

In comparison, a DFA for the example regular expression (\Cref{lst:strregex}) requires twelve states and 32 transitions (\Cref{fig:regexample}).
There are several options to implement the DFA using logic, memory, or even TCAM to encode the transitions.
Either way, it can be shown that a DFA has exponential space requirements \cite{Sourdis2008Regular}.

%
%%% CONCLUSION %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
\section{Conclusion}

We proposed a novel matching engine that is capable of matching a large subset of regular expressions.
The single-byte matcher is the base building block to build parallel matching engines that potentially can match all rules in the ruleset against the packet payload in parallel.

Compared to finite automatons our approach trades memory for logic.
This can be an important design decision, especially if matching speed is important, since memory accesses are generally on the slow side.
Full-fledged regular expression matching still requires DFAs\slash{}NFAs, since the SBM is not capable of performing all match functions.
But many rules in contemporary rulesets do not require variable length quantifiers or assertions.
For those rules the SBM is an excellent choice to speed up the matching and reduce logic and memory requirements.
