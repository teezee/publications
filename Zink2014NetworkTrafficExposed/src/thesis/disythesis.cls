% Latex-Class-File: disythesis.cls
% ============================================================================
% 2012/09/19 disythesis.cls V 0.4 (c) 2012 Thomas Zink
%
% ***************************************************************************
% Legal Notice:
% This code is offered as-is without any warranty either expressed or
% implied; without even the implied warranty of MERCHANTABILITY or
% FITNESS FOR A PARTICULAR PURPOSE! 
%
% This work is distributed under the LaTeX Project Public License (LPPL)
% ( http://www.latex-project.org/ ) version 1.3, and may be freely used,
% distributed and modified. A copy of the LPPL, version 1.3, is included
% in the base LaTeX documentation of all distributions of LaTeX released
% 2003/12/01 or later.
% Retain all contribution notices and credits.
% ** Modified files should be clearly indicated as such, including  **
% ** renaming them and changing author support contact information. **
% ***************************************************************************
%
% Usage:
% Copy this file to local texmf/tex directory, or into your specific project.
% Then use with:
% \documentclass[Options]{disythesis}
%
% Options:
% [oneside] - use oneside as argument to book
% [quotchap] - use quotchap package, quotes before chapters
% [minitoc] - use minitoc package
% [bachelor] - for bachelor thesis
% [master] - for master thesis
% [phd] - for phd thesis / dissertation [default]
%
% ============================================================================

\ProvidesClass{disythesis}[2012/09/19 v0.4 Disy Thesis class]
\NeedsTeXFormat{LaTeX2e}

\typeout{Class for writing a Thesis}

% ============================================================================
% OPTIONS
% ============================================================================

% [oneside]
\newif\if@oneside
\DeclareOption{oneside} {\@onesidetrue}

% [quotchap]
\newif\if@quotchap
\DeclareOption{quotchap} {\@quotchaptrue}

% [bachelor]
\DeclareOption{bachelor} {
	\gdef\@degree{Bachelor}
	\gdef\@degreetext{Bachelor Thesis}
}

% [master]
\DeclareOption{master} {
	\gdef\@degree{Master}
	\gdef\@degreetext{Master Thesis}
}

% [phd]
\DeclareOption{phd} {
	\gdef\@degree{Doctor}
	\gdef\@degreetext{Dissertation}
}

% [minitoc]
\newif\if@minitoc
\DeclareOption{minitoc} {\@minitoctrue}

% [publish]
\newif\if@publish
\DeclareOption{publish} {\@publishtrue}

% set defaults and process
\ExecuteOptions{phd}
\ProcessOptions

% ============================================================================
% REQUIREMENTS / PACKAGES
% ============================================================================
% Load base class
\if@oneside
	\typeout{disythesis.sty [oneside]: oneside book.}
	\LoadClass[12pt, a4paper, oneside]{book}
\else
	\LoadClass[12pt, a4paper]{book}
\fi

% layout 
\if@quotchap
	\typeout{disythesis.sty [quotchap]: usepackage quotchap.}
	\usepackage[times]{quotchap} % chapters with quotes and big numbers
\fi

\if@minitoc
	\typeout{disythesis.sty [minitoc]: usepackage minitoc.}
	\usepackage{minitoc} % \minitoc for chapter contents
	\newcommand{\mtocbs}{\minitoc \bigskip}
\fi
	
\RequirePackage{setspace} 
\singlespacing

% margins, have to be loaded before fancyheader
\setlength{\marginparwidth}{0pt} % no note space, seems not to do anyth.
\usepackage[lmargin=2.5cm,rmargin=2.5cm,tmargin=2.5cm,bmargin=2.5cm]{geometry}

% Graphicx for loading no copyright image
\RequirePackage{graphicx}
\DeclareGraphicsExtensions{.png,.pdf,.jpg,.svg}

% headers
\RequirePackage{fancyhdr}
\pagestyle{fancy}

\renewcommand{\chaptermark}[1]{\markboth{\chaptername\ \thechapter.\ #1}{}}
\renewcommand{\sectionmark}[1]{\markright{\thesection\ #1}{}}

\fancyhf{}
%\fancyhead[RO,LE]{\bfseries\thepage}
\fancyfoot[CO,CE]{\bfseries\thepage} % consistent page num in center foot
\fancyhead[LO]{\nouppercase{\bfseries\rightmark}}
\fancyhead[RE]{\nouppercase{\bfseries\leftmark}}

\RequirePackage{url} % \url
	
\RequirePackage{titling} % \thetitle ...

% ============================================================================
% COMMANDS / DEFS / ENVIRONMENTS
% ============================================================================
%\def\degree#1{\gdef\@degree{#1}} % this is moved to options
\def\degreeyear#1{\gdef\@degreeyear{#1}}
\def\degreemonth#1{\gdef\@degreemonth{#1}}
\def\referee#1{\gdef\@referee{#1}} % the referee (1st)
\def\advisor#1{\gdef\@advisor{#1}} % the advisor (2nd)
\def\supervisor#1{\gdef\@supervisor{#1}} % the supervisor (3rd)
\def\department#1{\gdef\@department{#1}}
\def\faculty#1{\gdef\@faculty{#1}}
\def\field#1{\gdef\@field{#1}}
\def\university#1{\gdef\@university{#1}}
\def\universitycity#1{\gdef\@universitycity{#1}}
\def\universitystate#1{\gdef\@universitystate{#1}}
\def\oralexam#1{\gdef\@oralexam{#1}}

\newenvironment{alwayssingle}
	{\@restonecolfalse
		\if@twocolumn\@restonecoltrue\onecolumn
		\else\newpage\fi}
	{\if@restonecol\twocolumn\else\newpage\fi}
	
% commands for metapages
\renewcommand{\maketitle}{ 
	\thispagestyle{empty}
	\begin{center}
	% Title
	\vspace*{20mm}
	{\Large {\bfseries {\thetitle}} \par}
	\vspace*{30mm} 
	% Degree
	{\large {\bf \@degreetext\ submitted for the degree of}}\\
	\vspace*{2ex}
	{\large {\bf \@degree\ of \@field}}\\
	\vspace*{30mm}
	% Author
	{\Large {\bf Presented by}}\\
	\vspace*{2ex}
	{\Large {\bf \theauthor}}\\
	\vspace*{48pt}
	% University
	{\Large {\bf at the}}\\
	\vspace*{2ex}
	\includegraphics[width=0.4\linewidth]{unisignet}\\
	\vspace*{2ex}
	{\Large {\bf \@faculty}}\\
	\vspace*{2ex}
	{\Large {\bf \@department}}\\
	\vspace*{30mm}
	% Publish / Open
	\if@publish
		{\normalsize \bfseries
		Date of the oral examination: \@oralexam \\
		First supervisor: \@referee \\
		Second supervisor: \@advisor \\
		%Third supervisor: \@supervisor \\
		\par}
	\else
		\vspace{6ex}
		{\Large {\bf \@universitycity\ \thedate}}
	\fi
	\end{center}
}


% use this for exclusive copyright
\newcommand{\copyrightpage}{
	\newpage \thispagestyle{empty} \vspace*{\fill}
	\sc \noindent \copyright~\textit{\@degreeyear \hspace{3pt}~- \theauthor} \\
	\noindent All rights reserved.
	\vspace*{\fill} %\newpage \rm
}

% use this for creative commons
\newcommand{\nocopyrightpage}{
	\newpage \thispagestyle{empty} \vspace*{\fill}
	\begin{center}
	\includegraphics{by}\\
	\noindent (CC BY 4.0)\\
	\end{center}
	\noindent This work is licensed under a \href{http://creativecommons.org/licenses/by/4.0/}{Creative Commons Attribution 4.0 International License}.
	To view a copy of this license, visit \url{http://creativecommons.org/licenses/by/4.0/}.
	\vspace*{\fill} \newpage \rm
}

% dedicate this to somebody
\newcommand{\dedication}[1]{
	\newpage \thispagestyle{empty} \vspace*{\fill}
	\begin{center}
	\noindent #1
	\end{center}
	\vspace*{\fill} \newpage \rm
}

% abstract environment missing from book.cls
\newcommand\abstractname{Abstract}
\newenvironment{abstract}%
	{\newpage \thispagestyle{empty} \section*{\abstractname}}
	{\vspace*{\fill}\newpage\rm}%

% same for german 'kurzfassung'
\newcommand\kurzname{Kurzfassung}
\newenvironment{kurzfassung}%
	{\newpage \thispagestyle{empty} \section*{\kurzname}}
	{\vspace*{\fill}\newpage\rm}%


\endinput
