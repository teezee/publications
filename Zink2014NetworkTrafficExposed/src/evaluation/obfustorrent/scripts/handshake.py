#!/usr/bin/env python
# script to write all handshake messages bytes as pgm file
# apply the following wireshark  / tshark filter to find the handshakes:
# (!(ip.addr==134.34.165.147) && tcp.port>=9990 && tcp.port<10000 && !(tcp.port == 8080)) && (tcp.flags.ack ==1 && tcp.flags.push == 1) && (tcp.seq == 1)  && !tcp.analysis.retransmission 
# then save as new pcap file and load that here
# we could use libpcap filters, however, afaik no relative seq nums,
# would get complicated

import sys
import dpkt
import pcap
from optparse import OptionParser


class handshake_collector (object):
	data = []
	fname = ''
	
	def __init__ (self): pass
		
	def __call__ (self,pktlen,buf,ts):
		eth = dpkt.ethernet.Ethernet(buf)
		tcp = eth.data.data
		self.data.append(tcp.data)
	
	def loop (self, fname):
		po = pcap.pcapObject()
		po.open_offline(fname)
		po.setfilter(' ',0,0)
		po.loop(0, self)

def collect (fname):
	handler = handshake_collector()
	handler.loop(fname)
	return handler.data

class serializer (object):
	def __init__ (self): pass
	
	def __call__ (self, data, wid, format):
		maxlen = max([len(hs) for hs in data]) if ((wid==0) or wid >  max([len(hs) for hs in data])) else wid	
		f = eval("self." + format)
		f(data,maxlen)
		
	def pgm (self, data, wid):
		print 'P2'
		print wid, len(data)
		print '255'
		fstr = ' '.join(['{' + str(x) + ':3d}' for x in range(wid)])
		self._print(data,wid,fstr)
		
	def csv (self, data, wid):
		fstr = ', '.join(['{' + str(x) + ':4d}' for x in range(wid)])
		self._print(data,wid,fstr)
		
	def _print (self, data, wid, fstring):
		for d in data:
			ints = [ord(c) for c in d]
			if len(ints) < wid:
				ints += ([0] * (wid - len(ints)))
			print fstring.format(*ints)
		
def handshakes_to_pgm (handshakes, wid):
	maxlen = max([len(hs) for hs in handshakes]) if ((wid==0) or wid >  max([len(hs) for hs in handshakes])) else wid
	print 'P2'
	print maxlen, len(handshakes)
	print '255'
	fstr = ' '.join(['{' + str(x) + ':4d}' for x in range(maxlen)])
	for hs in handshakes:
		#print ' '.join(["%3s" % i for i in [ord(c) for c in hs]])
		ints = [ord(c) for c in hs]
		if len(ints) < maxlen:
			ints += ([0] * (maxlen - len(ints)))
		print fstr.format(*ints)

def parse_args (argv):
	p = OptionParser()
	p.add_option("-r", "--file", dest="fname",
		help="pcap file", default='', type="string")
	p.add_option("-o", "--offset", dest="off",
		help="offset", default=0, type="int")
	p.add_option("-l", "--length", dest="len",
		help="length", default=-1, type="int")
	p.add_option("-s", "--snaplen", dest="snaplen",
		help="number of bytes to output", default=0, type="int")
	p.add_option("-f", "--format", dest="format",
		help="output file format, csv | pgm", default="pgm", type="string")
	(opts, args) = p.parse_args()
	if opts.fname=='': p.error("specify at least input file")
	if opts.format not in ['pgm', 'csv']:
		p.error("unsupported format " + opts.format)
	return (opts,args)
		
def main(opts,args):
	handshakes = collect(opts.fname)
	lens = opts.len if opts.len>0 else len(handshakes)
	#f = eval("handshakes_to_" + opts.format)
	#f(handshakes[opts.off:lens], opts.snaplen)
	s = serializer()
	s(handshakes[opts.off:opts.off+lens], opts.snaplen, opts.format)
	
	return handshakes

if __name__ == '__main__':
	(opts,args) = parse_args(sys.argv)
	#print opts,args
	handshakes = main(opts,args)