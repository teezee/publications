#!/usr/bin/env python
# script to convert peer IDs from csv to pgm image files

import sys

class peerid (object):
    _peerid = []

    def __init__ (self, data):
        self._peerid = data

    @property
    def tries (self):
        return self._peerid[0]

    @property
    def ms (self):
        return self._peerid[1]

    @property
    def data (self):
        return self._peerid[2:]
    
    def __str__ (self):
        s = '{0:6d}, {1:4d}, '
        for i in range(len(self._peerid[2:])): s += '{%s:3d}, ' % (i+2)
        return s[:-2].format(*self._peerid)


def peerid_from_line (line):
        return peerid([int(x) for x in line.split(',')])


def peerids_from_file (fname):
    handle = open(fname,'r')
    peerids = []
    for line in handle:
        try:
            pid = peerid([int(x) for x in line.split(',')])
            peerids.append(pid)
            #print pid
        except (ValueError): pass
    handle.close()
    return peerids


def peerids_to_pgm (pids):
    print 'P2'
    print len(pids[0].data), len(pids)
    print '255'
    fstr = ' '.join(['{' + str(x) + ':4d}' for x in range(len(pids[0].data))])
    for pid in pids:
        print fstr.format(*pid.data)



if __name__ == '__main__':
    peerids = peerids_from_file(sys.argv[1])
    peerids_to_pgm(peerids)
