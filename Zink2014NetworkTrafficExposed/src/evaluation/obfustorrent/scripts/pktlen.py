#!/usr/bin/env python
#
# pktlen.py
# collect network packet lengths and calculate {min,max,avg}

import sys
import dpkt
import pcap
from math import *

def mean(d): return float(sum(d))/len(d)

def var(d): 
	m = mean(d)
	return sum([(x-m)**2 for x in d])/(len(d)-1)
	
def stdev(d): return sqrt(var(d))


class pktlencollector (object):
	lens = []
	time = []
	
	def __init__ (self): pass
		
	def __call__ (self,pktlen,buf,ts):
		#print pktlen, 
		self.lens.append(pktlen)
		self.time.append(ts)
	
	def loop (self, fname):
		po = pcap.pcapObject()
		po.open_offline(fname)
		po.setfilter(' ',0,0)
		po.loop(0, self)
	
	def __str__ (self):
		d = self.lens
		t = self.time
		dt = [(t[i+1]-t[i]) for i in range(len(t)-1)]
		
		return "num, sum, min, max, avg, sd, var\n%s, %s, %s, %s, %s, %s, %s\n%s, %s, %s, %s, %s, %s" % (len(d), sum(d), min(d), max(d), mean(d), stdev(d), var(d), len(dt), min(dt), max(dt), mean(dt), stdev(dt), var(dt))

def collect (fname):
	handler = pktlencollector()
	handler.loop(fname)
	return handler

def main(argv):
	return collect(argv[1])

if __name__ == '__main__':
	res = main(sys.argv)
	print res