# PICDFI Evaluation and Verification

## Thirdparty Tools

The following third party tools have been used to generate results, build a baseline and verify PICDFI.

### nDPI

Available from:

	svn co https://svn.ntop.org/svn/ntop/trunk/nDPI
	
**Beware**: the new `ndpiReader` has a new output format which breaks parsing the resultfile. Use the version `pcapReader` in the provided nDPI.tgz.

Build and compile

	$ cd nDPI
	$ ./autogen.sh
	$ ./configure
	$ make 
	$ sudo make install

Run the example and generate result (need `-v 2` switch):

	$ cd example
	$ ./pcapReader -i <iface or pcap file> -v 2 > ndpi.txt

### OpenDPI

Available from:

	$ svn checkout http://opendpi.googlecode.com/svn/trunk/ opendpi

Apply our patch to enable per flow output:
	
	$ cd opendpi/src/examples/OpenDPI_demo/
	$ patch < path/to/OpenDPI_picDFI.patch

Build and compile:

	$ cd opendpi
	$ ./autogen.sh
	$ ./configure
	$ make
	$ sudo make install

Run the example and generate result (need `-o` switch for output file):

	$ cd opendpi/src/examples/OpenDPI_demo/
	$ ./OpenDPI_demo -r ~/Workspace/phd/projects/picdfi/eval/captured_all.pcap -o opendpi.txt

### SPID

Available from:

	http://sourceforge.net/projects/spid/files/SPID%20-%20Protocol%20Identification/SPID%20Algorithm%20Proof-of-Concept%200.4.6/SPID_Algorithm_PoC-0-4-6.zip/download

Mono or .NET Runtime required.
Run example Application:

	$ cd SPID_Algorithm_PoC-0-4-6
	$ mono SPID.exe

Just open your trace, save the results and rename to `spid.txt`.

## Verify PICDFI

Copy picdfi results and all other result files to one directory. Copy `resultparser.py` to the same directory.
Run:

	./resultparser.py

The resultparser parses all result files, builds the baseline and compares and verifies the picdfi result. Output is in LATEX table syntax.
