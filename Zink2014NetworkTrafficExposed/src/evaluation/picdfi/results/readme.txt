Results have been generated using the pcap file

https://svn.uni-konstanz.de/disy/Huawei/ipv6_lookup/trunk/Testdata/pcap/captured_all.pcap.gz

The following third party classifiers have been used for comparison

OpenDPI
svn checkout http://opendpi.googlecode.com/svn/trunk/ opendpi-read-only

nDPI
svn co https://svn.ntop.org/svn/ntop/trunk/nDPI
http://www.ntop.org/get-started/download/


SPID
http://sourceforge.net/projects/spid/files/SPID%20-%20Protocol%20Identification/SPID%20Algorithm%20Proof-of-Concept%200.4.6/SPID_Algorithm_PoC-0-4-6.zip/download


Building OpenDPI
----------------
requires autoconf, pkgconfig, automake, libtool

./autogen.sh
./configure
make


Building nDPI
-------------
./configure
make


Execute SPID
------------
Requires Mono Framework. Only builds on Windows due to VStudio dependencies.
