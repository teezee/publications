#!/usr/bin/Rscript

wd = '/home/tzn/Workspace/phd/projects/picdfi/eval'
setwd(wd);

# read result files
picdfi = read.csv('picdfi.csv',header=T);
opendpi = read.csv('opendpi.csv',header=T);
ndpi = read.csv('ndpi.csv',header=T);
spid = read.csv('spid.csv',header=T);

# calc the total numbers
totals = data.frame(
  packets=c(sum(picdfi$packets),sum(opendpi$packets),sum(ndpi$packets),sum(spid$packets)),
  bytes=c(sum(picdfi$bytes),sum(opendpi$bytes),sum(ndpi$bytes),sum(spid$bytes)),
  flows=c(sum(picdfi$flows),sum(opendpi$flows),sum(ndpi$flows),sum(spid$flows)),  
  row.names=c("picdfi","opendpi","ndpi","spid")
);

