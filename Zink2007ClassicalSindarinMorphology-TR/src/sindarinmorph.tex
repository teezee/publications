% This is LLNCS.DEM the demonstration file of
% the LaTeX macro package from Springer-Verlag
% for Lecture Notes in Computer Science,
% version 2.2 for LaTeX2e
%
\documentclass[english,runningheads]{llncs}
%
\usepackage{makeidx}  % allows for indexgeneration
\usepackage{graphicx}
\usepackage[hyperindex,backref,breaklinks,colorlinks,linkcolor=blue,citecolor=blue,bookmarksnumbered,bookmarksopen]{hyperref}
%
\begin{document}
\pagestyle{headings}  
% switches on printing of running heads
\title{Classical Sindarin Morphology}
%\subtitle{Finite-State Morphology SS 2007}
%
\titlerunning{Classical Sindarin Morphology}
%
\author{Thomas Zink \and Andreas Engl}
\tocauthor{Thomas Zink, Andreas Engl (University of Konstanz)}
%
\institute{University of Konstanz\\
\email{$\left\{ thomas.zink, andreas.engl \right\}$ @uni-konstanz.de}}

\maketitle              % typeset the title of the contribution

\begin{abstract}
All of his life, the linguist and author J.R.R. Tolkien invented languages. Most effort went into his elvish languages, Quenya and Sindarin, and he made changes to them until his death in 1973 \cite{SALO}. His intention was to create realistic languages,  especially in Sindarin, with harmonic sounds. Thus, his languages don't follow logic rules but are full of irregularities. Though from a strict perspective
Sindarin cannot be considered a complete language \cite{Fauskanger}, enough published material is available to 
make a descent description of this beautiful language. This project tries to model the morphology of so called 
Classical Sindarin nouns. It is based on the the works of David Salo \cite{SALO} and Thorsten Renk \cite{Renk}.
\end{abstract}
%
%\tableofcontents
%
\section{A (very) brief history of Sindarin}
Tolkien did not only construct languages, he invented whole worlds in which his languages existed and developed
over time. Through the ages movements of peoples, wars and feuds have influenced the languages spoken. This also
holds for Sindarin. In Tolkien's writings multiple morphological and phonological changes in written and spoken
Sindarin can be observed. The roots of Sindarin reach back until the first age. The first people that awoke in
Middleearth were the Quendi which is the elvish word for 'people'(in fact, not the Quendi but the Noeg, Dwarfs, 
were the first people, but this is another story, told in \cite{Sil}). They created the first
spoken language which is known as Primitive Quendian. When the Quendi split into two Tribes, the Eldar and the Avari,
their language also changed, leading to Common Eldarin and Avarian. The Eldar were fond of travelling but some of them
only reluctantly travelled to the west. They became known as the Teleri and their language was Common Telerin which
developed parallel to Quenya. It is Common Telerin of which all Sindarin dialects are derived, however, the numerous
events that led to the foundation of these dialects can't be recounted here. Instead figure \ref{fig:elvishlang} shows the development of elvish languages.\\
%
\begin{figure}[h]
	\centering
	\includegraphics[height=8cm]{gfx/elvish_langs}
	\caption{\label{fig:elvishlang}Development of Elvish Languages}
\end{figure}

Classical Sindarin is the language spoken at the time of "The Lord of the Rings", it is the language of the Sindar, the
Grey-Elves, those who still live in Middleearth.
%
\section{Classical Sindarin Phonology}
The Sindarin writing system is phonetic with each character denoting a specific sound. The writing system changed drastically between different dialects from simple runes not unlike celtic writing to the beautiful Tengwar modes which are also depicted in the movies. But even Tengwar differs slightly in some dialects. The noldoric mode for instance uses diacritical
characters for vowels, the so called tehtars, while the belerian mode uses ordinary tengwar characters. These two modes also
differ in the characters used for nasal and glottal sounds. For a nice introduction to Tengwar and it's modes see \cite{Teng}.
%
\begin{figure}[h]
	\centering
	\includegraphics[height=8cm]{gfx/tengwar}
	\caption{\label{fig:tengwar}The Tengwar Chart }
\end{figure}
%

The sounds of Classical Sindarin can be described as in table \ref{tab:ClassicalSindarinVowels} and \ref{tab:ClassicalSindarinConsonants}. Sindarin Sounds are pretty much like german sounds except for some sounds like th and dh which more resemble english and others that are trilled like in spanish or russian.
%
\subsection{Vowels}
%
In Sindarin there are short vowels, long vowels, stressed vowels and diphthongs. For correctness we have to add, that in Tengwar no distinction is made between long and stressed vowels. However, Tolkien introduced different characters for long and stressed vowels in his own romanization. Stressed vowels only occur in monosyllables.\\

Long vowels are marked with an acute and stressed vowels are marked with a circumflex. Unfortunately there is no y character with circumflex but with umlaut, which is used instead in table \ref{tab:ClassicalSindarinVowels}. All single character Vowels can be long or stressed but not the diphthongs.
%
\begin{table*}
	\centering
	\begin{tabular}[h]{@{}p{2cm}|p{0.5cm}@{}p{0.5cm}@{}p{0.5cm}@{}p{0.5cm}@{}p{0.5cm}@{}p{0.5cm}@{}p{0.5cm}@{}p{0.5cm}@{}}
			\hline \hline
			Short & a & e & i & o & u & y \\
			Long & \'a & \'e & \'i & \'o & \'u & \'y \\
			Stressed & \^a & \^e & \^i & \^o & \^u & \"y \\
			Diphthong & ae & ai & au & aw & ei & oe & oi & ui \\
			\hline \hline
	\end{tabular}
	\caption{Classical Sindarin Vowels}
	\label{tab:ClassicalSindarinVowels}
\end{table*}

According to table \ref{tab:ClassicalSindarinVowels} the vocals can be defined in xfst as follows (all listings are presented with official 7-bit ascii characters only, thus special characters can not be displayed):
%
\begin{verbatim}
define Vshort [ a | e | i | o | u | y ];
define Vlong  [ 'a | 'e | 'i | 'o | 'u | 'y ];
define Vstressed [ %^a | %^e | %^i | %^o | %^u | %^y ];
define Vdiph  [ ae | ai | au | aw | ei | oe | oi | ui ];
define Vowels [ Vshort | Vlong | Vstressed | Vdiph ];
\end{verbatim}
%
\subsection{Consonants}
Consonants are best grouped by sound. The following table shows all consonantal Sounds.
%
\begin{table*}[h]
	\centering
		\begin{tabular}[b]{@{}p{2cm}|p{1.3cm}@{}p{1.7cm}@{}p{1.5cm}@{}p{1.5cm}@{}p{1.5cm}@{}p{1.5cm}@{}p{1.5cm}@{}}
			\hline \hline
			& labial & interdental & alveolar & palatal & velar & labialized & glottal \\
			\hline
			unaspirated stops & p b & & t d & & c g & gw \\
			nasal & m & & n & & ng \\
			trill & & & rh r\\
			fricative & f v ph& th dh & s & & ch & chw & h\\
			approximant & & & lh l & j & & hw & w \\
			\hline \hline
		\end{tabular}
	\caption{Classical Sinarin Consonants}
	\label{tab:ClassicalSindarinConsonants}
\end{table*}\\
%
The definitions for xfst thus are:
%
\begin{verbatim}
	define Cmono [ t | p | c | d | b | g | h | f | v | m | n | r | s | l | w ];
	define Cdi   [ gw | ng | rh | lh | ch | chw | hw | th | dh | ph ];
	define Consonants [ Cmono | Cdi ];
\end{verbatim}
%
Note that for Consonant and Vowel definitions all digraphs and diphthongs are defined as single characters. Since in Tengwar these sounds are also only a single character, mutations affect the sound itself. This is especially important for some mutation rules as we will see later. Unfortunately the ascii character set cannot represent the numerous sounds of Sindarin adequately.
%
\subsection{Basic Phonology}
There are some basic phonological rules which can be easily modeled in xfst.
%
\begin{enumerate}
	\item The unvoiced plosives p, t, c never occur following a vowel, but are lenited to b, d, g.\\
	\texttt{[ t -> d, p -> b, c -> g || Vowels \_ ]}\\
	There is one exception to this rule: the plural ending -ath (see section Grammar). However, since this rule is not used in this 	project we set this fact aside. 
	\item c is always pronounced as k.\\
	\texttt{[ c -> k ]}
	\item f at word end is always spelt as v.\\
	\texttt{[ f -> v || \_ .\#. ]}
	\item au at word end is always spelt as aw.\\
	\texttt{[ au -> aw || \_ .\#. ]}
\end{enumerate}
%
Rule 2 is dispensable. Tolkien decided to use character c for the sound k so we will stick with it. It is only listed for completeness. When using a lexicon file even the other rules would be pretty useless, assuming the lexicon file is free of typos.
%
\section{Grammar}
%
\subsection{Articles}
%
\subsubsection{Indefinite Article.}
There is no indefinite article in Sindarin, thus a noun that has no article is indefinite. Nouns can be singular or plural in an indefinite state.
%
\subsubsection{Definite Article.}
The singular article is \itshape i\normalfont. It strongly binds to the noun so it is concatenated with a \itshape - \normalfont leading to \itshape i-\normalfont. The singular article triggers the soft mutation (see section Mutations). If it precedes a word beginning with any i vowel it is mutated to \itshape ir\_\normalfont. Note that \_ represents the space character.
%
\begin{verbatim}
< ["Art+" .x. {i-}] "@P.NUM.SG@" "@U.MUT.SOFT@" >

[ {i-} -> {ir_} \/ .#. _ [$[i|i|%^i] & ~$[Consonants]] ]
\end{verbatim}
%
\subsubsection{Genitive Article.} The genitive article is \itshape en \normalfont and can be translated as \itshape of the \normalfont. It is usually only used with a common noun, if the noun is a title (like king) or name the article can be left. The genitive article causes mixed mutation (see section Mutations).
%
\begin{verbatim}
< ["Art+" .x. {en_}] "@P.CASE.GEN@" "@U.MUT.MIX@" >
\end{verbatim}
%
In irregular (assumingly optional) cases which can not be modeled using rules(\cite{Renk} and \cite{Fauskanger}), \itshape en \normalfont is shortened to \itshape e \normalfont. This is not considered in this project.
%
\subsubsection{Plural Article.} The plural article is \itshape in \normalfont in all cases. It triggers the nasal mutation. Confusingly, if it precedes a word beginning with a consonant it is mutated to \itshape i\normalfont. To show the difference to the singular article, the shortened \itshape in \normalfont is modeled as \itshape i\_ \normalfont rather than \itshape i-\normalfont. This also reflects that the the shortened \itshape in \normalfont doesn't bind as heavily to the noun. Strictly this isn't neccessary since the number of the noun can also be identified by inspecting the consonant and vowel mutations.
%
\begin{verbatim}
< ["Art+" .x. {in_}] "@P.NUM.PL@" "@U.MUT.NAS@" >

[ "in_" -> "i_" || _ [$[Consonants] & ~$[Vowels]] ]
\end{verbatim}
%
\subsubsection{Other Cases and Prepositions.}
Special cases arise, if a preposition precedes the articles. Then, depending on preposition stem and ending, article, and mutations, the articles and prepositions are merged to one word. Due to their complexity prepositions are not modeled in this project.
%
\subsection{Plurals}
Forming plurals raises a lot of consonant and vowel mutations which are covered in the section Mutations. However, there are other plurals which are covered here in short.
%
\subsubsection{Suffix Plurals.}
Some words have the base form plural. In these cases the singular is formed by attaching a special suffix. Some of these words are modeled and the required suffix is attached using flag diacritics.
%
\begin{verbatim}
< "+Noun":0 "+Sg":{od} "@R.SUFF.OD@">
< "+Noun":0 "+Sg":{og} "@R.SUFF.OG@">
< "+Noun":0 "+Sg":{ig} "@R.SUFF.IG@">
\end{verbatim}
%
\subsubsection{-ath Plurals.}
There is a special suffix \itshape -ath \normalfont which can be attached to nearly every word and means something like "all" or the entire set whereas the ordinary plural means some or lot or a subset more than one but less then all. Thus it is also called collective plural. The suffix is altered to \itshape -iath \normalfont whenever it follows a monosyllable of the form C - any i vowel - n,r or l. In addition, if the -ath suffix is attached all stressed i vowels and diphthongs with an i sound are shortened to i. The -ath suffix can not cooccur with any article.
%
\begin{verbatim}
< ["+Ath" .x. {ath}] "@R.ART.INDEF@" "@R.NUM.SG@">

[
         ['i|%^i|ai] -> i
         \/ _ ?* $[{ath}|{iath}] .#.
]
.o.
[
         [..] -> i
         \/ .#. Consonants [i|i|%^i|ai|ei|oi|ie|io] [n|r|l] _ {ath} .#.
]
\end{verbatim}
%
\subsubsection{Other Plurals.}
There still are a number of other plurals in Common Sindarin which are disregarded in this project for different reasons. One special case for example are defective singulars which are singular words which lost their final or primary syllable. When forming the plural of these words a suffix is attached which consists of the last vowel in the word and the final consonant n. Since it is only possible to identify defective singulars by inspecting Old Sindarin roots they are disregarded.\\
Another class of plurals are suffixes that mean something like people or horde, double affection plurals, -u and -in plurals.
%
\section{Mutations}
There are a large number of mutations in Classical Sindarin. Some are historical and form a cascade of changes from Old Sindarin and even Common Eldarin up to Classical Sindarin. Thus, classification of word stems are done regarding the original Old Sindarin roots. This proved to be quite a problem. In order to model all Classical Sindarin morphological rules, one would have to model all changes from the base languages up to Classical Sindarin. Unfortunately, this is out of the scope of this project. Additionally, some morphological changes are not quite clear and guesses by authors of numerous works, some of them even contradictial. These rules have been left out or modeled at will by the authors of this project as they seemed to fit. They are marked in the listings with '!!' at the end of a line. Some sounds are completely lost by mutation, this is denoted as the character ' in the rules.
%
\subsection{Consonant Mutations}
The consonant mutations describe changes of initial consonants. They basically happen all the time in Sindarin, when a word follows a certain word, a preposition, an article and so on. To complicate things even the articles and prepositions which cause initial consonants to change do mutate often. However, some of these changes are very irregular and dependent on archaic roots and are not modeled in this project. Prepositions and Pronouns are completely ignored.\\

Consonant mutations are modeled using flag diacritics. In cases where a mutation must occur a flag is set in the lexc source file. If this flag is set the mutation rule is applied.
%
\subsubsection{Soft Mutation.}
Is one of the most common mutations. Occurs after singular definite article \itshape i\normalfont, after most prepositions ending with a vowel and in other cases.
%
\begin{verbatim}
[
         d -> dh, b -> v, g -> ',
         t -> d, p -> b, c -> g,
         gl -> 'l, gr -> 'r, gw -> 'w,
         lh -> l, rh -> r,
         h -> ch, s -> h, m -> v,
         hw -> chw										!!
         || %@U%.MUT%.SOFT%@ _
]
\end{verbatim}
%
\subsubsection{Nasal Mutation.}
Is another important mutation which occurs after the plural article \itshape in \normalfont and some prepositions ending in a consonant.
%
\begin{verbatim}
[
         t -> th, p -> ph, c -> ch,
         b -> m, 
         lh -> l, rh -> r, !!
         h -> ch,
         hw -> chw !!
         || %@U%.MUT%.NAS%@ _
]         
.o.
[
         d -> n, g -> ng
         || %@U%.MUT%.NAS%@ _ Vowels
]
\end{verbatim}
%
\subsubsection{Mixed Mutation.}
The mixed mutation is caused by the genitive article \itshape en \normalfont and some prepositions combined with the article \itshape in\normalfont. It is called mixed mutation because it has elements of the nasal and soft mutations. 
%
\begin{verbatim}
[
         t -> d, p -> b, c -> g,
         lh -> l, rh -> r,
         s -> h,
         h -> ch, hw -> chw !!
         || %@U%.MUT%.MIX%@ _
]
\end{verbatim}
%
\subsubsection{Other Consonant Mutations.} There still are more consonant mutations in Sindarin, like the Stop, Liquid and Medial Mutations. However, they are caused by prefixes which have not been modeled, so they are disregarded and only mentioned for completeness. For a deeper look into these mutations see \cite{SALO} and \cite{Renk}.
%

Table \ref{tab:SindarinConsonantMutations} shows Consonant Mutations modeled in this project together with some examples which are included in the lexc source file.
%
\begin{table*}[h]
	\centering
		\begin{tabular}[b]{@{}p{2cm}@{  }p{3cm}@{  }p{3cm}@{  }p{3cm}@{}}
			\hline \hline
			Mutations & Soft & Nasal & Mixed\\
			\hline
			barad & i-varad & i\_meraid & en\_barad\\
			cant & i-gant & i\_chaint & en\_gant\\
			d\^ur & i-dh\^ur & i\_nuir & en\_d\^ur\\
			draug & i-dhraug & i\_droeg & en\_draug\\
			fl\^ad & i-fl\^ad & i\_flaid & en\_fl\^ad\\
			graug & i-'raug & i\_groeg & en\_graug\\
			h\^ith & i-ch\^ith & i\_ch\^ith & en\_ch\^ith\\
			l\^ug & i-l\^ug & i\_luig & en\_l\^ug\\
			mellon & i-vellon & i\_mellyn & en\_mellon\\
			naug & i-naug & i\_noeg & en\_naug\\
			parth & i-barth & i\_phairth & en\_barth\\
			ras & i-ras & i\_rais & en\_ras\\
			rhavan & i-ravan & i\_revain & en\_ravan\\
			sereg & i-hereg & i\_serig & en\_hereg\\
			taur	& i-daur & i\_thoer & en\_daur\\
			\hline \hline
		\end{tabular}
	\caption{Sindarin Consonant Mutations}
	\label{tab:SindarinConsonantMutations}
\end{table*}\\
%
\subsection{Vowel Mutations}
In addition to consonant mutations Sindarin also has a lot of vowel mutations, also called affections. The most of them occured from Old Sindarin roots to Common Sindarin. Vowel affections within Common Sindarin are due to plural changes or caused by articles and prepositions.
%
\subsubsection{Primary Final i Affection.}
The name originates from the fact that it occured in Old Sindarin words whenever a syllable preceded the final vowel i. In Common Sindarin it occurs only when forming plurals and only in monosyllable words or final syllables.
%
\begin{verbatim}
[
         [a|%^a] -> ai, e -> i, %^e -> %^i,
         [o|u] -> y, %^o -> %^y, %^u -> ui,
         au -> oe, ie -> i, io -> y
         || %@P%.NUM%.PL%@ ?+ _ [$[Consonants] & ~$[Vowels]] .#.
]
\end{verbatim}
%
\subsubsection{Secondary Affection.}
Secondary affection occurs on vowels within words that have been altered by primary affection. Practically this only affects multisyllable words when forming plurals.
%
\begin{verbatim}
[
         [a|'a|%^a] -> e, [o|'o|%^o] -> e, ['i|%^i] -> i, u -> y, %^u -> u
         || %@P%.NUM%.PL%@ ?* _ ?* [ $[ ai | i | y ] & ~$[ui] ] .#.
]
\end{verbatim}
%
Affection does not take place if the last or only vowel in a word is ui.\\

Table \ref{tab:SindarinPluralAffections} shows all changes in vowels together with some examples which are also included in the lexc file of the project.
%
\begin{table*}[h]
	\centering
		\begin{tabular}[b]{@{}p{1.5cm}@{  }p{1.7cm}@{  }p{1.5cm}@{  }p{2.5cm}@{  }p{2.2cm}@{}}
			\hline \hline
			vowel & becomes in nonfinal syllables & becomes in final syllables & Singular & Plural \\
			\hline
			a, \^a & e & ai & las, t\^al, adan & lais, tail, edain\\
			e, \^e & e & i, \^i & sereg, t\^ew, edhel & serig, t\^iw, edhil\\
			i, \^i & i & i, \^i & g\^il, gilith & g\^il, gilith\\
			o, \^o & e & y, \"y & p\^od, onod & p\"yd, enyd\\
			u & y & y & tulus & tylys\\
			\^u & u & ui & d\^ur & duir\\
			\"y & & \"y & m\"yl & m\"yl\\
			ai & & ai & taith & taith\\
			ae & ae & ae & gwael, aegas & gwael, aegais\\
			au & au & oe & naug & noeg\\
			ie & & i & miniel & minil\\
			io & & y & mirion & miryn\\
			ui & ui & ui & iuith & iuith\\			
			\hline \hline
		\end{tabular}
	\caption{Sindarin Plural Vowel Affections}
	\label{tab:SindarinPluralAffections}
\end{table*}\\
%

\subsubsection{Other Affections.}
Again there are many more vowel affections in Sindarin which occured from Old Sindarin to Common Sindarin. These are the a Affection, Primary Internal Affection and Double Affection. They are disregarded.

\section{xfst Project}
The project consists of the following files.
%
\begin{itemize}
	\item{\texttt{sindarin-lex.txt}}\\
	The lexicon file.
	\item{\texttt{sindarin-rul.txt}}\\
	The rule file. It models all morphological rules discussed in this paper.
	\item{\texttt{sindarin-def.txt}}\\
	Holds all definitions.
	\item{\texttt{sindarin.xfst}}\\
	The xfst script which contains all definitions, reads the lexicon and rule file and compiles the network.
	\item{\texttt{sindarin-stack.xfst}}\\
	A working stack file of the compiled project.
\end{itemize}
%
Listings of these files are left to prevent redundant information and for spatial reasons. The source files are well documented so we forego detailed explanations here and reference to the source files instead.\\
All files must be located in the same directory. After starting xfst from that directory the project can be loaded using the command:
%
\begin{verbatim}
xfst[0]: source sindarin.xfst
\end{verbatim}
%
If for some reason the source files won't work, the stack of the working project is included as \texttt{sindarin-stack.fst}. It should work in any xfst implementation on any platform albeit character encoding. The stack can be loaded with
%
\begin{verbatim}
xfst[0]: load stack < sindarin-stack.fst
\end{verbatim}
%

This project only covers Common Sindarin nouns, articles discussed in section Articles, and plurals as discussed in section Plurals.
%
\subsection{Projections}
%
\subsubsection{Upper Side Language.}
The upper side language consists of the base form of the nouns and various tags describing different states the nouns can take. Legal upper side strings are:
%
\begin{itemize}
	\item{Indef+'noun here'+Noun+Sg}
	\item{Indef+'noun here'+Noun+Pl
	\item{Indef+'noun here'+Noun+Ath}}
	\item{Art+'noun here'+Noun+Sg}
	\item{Art+'noun here'+Noun+Pl}
	\item{Art+'noun here'+Noun+Gen}
\end{itemize}
Replace 'noun here' with any noun of the wordlist.
%
\subsubsection{Lower Side Language.}
The lower side language shows the mutated nouns with or without article. That is
%
\begin{itemize}
	\item{'noun singular'}
	\item{'noun plural'}
	\item{'noun'ath}
	\item{'noun'iath}
	\item{i-'noun singular'}
	\item{ir\_'noun singular'}
	\item{in\_'noun plural'}
	\item{i\_'noun plural'}
	\item{en\_'noun genitive'}
\end{itemize}
%
\subsubsection{Examples.}
Some example output the network should give.\\
%
\begin{verbatim}
xfst[1]: down Art+edhel+Noun+Gen
en_edhel
xfst[1]: up en_d^al
Art+t^al+Noun+Gen
xfst[1]: down Art+idhil+Noun+Sg
ir_idhil
xfst[1]: down Art+edhel+Noun+Sg
i-edhel
xfst[1]: up i_threnairn
Art+trenarn+Noun+Pl
xfst[1]: down Art+galadh+Noun+Pl
i_ngelaidh
xfst[1]: down Indef+galadh+Noun+Pl
gelaidh
xfst[1]: up gwan^un
Indef+gwan^un+Noun+Pl
xfst[1]: down Indef+gwan^un+Noun+Sg
gwan^unig
xfst[1]: down Indef+h^ith+Noun+Ath
hithath
xfst[1]: down Indef+fair+Noun+Ath
firiath
\end{verbatim}
%
And some input the network should reject.
%
\begin{verbatim}
xfst[1]: up h^ithath
xfst[1]: up ir_edhel
xfst[1]: up in_gwaith
xfst[1]: down Art+onod+Noun+Ath
\end{verbatim}
%
\subsubsection{Known Bugs.}
There is one known bug which could be reproducted but no solution could be found. It references the mutation of the \itshape -ath \normalfont plural suffix to \itshape -iath\normalfont. The rule is located on lines 99 - 103 in the \texttt{sindarin-rul.txt} source file.
%
\begin{verbatim}
[
         [..] -> i
         \/ .#. Consonants [i|i|%^i|ai|ei|oi|ie|io] [n|r|l] _ {ath} .#.
]
\end{verbatim}
%
Though syntactically correct, the \itshape i \normalfont is not inserted. Strangely the rule works when the word symbols \itshape .\#. \normalfont are left out. This, however, leads to wrong plurals as e.g. \itshape idhiliath\normalfont. When applied in a clean xfst environment (i.e. only this rule is read as regular expression) the rule matches and the \itshape i \normalfont is inserted correctly. Whether this bug is due to conflicting rules of the rulebase or to the limited capabilities of the xfst evaluation version (which always complains that the network is too large) could not be discovered. It is left in the sources and marked as \texttt{! BUG}.
%
\section{Discussion}
This project only covers a small part of Classical Sindarin. Unfortunately, much has to be left undone. Sindarin is a beautiful language with morphological changes and historical background that is amazing for a constructed language. The different modes in writing and different dialects are hard to distinguish for the novice's eye. For that reason the numerous documentations of Sindarin which can be found in the Internet are often contradictal. Even after having studied and compared three different sources of which two are written by linguists some things remain mysteries. Sindarin has never been completely described by Tolkien, all what is known has been derived by analysing the numerous Sindarin writings that exist. Thus, no source is perfect. The authors of this project tried to model rules that seem to be commonly accepted as fact. No claim for completeness or even correctness can be made.
%
% ---- Bibliography ----
%
\begin{thebibliography}{6}
%
\bibitem {SALO}
Salo, D.:
A Gateway to Sindarin.
The University of Utah Press, Salt Lake City 2004.

\bibitem {Renk}
Renk, T.:
Pedin Edhellen, a Sindarin Course. 2004,
http://www.sindarin.de/index.html. visited 2007.

\bibitem {Fauskanger}
Fauskanger, H. K.: 
Of the Tongues of Arda, the invented world of J.R.R. Tolkien.
http://www.uib.no/people/hnohf/sindarin.htm. visited 2007


\bibitem {Sil}
Tolkien, J.R.R., Tolkien, Christopher (ed.):
The Silmarillion. HarperCollins Publishers 1999.

\bibitem {Teng}
Katzer, G.:
Tengwar und ihre Verwendung,
http://www.uni-graz.at/~katzer/tengwar.html. visited 2007.

\bibitem {LM}
http://www.langmaker.com/db/Sindarin. visited 2007

\end{thebibliography}

\end{document}