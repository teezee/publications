%!TEX root = ../paper.tex
% ============================================================================
\section{Obfuscating BitTorrent} %
\label{sec:obfuscate}
% ============================================================================
The BitTorrent protocol specification \cite{Cohen2008BitTorrent} defines the Peer Wire Protocol, which is used to establish connections and exchange messages. It requires the peers to do an initial BitTorrent handshake directly following the TCP handshake. This BitTorrent handshake (\figref{fig:bthandshake}) uses a fixed string signature at a fixed position that is easily detectable using simple string matching. Contemporary DPI systems like openDPI \footnote{\url{http://www.opendpi.org/}, retreived 12.01.12} or l7-filter \footnote{\url{http://l7-filter.sourceforge.net/}, retreived 12.01.12} simply compare the first 20 bytes to the pattern ``0x13BitTorrent protocol'' to identify BitTorrent traffic.

\begin{figure}[ht!]
	\centering
	\includegraphics[width=\linewidth]{\gfx bthandshake}
	\caption{\footnotesize{BitTorrent handshake message. \emph{Name Length} equals ``0x13'', \emph{Protocol Name} equals ``BitTorrent protocol''. The \emph{Reserved} field is used to signal extension support. \emph{Info Hash} is the sha1 hash of the torrent file's info hash value. \emph{Peer ID} is a client's random peer ID}}
	\label{fig:bthandshake}
\end{figure}

\emph{MSE} is now the de-facto standard for encrypted BitTorrent connections. However, \cite{John2010Breaking-techreport} has shown, that even \emph{MSE} can be detected due to characteristics of the key exchange and too little variance in padding implementations. While the lack of padding can easily be fixed, \emph{MSE} suffers from high complexity and impact on cpu usage as well as bandwidth.

Following \cite{John2010Breaking-techreport}, we analyze how BitTorrent obfuscation can be improved. We propose an obfuscation extension that consists of multiple independent techniques that address both the payload data as well as flow features. First we use the globally known \emph{Info Hash} and the target peer's \emph{Peer ID} to obfuscate the first 28 bytes of the handshake. Thus, the packet contents appears random to an observer, defeating DPI systems. Second, we introduce a new message type called \emph{Padding Message}, which allows injection of a random number of random bytes into BitTorrent conversations. This raises variance in packet lengths and payload values and thus increases the difficulty of statistical fingerprinting. We also discuss the applicability of random flushing and random changes of flow direction.

Note that the proposed techniques do only address peer-to-peer communication and not tracker traffic. As a result it does not protect against the so called Sandvine attack. Tracker-to-peer traffic obfuscation that addresses the Sandvine attack problem has been proposed in \cite{Harrison2008TrackerPeer}.
%
% ---------------------------------------------------------------------------
\subsection{Obfuscated Handshaking} \label{sec:sigfree}
% ---------------------------------------------------------------------------
%
Since the BitTorrent handshake is the most discriminating feature of the BitTorrent protocol, concealing the handshake messages itself will nullify the effect of most DPI systems. We propose an obfuscation technique, which is easy to implement and introduces very little overhead to cpu and bandwidth usage. 

In the obfuscated handshake message the fields \verb+Name Length+, \verb+Protocol Name+ and \verb+Reserved+ are obfuscated by using the sha1 hash value of the target peer's ``peer ID'' concatenated with the shared ``info hash''. Since sha1 produces a 20 byte hash value, we actually use two hashes to produce 28 bytes. \figref{fig:obfuscate} shows the procedure.

\begin{figure}[ht!]
	\centering
	\includegraphics[width=\linewidth]{\gfx obfuscate}
	\caption{\footnotesize{Obfuscation of the handshake message. The fields \emph{Name Length}, \emph{Protocol Name} and \emph{Reserved} are replaced with sha1 hash values of the \emph{Info Hash} concatenated with the remote peer ID. Concatenation is denoted by (||), XOR is denoted by (+)}}
	\label{fig:obfuscate}
\end{figure}

The target peer ID and the info hash are concatenated twice in altering sequence to be used as input for two sha1 hash operations. Note, that this scheme requires the peer not to support the compact peer list extension \cite{Harrison2008Tracker} which allows trackers to return a more compact peer list, excluding the peer ID. The target peer ID is used to prevent an observer from extracting all decoding information from the actual message. An observer would need to associate IPs and ports with peer IDs from tracker responses to derive the input for the hash function. The first hash value is used to disguise the \verb+Name Length+ and \verb+Protocol Name+ fields. Eight bytes of the second hash value are XOR'ed with the \verb+Reserved+ value to produce a new one. This introduces a higher variance in byte values to increase difficulty for statistical fingerprinting. Sha1 is used as hash function since it is already used in BitTorrent. The fields \verb+Info Hash+ and \verb+Peer ID+ remain unchanged.

Though the handshake itself appears to be random, it still shows unique features that can be exploited for identification. The handshake message is always 68 bytes long and both request and response have the same info hash value. That is, bytes 28 to 47 are equal in two consecutive packets with opposite directions. In addition, incoming handshake messages will have equal byte values in the first 48 bytes for each unique $\{$info hash, peer ID$\}$ pair. The fields \verb+Info Hash+ and \verb+Peer ID+ will have equal values in every handshake message (either as request or response) that is related to the same torrent file and sent from the same peer.
%
% ---------------------------------------------------------------------------
\subsection{Random Padding} \label{sec:padding}
% ---------------------------------------------------------------------------
%
\cite{John2010Breaking-techreport} suggests using random padding to conceal flow features. In case of \emph{MSE}, padding is also used during key exchange, although the implemented padding length is insufficient to provide enough variance in packet lengths \cite{John2010Breaking-techreport}. Introducing random padding to the handshake solves the length issue. We believe that the equal info hash is not really a problem, since an observer needs to keep state and payload data to do the actual comparison. The same applies for the $\{$info hash, peer ID$\}$ pairs. The observer needs to keep variance of the first 48 bytes of inbound handshake messages. These messages can appear on different ports and will differ in case the peer shares multiple files. There are possible solutions like concealing the info hash and including the source peer ID in the hashing process. To maintain compatibility with other clients, peers supporting the obfuscated handshake extension first try to connect using an obfuscated handshake,and if this fails, they fall back to standard handshake.

BitTorrent's specification also specifies messages according to the type-length-value (TLV) standard. Specifically, messages are encoded as \verb+<length>+ \verb+<type>+ \verb+<payload>+. The first integer denotes the length followed by one byte which denotes the type followed by the message payload. Nine messages are specified, some clients also implement a tenth message.

We suggest a new message type called padding with a randomly chosen length and random payload. It can be appended to conversations at all time, however, is especially encouraged during handshaking which makes an early identification using statistical fingerprints extremely difficult. The message is depicted in \figref{fig:padding}.

\begin{figure}[ht!]
	\centering
	\includegraphics[width=\linewidth]{\gfx padding}
	\caption{\footnotesize{Padding message. A random number of random bytes. Introduces randomness in both packet length as well as content value which makes statistical fingerprinting difficult}}
	\label{fig:padding}
\end{figure}

Whenever the peer decides to use padding, the message is randomly generated and can be appended to any data due to be sent. We suggest using up to 1400 bytes to provide randomness close to the MTU. One downside of the message scheme is that the first two bytes in the length field will always be zero, while message type will always equal $0xa$.
%
% ---------------------------------------------------------------------------
\subsection{Magic Peer ID} \label{sec:magic}
% ---------------------------------------------------------------------------
%
The downside of using obfuscation is additional overhead due to reconnects if the connection is severed. The probability of this happening can be vastly reduced by encoding obfuscation support in the chosen peer ID. Since the peer IDs are announced by the tracker, peers in the swarm can easily determine which fellow peers support obfuscation. To indicate obfuscation support, the peer ID is chosen such that its sha1 hash value shows a specific 2-byte value at a predefined position. A peer that wants to connect to another peer first hashes the target peer's peer ID and checks the hash value for the predefined 2-byte indicator. On a match, the target peer supports obfuscation with high probability. Following we will refer to such a peer ID as ``magic'' and to a peer that has a magic peer ID as ``magic peer''. The process requires continuous random generating and testing of peer IDs during startup. However, computation is cheap and should on average not take more than $2^{16}$ tries. Naturally there is a margin of error if a peer chooses a magic peer ID by accident without supporting the obfuscation extension. Note, that the ``magic'' peer ID is not a requirement, nor a guarantee for obfuscation support. It is a means to reduce the probability of reconnects.
%
% ---------------------------------------------------------------------------
\subsection{Random Flush and Packet Direction} \label{sec:flushdir}
% ---------------------------------------------------------------------------
%
\cite{John2010Breaking-techreport} proposes random flushing and changes in packet directions to further hide flow features. However, random flushing of TCP streams cannot easily be performed since it requires disabling Nagle's algorithm, a procedure that might not be possible on all systems. It would also impact overall TCP performance.

The BitTorrent specification states, that ``peer connections are symmetrical [...], and data can flow in either direction.'' \cite{Cohen2008BitTorrent}. Both the seeders and leechers can and do initiate communication. However, peers that open a connection always initiate key exchange and handshake. This could be exploited for identification.

There is a simple reason for this behavior. The connecting peer wants to share a specific file identified by the info hash. Since peers can share many files, the target peer cannot know for which info hash the connection is established prior to the handshake. Theoretically, either peer could initiate the handshake or key exchange. This would, however, require the peers to keep dictionaries of IP, peer ID and info hash pairs and also a method to guarantee that all files are equally shared.

Considering all the problems, flushing the TCP stream is impractical, and changing the direction of the key exchange and handshake requires significant effort.