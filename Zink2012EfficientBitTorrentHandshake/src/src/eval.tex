%!TEX root = ../paper.tex
% ============================================================================
\section{Evaluation} %
\label{sec:eval}
% ============================================================================
%
We implemented our obfuscation scheme in {\em ttorrent 1.0.4} \footnote{\url{http://turn.github.com/ttorrent/}, retreived 12.01.12}, a lightweight java BitTorrent library that is easily extensible. It is an all in one solution, providing an API for clients, trackers, and torrent files alike. We also implemented a reference application in c as baseline and for testing purposes. It only provides the absolutely necessary functions and has about 100 lines of code. The basic implementation into {\em ttorrent} required changes to less than 200 lines of code.

We used Planet-Lab\footnote{\url{http://www.planet-lab.org}, retreived 12.01.12} as a testbed and deployed our modified {\em ttorrent} on 70 nodes which all initially acted as leechers. The tracker and initial seeder ran on dedicated machines and tracked a single file. We captured all traffic on the initial seeder's interface using {\em tcpdump}\footnote{\url{http://www.tcpdump.org/}, retreived 12.01.12}, which mimics the capabilities and view of the network access provider. In addition, we also tested interaction with a variety of popular BitTorrent client software and opentracker\footnote{\url{http://erdgeist.org/arts/software/opentracker/},  retreived 12.01.12}, which is the mostly used tracker software world wide.

We then analyzed the captured traffic using OpenDPI\footnote{\url{http://www.opendpi.org/}, retreived 12.01.12} and SPID\footnote{\url{http://sourceforge.net/projects/spid/}, retreived 12.01.12}. OpenDPI is Ipoque's open source version of its commercial PACE engine and a state-of-the-art DPI representative, while SPID is a statistical protocol identification algorithm, which uses trainable protocol models that can dynamically be applied to identify l7 protocols. In both cases we did not apply any optimizations but used the provided demo applications with default settings. Only in case of OpenDPI did we implement per flow output, to verify and compare the results with SPID that outputs per flow results by default.
%
% ---------------------------------------------------------------------------
\subsection{Client Compatibility}
% ---------------------------------------------------------------------------
%
To test communication of magic peers with ordinary peers we introduced popular clients to the swarm, including Vuze, uTorrent, Transmission, rtorrent, and libtorrent-rasterbar. This set covers the vast majority of BitTorrent clients in the wild today and is a decent representation of actual swarms.

All clients could successfully participate in the obfuscated swarm. We also forced random magic clients to initiate an obfuscated conversation regardless of the target peer's peer ID to test the effect of fast reconnects. As expected, the obfuscated handshake was rejected but the subsequent plain handshake was accepted at all times. We did observe random attempts to perform an obfuscated handshake with Transmission clients. Apparently, the chosen peer ID had magic properties.

We also tested the behavior of a magic peer participating in an ordinary swarm. Again, the magic peer behaved as expected and could communicate, download, and seed in the ordinary swarm. We encountered no connection problems or noticeable delays.
%
% ---------------------------------------------------------------------------
\subsection{Magic Peer ID generation}
% ---------------------------------------------------------------------------
%
We generated $100k$ magic peer IDs to analyze the number of random tries needed to find a magic peer ID.

\begin{figure}[ht!]
	\centering
	\includegraphics[width=.9\linewidth]{\gfx peerid_tries}
	\caption{\footnotesize{Number of tries to generate the magic Peer ID}}
	\label{fig:pidtries}
\end{figure}

\figref{fig:pidtries} shows the number of tries needed to generate the magic Peer IDs. The maximum number of tries is pretty high $766,152$. In the worst cases, the delay introduced by searching for a magic peer ID might be enough to be recognizable and annoying to the user. However, the Peer ID is generated only during startup and then remains constant for the remainder of the session. As expected, on average the client needed roughly $2^{16}$ tries to find a suitable magic peer ID.
%
% ---------------------------------------------------------------------------
\subsection{Handshake message}
% ---------------------------------------------------------------------------
%
We extracted all handshake messages from the recorded traffic to analyze them for randomness and traits that could be exploited for identification.

\figref{fig:hsbytes} shows the standard deviation and mean of the handshake messages' byte values. It can clearly be seen that the standard deviation of bytes 28 to 47 (info hash) as well as bytes 68, 69 , 72 (message header) are all zero. In addition, standard deviation is also only medium for the first 28 bytes.

\begin{figure}[ht!]
	\centering
	\includegraphics[width=\linewidth]{\gfx hs_bytes}
	\caption{\footnotesize{SD and mean of handshake message bytes}}
	\label{fig:hsbytes}
\end{figure}

Since we only shared a single file, the info hash is equal in all of the recorded handshakes, which will not be the case for peers sharing more then one file. The number of different info hashes is, however, equal to the number of files shared.

The message header's length field is an integer. The actual length is lower or equal to 1,400 bytes. The first two bytes of the header (offsets 68 to 69) will therefore always be 0, while the type field (offset 72) will always be 0xa. While three bytes might not be enough to reliably identify the handshake message, they introduce a reoccurring pattern that, combined with other traits, can be exploited.

The low standard deviation in the first 28 bytes is again a result of sharing only one file. Inbound handshakes will always have the same obfuscated signature for equal info hashes. The first 48 bytes of incoming handshake messages will be equal for any source sharing the same file.

\remove{In summary, standard deviation of the first 48 bytes is directly connected to the info hashes shared. An observer can use this information for identification attempts. However, it requires significant effort, due to the amount of state that needs to be kept.}
%
% ---------------------------------------------------------------------------
\subsection{Identification}
% ---------------------------------------------------------------------------
%

The results of openDPI and SPID identification are shown in \tblref{tbl:ident}. Both, DPI as well as SPI, were not able to detect and identify the obfuscated BitTorrent traffic. OpenDPI identifies 29 flows as BitTorrent. These are all seeder to tracker announces, which OpenDPI recognizes by searching for the string \verb+GET + and then parsing the whole tracker request message. These flows are equivalent to the 29 HTTP flows detected by SPID. Tracker requests look similar to HTTP get requests and SPID was not able to distinguish between these two. In fact, in most cases SPID reported higher similarities of the detected flows to eDonkey and ISAKMP than to BitTorrent. 

\begin{table}[ht!]
	\centering
\begin{tabular}{ l | c | c | r }
	\hline \hline
	{\bf Algorithm} & {\bf total flows} & \multicolumn{2}{ c }{\bf Identified} \\ 
	& & {\bf protocol} & {\bf number flows} \\
	\hline
	\multirow{2}{*}{OpenDPI} & \multirow{2}{*}{403} & unknown & 374 \\
	 & & BitTorrent & 29 \\
	\hline
	\multirow{2}{*}{SPID} & \multirow{2}{*}{389} & unknown & 360 \\
	 & & HTTP & 29 \\
	\hline \hline
\end{tabular}
	\label{tbl:ident}
	\caption{\footnotesize{Identification results for OpenDPI and SPID}}
\end{table}

This shows, that techniques using payload information and even flow features, can easily be fooled by disguising the traffic with properties similar to other well known protocols like HTTP or ISAKMP.
%\newpage