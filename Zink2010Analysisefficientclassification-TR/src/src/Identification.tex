%!TEX root = ../p2p_techreport_2010.tex
%=============================================================================
% 						Efficient Identification
%-----------------------------------------------------------------------------
%
%=============================================================================
\section{Efficient P2P Classification} % (fold)
\label{sec:identification}
%=============================================================================
%
Contemporary traffic classification systems rely on complex deep packet (DPI) inspection or statistical\slash behavior analysis (BA) and require vast amounts of resources. These systems are usually highly integrated and feature a multi-layered approach that combines different methods.

We present a simple identification algorithm, which is targeted on resource restricted environments like physical interface cards with few amounts of memory and little processing power. The algorithm is not intended to replace complex traffic classification systems but to provide fast and efficient pre-classification to single out interesting flows and thus reduce the pressure in later identification stages.

The algorithm exploits the observation that P2P applications usually use both TCP and UDP in parallel for signal and transfer traffic. Depending on the point of identification in the network, either core or edge, the view of network traffic between hosts is different.
%
%-----------------------------------------------------------------------------
\subsection{Classify large flows as P2P on Edge} % (fold)
\label{sub:largeflows}
%-----------------------------------------------------------------------------
%
Since an edge router connects the provider's network (PN) to foreign networks (FN) we must distinguish between incoming and outgoing traffic. The edge router can detect all connections from clients in the PN but has very limited information about connections from clients in FNs. 

\paragraph{Outgoing connections} % (fold)
\label{par:outgoing_connections}
Nodes participating in P2P networks that are attached to the PN produce signal traffic to multiple nodes in one or more FNs. File transfers are initialized from the PN and produce massive amounts of incoming TCP traffic. Thus the edge router has a complete view of all signaling and transfer traffic originating from a PN node. Figure \ref{fig:edge_out} depicts P2P traffic from a sample PN node.\\
%
\begin{figure}[h!t]
	\centering
	\includegraphics[scale=0.5]{./gfx/edge_out}
	\caption{\footnotesize{Outgoing P2P traffic.}}
	\label{fig:edge_out}
\end{figure}
%
In this scenario signal traffic has both a fixed source IP and port while the destination IPs and ports appear random. The TCP transfer traffic has a random source port but a fixed destination port for each distinct destination IP. For large incoming TCP flows which have been initialized from the PN there are many UDP signal packets from the destination PN node to FN nodes where a fraction must have destination IP and port equal to the TCP flows source IP and port. In addition transfer flows are only acknowledged with TCP ACK packets without payload having a constant size. This allows to differentiate between HTTP and P2P traffic. Table \ref{tab:edge_out} summarizes the properties.\\
%
\begin{table}[h!]
\begin{center}
\begin{smalltab}{*{3}{|l}|}
	\hline \hline
properties & signal traffic & transfer traffic \\
	\hline \hline
outgoing src port & fix & rnd \\
outgoing dst port & fix & fix \\
	\hline
incoming src port & fix & fix \\
incoming dst port & fix & rnd \\ 
	\hline \hline
\end{smalltab}
	\caption{\footnotesize{Properties of P2P traffic originating from PN nodes.}}
	\label{tab:edge_out}
\end{center}
\end{table}
%
The following algorithm is a simple scheme for identification.\\
%
\begin{verbatim}
	if in.TCP >> out.TCP and out.TCP.ACK:
	if exist out.UDP: 
	if out.UDP.dstport == out.TCP.dstport:
	tag flow as possible P2P
\end{verbatim}
%
% paragraph outgoing_connections (end)
\paragraph{Incoming Connections} % (fold)
\label{par:incoming_connections}
For incoming P2P traffic the same rules as for outgoing traffic apply. However, the rate of signal and transfer packets is much lower thus thresholds must be chosen appropriately. Figure \ref{fig:edge_in} shows P2P traffic of FN nodes.\\
%
\begin{figure}[h!t]
	\centering
	\includegraphics[scale=0.5]{./gfx/edge_in}
	\caption{\footnotesize{Incoming P2P traffic.}}
	\label{fig:edge_in}
\end{figure}
%
For TCP transfer traffic that flows out of the PN there still must be signal packets that have been sent by the downloading FN node but the source port is different. Usually there are outgoing signal packets to the FN node from the uploading host where the outgoing source port equals the incoming destination port. But these messages might have a rather long time gap. So it is advisable to check for both incoming and outgoing UDP packet for participating nodes. Table \ref{tab:edge_in} shows the flow properties.\\
%
\begin{table}[h!]
\begin{center}
\begin{smalltab}{*{3}{|l}|}
	\hline \hline
properties & signal traffic & transfer traffic \\
	\hline \hline
outgoing src port & fix & fix \\
outgoing dst port & fix & rnd \\
	\hline
incoming src port & fix & rnd \\
incoming dst port & fix & fix \\ 
	\hline \hline
\end{smalltab}
	\caption{\footnotesize{Properties of P2P traffic originating from FN nodes.}}
	\label{tab:edge_in}
\end{center}
\end{table}
%
The following algorithm shows a simple scheme for identification similar to that of outgoing connections.\\
%
\begin{verbatim}
	if out.TCP >> in.TCP and in.TCP.ACK:
	if exist out.UDP or in.UDP:
	if in.TCP.dstport == (out.UDP.srcport or in.UDP.dstport):
	tag flow as possible P2P
\end{verbatim}
%
One weakness of this scheme is that it relies on equal TCP and UDP port numbers. This is not the case for all P2P protocols. The ed2k clients usually define different port numbers for TCP and UDP traffic and thus can not be detected. So this is an easy exploit for P2P designers to prevent identification. 
%
%-----------------------------------------------------------------------------
\subsection{Identification Algorithm} % (fold)
\label{sub:algorithm}
%-----------------------------------------------------------------------------
%
The limitations in logic and memory on the target platform do not allow complex computations or data structures. A simple algorithm based on the assumptions presented in previous sections can be easily implemented in hardware and requires little amount of memory. The idea is to use a combination of simple behavior analysis and payload matching. Figure \ref{fig:flowchart} shows a flow chart of the algorithm.
%
\begin{figure}[ht!]
	\centering
	\includegraphics[scale=0.7]{./gfx/flowchart}
	\caption{\footnotesize{Identification Algorithm flow chart.}}
	\label{fig:flowchart}
\end{figure}
%

The algorithm is based on the assumption that P2P applications use both TCP and UDP on the same port to connect to or receive connections from different machines. For every \{Address, Port\} pair that engages in a UDP conversation a record of the form \{ts, certain\} is kept in a dedicated identification table. The timestamp '`ts`' is used to match the entry against a timeout to check if it is recent enough and can be considered for the currently observed packet. It is also needed for aging and removing old entries from the identification table. The field '`certain`' is a boolean value that indicates whether it is certain that the host is engaged in a P2P conversation on this specific port or not. Every host that has UDP traffic is added to the identification table with the packet's timestamp (or an abbreviation of it) and certain set to 0. These entries are marked as '`possible`' P2P candidates. Algorithm \ref{algo:algorithm} explains the algorithm in more detail.
%
\begin{algorithm}[ht!]
	%\SetLine
	\KwData{received packet $P$, flow \Flow}
	\KwResult{flow type}
	\lIf{$\identified(\Flow)$} {\Return{}};\\
	$\Pay = \GetPay(P)$;\\
	\If{\Pay \In \Sig} {
		$\Update(\Src,\Dst,\TS,1)$;\\
		$\SetType(\Flow,\PTP)$;\\
		\Return{};\\
	}
	$\TS = \GetTS(P)$;\\
	$\IPP = \Proto(P)$;\\
	\If{$\IPP == \UDP$} {
		\If{$\Certain(\Src,\TS)\ \Or \ \Certain(\Dst,\TS)$} {
			$\Update(\Src,\Dst,\TS,1)$;\\
			$\SetType(\Flow,\PTP)$;\\
			\Return{};\\
		}
		$\Update(\Src,\Dst,\TS,0)$;\\
		$\SetType(\Flow,\POSS)$;\\
		\Return{};\\
	}
	\If{$\IPP == \TCP\ \And\ (\Recent(\Src,\TS)\ \Or \ \Recent(\Dst,\TS))$} {
		$\Update(\Src,\Dst,\TS,1)$;\\
		$\SetType(\Flow,\PTP)$;\\
		\Return{};\\
	}
	\Return{} \;
	\caption{\footnotesize{Flow identificaion Algorithm}}
	\label{algo:algorithm}
\end{algorithm} 
%

The signatures for pattern matching are up to four bytes short and few in number. Only a small set (on average 3) of signatures are needed for a set of P2P protocols. They can easily be stored in registers and matched in parallel to avoid stressing the memory and reduce timing. Note that signature matching works on each packet and can run independently from and parallel to the flow table updates. The identification algorithm itself then runs on not yet identified flows and uses information from the flow table (specifically the type).

Whether an entry is supposed to be recent depends on the state of the certain bit. An entry that is certainly identified as a P2P related entry (certain=1) will have a longer timeout. This is based on the observation that hosts engaging in P2P conversations are usually connected longer to the network and especially to down\slash upload partners. Thus, two different timeouts must be kept, one short timeout for entries that are not certain and one long timeout for entries that are certain. Listing \ref{lst:recent} shows a possible pseudo c implementation of the timeouts and recent function.
%
\begin{figure*}[ht!]
\begin{lstlisting}[caption=Timeouts and recent function, captionpos=b, label=lst:recent]
	#define SHORT 5 // 5 secs short timeout (certain=0)
	#define LONG (5*60) // 5 mins long timeout (certain=1)
	bool recent (age, certain) {
		time_t delta = certain ? LONG : SHORT;
		return ((time()-age) <= delta);
	}
\end{lstlisting}
\end{figure*}
%

It is crucial to find suitable values for the timeouts to prevent tainting innocent hosts. Machines may just have connected to the network and received the IP of a previously identified host. Thus a timeout that is set too high will falsely classify these hosts as P2P hosts. On the other hand, a timeout that is set too low will not find related P2P connections.
%
%-----------------------------------------------------------------------------
\subsection{Pattern matching} % (fold)
\label{sub:pattern_match}
%-----------------------------------------------------------------------------
%
For pattern matching only the first four bytes of the packet payload are needed. These four bytes are compared to a small set of well known P2P strings using exact matches. The signatures to compare to are shown in listing \ref{lst:signatures}.

%
\begin{figure*}[ht!]
\begin{lstlisting}[caption=Signatures for pattern matching, captionpos=b, label=lst:signatures]
	const char *signatures[] = {
		"\x13""Bit","d1:a","d1:r","d1:e",
		"GNUT","GIV ","GND ","GO!!","MD5 ",
		"\x27\x00\x00\x00","\xe3\x19\x01\x00","\xc5\x3f\x01\x00"
	};
\end{lstlisting}
\end{figure*}

Since exact matches are relatively cheap all patterns can be stored in 32 bit registers which can be compared to the 4 byte packet content in parallel. The logic or of the individual result indicates whether there was a match or not.
%
%-----------------------------------------------------------------------------
\subsection{Data Structures} % (fold)
\label{sub:data_structures}
%-----------------------------------------------------------------------------
%
The identification algorithm only needs a relatively small table that holds records for every host that engages in a UDP conversation or which is part of a flow otherwise identified as P2P (e.g. by DPI or signature matches). The table is a dictionary style structure indexed by a key and storing an associated value.

The key is a pair of \{Address, Port\}. The hash of the key is used to index the table. Listing \ref{lst:key} shows the key definition.

%
\begin{figure*}[ht!]
\begin{lstlisting}[caption=Key type definition, captionpos=b, label=lst:key]
	typedef struct key_s {
		uint32_t addr;
		uint16_t port;
	} key_t;
\end{lstlisting}
\end{figure*}
%

The stored values can be reduced to the pair \{timestamp, certain flag\} and can both be encoded in a single value with a few bits. Since entries are kept only a short amount of time and are regularly removed during aging, there is no need to keep exact timestamps. Instead, only the time offset of the entry in milliseconds relative to the timestamp of the last aging cycle is stored. In addition, a small counter value is needed that keeps track of the number of aging cycles that the value survived.

%
\begin{figure*} [th!]
\begin{lstlisting}[caption=Type definitions of the table's entries, captionpos=b, label=lst:value]
	#define d_age 30000 // 30 secs aging
	#define d_short 5000 // 5 secs short timeout
	#define d_long 5 * 60 * 1000 // 5 mins long timeout

	typedef struct value_s {
		uint8_t age : 5;
		uint8_t cycles : 4;
		uint8_t certain : 1; 
	} value_t;
\end{lstlisting}
\end{figure*}
%

Let $o$ the offset in milliseconds, $c$ the number of cycles, $d_{age}$ the aging interval in millisecond. Then the real age of an entry in milliseconds can be derived using the following equation. 
%
$$ {\tt realage} = o + c * d_{age}$$
%
When an entry is created at timestamp $T_{ts}$ and the last aging occurred at $T_{age}$, then
%
$$o = \lceil (T_{ts} - T_{age}) \cdot 10^{3} \rceil$$
%
On each aging cycle, the counter of the entry is incremented by 1 if the entry remains in the table. This way, the age of an entry can efficiently be encoded without the need to keep 64 bit timestamp values. The total number of bits needed for an entry $e$ is
%
$$|e| = |o| + |c| + 1 $$
%
where $|x|$ is the length of $x$ in bits. The number of bits needed for the offset $o$ and counter $c$ fields are derived as follows
%
$$|o| = \lceil \log{d_{age}} \rceil$$
$$|c| = \lceil \log{\frac{d_{long}}{d_{age}}} \rceil$$
%
where $d_{long}$ is the long timeout in milliseconds.

Listing \ref{lst:value} shows an example type definition of the value type using an aging interval of 30 seconds, a short timeout of 5 seconds and a long timeout of 5 minutes. The resulting table entry only needs 10 bits space. Thus, in a typical 32 bit wide memory three entries could be stored per memory word. So even with several millions of entries the table is only a few MiB in size.

Storing only the age and certain bit can lead to false positives if the hash function produces collisions for different keys. There are multiple ways of how to define a suitable hash function or even a set of hash functions and use multiple hashes to significantly reduce the probability of hash collisions and false positives. An overview is given in \cite{Zink2009Packet}.