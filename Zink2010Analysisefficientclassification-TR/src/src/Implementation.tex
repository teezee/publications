%!TEX root = ../p2p_techreport_2010.tex
%=============================================================================
% 						Software Implementation
%-----------------------------------------------------------------------------
\section{Software Simulator} % (fold)
\label{sec:software}
%-----------------------------------------------------------------------------
A prototype of the identification algorithm has been implemented in software to simulate and verify behavior and results of the developed scheme. The software simulation addresses the following issues.
%
\begin{itemize}
	\item simulate identification behavior
	\begin{itemize}
		\item using live data
		\item using captured data
	\end{itemize}
	\item verify identification results
	\begin{itemize}
		\item computer aided
		\item automatically
	\end{itemize}
\end{itemize}
%

The simulator is written in c\slash c++ and uses libpcap\footnote{http://www.tcpdump.org} to process packet data either by live capture or using a pcap file. The simulator can also process verification files used to verify the identification results.
%-----------------------------------------------------------------------------
\subsection{Classes and Data structures} %(fold)
\label{sub:classes}
%-----------------------------------------------------------------------------
%
Figure \ref{fig:classes} shows the class diagram of the classes used by the picDFI simulator. It is pretty straight forward.
%
\begin{figure}[h!t]
	\centering
	\includegraphics[scale=0.75]{./gfx/classes}
	\caption{\footnotesize{Class diagram.}}
	\label{fig:classes}
\end{figure}
%

\begin{itemize}
	\item {\tt pic\_service}: A single service or host, defined as the pair {\tt addr, port}. Used both for connections as well as key to the identification table.
	\item {\tt pic\_connection}: A pair of two connected services and the connection protocol. It serves as the key to the flow table.
	\item {\tt pic\_flow}: The actual flow record. Keeps track of the times of creation and last touched as well as the number of packets, bytes, some options (which are unused) and the flow\_type, which is initialized to {\tt UNDEF}. The value of the flow table. The pair \{{\tt pic\_connection}, {\tt pic\_flow}\} are defined as the entries for the flow table.
	\item {\tt pic\_ident}: Values for the identification table. The pair \{{\tt pic\_service}, {\tt pic\_ident}\} are entries to the identification table.
	\item {\tt pic\_stat}: Used to keep and compute statistical information.
\end{itemize}
%

The simulator uses a number of tables (c++ {\tt map}s) to store certain information and associations.

\paragraph{Flow Table.}
Keeps track of all flows. \{key, value\} pair is \{{\tt pic\_connection}, {\tt pic\_flow}\}. For each arriving IP packet the corresponding flow is retrieved from the table and updated, or created if not available. The flow ages at certain time intervals, that is, all flows that are older than the last aging cycle are removed from the table. The default is 30 seconds, but the value can be defined by the user. Note that there is only one global aging timeout that also applies to the identification table.

\paragraph{Identification Table.}
The table used to keep the identification records needed to identify a flow. \{key, value\} pair is \{{\tt pic\_service}, {\tt pic\_ident}\}. The identification table also ages at the same time as the flow table.

\paragraph{Verification Table.}
Only used if a verification file is provided. Then it is the first table that is built. The verification table is practically a static second flow table. It maps connections to flow records. However, there are no updates or aging to the table during runtime. The flow type of the flow records in the verification table are considered to be the accurate or real flow type. Thus every flow that is identified is compared to the corresponding flow in the verification table. Thus it can be determined if the flow has bee identified correctly or not. 
%
\paragraph{Statistics Table.}
Used to keep and compute statistics of flow types and their corresponding number of flows, packets and bytes. 
%
%-----------------------------------------------------------------------------
\subsection{Simulation Flow} %(fold)
\label{sub:simulation}
%-----------------------------------------------------------------------------
%
The simulator uses {\tt libpcap} to capture packets from the physical interfaces and to read\slash write pcap capture files. It analyses the packet headers and takes appropriate actions depending on the layer protocols. These actions are performed by so called {\tt handler}s that operate on different layers and stages. The following steps are performed.
%
\begin{enumerate}
	\item Parse command line options.
	\item If a verification file is provided, parse the file and build verification table.
	\item Initialize tables.
	\item Get next packet from interface or pcap file.
	\item If number of passed seconds is greater than aging timeout age the flow table then the identification table.
	\item Update the flow table with packet information.
	\item If needed run the identification algorithm.
	\item Goto 4.
\end{enumerate}
%

Every time a flow is removed from the flow table, either through aging or some other means, it triggers certain actions defined by \verb+handlers+. The statistics table is updated using the flow's informations. If a verification table is available the flow is verified. The program runs as long as there are still packets in the file, or in case of capturing directly from the interface, as long as no interrupt signal is received. Then it terminates the packet capturing and analyzation, flushes the flow table and prints the final results on \verb+stdout+.
%
%-----------------------------------------------------------------------------
\subsection{Verification} %(fold)
\label{sub:verification}
%-----------------------------------------------------------------------------
%
The problem with verification is, that it can hardly be done automatically. To verify the correctness of the identification algorithm there must be a way of reliably know the real type of every flow. But the lack of existence of a reliable identification scheme, which is able to accurately identify every flow prevents automatic verification. The only way to verify the identification algorithm is using a controlled environment where global knowledge about every communication is absolute. This is possible in a lab environment but impractical on real world data. Thus, the only way of 'verification' is actually to compare the results to those of other identification mechanisms. An example is 'OpenDPI' from Ipoque. It is an open source DPI engine that can do protocol identification based on packet payloads. However, since it is prone to error in presence of encrypted and obfuscated protocols. As such 'OpenDPI' is not a reliable source of verification. It can be shown that it falsely classifies a large number of flows under certain circumstances. In fact, the picDFI algorithms has a better True Positive rate than OpenDPI.

Verification is done using a 'verification file'. It records plain text comma separated serializations of flow records and has the following fields:

\verb+la,lp,ua,up,protocol,created_ts,touched_ts,npackets,nbytes,type\n+\\
%
Flow records are written in plain text and terminated with a newline. The implemented verification process actually only uses the flow ID and type for verification and neglects time stamp information. As a result, flows that have the same ID but appear over different time spans and also have different times are overlooked. More specifically, only the latest flow (as appears in the file, not by time) is considered. However, the file format is design to support also time based verifications.

Verification computes the following statistics.
\begin{itemize}
	\item \verb+na_positive+, could not verify, identified p2p
	\item \verb+na_negative+, could not verify, identified nonp2p
	\item \verb+true_positive+, identified and verified p2p
	\item \verb+false_positive+, identified p2p, verified nonp2p
	\item \verb+true_negative+, identified and verified nonp2p
	\item \verb+false_negative+, identified nonp2p, verified p2p
\end{itemize}
%
%-----------------------------------------------------------------------------
\subsection{Evaluation} %(fold)
\label{sub:evaluation}
%-----------------------------------------------------------------------------
%
Preliminary simulations show solid identification behavior of over 90 \% accuracy and better quality than 'OpenDPI' especially in the presence of encryption. However, this is based on small captured data within a controlled environment. Though this is a good means of reliably verify the identification quality it is not representable for large-scale router traffic. Further tests and evaluation are performed in a real corporate setting after final adjustments and are subject to future work.