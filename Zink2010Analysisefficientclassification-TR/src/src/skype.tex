%!TEX root = ../p2p_techreport_2010.tex
%=============================================================================
% 									SKYPE
%-----------------------------------------------------------------------------
\subsection{Skype} % (fold)
\label{sub:skype}
\emph{Skype} has been developed in 2003 by the same people that also invented \emph{KaZaA}. Thus, it shares several features with P2P protocols, especially the overlay network. However, it is used mostly in a one-to-one or one-to-few contact scenario, typically between people knowing each other, and rarely for filesharing. \emph{Skype} traffic is heavily encrypted and obfuscated. The binary as well as network traffic have been reverse engineered in \cite{Biondi2006Silver}, \cite{Desclaux2006Vanilla}. There have been previous attempts to identify \emph{Skype} traffic which will be introduced in the following sections.
%-----------------------------------------------------------------------------
\subsubsection{Overlay Network.} % (fold)
\label{sub:skypeoverlay}
The \emph{Skype} overlay network is relatively similar to the \emph{KaZaA} network. It consists of super-nodes which can be used to relay traffic behind NATs and firewalls. Any node can become a super-node based on availability, bandwidth and similar criteria. \cite{Biondi2006Silver} states that there are $\approx 200$ super-nodes hardcoded into the \emph{Skype} binary that change on every release. It is currently unclear, whether the super-nodes are also used as a distributed dictionary of nodes and skype IDs, or if a centralized approach is taken.
%-----------------------------------------------------------------------------
\subsubsection{Encryption and Obfuscation}
\label{sub:skypeobf}
\emph{Skype} uses multistage encryption based on hashing, AES, RSA and RC4 to encrypt it's messages. It uses both TCP and UDP as transport layer protocol. In case of TCP the whole message is encrypted. In case of connectionless UDP, the message cannot be fully encrypted but only obfuscated, that is, the user agent must extract header information to encrypt\slash decrypt messages. In addition, the UDP port must be fixed. 

\paragraph{Skype UDP.} % (fold)
\label{par:skype_udp}
Skype UDP packets are encrypted with RC4. The key is calculated using elements from the datagram, including src\slash dst IP, a Skype packetID and parts of the actual payload. The following fields can be identified in a Skype UDP message.
%
\begin{enumerate}
	\item [\texttt{ID}] 16-bit random identifier.
	\item [\texttt{FUN}] 3 random bits followed by a 5 bit function (payload) type. Five different types have been identified: \{\str{0x02},\str{0x03},\str{0x07},\str{0x0f},\str{0x0d}\}.
\end{enumerate}
%

\paragraph{Skype TCP.} % (fold)
\label{par:skype_tcp}
The whole TCP stream is encrypted using RC4. The seed is sent in the first 4 bytes. In addition the data can be fragmented and sent over multiple packets. Data is packed using arithmetic compression very similar to huffman encoding prior to encryption.
%
%-----------------------------------------------------------------------------
\subsubsection{Traffic.}
\label{sub:skypetraffic}
\cite{Bonfiglio2007Revealing} identifies three types of traffic caused by \emph{Skype} which can be categorized into E2E (end-to-end or peer-to-peer) and E2O (skypeout messages over PSTN gateways). Depending on the type of traffic packets show different characteristics which provide first clues for identification.
%
\begin{description}
	\item[E2E over UDP]  The five \texttt{FUN} bits are deterministic. Other bits appear completely random.
	\item[E2O over UDP] The first four bytes are deterministic and represent a \emph{Connection Identifier} (\texttt{CID}). The \texttt{CID} is likely to change during connection setup but is stable afterwards. All other bytes are cyphered and random. The port is fixed to 12340. 
	\item[E2E\slash E2O over TCP] The whole message is cyphered and completely random.
\end{description}
%
In addition the following observations are worth mentioning.
%
\begin{itemize}
	\item Packet size depends on the codec used and is stable throughout the conversation.
	\item Transmission rate must be fast and constant to maintain a good quality of service.
\end{itemize}
%
In essence, this means that \emph{Skype} traffic consists of rapid, equal-sized packets, transported via UDP\slash TCP. With the exception of a few bits depending on the type the packets appear to be completely random.

\subsubsection{Identification}
\label{sub:skypeident}
There have been previous attempts to identify \emph{Skype} traffic. A rudimentary guideline is given in \cite{Biondi2006Silver},\cite{Desclaux2006Vanilla}, who try to detect \emph{Skype} by the first UDP NAck packet, which can be identified by length and the \texttt{FUN} bits. Further effort is done in \cite{Bonfiglio2007Revealing}. The authors propose three different \emph{Skype} classifiers, based on methods prominent in data mining applications that exploit statistical information and probabilities. They are usually quite expensive and cannot easily be done in hardware. Another approach that tries to solve complexity and bandwidth problems is proposed in \cite{Schmucki2008Unkooperative}. Here, a signature based on deltas between packets of a flow is used for identification.

\paragraph{Chi-Square Classification.} % (fold)
\label{par:chisquare}
\hfill \\
The chi-square classifier of \cite{Bonfiglio2007Revealing} tests whether the inspected flow behaves like a \emph{Skype} flow. To determine the chi-square error, groups of bits of the packet are examined over time and compared to the expected behavior stated in section \ref{sub:skypetraffic}. Random bits are expected to follow a specific distribution and the error can be relatively easy computed. 

\paragraph{Na\"ive Bayes Classification.} % (fold)
\label{par:naive_bayes_classification}
\hfill \\
Bayes classification is a standard information mining application and also prominent in statistical analysis. It examines certain features and based on the values of the features computes a probability that the examined object belongs to a specific class. In this case the authors are interested in message size and transmission rate of the packets. Based on the codec used the packets have a distinct size. The rate may not drop below a certain threshold. Using these features the probability that a flow is either \emph{Skype} or not can be computed.

\paragraph{Payload Based Classification.} % (fold)
\label{par:payload_based_classification_}
\hfill \\
\emph{Skype} makes payload based classification difficult due to encryption and obfuscation. However, there is some information both in the payload as well as in the headers that can be used for identifying \emph{Skype} flows. The Classifier uses payload information introduced in section \ref{sub:skypetraffic} to identify E2E and E2O packets. Then timing information is taken into account to classify the flows.

\paragraph{Uncooperative Identification.} % (fold)
\label{par:uncooperative_identification}
\hfill \\
In \cite{Schmucki2008Unkooperative}, the author bases his scheme on the following observations.
%
\begin{itemize}
	\item \emph{Skype} flows have a constant rate (isochronicity).
	\item \emph{Skype} flows have a constant packet size.
	\item Packets in \emph{Skype} flows appear random, with the exception of certain bits.
\end{itemize}
% 
The identification of \emph{Skype} traffic works in multiple stages.
%
\begin{itemize}
	\item Flows are defined only by the tuple \{dst IP, dst port\}. Since the Source IP\slash port can be easily forged and NAT and proxy networks obfuscate the true source they are not taken into account when identifying flows.
	\item Incoming packets that are bigger in size than a certain threshold are filtered out.
	\item Remaining packets are hashed into a table of flows and each flow is associated with a timer and a counter.
	\item Flows in which the number of packets in a given interval exceed and under-run specific thresholds over a period of time (30-100 packets per second), are removed from consideration.
	\item Remaining flows are matched against a pre-computed signature. The signature is based on changes of 4 bit wide blocks of the packet payload within the flow. Each 4 bit block is seen as an integer value. The delta between packets is derived by simple subtraction. Then the mean and standard derivation of the deltas is computed and used as a signature. As shown in the thesis \cite{Schmucki2008Unkooperative}, different protocols show a specific signature -- including Skype -- which can be used to distinguish them from other protocols.
\end{itemize}
%
The methodology works on high speed due to early filtering of flows and uses multiple tricks to reduce memory requirements (multiple hashing, counting bloom filters, aggregated timestamps). Furthermore it is easy to implement in hardware. The downside is, that it is not well tested and only applied to actual \emph{Skype} voice conversation. However, the use of filtering and flow signatures is intriguing and a good starting point for detecting encrypted flows like \emph{Skype} or maybe other encrypted P2P protocols.
% subsection skype (end)
