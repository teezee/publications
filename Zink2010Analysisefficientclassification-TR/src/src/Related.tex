%!TEX root = ../p2p_techreport_2010.tex
%=============================================================================
% 						Related Work: Peer-to-Peer Traffic Identification
%-----------------------------------------------------------------------------
\section{Related Work} % (fold)
\label{sec:related_work}
Multiple different approaches for P2P traffic identification have been suggested based on signatures, patterns, payload or behavior. This section presents a summary of different identification mechanisms and exercises their applicability and dis-\slash advantages.
%
%-----------------------------------------------------------------------------
\subsection{Transport layer identification} % (fold)
\label{sub:transport_layer_identification}
%-----------------------------------------------------------------------------
%
Previous work tries to identify P2P traffic \cite{Karagiannis2004Transport} and super nodes \cite{DJOneilTransport} on the transport layer using flow identification techniques based on heuristics using the 5-tuple \{source IP, destination IP, source port, destination port, protocol\}. Traffic identification works as follows.
%
\begin{enumerate}
	\item Source\slash destination IP pairs that use both TCP and UDP are flagged as P2P candidates. Only few other applications like DNS, IRC, online gaming\ldots{} have similar behavior and are eliminated if identified using a lookup table of port numbers.
	\item \{source IP, source port\} and \{destination IP, destination port\} pairs are examined. Pairs for which the number of distinct IPs and distinct ports are equal are considered P2P.
	\item To decrease the risk of false-positives the identified P2P flows are matched against the behavior of known applications like eMail, DNS, HTTP, malware and online gaming. 
\end{enumerate}
%
The authors prove that P2P traffic can be identified with $95\%$ accuracy using their heuristics.\\

\paragraph{Advantages and Limitations.} % (fold)
\label{par:tli_advantages_and_limitations_}
\begin{description}
	\item[+] Protocol independent. Works on proprietary and encrypted protocols.
	\item[+] Non-intrusive. Requires little changes to flow identification schemes and does not strongly affect flows.
	\item[-] No P2P application identification.
	\item[-] Must be matched against the behavior of other well known applications to reduce false-positives.
\end{description}

To identify super nodes the previously identified P2P traffic is filtered using the following conditions.
\begin{enumerate}
	\item Well-known super node ports. Super nodes usually use reserved well known port numbers.
	\item Forwarding queries from peers. If egress timestamp $-$ ingress timestamp $<$ expected maximum forwarding time and the payload size of the egress packet $=$ payload size of the ingress packet, it is considered to be a forward query.
	\item Once a super node is identified, all nodes the query is forwarded to are marked as super nodes.
\end{enumerate}
The authors conclude that the algorithm could not be verified because they were not able to provide a super node themselves so this approach to identify super nodes is to be considered experimental.
%
%-----------------------------------------------------------------------------
\subsection{Application Signature Identification} % (fold)
\label{sub:application_signature}
%-----------------------------------------------------------------------------
%
\cite{Sen2004Accurate} presents an application signature based identification scheme to identify P2P download traffic. The authors only concentrate on TCP transfer traffic of well known P2P protocols. They use fixed-offset string matching to identify P2P TCP segments. The following signatures are used for identification (we omit 'dead' protocols for simplicity).
%
\begin{description}
	\item[Gnutella] \hfill \\
		\begin{itemize}
			\item First string following TCP\slash IP headers is \str{GNUTELLA}, \str{GET}, \str{HTTP}
			\item If first string is \str{GET} or \str{HTTP} there must follow a field with either \str{User-Agent:}, \str{UserAgent:} or \str{Server:}.
		\end{itemize} 
	\item[ed2k] \hfill \\
		\begin{itemize}
			\item First byte after TCP\slash IP headers is the ed2k marker \str{0xe3}.
			\item next four bytes as integer is equal to packet size - size of headers + 5.
		\end{itemize}
	\item[BitTorrent] \hfill \\
		\begin{itemize}
			\item First 20 bytes in TCP payload equal \str{0x13BitTorrent Protocol}.
		\end{itemize}
\end{description}
%
Only the first packets at the beginning of the transfer phase need to be analyzed. Since fixed-offset string matching is considerable cheap the overall cost is reasonably small. The authors show that more than 99\% of P2P traffic could be identified with their method.

\paragraph{Advantages and Limitations.} % (fold)
\label{par:app_adv_limit}
\begin{description}
	\item[+] Reasonably small cost.
	\item[-] Does not work on unknown, proprietary or encrypted protocols, unless the encryption sequences are captured.
\end{description}
%
%-----------------------------------------------------------------------------
\subsection{Match Characteristic Strings}
\label{sub:characteristic_strings}
%-----------------------------------------------------------------------------
%
Similar to \cite{Sen2004Accurate} Karagiannis \textit{et al.} propose a string based characterization of P2P traffic identification \cite{Karagiannis2004Is},\cite{Karagiannis2003File-sharing}. The authors analyze both TCP and UDP traffic and exercise multiple stages of identification. The following table summarizes the string against the packet payload is matched (again 'dead' protocols are omitted, for a complete overview see \cite{Karagiannis2003File-sharing}).

%
\begin{table}[h!]
\begin{center}
\begin{smalltab}{*{3}{|c|}}
	\hline \hline
P2P protocol & Strings & Transport Protocol \\
	\hline \hline
ed2k & \str{oxe3},\str{oxc5} & TCP\slash UDP \\
	\hline
\multirow{3}{*}{BitTorrent}
	& \str{GET /announce?info\_hash}, \str{GET /torrents} & TCP \\
	& \str{GET TrackPak}, \str{0x13BitTorrent} & TCP \\
	& \str{0x00000005},\str{0x0000000d},\str{0x00004009} & TCP \\
	\hline
\multirow{3}{*}{Gnutella}
	& \str{GNUTELLA}, \str{GIV}, \str{GET /uri-res/}, \str{GET /get/} & TCP \\
	& \str{X-\{Versio,Dynami,Query,Ultrap,Try,\dots\}} & TCP \\
	& \str{GND} & UDP \\
	\hline \hline
\end{smalltab}
	\label{tab:stringmatch}\caption{\footnotesize{Strings used for P2P identification}}
\end{center}
\end{table}
%
To identify P2P traffic the following algorithm is used.
%
\begin{enumerate}
	\item If src\slash dst port matches well known P2P ports the flow is flagged as P2P.
	\item Payload is compared to the string table. In case of a match the flow is flagged as P2P, else the flow is flagged as non-P2P.
	\item If a UDP flow is flagged as P2P from 2, src\slash dst IPs are hashed into a table of IPs. All flows that contain an IP of this table are also flagged as P2P, even if there is no payload match. Flows with src\slash dst port of other well known applications are excluded.
	\item  If a TCP flow is flagged as P2P, src\slash dst IPs are hashed into a second table of IPs. All flows that contain an IP of this table are flagged as possible-P2P if they have been identified from 2. Flows with src\slash dst port of other well known applications are excluded.
\end{enumerate}
%
%-----------------------------------------------------------------------------
\subsection{Algorithm Behavior Identification} % (fold)
\label{sub:algorithm_behavior}
%-----------------------------------------------------------------------------
%
\cite{Ngiwlay2008Bittorrent} proposes a scheme to identify BitTorrent peers using the behavior of BitTorrent's choke algorithm. Algorithm behavior analysis could also be exercised to other protocols but is protocol and revision specific. As such it is not further investigated. Interested readers are referred to the original paper.
%
%-----------------------------------------------------------------------------