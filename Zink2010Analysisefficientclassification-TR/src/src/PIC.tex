%!TEX root = ../p2p_techreport_2010.tex
%=============================================================================
%
%-----------------------------------------------------------------------------
\section{A guide for hardware implementation} % (fold)
\label{sec:hardware}
%-----------------------------------------------------------------------------
%
The hardware implementation needs to make efficient use of the DFI engine and limit memory accesses to both the flow and identification tables. A typical implementation in hardware or on an FPGA vastly utilizes parallelism of multiple engines to reduce computation time.

In our proposed design we split the flow inspection into two parts. One is the pattern matching engine (further denoted PAT engine), the other is the DFI engine, that executes the flow identification algorithm. This separation is possible and useful since both parts operate on different information of the packet. The PAT engine uses only the first four bytes (further referred to as content) of the packets payload. The DFI engine needs the five tuple \{ src, sport, dst, dport, protocol\} from the packet's headers as well as the timestamp of the packet. A more complex design with more resources can include more sophisticated DPI and Statistical Packet Inspection (SPI) engines that would run in parallel. The design is depicted in figure \ref{fig:hardware}.
%
\begin{figure}[h!t]
	\centering
	\includegraphics[scale=0.70]{./gfx/hardware}
	\caption{\footnotesize{Possible design of the engines.}}
	\label{fig:hardware}
\end{figure}
%

Upon arrival of a packet, the header information and the content are extracted and sent to the PAT and DFI engines which can operate in parallel. The content is immediately processed by the string matching part of the DFI engine. The result will either be a match, in which case the P2P protocol can be identified, or a mismatch, in which case the protocol will be unknown. Note, that the string matching only needs to check a small set of equal sized strings, which can all run in parallel and which is much faster than full-fledged DPI.

Meanwhile, the header information is used by the DFI engine to access the identification table and retrieve the table entries if available. These entries should be copied to local registers for later usage, together with their indexes. Note, that it suffices to retrieve one of the entries. So if the first one is found there is no need for a second table access. The DFI module should have an interrupt wire that can cause an instant interrupt and reset the DFI states. Reasons for an interrupt could be, that the flow has been identified by PAT or DPI engines or that the flow table returns that the flow is already identified. Depending on the packet's protocol, either UDP or TCP, one of the identification machines is used. So the protocol field of the packet's header triggers execution of either the UDP or the TCP machine, but never both. The DFI engine needs access to both the identification as well as the flow table to query and update information. It also needs one internal register (alternatively two smaller ones) to mirror the two identification entries and keep the indexes. The TCP and UDP machines both can access these registers to read and compare the age and certain values of the entry. If identification leads to changes of the entries, they have to be written to the table, but only, if there has been a change or a new entry is created.
%
%-----------------------------------------------------------------------------
\subsubsection{Logic Requirements} % (fold)
\label{sub:logic}
%-----------------------------------------------------------------------------
%
\paragraph{PAT engine.}
The PAT engine needs $n$ 4 byte registers for $n$ protocol signatures. The incoming 4-byte packet payload is compared to the $n$ signatures in parallel. The comparators can be implemented with simple {\tt and} gates which are {\tt or}'ed on output to indicate a match or mismatch. This part of the identification algorithm can therefore be implemented with a small amount of logic. A very simplified implementation scheme can be seen in figure \ref{fig:pat}.
%
\begin{figure}[h!t]
	\centering
	\includegraphics[scale=0.70]{./gfx/PAT_engine}
	\caption{\footnotesize{Simplified PAT engine implementation.}}
	\label{fig:pat}
\end{figure}
%

\paragraph{DFI engine.}
First component needed are the registers to keep the hash values and the table entries as well as the logic to access the identification table. In addition the DFI engine needs to keep state information about the two entries, which can be read and written during different stages of the identification. There are basically only two states that need to be kept, namely if the entry is updated (or created) or not. This can be modeled with a single flip-flop for the state \verb+update+.
The protocol header field must be compared against the constant values $6,\ 17$ to select which path is executed. This can be implemented using less than 16 bit precision, since we are only interested in certain bits of the protocol field, namely those that must be set in case the one of the values are seen. So only those bits need to be \verb+AND+'ed to get a match for either of the values. The output is fed into a multiplexor that selects which bits of the internal registers (age and certain value) are used to determine the flow type. The identification table needs only be updated if at least one of the entries is updated. The flow table only needs to be updated if the flow could be identified as either \verb+P2P+ or \verb+NONP2P+, else not. This DFI engine also has small logic requirements. See figure \ref{fig:dfi} for a simplified and abstract implementation sketch.
%
\begin{figure}[h!t]
	\centering
	\includegraphics[scale=0.70]{./gfx/DFI_engine}
	\caption{\footnotesize{Simplified DFI engine implementation.}}
	\label{fig:dfi}
\end{figure}
%