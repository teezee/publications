%!TEX root = ../p2p_techreport_2010.tex
%=============================================================================
% 									BITTORRENT
%-----------------------------------------------------------------------------
\subsection{BitTorrent} % (fold)
\label{sub:bittorrent}
%
\emph{BitTorrent} started in 2001 with a simple post on a forum: ``My new app, BitTorrent, is now in working order, check it out here'' \footnote{\url{http://finance.groups.yahoo.com/group/decentralization/message/3160}}. It has been developed by Bram Cohen who describes it in \cite{Cohen2003Incentives}. Since then it has grown to be the most popular file sharing tool currently in use.
%-----------------------------------------------------------------------------
\subsubsection{Overlay Network.} % (fold)
\label{sub:bitnetwork}
The \emph{BitTorrent} network consists of the following entities:
%
\begin{description}
	\item[.torrent] aka metainfo file. File with meta-data about the download file.
	\item[Webserver] An ordinary web server that hosts the .torrent file.
	\item[Tracker] A centralized server that maintains a list and the states of peers actively downloading and uploading the file. It mediates between peers and answers requests. A new extension \footnote{\url{http://www.bittorrent.org/beps/bep\_0005.html}} also allows decentralized trackers based on a Kademlia DHT.
	\item[Peer] Any host participating in the \emph{BitTorrent} network.
	\item[Seed] Any peer that has all parts of the shared file. At least one seed for each file must be present in the network.
	\item[Leech] Any peer that is not a {\em Seed}. The term is generally used negatively for users downloading a file and not seeding afterwards.
\end{description}
%
Figure \ref{fig:bittorrent} shows the network.
%
\begin{figure}
	\centering
	\includegraphics[scale=0.5]{./gfx/bittorrent}
	\caption{\footnotesize{BitTorrent network.}}
	\label{fig:bittorrent}
\end{figure}
%
%-----------------------------------------------------------------------------
\subsubsection{bencoding.} % (fold)
\label{sub:bitbencoding}
Nearly all messages as well as the .torrent file are encoded using a scheme known as {\em bencoding}. It defines the following formats.
\begin{description}
	\item [\texttt{byte strings}] $<$len in base 10 ascii$>$:$<$string$>$
	\item [\texttt{integer}] i$<$integer in base 10 ascii$>$e
	\item [\texttt{list}] l$<$bencoded values$>$e
	\item [\texttt{dictionary}] d$<$bencoded string$><$bencoded value$>$e
\end{description}
%-----------------------------------------------------------------------------
\subsubsection{Protocol Specification.} % (fold)
\label{sub:bitprotocol}
The trackers use a simple protocol on top of HTTP. A downloader sends a tracker-\texttt{GET} request which contains its random ID, IP, Port and information about downloaded and uploaded amount of data. The tracker responds with a (randomly) generated list of peers that offer the file. The peers then connect to each other exchanging pieces of the file. Peer connections are symmetrical. Files are split into pieces of commonly $2^{18}$ byte. An extension allows UDP connections for tracker requests. It is supported by a small number of clients, including Azureus\slash Vuze.

Peers communicate and exchange data over TCP. The \emph{BitTorrent} protocol is also known as ``peer wire protocol''. After the TCP handshake a peer must immediately send the \emph{BitTorrent} handshake message, which is easily detectable. Then data or message exchange can commence. The handshake message is 49+len(pstr) Bytes long and has the following fields.
%
\begin{description}
	\item [\texttt{pstrlen}] 1B, length of the following string
	\item [\texttt{pstr}] string identifier
	\item [\texttt{reserved}] 8B, for extensions. Each bit can be used to state support for different extensions.
	\item [\texttt{info\_hash}] 20B, SHA1 hash of the info key of the .torrent file.
	\item [\texttt{peer\_id}] 20B, unique peer ID. Includes the client ID and client version.
\end{description}
%
The current protocol implementations all use pstrlen = \str{0x13} and\\ pstr = \str{BitTorrent Protocol}. All other messages using the peer wire protocol have the following structure.
%
\begin{description} 
	\item[\texttt{message length}] 4B, length of the message
	\item[\texttt{type}] 1B, type of message, not present for keep-alive
	\item[\texttt{payload}] optional message content
\end{description}
%

There are multiple messages peers can exchange. Some of them are used to exchange status information or request and find file pieces. Depending on the type of message the payload is present or not which usually contains index information for file pieces.

In addition to the peer wire protocol the DHT extension also implements the so called KRPC protocol used to query for files and nodes in the DHT. It is a simple RPC mechanism which is transported over UDP. Each KRPC message is sent within one UDP packet without any retransmits. The UDP packet contains a bencoded dictionary that encodes query arguments and return values. There are three types of KRPC messages, query, response and error. They start with the following string sequences.
%
\begin{description}
	\item[\texttt{query}] \str{d1:a}%; the more specific start sequence of \str{d1:ad2id:} might work better?
	\item[\texttt{response}] \str{d1:r}
	\item[\texttt{error}] \str{d1:e}
\end{description}
%
Query messages transport query strings as arguments (as indicated by the key a) inside a dictionary. Responses also are dictionaries with response strings. Error messages contain a list of error codes and strings
%
%-----------------------------------------------------------------------------
\subsubsection{Traffic.}
\label{sub:bittraffic}
Table \ref{tab:bitflow} summarizes the flows we could identify in a few hours long \emph{BitTorrent} session. We used the official \emph{BitTorrent} client on multiple virtual machines and shared and seeded freely available files. We do not analyze Tracker traffic, since it is far to rare to be used for flow identification. In the table, 'rnd' means a port randomly chosen on initiating the connection while 'def' denotes the user agent's defined listen port which is either a default value or chosen by the user. 
%
\begin{table}
\begin{center}
\begin{smalltab}{*{3}{|l}|}
	\hline
description & KRPC messages & messages\slash data \\
	\hline \hline
type & signal & signal, transfer \\
	\hline
transport layer & UDP & TCP\\
	\hline
application layer & peer wire\slash KRPC & peer wire\\
	\hline
direction & in\slash out & in\slash out\\
	\hline
src port & def & rnd\\
	\hline
dst port & def & def\\
	\hline
purpose & exchange messages & exchange message\slash data \\
	\hline
handshake & no & bittorrent \\
	\hline
header & message header & message header \\
	\hline
\multirow{2}{*}{observed signatures}
	& size $>= 5$ B & size $>= 49$ B \\
	& B0-4: \str{d1:\{a,r,e\}} & B0-19: \str{0x13BitTorrent Protocol}\\
	\hline
\end{smalltab}
	\caption{\footnotesize{Captured BitTorrent Traffic}}
	\label{tab:bitflow}
\end{center}
\end{table}
%
%-----------------------------------------------------------------------------
\subsubsection{Encryption and Obfuscation}
\label{sub:bitencryption}
Many \emph{BitTorrent} clients -- excluding the official one -- implement methods for encryption and obfuscation, known as {\em Message Stream Encryption (MSE)\slash Protocol Encryption {PE}}. The official \emph{BitTorrent} client is able to accept but does not reply with encrypted packets. \emph{BitTorrent} uses the infohash field of the .torrent file combined with a Diffie-Hellman-Merkle key exchange to set up an RC4 encrypted connection. Using the infohash from the .torrent file prevents man-in-the-middle attacks. \emph{BitTorrent} only encrypts TCP traffic transported using the peer wire protocol. Signaling traffic, which is transported via UDP is unencrypted since no key exchange is possible. 
%
%-----------------------------------------------------------------------------
\subsubsection{Identification} % (fold)
\label{sub:bitidentification}
\begin{enumerate}
	\item If $\texttt{protocol} = \texttt{TCP}\ \& \ \texttt{rel.seq.num.} = 4\ \& \ \texttt{bytes}(0,4) = \str{0x13Bit}$, then a BitTorrent Handshake is caught and data transfer is initiated. Src IP, dst IP\slash port can be tagged as BitTorrent peers. The src port is random and specific for that connection.
	\item If $\texttt{protocol} = \texttt{UDP}\ \& \ \texttt{length} \ge 5\ \& \ \texttt{bytes}(0,4) \in \str{d1:\{a,r,e\}}$ a KRPC message is caught and the src IP\slash port and dst IP\slash port can be added to known BitTorrent hosts. These ports also listen for data transfer.
\end{enumerate}
%

Since \emph{BitTorrent} UDP traffic is always unencrypted it is easy to identify \emph{BitTorrent} peers solemnly on capturing signaling traffic. This allows easy identification of IPs and listening ports. Any traffic to these peers can be classified as \emph{BitTorrent} traffic.
%
%-----------------------------------------------------------------------------
% subsection bittorrent (end)
%-----------------------------------------------------------------------------