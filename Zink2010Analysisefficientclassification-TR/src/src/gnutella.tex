%!TEX root = ../p2p_techreport_2010.tex
%=============================================================================
% 									GNUTELLA
%-----------------------------------------------------------------------------
\subsection{Gnutella} % (fold) 
\label{sub:gnutella}
\emph{Gnutella} is a direct successor of \emph{Napster} and the first P2P application that introduced a decentralized overlay network. In the \emph{Gnutella} network all peers function as clients and servers alike, called \textsc{servents}. It was introduced by Nullsoft and soon made open-source. Today the most prominent \emph{Gnutella} client is \emph{LimeWire}\footnote{\url{http://www.limewire.com/}}. Since \emph{Gnutella} is open-source it is well documented and developers frequently exchange and implement RFCs.
%
%-----------------------------------------------------------------------------
\subsubsection{Terminology.}
\label{sub:gnuterm}
Throughout the next paragraphs the following terms will be used.
\begin{description}
	\item[servent] Any peer in the Gnutella network
	\item[client] A client that wishes to connect, issue queries, download files
	\item[server] A peer that responds to queries and uploads files.
	\item[ultrapeer] A special peer that serves as a mediator between leafes
	\item[leaf] Any peer that is not an ultrapeer
	\item[user agent] The Gnutella client software running on a peer.
\end{description}
%
%-----------------------------------------------------------------------------
\subsubsection{Overlay Network.} % (fold)
\label{sub:gnuoverlay}
The \emph{Gnutella} overlay has significantly change from \emph{v0.4} to \emph{v0.6}. Older \emph{Gnutella v0.4} servents can both issue and respond to queries. Queries are distributed by floods. A servent broadcasts the query to all neighbors which forward the query similarly up to a maximum radius. Figure \ref{fig:gnutella04} depicts the \emph{Gnutella v0.4} architecture.
%
\begin{figure}
	\centering
	\includegraphics[scale=0.5]{./gfx/gnutella04}
	\caption{\footnotesize{Gnutella v.04 network}}
	\label{fig:gnutella04}
\end{figure}
%

This design had certain flaws which could lead to bottlenecks. \emph{Gnutella v0.6} introduces \textsc{ultrapeers} which implement special services like flow control and Pong-caching. Other peers are called \emph{leaves} and keep only a small number of connections to ultrapeers. Figure \ref{fig:gnutella06} shows the \emph{Gnutella v0.6} network. Each node decides independently to become an ultrapeer based on local information which includes firewalling, operating system, bandwidth, uptime, RAM and CPU power. Leaves only forward messages to all it's ultrapeers while an ultrapeer relays messages to all other connected ultrapeers and only to leaves if they decide the leaf can satisfy the request.
%
\begin{figure}
	\centering
	\includegraphics[scale=0.5]{./gfx/gnutella06}
	\caption{\footnotesize{Gnutella v.06 network}}
	\label{fig:gnutella06}
\end{figure}
%
%
\paragraph{Connect and Participate.} % (fold)
\label{par:gnuconnect}
To connect to the network a peer must be aware of ultrapeers that are already connected to the \emph{Gnutella} network. A peer keeps a local cache of well known \emph{Gnutella} servents. The joining peer rapidly sends UDP \texttt{PING} messages (see below) to known servents to discovery which are actually online. On reply (\texttt{PONG}), the peer tries to connect using the \emph{Gnutella} handshake (see below). After connecting to an ultrapeer the client propagates its file list to the ultrapeers and can start exchanging binary messages.

A \emph{Gnutella} session can thus be described as follows.
\begin{description}
	\item [Retrieve ultrapeers] Client connects to ultrapeer databases and retrieves recent ultrapeer lists. Usually manual or automatic HTTP download.
	\item [Discovery] Client rapidly sends UDP \texttt{PING} messages to find online ultrapeers. Source port is user-agent listen port.
	\item [Ultrapeer Connection] Open TCP connection to online ultrapeers. Immediately perform \emph{Gnutella} handshake (see below). On error, receive new ultrapeers and try again.
	\item [Message Exchange] User defined \texttt{QUERY} messages are forwarded to all connected ultrapeers, then between ultrapeers and finally to leaves suitable for the query. \texttt{QUERYHIT}s can either be directed at the responsible leaf or it's ultrapeer using UDP.
	\item [Client-Client Communication] Users can browse through shared folders of each other. Requests and file lists are exchanged using UDP.
	\item [Data Transfer] Downloads are done using direct HTTP connections between servents. The client initiates the TCP connection and requests files using HTTP GET requests. The server answers with HTTP OK.
\end{description}
%
%-----------------------------------------------------------------------------
\subsubsection{Protocol Specification.} % (fold)
\label{sub:gnuprotocol}
The current \emph{Gnutella} protocol is version 0.6 \footnote{\url{http://rfc-gnutella.sourceforge.net/src/rfc-0_6-draft.html}}. 
A Gnutella servent connects to the network by establishing connections with ultrapeers already present in the network. There are multiple methods to retrieve lists for connected hosts like manual or automatic downloads. Connecting clients must first establish a TCP connection and then proceed with the Gnutella handshake.

\paragraph{Gnutella Handshake.} % (fold)
\label{par:gnutella_handshake_}
%
\begin{enumerate}
	\item Client establishes TCP connection.
	\item Client sends \str{GNUTELLA CONNECT/0.x}, where x is the protocol version. The connection request also carries client information like supported features, user-agent, the client IP and port.
	\item Server answers either with \str{GNUTELLA/0.x 200} in case the connection is accepted or with \str{GNUTELLA/0.x 503} when no leaf slots are available. In that case the Ultrapeer replies with an \texttt{X-Try-Ultrapeers} header bearing \texttt{\{IP:Port\}} tuples of known ultrapeers.
	\item A client that wishes to connect sends \str{GNUTELLA/0.x 200}.
	\item Both peers can start to exchange binary messages at will.
\end{enumerate}
%
Note that the user agent opens multiple ports for connections to ultrapeers  and send queries from different ports than it accepts connections.

An observed client connection request looks like follows (all IPs and ports are replaced by random numbers).
\begin{verbatim}
	GNUTELLA CONNECT/0.6
	X-Max-TTL: 3
	X-Dynamic-Querying: 0.1
	X-Requeries: false
	X-Query-Routing: 0.1
	User-Agent: LimeWire/5.1.2
	Vendor-Message: 0.2
	X-Ultrapeer-Query-Routing: 0.1
	GGEP: 0.5
	Listen-IP: 207.112.39.83:17784
	Accept-Encoding: deflate
	Pong-Caching: 0.1
	X-Guess: 0.1
	X-Ultrapeer: False
	X-Degree: 32
	X-Locale-Pref: en
	Remote-IP: 19.179.51.127
\end{verbatim}

An example ultrapeer 503 reply:
\begin{verbatim}
	GNUTELLA/0.6 503 No Leaf Slots
	X-Try-Ultrapeers: 201.170.245.144:39697,214.204.30.68:19678,67.225.145.85:31727
\end{verbatim}

An example ultrapeer 200 reply:
\begin{verbatim}
	GNUTELLA/0.6 200 OK
	Listen-IP: 172.38.35.223:32758
	Remote-IP: 48.105.117.144
	User-Agent: LimeWire/4.18.8
	X-Ultrapeer: True
	X-Degree: 24
	X-Query-Routing: 0.1
	X-Ultrapeer-Query-Routing: 0.1
	X-Max-TTL: 3
	X-Dynamic-Querying: 0.1
	X-Locale-Pref: en
	GGEP: 0.5
	Bye-Packet: 0.1
	X-Try-Ultrapeers: 199.146.247.162:24971,186.65.239.213:18912,118.200.6.233:11669
\end{verbatim}

%
\paragraph{Gnutella Messages.} % (fold)
\label{par:gnutella_messages}
Messages are exchanged using UDP. Source port is the user agent's listen port. Each message has a 23 byte header as depicted in figure \ref{fig:gnutellahead}.
%
\begin{figure}[ht!]
	\centering
	\includegraphics[scale=0.5]{./gfx/gnutellahead}
	\caption{\footnotesize{Gnutella message header}}
	\label{fig:gnutellahead}
\end{figure}
%

\begin{description}
	\item[\texttt{GUID}] A globally unique message ID. Byte 8 should contain \str{0xff} for all modern clients. Byte 15 should contain \str{0x00} since it is reserved for future use.
	\item[\texttt{Payload Type}] The type of message (see below)
	\item[\texttt{TTL}] Time-To-Live. Number of times the message will be forwarded in the overlay before it is removed.
	\item[\texttt{Hops}] Number of times the message has been forwarded in the overlay.
	\item[\texttt{Payload Length}] The length of the message in bytes from end of header. In little endian. This number plus $23$ equals the message size in bytes and are the only reliable source for the client to identify the actual message content.
\end{description}

The following message types are allowed.

\begin{description}
	\item[\texttt{0x00 PING}] Used to actively discover hosts on the network. A servent receiving a Ping message is expected to respond with one or more Pong messages.
	\item[\texttt{0x01 PONG}] The response to a Ping. Includes the address of a connected Gnutella servent, the listening port of that servent, and information regarding the amount of data it is making available to the network.
	\item[\texttt{0x80 QUERY}] The primary mechanism for searching the distributed network. A servent receiving a Query message will respond with a Query Hit if a match is found against its local data set.
	\item[\texttt{0x81 QUERYHIT}] The response to a Query. This message provides the recipient with enough information to acquire the data matching the corresponding Query.
	\item[\texttt{0x40 PUSH}] A mechanism that allows a firewalled servent to contribute file-based data to the network.
	\item[\texttt{0x02 BYE}] An optional message used to inform the remote host that the servent is closing the connection, and the reason for doing so.
\end{description}

\paragraph{Query Routing Protocol QPR} % (fold)
\label{par:query_routing_protocol_qpr}
The QPR is the protocol used in an ultra peer scenario to route queries in the network. Each ultrapeer maintains a local hash table mapping files to connected peers. Peers use QRP to update the ultrapeers hash table as well as query files.

\paragraph{Other Protocols.} % (fold)
\label{par:gnu_other_protocols}
Multiple extension to the protocol exist, such as GGEP (Gnutella Generic Extension Protocol), HUGE and XML. Reserved bytes in the message payload indicate which extensions the peers support and affects both signaling as well as data transfer traffic.
%
%-----------------------------------------------------------------------------
\subsubsection{Traffic} % (fold)
\label{sub:gnuflow}
We have identified different types of traffic which are summarized in table \ref{tab:gnuflow}. Here 'rnd' means a port randomly chosen on initiating the connection while 'def' denotes the user agent's defined listen port which is either left on the default value or chosen by the user. 
%
\begin{table}
\begin{center}
\begin{smalltab}{|*{4}{|l}|}
	\hline
	        & peer-2-ultrapeer &     & \\
description &  connection & messages & data transfer \\
	\hline \hline
type & control & control & data\\
	\hline
transport layer & TCP & UDP & TCP \\
	\hline
application layer & QRP & gnutella & HTTP\slash HUGE \\
	\hline
direction & p2up & p2p, p2up, up2p & p2p \\
	\hline
src port & rnd & def & rnd \\
	\hline
dst port & def & def & def \\
	\hline
\multirow{3}{*}{purpose} & join & exchange messages & exchange data \\
	& participate & & \\
	& query routing & & \\
	\hline
handshake & Gnutella (join) & no & TCP \\
	\hline
header & no ? & message header & no \\
	\hline
\multirow{8}{*}{signature} 
	& handshake & paket size $>= 23$ B & should start with \\
	& \str{GNUTELLA} & B8 should be \str{0xff}, rarely is &  \str{GET}, rarely does \\
	& & B15 should be \str{0x00} & \\
	& & B16 in & \\ 	 
	& & \{\str{0},\str{1},\str{2},\str{0x40},\str{0x80},\str{0x81}\} & \\
	& & B19-22 (little endian): Gnutella payload length& \\
	& & ($= \textit{UDP payload length} - 23$) & \\
	\hline
\multirow{2}{*}{frequency}
	& high at start & $\approx 7/sec$ & only during transfer \\
	& infrequently afterwards & frequent & high amount of traffic \\
%signature & handshake & 
	\hline
\end{smalltab}
	\caption{Analysis of captured Gnutella traffic}
	\label{tab:gnuflow}
\end{center}
\end{table}
%
%-----------------------------------------------------------------------------
\subsubsection{Identification} % (fold)
\label{sub:gnuidentification}
The following algorithm shows how to identify IP\slash port pairs of \emph{Gnutella} peers.
%
\begin{enumerate}
	\item If $\texttt{protocol} = \texttt{TCP}\ \& \ \texttt{relative sequence number} = 4\ \& \ \texttt{bytes}(0,7) = \str{GNUTELLA}$, then a leaf Gnutella handshake message is caught. The scr IP can be added to the list of Gnutella peers, the src port is a random port for this connection. The dst IP and port can be added to the Gnutella candidate peers.
	\item If the response messages first 8 bytes equal \str{GNUTELLA}, the dst IP and port can be added to Gnutella peers.
	\item It is possible but expensive to further dissect the response message to identify other Gnutella peers.
	\item If $\texttt{protocol} = \texttt{UDP}\ \& \ \texttt{length} \ge 23\ \& \ \texttt{byte}(15) = 0\\ \& \ \texttt{byte}(16) \in \{0,1,2,0x40,0x80,0x81\}\ \& \ \texttt{bytes}(19,22) = \texttt{length}-23$, then src IP\slash port and dst IP\slash port are Gnutella peers. These ports are also the listening ports on which data transfer flows. And can be used to identify connecting hosts as Gnutella peers. 
\end{enumerate}
%
% subsection gnutella_etc_ (end)